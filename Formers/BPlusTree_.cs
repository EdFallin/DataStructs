﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public static class BptConstants_
    {
        public const int MaxNodes = 10;
        public const int None = -1;
    }


    public enum BPlusNodeType_
    {
        Fork,
        Leaf,
    }

    public class BPlusTree_<T> where T : IEquatable<T>, IComparable<T>
    {
        NodeBpt_<T> _root;

        public BPlusTree_(KeyPointerBpt_<T> item) {
            /*  root is defined as leaf until a second level, then a fork  */
            _root = new LeafBpt_<T>(item.Key);
            _root.Children.Add(item);
        }

        public IKeyedObject<T> Search(T key) {
            //  invoke recursor and return its results  //
            IKeyedObject<T> result = SearchRecursor(_root, key);
            return (result);
        }

        public IKeyedObject<T> SearchRecursor(NodeBpt_<T> node, T key) {
            //  get the next place to look  //
            int branch = node.Search(key);

            //  if at a fork, rcall  //
            if (node.NodeType == BPlusNodeType_.Fork) {
                SearchRecursor((NodeBpt_<T>)node.Children[branch], key);
            }

            //  if at a leaf, return search result, null if no match  //
            if (node.NodeType == BPlusNodeType_.Leaf) {
                if (branch != BptConstants_.None) {
                    //  search succeeded, so return what was found  //
                    return (node.Children[branch]);
                }
                else {
                    //  search failed, so nothing to return  //
                    return (null);
                }
            }

            return (null);
        }

        public void Add(IKeyedObject<T> subject) {
            //  invoke recursor; no return values  //
            AddRecursor(_root, subject);
        }

        public Tuple<NodeBpt_<T>, NodeBpt_<T>> AddRecursor(NodeBpt_<T> node, IKeyedObject<T> subject) {
            //  move forward from root, searching for leaf to add to, //
            //  then add it there and restructure when any overflows  //

            //  get the branch point  //
            int branch = node.Search(subject.Key);


            //  if at a fork, rcall forward; returned value may indicate //
            //  a overflow at rcalled, in which case interleave here at rcollapse //
            if (node.NodeType == BPlusNodeType_.Fork) {
                //  get the branch to rcall on, perform rcall  //
                NodeBpt_<T> child = node.Children[branch] as NodeBpt_<T>;
                Tuple<NodeBpt_<T>, NodeBpt_<T>> passer = AddRecursor(child, subject);

                //  if only one node came back, structure altering is finished, so pass a one-node tuple to rcaller  //
                if (passer.Item2 == null) {
                    return (new Tuple<NodeBpt_<T>, NodeBpt_<T>>(node, null));
                }
                else {
                    //  if two nodes came back, add the new one into the children of ~node, and pass  //
                    //  its duple (either 1 or 2 nodes) to rcaller, which might restructure as well  //
                    Tuple<NodeBpt_<T>, NodeBpt_<T>> local = node.Add(subject);
                    return (local);
                }
            }
            //  if at a leaf, add using factored method and begin rcollapse;  //
            //  return value depends on overflow  //
            else {
                LeafBpt_<T> leaf = node as LeafBpt_<T>;

                /*  .Add() incorporates .Split()  */
                Tuple<NodeBpt_<T>, NodeBpt_<T>> passer = leaf.Add(subject);
                return (passer);
            }

        }

        public void Remove(IKeyedObject<T> subject) {
            //  call the recursor; return value thrown out  //
            RemoveRecursor(_root, subject);
        }

        private IKeyedObject<T> RemoveRecursor(NodeBpt_<T> node, IKeyedObject<T> subject) {
            //  rcall forward to the leaves, then call  //
            int branch = node.Search(subject.Key);
            NodeBpt_<T> child = node.Children[branch] as NodeBpt_<T>;

            /*  leaf path  */
            //  actually delete, and return value to rcaller / parent fork;  //
            //  no sloshing or merging within leaves  //
            if (child is LeafBpt_<T>) {
                IKeyedObject<T> removed = child.Remove(subject);

                //  if nothing was removed, .Remove() on node returns ~null, letting rcallers  //
                //  know that no structure-altering is needed  //
                return (removed);
            }

            /*  fork path  */

            /*  the rcall; returns ~null if no structural changes  */
            IKeyedObject<T> passer = RemoveRecursor(child, subject);

            //  if at this rpass this ~node's children weren't changed, now pass  //
            //  along the ~null indicating that, skipping structure-altering ops  //
            if (passer == null) {
                return (passer);
            }

            //  if ops in rcalled didn't cause an underflow, exit returning ~null so rcaller skips altering code  //
            if (!child.IsHalfEmpty) {
                return (null);
            }
            //  if something actually _was_ removed, look for underflow and try to compensate  //
            else {
                NodeBpt_<T> anyLeftSibling = (
                    branch > 0
                    ? (NodeBpt_<T>)node.Children[branch - 1]
                    : null);
                NodeBpt_<T> anyRightSibling = (
                    branch < node.Children.Top
                    ? (NodeBpt_<T>)node.Children[branch + 1]
                    : null);

                child.Slosh(anyLeftSibling, anyRightSibling);  // null nodes bypassed internally 

                //  if sloshing in worked, no further altering needed, so null returned to rcaller  //
                if (!child.IsHalfEmpty) {
                    return (null);
                }
                //  if nothing sloshed in, this is still ~true, so merge  //
                else {
                    NodeBpt_<T> combined;

                    // only one sibling is ever null; children combined into L of two, merged-in node is dropped //
                    if (anyLeftSibling != null) {
                        combined = anyLeftSibling.CombineWithRightSibling(child);
                        node.Remove(child);
                    }
                    else {
                        combined = child.CombineWithRightSibling(anyRightSibling);
                        node.Remove(anyRightSibling);
                    }

                    //  if merged, return the combined node so the rcaller  //
                    //  knows it may have to alter structure as well  //
                    return (combined);
                }
            }
        }
    }



    /*  ~abstract~ in a class slugline only means that you can't directly instantiate it; 
     *      it can have concrete members, and it can have a constructor with ops, 
     *      called when a concrete class instance is inited  */
    public abstract class NodeBpt_<T> : IKeyedObject<T>
        where T : IEquatable<T>, IComparable<T>
    {
        #region Properties

        public abstract BPlusNodeType_ NodeType { get; }

        public T Key { get; set; }

        /*  for in-tree navigation  */
        public int Offset { get; set; }
        public NodeBpt_<T> Parent { get; set; }

        /*  you can publicly ~set an individual list item, but not the whole collection  */
        public KeyedListBpt_<T> Children { get; protected set; }

        public bool IsFull /* passed */ {
            get { return (Children.Count == Children.Size); }
        }

        public bool IsHalfEmpty  /* passed */  {
            get { return (Children.Count <= (Children.Size / 2)); }
        }

        #endregion Properties


        #region Constructors

        public NodeBpt_()  /* verified */  {
            Children = new KeyedListBpt_<T>(BptConstants_.MaxNodes);
            Offset = BptConstants_.None;
        }

        public NodeBpt_(T key) : this()  /* verified */  {
            Key = key;
        }

        #endregion Constructors


        #region Public Methods

        public Tuple<NodeBpt_<T>, NodeBpt_<T>> Add(IKeyedObject<T> subject) {
            //  add if ~this is not full; otherwise invoke .Split() and add the new node //
            //  in whichever of the two resulting it belongs  //

            if (this.IsFull) {
                //  splitting _this_ node  //
                Tuple<NodeBpt_<T>, NodeBpt_<T>> split = this.Split();

                NodeBpt_<T> nowLeftLeaf = split.Item1;  // same as ~this 
                NodeBpt_<T> nowRightLeaf = split.Item2;

                NodeBpt_<T> addToLeaf;

                //  add the element to the correct node ( this / original or new)  //
                /*  !! possibly missing here is promotion of key from subject if it ends up Lmost in a node, etc.  */

                //  first get the node to add to  //
                if (subject.Key.CompareTo(nowRightLeaf.Key) > 0) {
                    addToLeaf = nowRightLeaf;
                }
                else {
                    addToLeaf = nowLeftLeaf;
                }

                //  now actually add ~subject there  //
                int site = addToLeaf.Search(subject.Key);
                addToLeaf.Children.Insert(subject, site);

                //  return a tuple with both nodes full, so rcaller can change tree structure  //
                return (split);
            }
            else {
                /*  if this node isn't full, just a plain insert, and return a tuple with only the first node, 
                 *  which code looks at during recursion to determine what to do next  */
                int site = this.Search(subject.Key);
                Children.Insert(subject, site);

                Tuple<NodeBpt_<T>, NodeBpt_<T>> same = new Tuple<NodeBpt_<T>, NodeBpt_<T>>(this, null);
                return (same);
            }
        }

        /*  dependency of .Add()  */
        public Tuple<NodeBpt_<T>, NodeBpt_<T>> Split()  /* passed */  {
            NodeBpt_<T> left = this;
            NodeBpt_<T> right = (NodeBpt_<T>)this.MemberwiseClone();   /*  shallow copy, same object type  */

            int halfCount = this.Children.Count / 2;

            /*  key promotion for ^right  */
            IKeyedObject<T> passer = left.Children[halfCount];
            right.Key = ((passer != null) ? passer.Key : default(T));

            /*  new .Children for ^right so prop doesn't point to ^left's prop  */
            right.Children = new KeyedListBpt_<T>(BptConstants_.MaxNodes);


            /*  remove 1/2 from ^left, add to ^right, in order  */
            for (int i = 0; i < halfCount; i++) {
                /*  remove from R half, always at same offset because each removal shifts remainder Lward  */
                IKeyedObject<T> child = this.Children.Remove(halfCount);
                right.Children.Add(child);
            }

            Tuple<NodeBpt_<T>, NodeBpt_<T>> pair =
                new Tuple<NodeBpt_<T>, NodeBpt_<T>>(left, right);

            return (pair);
        }

        public IKeyedObject<T> Remove(IKeyedObject<T> subject) {
            int offset = this.Search(subject.Key);

            //   if there's something to delete, delete it and also return it  //
            if (offset != BptConstants_.None) {
                IKeyedObject<T> passer = this.Children.Remove(offset);
                return (passer);
            }
            else {
                return (null);
            }
        }

        /*  sloshing happens _into_ this node at underflow, if possible  */
        public void Slosh(NodeBpt_<T> left, NodeBpt_<T> right) {
            //  if sloshing needs to be done, it's from ~left or ~right _to_ ~this  //

            if (left != null && !left.IsHalfEmpty) {
                //  move Rmost of ~left's children to first here //
                IKeyedObject<T> sloshed = left.Children.Drop();  // takes from end 
                this.Children.Insert(sloshed, 0);
            }
            else if (right != null && !right.IsHalfEmpty) {
                //  move Lmost of ~right's children to top here  //
                IKeyedObject<T> sloshed = right.Children.Remove(0);
                this.Children.Add(sloshed);  // adds at end 
            }

        }

        public NodeBpt_<T> CombineWithRightSibling(NodeBpt_<T> that) {
            /*  combines ~this with ~that, returns combined ~this;  */
            /*  it's assumed here that ~this has enough room to combine  */

            for (int i = 0; i < that.Children.Count; i++) {
                //  added at end  //
                this.Children.Add(that.Children[i]);
            }

            return (this);
        }

        #endregion Public Methods

    }


    public class ForkBpt_<T> : NodeBpt_<T> where T : IEquatable<T>, IComparable<T>
    {
        #region Properties

        public override BPlusNodeType_ NodeType {
            get {
                return (BPlusNodeType_.Fork);
            }
        }

        public int EmptyChildren  /* passed */  {
            get { return (Children.Size - Children.Count); }
        }

        #endregion Properties


        #region Constructors

        public ForkBpt_() : base() /* verified */  {
        }

        public ForkBpt_(T key) : base(key)  /* verified */  {
        }

        #endregion Constructors

    }


    public class LeafBpt_<T> : NodeBpt_<T> where T : IEquatable<T>, IComparable<T>
    {
        /*  leaves are both tree nodes and doubly-linked list nodes, 
         *  but final key-pointer pairs are "untree" members of leaves  */


        #region Properties

        public ForkBpt_<T> NextLeaf { get; set; }
        public ForkBpt_<T> PreviousLeaf { get; set; }

        public override BPlusNodeType_ NodeType {
            get {
                return (BPlusNodeType_.Leaf);
            }
        }

        #endregion Properties


        #region Constructors

        public LeafBpt_() {

        }

        public LeafBpt_(T value) {
            this.Key = value;
        }

        #endregion Constructors

    }


    public class KeyPointerBpt_<T> where T : IEquatable<T>, IComparable<T>
    {
        #region Properties

        public T Key { get; set; }
        public int Pointer { get; set; }   // pointer to record on disk as offset in file 

        #endregion Properties


        #region Constructors

        public KeyPointerBpt_(T key) {
            Key = key;
        }

        public KeyPointerBpt_(T key, int pointer) : this(key) {
            Pointer = pointer;
        }

        #endregion Constructors

    }

    #region Searching

    public int SearchExact(K sought)  /* passed */  {
        int bottom = 0; int top = Top;

        /*  repeat until match or range of 1 (bottom == top) was not a match; ~<~ only skips some  */
        while (bottom <= top) {
            int mid = (top + bottom) / 2;

            /*  return any exact match  */
            if (_contents[mid].Key.Equals(sought)) {
                return (mid);
            }

            /*  go L if ^sought < ^mid, go R if ^sought > ^mid; 
             *  must decr (L) or incr (R) ^mid so that all subject elements can be reached 
             *  (otherwise some searched cause repeat of same 1/2-of offset forever)  */
            if (sought.CompareTo(_contents[mid].Key) < 0) {
                top = --mid;   /*  if not using -1, must use *prefix* operator to pass altered value  */
            }
            else {
                bottom = ++mid;   /*  if not using +1, must use *prefix* operator to pass altered value  */
            }
        }

        /*  if no match before loop exits, no match  */
        return (BptConstants_.None);
    }

    public int SearchClosest(K sought)  /* passed */  {
        /*  algo: handle as exact search, except retain highest succeed, 
         *        and proceed until search algo reaches start == end; 
         *        then highest succeed is the correct return value  */

        int bottom = 0; int top = Top;
        int topGood = BptConstants_.None;   // anti-default; holds highest found on desired side of ^sought 


        /*  same stop condition as exact search; always proceeds to bottom == top if not an exact match  */
        while (bottom <= top) {
            int mid = (top + bottom) / 2;

            /*  exact match still desirable for closest-search  */
            if (sought.Equals(_contents[mid].Key)) {
                return (mid);
            }

            /*  refigure bounds to look at half of last subject, track highest success  */
            if (sought.CompareTo(_contents[mid].Key) < 0) {
                /*  *always-bad* path: go L when ^sought < ^mid; any correct is less than ^mid  */
                top = --mid;
            }
            else {
                /*  *sometimes-good* path: go R when ^sought > ^mid; any correct is ^mid or greater than ^mid   */
                /*  the *and*: track highest good found  */
                if (mid > topGood) {
                    topGood = mid;
                }

                bottom = ++mid;
            }
        }

        /*  return any closest-not-exact, or else fail-val when no such closest  */
        return (topGood);
    }

    #endregion Searching


    #region Changing List Contents

    public void Add(IKeyedObject<K> item)  /* passed */  {
        /*  add at end with correct metadata  */
        Top++; Count++;
        _contents[Top] = item;
    }

    /*  removes at end, returning removed  */
    public IKeyedObject<K> Drop()  /* passed */  {
        IKeyedObject<K> passer = _contents[Top];

        /*  emptying pointer at top el, to avoid confusion  */
        _contents[Top] = null;

        Top--; Count--;
        return (passer);
    }

    public void Insert(IKeyedObject<K> item, int index)  /* passed */  {
        /*  add Count-vs-Size -based exceptions  */
        ShiftOneRight(index);
        _contents[index] = item;

        Top++;
    }

    public IKeyedObject<K> Remove(int index)  /* passed */  {
        IKeyedObject<K> passer = _contents[index];
        ShiftOneLeft(index);

        /*  emptying pointer at top el, to avoid confusion  */
        _contents[Top] = null;

        Top--;

        return (passer);
    }

    public void Clear()  /* passed */  {
        /*  a hard clear, not just changing metadata  */

        for (int i = 0; i < _contents.Length; i++) {
            _contents[i] = null;
        }

        Count = 0;
        Top = -1;
    }


    /*  shifting only needed by 1 at a time; ^leftmost is insert / remove point, but [ +1] within methods  */

    /*  shifting R when inserting  */
    private void ShiftOneRight(int leftmost)  /* verified */  {
        leftmost++;   // local scope only 

        /*  shifting R, step Lward so you don't overwrite values as you go, 
         *  from Count because it will be new top, 
         *  stopping loop at [[orig ^Lmost] +1] because [ -1] in body  */
        for (int i = Count; i >= leftmost; i--) {
            _contents[i] = _contents[i - 1];
        }

        Count++;
    }

    /*  shifting left when removing  */
    private void ShiftOneLeft(int leftmost)  /* verified */  {
        leftmost++;   // local scope only 

        /*  shifting L, step Rward so you don't overwrite as you go; 
         *  from [[orig ^Lmost] +1] because [ -1] in body, 
         *  to [Count -1] because [at Count] is beyond valid region  */
        for (int i = leftmost; i < Count; i++) {
            _contents[i - 1] = _contents[i];
        }

        Count--;
    }

    #endregion Changing List Contents

}
