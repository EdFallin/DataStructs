﻿using System;
using System.Collections;

using DataStructs;
using Advanceds;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass]
    public class BPlusTree_Tests
    {
        #region Children Fullness

        [TestMethod()]
        public void EmptyChildren_Test__Known_Empties__Assert_Expected()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> fork = new ForkBpt_<int>();

            /*  3/5 full (6), so 2/5 empty (4)  */
            for (int i = 0; i < ((BptConstants_.MaxNodes * 3) / 5); i++) {
                fork.Children.Add(new ForkBpt_<int>(i));
            }


            ///  exercising the code  ///
            int actual = fork.EmptyChildren;


            ///  testing  ///
            Assert.AreEqual(4, actual);
        }


        /*  + under 1/2 flag  */

        [TestMethod()]
        public void IsHalfEmpty_Test__Is_Not__Assert_False()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            /*  3/4 full  */
            for (int i = 0; i < ((BptConstants_.MaxNodes * 3) / 4); i++) {
                node.Children.Add(new ForkBpt_<int>(i));
            }


            ///  exercising the code  ///
            bool actual = node.IsHalfEmpty;


            ///  testing  ///
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void IsHalfEmpty_Test__Is__Assert_True()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            /*  about 1/2 full; exactly or 1 less  */
            for (int i = 0; i < (BptConstants_.MaxNodes / 2); i++) {
                node.Children.Add(new ForkBpt_<int>(i));
            }


            ///  exercising the code  ///
            bool actual = node.IsHalfEmpty;


            ///  testing  ///
            Assert.AreEqual(true, actual);
        }


        /*  + at 2/2 flag  */

        [TestMethod()]
        public void IsFull_Test__Is_Not_Full__Assert_False() /* working */ {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            // all child pointers but the last are assigned a node 
            for (int i = 0; i < (BptConstants_.MaxNodes - 1); i++) {
                node.Children.Add(new ForkBpt_<int>(i));
            }


            ///  exercising the code  ///
            bool actual = node.IsFull;


            ///  testing  ///
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void IsFull_Test__Is_Full__Assert_True() /* working */ {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            // all child pointers assigned a node 
            for (int i = 0; i < BptConstants_.MaxNodes; i++) {
                node.Children.Add(new ForkBpt_<int>(i));
            }

            ///  exercising the code  ///
            bool actual = node.IsFull;


            ///  testing  ///
            Assert.AreEqual(true, actual);
        }

        #endregion Children Fullness


        #region Searching Keys

        [TestMethod()]
        public void Search_Test__Fork_Exact_Match__Assert_Expected_Offset()  /* working */  {
            /*  search returns a node, either a fork or a leaf  */

            ///  groundwork  ///
            NodeBpt_<char> subject = new ForkBpt_<char>('\0');   /*  ~null char; never matches anything  */
            int expected = 6;

            /*  chars from 'a' to 'j', C-style  */
            for (int i = 0; i < 10; i++) {
                ForkBpt_<char> leaf = new ForkBpt_<char>((char)('a' + i));
                ((ForkBpt_<char>)subject).Children.Add(leaf);
            }


            ///  exercising the code  ///
            int actual = subject.Search('g');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Search_Test__Fork_Inexact_Match__Assert_Expected_Offset()  /* working */  {
            /*  search returns a node, either a fork or a leaf  */

            ///  groundwork  ///
            NodeBpt_<decimal> subject = new ForkBpt_<decimal>(-1m);
            int expected = 2;

            for (int i = 0; i < 10; i++) {
                ForkBpt_<decimal> leaf = new ForkBpt_<decimal>(i * 25m);
                ((ForkBpt_<decimal>)subject).Children.Add(leaf);
            }


            ///  exercising the code  ///
            int actual = subject.Search(63m);   // between 50m and 75m, ->> 50m / @2 


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Search_Test__Leaf_Exact_Match__Assert_Expected_Offset()  /* working */  {
            /*  search returns a key-pointer if an exact match, else null  */

            ///  groundwork  ///
            NodeBpt_<int> subject = new LeafBpt_<int>(-1);
            int expected = 5;

            LeafBpt_<int> local = (LeafBpt_<int>)subject;

            for (int i = 0; i < 10; i++) {
                local.Children.Add(new KeyPointerBpt_<int>(i * 100));
            }


            ///  exercising the code  ///
            int actual = subject.Search(500);


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Search_Test__Leaf_No_Exact_Match__Assert_None()  /* working */  {
            /*  search returns a key-pointer if an exact match, else null  */

            ///  groundwork  ///
            NodeBpt_<char> subject = new LeafBpt_<char>('\0');   /*  ~null char; never matches anything  */
            int expected = BptConstants_.None;

            LeafBpt_<char> local = (LeafBpt_<char>)subject;

            /*  chars from 'a' to 'j', C-style  */
            for (int i = 0; i < 10; i++) {
                local.Children.Add(new KeyPointerBpt_<char>((char)('a' + i)));
            }


            ///  exercising the code  ///
            int actual = subject.Search('s');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        #endregion Searching Keys


        #region ForkBpt_<T>.Split()

        [TestMethod()]
        public void Split_Test__Fork_Knowns__Assert_Two_Nodes_Returned()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            /*  Elvis operator ~?.~ makes it easier to test types of maybe-encapsulated outputs  */
            Assert.IsInstanceOfType(actual?.Item1, typeof(NodeBpt_<int>));
            Assert.IsInstanceOfType(actual?.Item2, typeof(NodeBpt_<int>));
        }

        [TestMethod()]
        public void Split_Test__Fork_Knowns__Assert_Two_Forks_Returned()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            /*  Elvis operator ~?.~ makes it easier to test types of maybe-encapsulated outputs  */
            Assert.IsInstanceOfType(actual?.Item1, typeof(ForkBpt_<int>));
            Assert.IsInstanceOfType(actual?.Item2, typeof(ForkBpt_<int>));
        }

        [TestMethod()]
        public void Split_Test__Fork_Knowns__Assert_Each_Split_Fork_Half_Full()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            TestFixtures.AddSequentialIntKeyedEmpties(node, BptConstants_.MaxNodes);

            int halfMax = BptConstants_.MaxNodes / 2;

            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            ForkBpt_<int> left = actual.Item1 as ForkBpt_<int>;
            ForkBpt_<int> right = actual.Item2 as ForkBpt_<int>;

            Assert.AreEqual(halfMax, left.Children.Count);
            Assert.AreEqual(halfMax, right.Children.Count);
        }

        [TestMethod()]
        public void Split_Test__Fork_Knowns__Assert_Each_Split_Fork_Expected_Children_Order()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            TestFixtures.AddSequentialIntKeyedEmpties(node, BptConstants_.MaxNodes);

            int halfMax = BptConstants_.MaxNodes / 2;

            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            ForkBpt_<int> left = actual.Item1 as ForkBpt_<int>;
            ForkBpt_<int> right = actual.Item2 as ForkBpt_<int>;

            for (int i = 0; i < halfMax; i++) {
                IKeyedObject<int> item = left.Children[i];
                Assert.AreEqual(i, item.Key);
            }

            for (int i = 0; i < halfMax; i++) {
                IKeyedObject<int> item = right.Children[i];
                Assert.AreEqual(i + halfMax, item.Key);
            }
        }

        [TestMethod()]
        public void Split_Test__Fork_Knowns__Assert_Expected_Keys()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();
            TestFixtures.AddSequentialIntKeyedEmpties(node, 10);


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();
            NodeBpt_<int> left = actual.Item1;
            NodeBpt_<int> right = actual.Item2;


            ///  testing  ///
            Assert.AreEqual(0, left.Key);
            Assert.AreEqual(5, right.Key);
        }

        #endregion ForkBpt_<T>.Split()


        #region LeafBpt_<T>.Split()

        [TestMethod()]
        public void Split_Test__Leaf_Knowns__Assert_Two_Nodes_Returned()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            /*  Elvis operator ~?.~ makes it easier to test types of maybe-encapsulated outputs  */
            Assert.IsInstanceOfType(actual?.Item1, typeof(NodeBpt_<int>));
            Assert.IsInstanceOfType(actual?.Item2, typeof(NodeBpt_<int>));
        }

        [TestMethod()]
        public void Split_Test__Leaf_Knowns__Assert_Two_Leaves_Returned()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            /*  Elvis operator ~?.~ makes it easier to test types of maybe-encapsulated outputs  */
            Assert.IsInstanceOfType(actual?.Item1, typeof(ForkBpt_<int>));
            Assert.IsInstanceOfType(actual?.Item2, typeof(ForkBpt_<int>));
        }

        [TestMethod()]
        public void Split_Test__Leaf_Knowns__Assert_Each_Split_Leaf_Half_Full()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            TestFixtures.AddSequentialIntKeyedEmpties(node, BptConstants_.MaxNodes);

            int halfMax = BptConstants_.MaxNodes / 2;

            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            ForkBpt_<int> left = actual.Item1 as ForkBpt_<int>;
            ForkBpt_<int> right = actual.Item2 as ForkBpt_<int>;

            Assert.AreEqual(halfMax, left.Children.Count);
            Assert.AreEqual(halfMax, right.Children.Count);
        }

        [TestMethod()]
        public void Split_Test__Leaf_Knowns__Assert_Each_Split_Leaf_Expected_Children_Order()  /* working */  {
            ///  groundwork  ///
            ForkBpt_<int> node = new ForkBpt_<int>();

            TestFixtures.AddSequentialIntKeyedEmpties(node, BptConstants_.MaxNodes);

            int halfMax = BptConstants_.MaxNodes / 2;

            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();


            ///  testing  ///
            ForkBpt_<int> left = actual.Item1 as ForkBpt_<int>;
            ForkBpt_<int> right = actual.Item2 as ForkBpt_<int>;

            for (int i = 0; i < halfMax; i++) {
                IKeyedObject<int> item = left.Children[i];
                Assert.AreEqual(i, item?.Key);   // Elvised for clearer testing 
            }

            for (int i = 0; i < halfMax; i++) {
                IKeyedObject<int> item = right.Children[i];
                Assert.AreEqual(i + halfMax, item?.Key);   // Elvised, clearer 
            }
        }

        [TestMethod()]
        public void Split_Test__Leaf_Knowns__Assert_Expected_Keys()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> node = new ForkBpt_<int>();
            TestFixtures.AddSequentialIntKeyedEmpties(node, 10);


            ///  exercising the code  ///
            Tuple<NodeBpt_<int>, NodeBpt_<int>> actual = node.Split();
            NodeBpt_<int> left = actual.Item1;
            NodeBpt_<int> right = actual.Item2;


            ///  testing  ///
            Assert.AreEqual(0, left.Key);
            Assert.AreEqual(5, right.Key);
        }

        #endregion LeafBpt_<T>.Split()


        #region ForkBpt_<T>.Combine()

        [TestMethod()]
        public void Combine_Test__Fork_Knowns__Assert_Same_Node_Same_Type_Returned()  /* working */  {
            /*  two asserts  */

            ///  groundwork  ///
            NodeBpt_<int> subject = new ForkBpt_<int>(0);
            NodeBpt_<int> other = new ForkBpt_<int>(5);


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.IsInstanceOfType(combined, typeof(ForkBpt_<int>));
            Assert.AreEqual(subject, combined);   // no IEquality<>, so compared as pointers: identity 
        }

        [TestMethod()]
        public void Combine_Test__Fork_To_Full_Minus_One__Assert_Expected_Children_Count()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> subject = new ForkBpt_<int>(0);

            /*  half full */
            for (int i = 0; i < 5; i++) {
                LeafBpt_<int> child = new LeafBpt_<int>(i);
                subject.Children.Add(child);
            }

            NodeBpt_<int> other = new ForkBpt_<int>(5);

            /*  1 less than half-full  */
            for (int i = 0; i < 4; i++) {
                LeafBpt_<int> child = new LeafBpt_<int>(i);
                other.Children.Add(child);
            }


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.AreEqual(9, combined.Children.Count);
        }

        [TestMethod()]
        public void Combine_Test__Fork_To_Full_Minus_One__Assert_Expected_Children_And_Order()  /* working */  {
            /*  correct count of children is assumed here (and tested separately)  */

            ///  groundwork  ///
            NodeBpt_<int> subject = new ForkBpt_<int>(0);

            LeafBpt_<int>[] expected = new LeafBpt_<int>[9];
            LeafBpt_<int>[] actual = new LeafBpt_<int>[9];

            /*  half full */
            for (int i = 0; i < 5; i++) {
                LeafBpt_<int> child = new LeafBpt_<int>(i);
                subject.Children.Add(child);
                expected[i] = child;
            }

            NodeBpt_<int> other = new ForkBpt_<int>(5);

            /*  1 less than half-full  */
            for (int i = 0; i < 4; i++) {
                LeafBpt_<int> child = new LeafBpt_<int>(i);
                other.Children.Add(child);
                expected[i + 5] = child;
            }


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);

            for (int i = 0; i < combined.Children.Count; i++) {
                actual[i] = (LeafBpt_<int>)combined.Children[i];
            }


            ///  testing  ///
            /*  tests both which children, and what order, in effect  */
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Combine_Test__Fork_Knowns__Assert_Expected_Key()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> subject = new ForkBpt_<int>(0);
            NodeBpt_<int> other = new ForkBpt_<int>(5);


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.AreEqual(subject.Key, combined.Key);
        }

        #endregion ForkBpt_<T>.Combine()


        #region LeafBpt_<T>.Combine()

        [TestMethod()]
        public void Combine_Test__Leaf_Knowns__Assert_Same_Node_Same_Type_Returned()  /* working */  {
            /*  two asserts  */

            ///  groundwork  ///
            NodeBpt_<int> subject = new LeafBpt_<int>(0);
            NodeBpt_<int> other = new LeafBpt_<int>(5);


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.IsInstanceOfType(combined, typeof(LeafBpt_<int>));
            Assert.AreEqual(subject, combined);   // no IEquality<>, so compared as pointers: identity 
        }

        [TestMethod()]
        public void Combine_Test__Leaf_To_Full_Minus_One__Assert_Expected_Children_Count()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> subject = new ForkBpt_<int>(0);

            /*  half full */
            for (int i = 0; i < 5; i++) {
                KeyPointerBpt_<int> child = new KeyPointerBpt_<int>(i);
                subject.Children.Add(child);
            }

            NodeBpt_<int> other = new ForkBpt_<int>(5);

            /*  1 less than half-full  */
            for (int i = 0; i < 4; i++) {
                KeyPointerBpt_<int> child = new KeyPointerBpt_<int>(i);
                other.Children.Add(child);
            }


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.AreEqual(9, combined.Children.Count);
        }

        [TestMethod()]
        public void Combine_Test__Leaf_To_Full_Minus_One__Assert_Expected_Children_And_Order()  /* working */  {
            /*  correct count of children is assumed here (and tested separately)  */

            ///  groundwork  ///
            NodeBpt_<int> subject = new LeafBpt_<int>(0);

            KeyPointerBpt_<int>[] expected = new KeyPointerBpt_<int>[9];
            KeyPointerBpt_<int>[] actual = new KeyPointerBpt_<int>[9];

            /*  half full */
            for (int i = 0; i < 5; i++) {
                KeyPointerBpt_<int> child = new KeyPointerBpt_<int>(i);
                subject.Children.Add(child);
                expected[i] = child;
            }

            NodeBpt_<int> other = new LeafBpt_<int>(5);

            /*  1 less than half-full  */
            for (int i = 0; i < 4; i++) {
                KeyPointerBpt_<int> child = new KeyPointerBpt_<int>(i);
                other.Children.Add(child);
                expected[i + 5] = child;
            }


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);

            for (int i = 0; i < combined.Children.Count; i++) {
                actual[i] = (KeyPointerBpt_<int>)combined.Children[i];
            }


            ///  testing  ///
            /*  tests both which children, and what order, in effect  */
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Combine_Test__Leaf_Knowns__Assert_Expected_Key()  /* working */  {
            ///  groundwork  ///
            NodeBpt_<int> subject = new LeafBpt_<int>(0);
            NodeBpt_<int> other = new LeafBpt_<int>(5);


            ///  exercising the code  ///
            NodeBpt_<int> combined = subject.CombineWithRightSibling(other);


            ///  testing  ///
            Assert.AreEqual(subject.Key, combined.Key);
        }

        #endregion LeafBpt_<T>.Combine()


        #region SearchToOp()

        #region Only Finding, With No Op

        [TestMethod()]
        public void SearchToOp_Test__Find_Only__Two_Level_Tree__Exact_Match__Assert_Expected_Pointer()  /* working */  {
            ///  groundwork  ///
            BPlusTree_<int> tree = TestFixtures.HardConstructOnesBPlusTree();

            int key = 15;
            int expected = 375;   // faux pointer to a disk block 


            ///  exercising the code  ///
            IKeyedObject<int> passer = tree.SearchToOp(key, BptOps_.Find);
            KeyPointerBpt_<int> actual = (KeyPointerBpt_<int>)passer;


            ///  testing  ///
            Assert.AreEqual(expected, actual?.Pointer);   // Elvised 
        }

        [TestMethod()]
        public void SearchToOp_Test__Find_Only__Two_Level_Tree__Nearest_Match__Assert_Null()  /* working */  {
            /*  all these searches are exact-match searches  */

            ///  groundwork  ///
            BPlusTree_<int> tree = TestFixtures.HardConstructTwosBPlusTree();   // sequential even numbers 

            int sought = 31;   // 1 more than an extant key 


            ///  exercising the code  ///
            IKeyedObject<int> passer = tree.SearchToOp(sought, BptOps_.Find);
            KeyPointerBpt_<int> actual = (KeyPointerBpt_<int>)passer;


            ///  testing  ///
            Assert.AreEqual(null, actual?.Pointer);   // Elvised 
        }

        [TestMethod()]
        public void SearchToOp_Test__Find_Only__Three_Level_Tree__Exact_Match__Assert_Expected_Pointer()  /* working */  {
            ///  groundwork  ///
            BPlusTree_<int> tree = TestFixtures.HardConstructThreesBPlusTree();

            int key = 237;
            int expected = 474;   // faux pointer to a disk block 


            ///  exercising the code  ///
            IKeyedObject<int> passer = tree.SearchToOp(key, BptOps_.Find);
            KeyPointerBpt_<int> actual = (KeyPointerBpt_<int>)passer;


            ///  testing  ///
            Assert.AreEqual(expected, actual?.Pointer);   // Elvised 
        }

        [TestMethod()]
        public void SearchToOp_Test__Find_Only__Three_Level_Tree__Nearest_Match__Assert_Null()  /* working */  {
            /*  all these searches are exact-match searches  */

            ///  groundwork  ///
            BPlusTree_<int> tree = TestFixtures.HardConstructThreesBPlusTree();   // sequential multiples of 3 

            int sought = 239;   // 2 more than extant key 237 


            ///  exercising the code  ///
            IKeyedObject<int> passer = tree.SearchToOp(sought, BptOps_.Find);
            KeyPointerBpt_<int> actual = (KeyPointerBpt_<int>)passer;


            ///  testing  ///
            Assert.AreEqual(null, actual?.Pointer);   // Elvised 
        }

        #endregion Only Finding, With No Op

        /*  --> reesume tests here, as remarked in "reesume" further down here <--  */

        #endregion SearchToOp()


        /*  !! reesume with searching with ops: add, remove, split, combine; 
         *      invoked by parent node, performing split / combine at child(ren)'s overflow / underflow  */

        /*  !! add tests for adding split node to parent node <--  */

        /*  !! tests to code and run:  */

        /*  parent node not full, children interleaved: 
         *      new children count; 
         *      new ordered keys; 
         *      new children actually present at ordered keys  */

        /*  parent node made full by split children  */
        /*  parent node only capable of taking 1 of 2 split children  */

        /*  chained upward splitting; at rcollapse of a search chain; isolinear  */

        /*  chained upward combining; also at rcollapse of search chain; isolinear  */


        /*  traversing over leaves and their key-pointer children  */

        /*  ?sloshing  */

        /*  ?slosh children L without split  */
        /*  ?slosh children R without split  */

        /*  ?changing keys at slosh  */

    }


    [TestClass]
    public class KeyedListBpt_Tests
    {
        #region Add(), Drop(), Indexer get / set

        [TestMethod()]
        public void Add_Test__3_Adds__Assert_Expected_Top()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);


            ///  exercising the code  ///
            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  testing  ///
            Assert.AreEqual(2, list.Top);
        }

        [TestMethod()]
        public void Add_Test__3_Adds__Assert_Expected_Count()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);


            ///  exercising the code  ///
            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  testing  ///
            Assert.AreEqual(3, list.Count);
        }

        [TestMethod()]
        public void Add_Test__3_Adds__Assert_Expecteds_By_Indexer()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);


            ///  exercising the code  ///
            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  testing  ///
            for (int i = 0; i < 3; i++) {
                Assert.AreEqual(i, list[i].Key);
            }
        }

        [TestMethod()]
        public void Add_Test__Reference_T_3_Adds_1_Drop_Get_3rd__Assert_Null()  /* working */  {
            /*  addressing an index beyond the valid region 
             *  returns ~null as max useful for Bpt internals  */

            ///  groundwork  ///
            KeyedListBpt_<string> list = new KeyedListBpt_<string>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<string>($"{i}"));   // === .ToString() 
            }

            list.Drop();

            IKeyedObject<string> actual;


            ///  exercising the code  ///
            actual = list[2];


            ///  testing  ///
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void Add_Test__3_Adds_1_Drop_Set_3rd__Assert_Expected_Exception()  /* working */  {
            /*  unlike getting an outside-valid-range item, setting one has no 
             *  logical meaning and indicates bad invoking code, so exception retained  */

            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }

            list.Drop();


            ///  exercising the code  ///
            try {
                IKeyedObject<int> passer = new KeyedEmpty<int>(int.MaxValue);
                list[2] = passer;   // have to use setter to get expected fail 
            }
            catch (Exception e) {
                Assert.IsInstanceOfType(e, typeof(ArgumentOutOfRangeException));
                return;
            }


            ///  testing  ///
            Assert.Fail("Expected exception not thrown.");
        }

        [TestMethod()]
        public void Drop_Test__No_Index_3_Adds_2_Drops__Assert_Expected_Top()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  exercising the code  ///
            list.Drop();
            list.Drop();


            ///  testing  ///
            Assert.AreEqual(0, list.Top);   // at 1st item 
        }

        [TestMethod()]
        public void Drop_Test__No_Index_3_Adds_2_Drops__Assert_Expected_Count()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  exercising the code  ///
            list.Drop();
            list.Drop();


            ///  testing  ///
            Assert.AreEqual(1, list.Count);
        }

        [TestMethod()]
        public void Drop_Test__No_Index_3_Adds_1_Drop__Assert_Expecteds_By_Indexer()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }


            ///  exercising the code  ///
            list.Drop();


            ///  testing  ///
            for (int i = 0; i < 2; i++) {
                Assert.AreEqual(i, list[i].Key);
            }
        }

        #endregion Add(), Drop(), Indexer get / set


        #region Insert(), Remove()

        [TestMethod()]
        public void Insert_Test__1_Add_3_Inserts__Assert_Expected_Indexed_Values()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);
            list.Add(new KeyedEmpty<int>(int.MaxValue));   // at 0 


            ///  exercising the code  ///
            /*  creating sequence <<0, 1, 2>> out of order, with max-value at [3]  */
            list.Insert(new KeyedEmpty<int>(2), 0);   // <<2, int.MaxValue>> 
            list.Insert(new KeyedEmpty<int>(0), 0);   // <<0, 2, int.MaxValue>> 
            list.Insert(new KeyedEmpty<int>(1), 1);   // <<0, 1, 2, int.MaxValue>> 


            ///  testing  ///
            Assert.AreEqual(0, list[0].Key);
            Assert.AreEqual(1, list[1].Key);
            Assert.AreEqual(2, list[2].Key);
            Assert.AreEqual(int.MaxValue, list[3].Key);
        }

        [TestMethod()]
        public void Insert_Test__1_Add_3_Inserts__Assert_Expected_Count_And_Top()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);
            list.Add(new KeyedEmpty<int>(int.MaxValue));


            ///  exercising the code  ///
            list.Insert(new KeyedEmpty<int>(2), 0);
            list.Insert(new KeyedEmpty<int>(0), 0);
            list.Insert(new KeyedEmpty<int>(1), 1);


            ///  testing  ///
            Assert.AreEqual(3, list.Top);
            Assert.AreEqual(4, list.Count);
        }

        [TestMethod()]
        public void Remove_Test__4_Adds_2_Removes__Assert_Expected_Indexed_Values()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }

            list.Add(new KeyedEmpty<int>(int.MaxValue));   // at 3 


            ///  exercising the code  ///
            list.Remove(1);   // <<0, 2, int.MaxValue>> 
            list.Remove(1);   // <<0, int.MaxValue>> 

            ///  testing  ///
            Assert.AreEqual(0, list[0].Key);
            Assert.AreEqual(int.MaxValue, list[1].Key);
        }

        [TestMethod()]
        public void Remove_Test__4_Adds_2_Removes__Assert_Expected_Removed_Values()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }

            list.Add(new KeyedEmpty<int>(int.MaxValue));   // at 3 


            ///  exercising the code  ///
            int actualOne = list.Remove(1).Key;   // <<0, 2, int.MaxValue>> 
            int actualTwo = list.Remove(1).Key;   // <<0, int.MaxValue>> 

            ///  testing  ///
            Assert.AreEqual(1, actualOne);
            Assert.AreEqual(2, actualTwo);
        }

        [TestMethod()]
        public void Remove_Test__4_Adds_2_Removes__Assert_Expected_Count_And_Top()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(5);

            for (int i = 0; i < 3; i++) {
                list.Add(new KeyedEmpty<int>(i));
            }

            list.Add(new KeyedEmpty<int>(int.MaxValue));


            ///  exercising the code  ///
            list.Remove(1);
            list.Remove(1);

            ///  testing  ///
            Assert.AreEqual(1, list.Top);
            Assert.AreEqual(2, list.Count);
        }

        #endregion Insert(), Remove()


        #region Clear()

        [TestMethod()]
        public void Clear_Test__Knowns__Assert_Zero_Count()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(10);
            TestFixtures.AddSequentialIntKeyedEmpties(list, 10);


            ///  exercising the code  ///
            list.Clear();


            ///  testing  ///
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod()]
        public void Clear_Test__Knowns__Assert_MinusOne_Top()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(10);
            TestFixtures.AddSequentialIntKeyedEmpties(list, 10);


            ///  exercising the code  ///
            list.Clear();


            ///  testing  ///
            Assert.AreEqual(-1, list.Top);
        }

        [TestMethod()]
        public void Clear_Test__Knowns__Assert_All_Nulls()  /* working */  {
            /*  checking internal values because I want to test a hard-erase of all  */

            ///  groundwork  ///
            KeyedListBpt_<int> list = new KeyedListBpt_<int>(10);
            TestFixtures.AddSequentialIntKeyedEmpties(list, 10);
            PrivateObject p = new PrivateObject(list);

            IKeyedObject<int>[] objects = (IKeyedObject<int>[])p.GetField("_contents");


            ///  exercising the code  ///
            list.Clear();


            ///  testing  ///
            for (int i = 0; i < objects.Length; i++) {
                Assert.AreEqual(null, objects[i]);
            }
        }


        #endregion Clear()


        #region SearchExact()

        [TestMethod()]
        public void SearchExact_Test__Exact_Match_Low__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of exact match if any  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            int expected = 6;   // 7th letter; zbi 


            ///  exercising the code  ///
            int actual = subject.SearchExact('g');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchExact_Test__Exact_Match_High__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of exact match if any  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            int expected = 22;   // 23rd letter; zbi 


            ///  exercising the code  ///
            int actual = subject.SearchExact('w');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchExact_Test__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns index of exact match if any; 
             *  testing *all* els of subject for thoroughness here  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(20);

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject.Add(new KeyedEmpty<int>(element));
            }


            ///  exercising the code and testing  ///
            /*  not in original order, to make me feel better  */
            int actual;

            /*  low half of subject  */
            for (int i = 9; i >= 0; i--) {
                actual = subject.SearchExact(i * 17);
                Assert.AreEqual(i, actual);
            }

            /*  high half of subject  */
            for (int i = 19; i >= 10; i--) {
                actual = subject.SearchExact(i * 17);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void SearchExact_Test__No_Match__Assert_Expected_None()  /* working */  {
            /*  search returns index of exact match if any  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            int expected = BptConstants_.None;   // -1 

            ///  exercising the code  ///
            int actual = subject.SearchExact('5');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        #endregion SearchExact()


        #region SearchClosest()

        [TestMethod()]
        public void SearchClosest_Test__Exact_Match_Low__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of exact match if any, else next-lower  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            int expected = 6;   // 7th letter; zbi 


            ///  exercising the code  ///
            int actual = subject.SearchClosest('g');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__Exact_Match_High__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of exact match if any, else next-lower  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            int expected = 22;   // 23rd letter; zbi 


            ///  exercising the code  ///
            int actual = subject.SearchClosest('w');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__Closest_Below_Low__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of next-lower if no exact match  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(20);

            for (int i = 0; i < 20; i++) {
                subject.Add(new KeyedEmpty<int>(i * 10));
            }

            int expected = 5;   // 50; highest lower than 55 


            ///  exercising the code  ///
            int actual = subject.SearchClosest(55);


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__Closest_Below_High__Assert_Expected_Offset()  /* working */  {
            /*  search returns index of next-lower if no exact match  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(20);

            for (int i = 0; i < 20; i++) {
                subject.Add(new KeyedEmpty<int>(10 * i));
            }

            int expected = 17;   // 170; highest below 175 


            ///  exercising the code  ///
            int actual = subject.SearchClosest(175);


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__No_Match__Assert_Minus_One()  /* working */  {
            /*  search returns index of exact match if any, else next-lower  */

            ///  groundwork  ///
            KeyedListBpt_<char> subject = new KeyedListBpt_<char>(26);

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject.Add(new KeyedEmpty<char>(letter));
            }

            /*  '5' is lower than any letters, so no next-lower in subject  */
            int expected = -1;


            ///  exercising the code  ///
            int actual = subject.SearchClosest('5');


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__Above_All__Assert_Top()  /* working */  {
            /*  search returns index of exact match if any, else highest below; 
             *  when sought is above all, highest below is top  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(10);

            for (int i = 0; i < 10; i++) {
                subject.Add(new KeyedEmpty<int>(i * 25));
            }

            /*  if above all present, then might be in final subtree, above last key  */
            int expected = 9;


            ///  exercising the code  ///
            int actual = subject.SearchClosest(400);


            ///  testing  ///
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SearchClosest_Test__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /*  testing all for thoroughness  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(20);

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject.Add(new KeyedEmpty<int>(element));
            }


            ///  exercising the code and testing  ///
            /*  not in original order, to make me feel better  */
            int actual;

            /*  low half of subject  */
            for (int i = 9; i >= 0; i--) {
                actual = subject.SearchClosest(i * 17);
                Assert.AreEqual(i, actual);
            }

            /*  high half of subject  */
            for (int i = 19; i >= 10; i--) {
                actual = subject.SearchClosest(i * 17);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void SearchClosest_Test__Closest_Below_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns highest el that is <= sought; 
             *  testing all for thoroughness  */

            ///  groundwork  ///
            KeyedListBpt_<int> subject = new KeyedListBpt_<int>(20);

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject.Add(new KeyedEmpty<int>(element));
            }


            ///  exercising the code and testing  ///
            /*  not in original order, to make me feel better  */
            int actual;

            /*  low half of subject; each sought is slightly higher than an el  */
            for (int i = 9; i >= 0; i--) {
                actual = subject.SearchClosest((i * 17) + 2);
                Assert.AreEqual(i, actual);
            }

            /*  below low half of subject; .None / -1 is correct result  */
            actual = subject.SearchClosest(-3);
            Assert.AreEqual(-1, actual);

            /*  high half of subject; each sought is slightly higher than an el  */
            for (int i = 19; i >= 10; i--) {
                actual = subject.SearchClosest((i * 17) + 5);
                Assert.AreEqual(i, actual);
            }
        }

        #endregion SearchClosest()


        #region Enumerability

        [TestMethod()]
        public void Enumerability_Test__Known_Contents__Assert_Expecteds()  /* working */  {
            ///  groundwork  ///
            KeyedListBpt_<double> subject = new KeyedListBpt_<double>(15);

            for (int i = 0; i < subject.Size; i++) {   // *not* ~subject.Count~; that is 0 until .Add()s
                subject.Add(new KeyedEmpty<double>(i * 3.68));
            }

            int traverser = 0;
            double[] expecteds = {
                00.00, 03.68, 07.36, 11.04, 14.72,
                18.40, 22.08, 25.76, 29.44, 33.12,
                36.80, 40.48, 44.16, 47.84, 51.52
            };


            ///  exercising the code and testing  ///
            foreach (KeyedEmpty<double> item in subject) {
                /*  allowed difference between expected & actual, since ~double inexact  */
                double delta = 0.01F;
                Assert.AreEqual(expecteds[traverser], item.Key, delta);
                traverser++;
            }
        }

        #endregion Enumerability
    }


    [TestClass]
    public class KeyPointerBpt_Tests
    {
        #region IEquatable<T> : Equals()

        [TestMethod()]
        public void Equals_Test__Equal_Keys__Assert_True()  /* working */  {
            /*  key-pointer pairs are equal by keys, regardless of other contents  */

            ///  groundwork  ///
            KeyPointerBpt_<string> left = new KeyPointerBpt_<string>("same");
            KeyPointerBpt_<string> right = new KeyPointerBpt_<string>("same");


            ///  exercising the code  ///
            bool actual = left.Equals(right);


            ///  testing  ///
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void Equals_Test__Keys_Not_Equal__Assert_False()  /* working */  {
            /*  key-pointer pairs are equal by keys, regardless of other contents  */

            ///  groundwork  ///
            KeyPointerBpt_<string> left = new KeyPointerBpt_<string>("this");
            KeyPointerBpt_<string> right = new KeyPointerBpt_<string>("different");


            ///  exercising the code  ///
            bool actual = left.Equals(right);


            ///  testing  ///
            Assert.AreEqual(false, actual);
        }

        #endregion IEquatable<T> : Equals()


        #region IComparable<T> : CompareTo()

        [TestMethod()]
        public void CompareTo_Test__Equal_Keys__Assert_0()  /* working */  {
            /*  key-pointer pairs are compared by keys, regardless of other contents  */

            ///  groundwork  ///
            KeyPointerBpt_<string> left = new KeyPointerBpt_<string>("same");
            KeyPointerBpt_<string> right = new KeyPointerBpt_<string>("same");


            ///  exercising the code  ///
            int actual = left.CompareTo(right);


            ///  testing  ///
            Assert.AreEqual(0, actual);
        }

        [TestMethod()]
        public void CompareTo_Test__Left_Greater_Key__Assert_1()  /* working */  {
            /*  key-pointer pairs are compared by keys, regardless of other contents  */

            ///  groundwork  ///
            KeyPointerBpt_<int> left = new KeyPointerBpt_<int>(101);
            KeyPointerBpt_<int> right = new KeyPointerBpt_<int>(100);


            ///  exercising the code  ///
            int actual = left.CompareTo(right);


            ///  testing  ///
            Assert.AreEqual(1, actual);
        }

        [TestMethod()]
        public void CompareTo_Test__Equal_Keys__Assert_Minus_1()  /* working */  {
            /*  key-pointer pairs are compared by keys, regardless of other contents  */

            ///  groundwork  ///
            KeyPointerBpt_<int> left = new KeyPointerBpt_<int>(100);
            KeyPointerBpt_<int> right = new KeyPointerBpt_<int>(101);


            ///  exercising the code  ///
            int actual = left.CompareTo(right);


            ///  testing  ///
            Assert.AreEqual(-1, actual);
        }

        #endregion IComparable<T> : CompareTo()

    }


    #region Test Fixtures

    public class TestFixtures
    {
        public static void AddSequentialIntKeyedEmpties(NodeBpt_<int> subject, int number)  /* verified */  {
            for (int i = 0; i < number; i++) {
                subject.Children.Add(new KeyedEmpty<int>(i));
            }
        }

        public static void AddSequentialIntKeyedEmpties(KeyedListBpt_<int> subject, int number)  /* verified */  {
            for (int i = 0; i < number; i++) {
                subject.Add(new KeyedEmpty<int>(i));
            }
        }

        public static BPlusTree_<int> HardConstructOnesBPlusTree()  /* verified */  {
            /*  builds a partly full, 2-level tree from root only for testing; 
             *      constructed by hand, in contrast to normal ops; 
             *  keys are consecutive integers, 1 apart  */


            /*  basic tree  */
            KeyPointerBpt_<int> rootIko = new KeyPointerBpt_<int>(0, 0);
            BPlusTree_<int> tree = new BPlusTree_<int>(rootIko);


            /*  to manipulate internals  */
            PrivateObject p = new PrivateObject(tree);


            /*  replace tree's root leaf with a root fork  */
            NodeBpt_<int> newRoot = new ForkBpt_<int>(0);   // when children, root must be fork 
            p.SetField("_root", newRoot);


            /*  add child leaves and their child items; 3 leaves with 7 items each  */
            for (int leaves = 0; leaves < 28; /*  no ops; inner loop  */) {
                LeafBpt_<int> leaf = new LeafBpt_<int>(leaves);

                /*  each leaf is 7/10 full  */
                for (int items = 0; items < 7; items++, leaves++) {
                    //leaves++;   /*  incr'g here preserves overall net offset  */
                    KeyPointerBpt_<int> item = new KeyPointerBpt_<int>(leaves, leaves * 25);
                    leaf.Children.Add(item);
                }

                newRoot.Children.Add(leaf);
            }

            return (tree);
        }

        public static BPlusTree_<int> HardConstructTwosBPlusTree()  /* verified */  {
            /*  builds a partly full, 2-level tree from root only for testing; 
             *      constructed by hand, in contrast to normal ops; 
             *  keys are consecutive even integers, 2 apart  */


            /*  basic tree  */
            KeyPointerBpt_<int> rootIko = new KeyPointerBpt_<int>(0, 0);
            BPlusTree_<int> tree = new BPlusTree_<int>(rootIko);


            /*  to manipulate internals  */
            PrivateObject p = new PrivateObject(tree);


            /*  replace tree's root leaf with a root fork  */
            NodeBpt_<int> newRoot = new ForkBpt_<int>(0);   // when children, root must be fork 
            p.SetField("_root", newRoot);


            /*  add child leaves and their child items; 4 leaves with 7 items each  */
            for (int leaves = 0; leaves < 56; /*  no ops; inner loop  */) {
                LeafBpt_<int> leaf = new LeafBpt_<int>(leaves);

                /*  each leaf is 7/10 full  */
                for (int items = 0; items < 7; items++, leaves += 2) {
                    //leaves++;   /*  incr'g here preserves overall net offset  */
                    KeyPointerBpt_<int> item = new KeyPointerBpt_<int>(leaves, (leaves / 2) * 25);
                    leaf.Children.Add(item);
                }

                newRoot.Children.Add(leaf);
            }

            return (tree);
        }

        public static BPlusTree_<int> HardConstructThreesBPlusTree()  /* verified */  {
            /*  builds a 3-level B+tree; keys are consecutive multiples of 3  */

            /*  basic tree  */
            KeyPointerBpt_<int> iko = new KeyPointerBpt_<int>(int.MinValue, int.MaxValue);
            BPlusTree_<int> tree = new BPlusTree_<int>(iko);


            /*  to manipulate internals  */
            PrivateObject p = new PrivateObject(tree);


            /*  new root, a fork, with min real key  */
            NodeBpt_<int> root = new ForkBpt_<int>(0);
            p.SetField("_root", root);

            /*  build 3 levels below root, each one 7/10 full; separate counters those loops  */
            /*  1st 2 levels are nodes, last level is key-pointer pairs; 
             *  same leading key in object at each level as expected  */
            for (int key = 0; key < (7 * 7 * 7); /*  @innermost  */) {
                NodeBpt_<int> childOfRoot = new ForkBpt_<int>(key);

                for (int grandchild = 0; grandchild < 7; grandchild++) {
                    NodeBpt_<int> grandchildOfRoot = new LeafBpt_<int>(key);   // final actual in-tree level 

                    for (int greatGrandchild = 0; greatGrandchild < 7; greatGrandchild++, key += 3) {
                        IKeyedObject<int> item = new KeyPointerBpt_<int>(key, key * 2);
                        grandchildOfRoot.Children.Add(item);
                    }

                    childOfRoot.Children.Add(grandchildOfRoot);
                }

                root.Children.Add(childOfRoot);
            }

            return (tree);
        }

    }

    public class KeyedEmpty<T> : IKeyedObject<T> where T : IEquatable<T>, IComparable<T>
    {
        /*  normally an IKeyedObject<> would have an object, like a node or data pointer  */

        public T Key { get; set; }

        public KeyedEmpty(T key)  /* verified */  {
            Key = key;
        }
    }

    #endregion Test Fixtures

}
