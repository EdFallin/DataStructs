﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructs
{
    class List_Array_x<T> : List_x<T>
    {
        #region Definitions

        const int DEFAULT_SIZE = 16;

        #endregion Definitions



        #region Fields

        int _arraySize;  //  of entire array
        int _listSize;   //  of array elements used by list
        int _currentIndex;

        T[] _array;      //  underlying data structure, consecutive memory blocks of the same size;
        //  in this enviro, each block holds a pointer to an object ( unless T is a primitive )

        #endregion Fields



        #region Constructors

        public List_Array_x()
            : this(DEFAULT_SIZE)
        {
            //  no operations
        }

        public List_Array_x(int length)
        {
            _array = new T[length];
            _arraySize = length;
        }

        #endregion Constructors



        #region Properties - List Metadata

        public override int Length
        {
            get { return (_listSize); }
        }

        private int Max
        {
            get { return (_arraySize); }
        }

        public override int CurrentPosition
        {
            get { return (_currentIndex); }
        }

        internal override int ListTop
        {
            get { return (_listSize - 1); }
        }

        #endregion Properties - List Metadata



        #region Property - Current Element Value

        public override T CurrentValue
        {
            get
            {
                return (_array[_currentIndex]);
            }
            set
            {
                _array[_currentIndex] = value;
            }
        }

        #endregion Property - Current Element Value



        #region Changing List Contents

        //  inserts rather than replacing
        public override void InsertAtCurrent(T element)
        {
            if (_listSize == _arraySize)
                throw new Exception("@InsertAtCurrent: _listSize == _arraySize");

            //  first, existing elements have to be shifted forward in the array if possible;
            //      using full _listSize because it will be filled after shifting
            for (int i = _listSize; i > _currentIndex; i--)
                _array[i] = _array[i - 1];

            //  second, the new element is inserted
            _array[_currentIndex] = element;

            //  third, metadata
            _listSize++;
        }

        //  returns removed element
        public override T RemoveAtCurrent()
        {
            //  first, get value to return
            T current = _array[_currentIndex];

            //  second, shift all elements down by one;
            //      that also throws away removed element;
            //      leftover element is ignored, as it is left unindexed
            for (int i = _currentIndex; i < this.ListTop; i++)
            {
                _array[i] = _array[i + 1];
            }

            //  third, metadata
            _listSize--;

            //  fourth, output
            return (current);
        }

        public override void Append(T element)
        {
            if (_listSize == _arraySize)
                throw new Exception("@ Append: _listSize == _arraySize");

            _listSize++;
            _array[_listSize] = element;
        }

        public override void Clear()
        {
            //  no deallocation; left to the GC
            _listSize = 0;
            _currentIndex = 0;
        }

        #endregion Changing List Contents



        #region Consecutive Positioning

        public override void Next()
        {
            if (_currentIndex == this.ListTop)
                throw new Exception("@Next: _currentIndex == this.ListTop");

            _currentIndex++;
        }

        public override void Previous()
        {
            if (_currentIndex == 0)
                throw new Exception("@PositionAtStart: _currentIndex == 0");

            _currentIndex--;
        }

        #endregion Consecutive Positioning



        #region Nonconsecutive Positioning

        public override void PositionAtStart()
        {
            _currentIndex = 0;
        }

        public override void PositionAtEnd()
        {
            _currentIndex = this.ListTop;
        }

        public override void PositionAtIndex(int index)
        {
            if (index < 0 || index > this.ListTop)
                throw new Exception("@PositionAtIndex: index < 0 || index > this.ListTop");

            _currentIndex = index;
        }

        #endregion Nonconsecutive Positioning
    }
}
