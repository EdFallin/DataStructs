﻿namespace DataStructs
{
    partial class Form_DataStructs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Exit = new System.Windows.Forms.Button();
            this.label_Explanation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Exit
            // 
            this.button_Exit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Exit.Location = new System.Drawing.Point(153, 141);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(81, 29);
            this.button_Exit.TabIndex = 0;
            this.button_Exit.Text = "Exit";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // label_Explanation
            // 
            this.label_Explanation.AutoSize = true;
            this.label_Explanation.BackColor = System.Drawing.Color.LightGray;
            this.label_Explanation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Explanation.Location = new System.Drawing.Point(17, 9);
            this.label_Explanation.Name = "label_Explanation";
            this.label_Explanation.Padding = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.label_Explanation.Size = new System.Drawing.Size(352, 120);
            this.label_Explanation.TabIndex = 1;
            this.label_Explanation.Text = "DataStructs exists to test different data structures.\r\nThere is no user interacti" +
                "on.\r\n\r\nTo use DataStructs, open it in Visual Studio and\r\nrun its various tests.\r" +
                "\n\r\nClick Exit to close the program now.";
            // 
            // Form_DataStructs
            // 
            this.AcceptButton = this.button_Exit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button_Exit;
            this.ClientSize = new System.Drawing.Size(386, 180);
            this.Controls.Add(this.label_Explanation);
            this.Controls.Add(this.button_Exit);
            this.Name = "Form_DataStructs";
            this.Text = "DataStructs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Label label_Explanation;
    }
}

