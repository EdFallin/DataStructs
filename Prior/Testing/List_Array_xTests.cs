﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataStructs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass()]
    public class List_Array_xTests
    {
        #region Test Context

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion Test Context



        #region Class Methods

        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}

        #endregion Class Methods


        //  according to refsite, GenericParameterHelper is a nonspecific T you can use for testing;
        //  it wraps only Int32 values you want to provide for tests, accessible through the .Data member

        [TestMethod()]
        [DeploymentItem("DataStructs.exe")]
        public void List_Array_xConstructor_With_Arg_Test()
        {
            //  GROUNDWORK  
            List_Array_x_Accessor<GenericParameterHelper> target;
            int length = 9;

            int expectedLength = 0;
            int actualLength;
            int expectedMax = length;
            int actualMax;

            //  EXERCISING THE CODE  
            target = new List_Array_x_Accessor<GenericParameterHelper>(length);
            actualLength = target.Length;
            actualMax = target.Max;


            //  TESTING  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedMax, actualMax);
        }

        [TestMethod()]
        [DeploymentItem("DataStructs.exe")]
        public void List_Array_xConstructor_No_Arg_Test()
        {
            //  GROUNDWORK  
            List_Array_x_Accessor<GenericParameterHelper> target;

            int expectedLength = 0;
            int actualLength;
            int expectedMax = 16;
            int actualMax;

            //  EXERCISING THE CODE  
            target = new List_Array_x_Accessor<GenericParameterHelper>();
            actualLength = target.Length;
            actualMax = target.Max;


            //  TESTING  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedMax, actualMax);
        }

        public void RemoveAtCurrent_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            T expected = default(T);
            T actual;
            actual = target.RemoveAtCurrent();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test()
        {
            RemoveAtCurrent_TestHelper<GenericParameterHelper>();
        }

        public void Previous_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            target.Previous();
        }

        [TestMethod()]
        public void Previous_Test()
        {
            Previous_TestHelper<GenericParameterHelper>();
        }

        public void PositionAtStart_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            target.PositionAtStart();
        }

        [TestMethod()]
        public void PositionAtStart_Test()
        {
            PositionAtStart_TestHelper<GenericParameterHelper>();
        }

        public void PositionAtIndex_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            int index = 0;
            target.PositionAtIndex(index);
        }

        [TestMethod()]
        public void PositionAtIndex_Test()
        {
            PositionAtIndex_TestHelper<GenericParameterHelper>();
        }

        public void PositionAtEnd_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            target.PositionAtEnd();
        }

        [TestMethod()]
        public void PositionAtEnd_Test()
        {
            PositionAtEnd_TestHelper<GenericParameterHelper>();
        }

        public void Next_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            target.Next();
        }

        [TestMethod()]
        public void Next_Test()
        {
            Next_TestHelper<GenericParameterHelper>();
        }

        public void Clear_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            target.Clear();
        }

        [TestMethod()]
        public void Clear_Test()
        {
            Clear_TestHelper<GenericParameterHelper>();
        }

        public void Append_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            T element = default(T);
            target.Append(element);
        }

        [TestMethod()]
        public void Append_Test()
        {
            Append_TestHelper<GenericParameterHelper>();
        }

        public void InsertAtCurrent_TestHelper<T>()
        {
            List_Array_x<T> target = new List_Array_x<T>();
            T element = default(T);
            target.InsertAtCurrent(element);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test()
        {
            InsertAtCurrent_TestHelper<GenericParameterHelper>();
        }
    }
}
