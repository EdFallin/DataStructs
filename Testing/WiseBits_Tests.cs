﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass()]
    public class WiseBits_Tests
    {

        #region Templated Defaults

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        #endregion Templated Defaults


        [TestMethod()]
        public void AsByte_Test_Assert_Expecteds()   // working  
        {
            string byteOf1 = "00000001";
            string byteOf4 = "00000100";

            byte one = WiseBits.AsByte(byteOf1);
            byte four = WiseBits.AsByte(byteOf4);

            Assert.AreEqual(one, 1);
            Assert.AreEqual(four, 4);

            // bitwise-OR of these two should give me 5 ( 00000101 )

            byte expected = 5;
            byte actual;

            actual = (byte)(one | four);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToByte_0_To_15_Test_Assert_Expecteds()   // working  
        {
            string byteString;
            byte expected;
            byte actual;

            // 0
            byteString = "00000000";
            expected = 0;
            actual = byteString.ToByte();
            Assert.AreEqual(expected, actual);

            // 1
            expected = 1;
            actual = "00000001".ToByte();
            Assert.AreEqual(expected, actual);

            // 2
            expected = 2;
            actual = "00000010".ToByte();
            Assert.AreEqual(expected, actual);

            // 3
            expected = 3;
            actual = "00000011".ToByte();
            Assert.AreEqual(expected, actual);

            // 4
            expected = 4;
            actual = "00000100".ToByte();
            Assert.AreEqual(expected, actual);

            // 5
            expected = 5;
            actual = "00000101".ToByte();
            Assert.AreEqual(expected, actual);

            // 6
            expected = 6;
            actual = "00000110".ToByte();
            Assert.AreEqual(expected, actual);

            // 7
            expected = 7;
            actual = "00000111".ToByte();
            Assert.AreEqual(expected, actual);

            // 8
            expected = 8;
            actual = "00001000".ToByte();
            Assert.AreEqual(expected, actual);

            // 9
            expected = 9;
            actual = "00001001".ToByte();
            Assert.AreEqual(expected, actual);

            // 10
            expected = 10;
            actual = "00001010".ToByte();
            Assert.AreEqual(expected, actual);

            // 11
            expected = 11;
            actual = "00001011".ToByte();
            Assert.AreEqual(expected, actual);

            // 12
            expected = 12;
            actual = "00001100".ToByte();
            Assert.AreEqual(expected, actual);

            // 13
            expected = 13;
            actual = "00001101".ToByte();
            Assert.AreEqual(expected, actual);

            // 14
            expected = 14;
            actual = "00001110".ToByte();
            Assert.AreEqual(expected, actual);

            // 15
            expected = 15;
            actual = "00001111".ToByte();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AsString_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            byte target = "00001000".ToByte();

            // binary for 8
            string expected = "00001000";
            string actual;


            // EXERCISING THE CODE  
            actual = WiseBits.AsString(target);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AsString_O_To_20_Test_Assert_Expecteds()   // working  
        {
            // GROUNDWORK  
            byte[] targets = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

            string[] expecteds = new string[] {
                    "00000000", "00000001", "00000010", 
                    "00000011", "00000100", "00000101",
                    "00000110", "00000111", "00001000",
                    "00001001", "00001010", "00001011",
                    "00001100", "00001101", "00001110",
                    "00001111", "00010000", "00010001",
                    "00010010", "00010011", "00010100",
                };
            string[] actuals = new string[targets.Length];


            // EXERCISING THE CODE  
            for (int i = 0; i < targets.Length; i++)
                actuals[i] = WiseBits.AsString(targets[i]);


            // TESTING  
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void ToBinaryString_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            byte target = 20;

            string expected = "00010100";
            string actual;


            // EXERCISING THE CODE  
            actual = target.ToBinaryString();


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

    }
}

/*
 * 0  == 00000000
 * 1  == 00000001
 * 2  == 00000010
 * 3  == 00000011
 * 4  == 00000100
 * 5  == 00000101
 * 6  == 00000110
 * 7  == 00000111
 * 8  == 00001000
 * 9  == 00001001
 * 10 == 00001010
 * 11 == 00001011
 * 12 == 00001100
 * 13 == 00001101
 * 14 == 00001110
 * 15 == 00001111
*/

