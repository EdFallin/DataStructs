﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

using DataStructs;
using DataStructs.HashState_;

namespace Testing
{
    [TestClass]
    public class HashTable_Tests
    {
        #region Definitions

        #region Metadata Constants

        const string HASH_TABLE_META =
        "DataStructs, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";

        const string QUAD_INT_INT_META =
            "DataStructs.HashTable_`2+Quadruple_[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]," +
            "[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";

        const string QUAD_STRING_STRING_META =
            "DataStructs.HashTable_`2+Quadruple_[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]," +
            "[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";

        const string DUPLE_INT_INT_META =
            "DataStructs.HashTable_`2+Duple_[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]," +
            "[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";

        const string DUPLE_STRING_STRING_META =
            "DataStructs.HashTable_`2+Duple_[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]," +
            "[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";

        #endregion Metadata Constants


        public struct TableElementStringString
        {
            public int Slot;
            public string Key;
            public string Value;
            public SlotState State;

            public TableElementStringString(string key, string value, int slot, SlotState state) {
                this.Key = key;
                this.Value = value;
                this.Slot = slot;
                this.State = state;
            }
        }

        public struct TableElementIntInt
        {
            public int Slot;
            public int Key;
            public int Value;
            public SlotState State;

            public TableElementIntInt(int key, int value, int slot, SlotState state) {
                this.Key = key;
                this.Value = value;
                this.Slot = slot;
                this.State = state;
            }
        }

        #endregion Definitions


        #region Friendlies

        public bool IsAnOddNumber(int number)  /* passed */  {
            return ((number % 2) != 0);
        }


        #region Whole Array

        public HashTable_<string, string> BuildHashTableWithArrayOfQuadruples(int startSize)  /* passed */  {
            HashTable_<string, string> hashTable = new HashTable_<string, string>(startSize, "failed");

            PrivateObject pHash = new PrivateObject(hashTable);

            // half-fill hash table, in alternating slots; .Size here is next power of 2 at or above startSize 
            for (int ix = 0; ix < hashTable.Size; ix++) {
                // skip even-indexed slots 
                if (ix % 2 == 0) {
                    continue;
                }

                // fill odd-indexed slots with a new int-pri quad object 
                PrivateObject pQuadruple = this.PrivateStringStringQuadruple(ix.ToString(), ix.ToString());
                pHash.SetArrayElement("_table", pQuadruple.Target, ix);
            }

            return hashTable;
        }

        public HashTable_<string, string> BuildHashTableWithGivenElements(int startSize, params TableElementStringString[] elements)  /* passed */  {
            /* allows directly placing quadruples in home position or probe sequence */

            HashTable_<string, string> hashTable = new HashTable_<string, string>(startSize, "failed");

            PrivateObject pHash = new PrivateObject(hashTable);

            foreach (TableElementStringString element in elements) {
                PrivateObject pQuadruple = this.PrivateStringStringQuadruple(element.Key, element.Value);
                pQuadruple.SetFieldOrProperty("State", element.State);
                pHash.SetArrayElement("_table", pQuadruple.Target, element.Slot);
            }

            return hashTable;
        }

        public HashTable_<int, int> BuildEmptyHashTableHalfFullHalfOfThoseTombstones(int startSize)  /* passed */  {
            /* allows directly placing quadruples in home position or probe sequence */
            /* not the same */

            HashTable_<int, int> hashTable = new HashTable_<int, int>(startSize, int.MinValue);
            PrivateObject pHash = new PrivateObject(hashTable);

            int arraySize = Basics.NextPowerOfTwo(startSize);
            int fillSize = (arraySize / 2);   // exactly half full; array size always power of 2, ∴ even 

            for (int ix = 0; ix < fillSize; ix++) {
                int slot = ix * 2;   // distributing throughout array 
                PrivateObject pQuadruple = this.PrivateIntIntQuadruple(ix * 3, ix * 6);

                /* setting half of set elements to tombstones; every other element where 1/2 offset is odd: 2, 6, 10, 14, ... */
                if ((ix % 2) != 0) {
                    pQuadruple.SetFieldOrProperty("State", SlotState.Tombstone);
                }

                /* actually adding the new quadruple to the hash-table array */
                pHash.SetArrayElement("_table", pQuadruple.Target, slot);   // .Target is the actual quadruple to store 
            }

            return hashTable;
        }

        #endregion Whole Array


        #region Quadruples And Duples

        public PrivateObject PrivateStringStringQuadruple(string key, string value)  /* verified */  {
            PrivateObject quadruple = new PrivateObject(HASH_TABLE_META, QUAD_STRING_STRING_META, key, value);
            return quadruple;
        }

        public PrivateObject PrivateIntIntQuadruple(int key, int value)  /* ok */  {
            PrivateObject quadruple = new PrivateObject(HASH_TABLE_META, QUAD_INT_INT_META, key, value);
            return quadruple;
        }

        public PrivateObject PrivateStringStringEmptyDuple()  /* verified */  {
            PrivateObject duple = new PrivateObject(HASH_TABLE_META, DUPLE_STRING_STRING_META, SearchResult.None);   // no no-args constructor 
            return duple;
        }

        public PrivateObject PrivateIntIntEmptyDuple()  /* ok */  {
            PrivateObject duple = new PrivateObject(HASH_TABLE_META, DUPLE_INT_INT_META, SearchResult.None);   // no no-args constructor 
            return duple;
        }

        #endregion Quadruples And Duples


        public void AssignKeysAndTombstonesCount(HashTable_<int, int> subject, int keys, int tombstones)  /* verified */  {
            PrivateObject p = new PrivateObject(subject);
            p.SetField("_keys", keys);
            p.SetField("_tombstones", tombstones);
        }


        #region Disabled Friendlies

        /* used with now-disabled properties of HashTable_<> to get literals of reflected Quadruple_ and Duple_ metadata */

        //[TestMethod()]
        //public void QuadrupleMeta() {
        //   HashTable_<int, int> hashTable = new HashTable_<int, int>(10, 0);
        //   PrivateObject p = new PrivateObject(hashTable);

        //   string quadrupleName = (string)p.GetProperty("QuadrupleFullName");
        //   string output = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "IntIntQuadruple.txt");

        //   System.IO.File.AppendAllText(output, quadrupleName);
        //}

        //[TestMethod()]
        //public void DupleMeta() {
        //   HashTable_<string, string> hashTable = new HashTable_<string, string>(10, "fail-value");
        //   PrivateObject p = new PrivateObject(hashTable);

        //   string dupleName = (string)p.GetProperty("DupleFullName");
        //   string output = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "StringStringDuple.txt");

        //   System.IO.File.AppendAllText(output, dupleName);
        //}


        /* this friendly is actually an experiment and source for usable strings in tests that I'll use as literals */

        //[TestMethod()]   
        //public void FindMatchingStringHashes() {
        //   /* before, I looked for hashes but forgot to mod them to the table size (and to abs them), so I'm trying again here */

        //   /* algo: store up a number of random strings and hash values, and after any two match, stop; each hashed same as in HashTable_ */

        //   string characters = "abcdefghijklmnopqrstuvwxyz";
        //   const int TRIES = 100;
        //   const int SLOTS = 8;

        //   List<Tuple<int, string>> possibles = new List<Tuple<int, string>>();
        //   List<Tuple<int, string>> repeateds = new List<Tuple<int, string>>();


        //   /* get all random-ish strings and their hash values */
        //   for (int tries = 0; tries < TRIES; tries++) {
        //       string subject = string.Empty;

        //       for (int letter = 0; letter < 3; letter++) {
        //           Random r = new Random(tries + letter);
        //           char local = characters[r.Next(25)];
        //           subject += local;
        //       }


        //       int hash = subject.GetHashCode();
        //       hash = Math.Abs(hash);
        //       hash = hash % SLOTS;

        //       possibles.Add(Tuple.Create(hash, subject));
        //   }


        //   /* group ^possibles by hashes and project them into ^repeateds */
        //   var groupings = from possible in possibles
        //                   orderby possible.Item1, possible.Item2
        //                   group possible by possible.Item1;

        //   string output = "Strings that share hash values:" + Environment.NewLine;

        //   foreach (var grouping in groupings) {
        //       // grouping.Key not used; it's included in the grouped tuples and may be clearer if used there 

        //       foreach (Tuple<int, string> item in grouping) {
        //           if (!output.Contains(item.Item2)) /* it turns out that many strings are repeated */ {
        //               output += string.Format("{0}: {1}", item.Item1, item.Item2);
        //               output += Environment.NewLine;
        //           }
        //       }
        //   }


        //   System.Windows.Forms.MessageBox.Show(output);
        //   Assert.IsTrue(true);   // there's no `Assert.Pass()` 
        //}

        #endregion Disabled Friendlies

        #endregion Friendlies


        #region Friendlies_Tests

        #region IsAnOddNumber()

        [TestMethod()]
        public void IsAnOddNumber_Test__Odd_Number_Input__Assert_True()  /* working */  {
            //**  groundwork  **//
            int target = 709;

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = this.IsAnOddNumber(target);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IsAnOddNumber_Test__Even_Number_Input__Assert_False()  /* working */  {
            //**  groundwork  **//
            int target = 708;

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = this.IsAnOddNumber(target);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion IsAnOddNumber()


        #region BuildHashTableWithArrayOfQuadruples()

        [TestMethod()]
        public void BuildHashTableWithArrayOfQuadruples_Test__Input_Size_10__Assert_8_Nulls_At_Even_Indices()  /* working */  {
            //**  groundwork  **//
            /* none */


            //**  exercising the code  **//
            HashTable_<string, string> hashTable = this.BuildHashTableWithArrayOfQuadruples(10);
            PrivateObject pHash = new PrivateObject(hashTable);


            //**  testing  **//
            for (int ix = 0; ix < 16; ix += 2) {
                object slotContent = pHash.GetArrayElement("_table", ix);
                Assert.AreEqual(null, slotContent);
            }
        }

        [TestMethod()]
        public void BuildHashTableWithArrayOfQuadruples_Test__Input_Size_10__Assert_8_Strings_As_Indexed_At_Odd_Indices()  /* working */  {
            //**  groundwork  **//
            PrivateObject pStringStringQuadReference = this.PrivateStringStringQuadruple("key", "value");
            PrivateObject pStringStringQuadSlotContent = this.PrivateStringStringQuadruple("key", "value");


            //**  exercising the code  **//
            HashTable_<string, string> hashTable = this.BuildHashTableWithArrayOfQuadruples(10);
            PrivateObject pHash = new PrivateObject(hashTable);


            //**  testing  **//
            for (int ix = 1; ix < 16; ix += 2) {
                pStringStringQuadSlotContent.Target = pHash.GetArrayElement("_table", ix);

                // slot contains a quadruple rather than any other object (which can be assigned to a PrivateObject even though a different type) 
                Assert.AreEqual(pStringStringQuadReference.Target.GetType(), pStringStringQuadSlotContent.Target.GetType());

                string key = (string)pStringStringQuadSlotContent.GetFieldOrProperty("Key");
                string value = (string)pStringStringQuadSlotContent.GetFieldOrProperty("Value");

                Assert.AreEqual(ix.ToString(), key);
                Assert.AreEqual(ix.ToString(), value);
            }
        }

        [TestMethod()]
        public void BuildHashTableWithArrayOfQuadruples_Test__Three_Entries__Assert_Expected_Entries_Remainder_Null()  /* working */  {
            //**  groundwork  **//
            PrivateObject pStringStringQuadReference = this.PrivateStringStringQuadruple("key", "value");
            PrivateObject pStringStringQuadSlotContent = this.PrivateStringStringQuadruple("key", "value");

            TableElementStringString one = new TableElementStringString("1", "one", 1, SlotState.Full);
            TableElementStringString two = new TableElementStringString("2", "two", 5, SlotState.Tombstone);
            TableElementStringString three = new TableElementStringString("3", "three", 10, SlotState.Full);

            TableElementStringString[] entries = { one, two, three };
            int[] usedSlots = { one.Slot, two.Slot, three.Slot };


            //**  exercising the code  **//
            HashTable_<string, string> hashTable = this.BuildHashTableWithGivenElements(10, one, two, three);
            PrivateObject pHash = new PrivateObject(hashTable);


            //**  testing  **//
            for (int ix = 0, entry = 0 /* see comment at end of loop */ ; ix < hashTable.Size; ix++) {
                object target = pHash.GetArrayElement("_table", ix);

                if (usedSlots.Contains(ix)) {
                    // you can't set a PrivateObject's .Target to a null 
                    if (target != null) {
                        pStringStringQuadSlotContent.Target = target;
                    }

                    // slot contains a quadruple rather than any other object (which can be assigned to a PrivateObject even though a different type) 
                    Assert.AreEqual(pStringStringQuadReference.Target.GetType(), pStringStringQuadSlotContent.Target.GetType());

                    string key = (string)pStringStringQuadSlotContent.GetFieldOrProperty("Key");
                    string value = (string)pStringStringQuadSlotContent.GetFieldOrProperty("Value");

                    Assert.AreEqual(entries[entry].Key, key);
                    Assert.AreEqual(entries[entry].Value, value);

                    /* entries have to be counted separately from array elements */
                    entry++;
                }
                else {
                    // all slots that aren't one of the 3 I've set should be completely empty 
                    Assert.IsNull(target);
                }
            }
        }

        #endregion BuildHashTableWithArrayOfQuadruples()


        #region BuildEmptyHashTableHalfFullHalfOfThoseTombstones()

        [TestMethod()]
        public void BuildEmptyHashTableHalfFullHalfOfThoseTombstones_Test__Array_Size_256__Five_Asserts__As_Remarked()  /* working */  {
            /* 5 asserts: expected size; expected non-empty elements; expected empty elements; expected tombstones; expected fulls */
            //**  groundwork  **//
            int[] filledSlots = new int[128];     // half of slots full 
            int[] emptySlots = new int[128];      // half of slots empty, each 1 more than a full slot 
            int[] tombstoneSlots = new int[64];   // one quarter of slots full but tombstones 
            int[] liveSlots = new int[64];   // the other quarter of slots 

            /* offsets for each full and empty slot */
            for (int ix = 0, next = 0; ix < 256; ix += 2, next++) {
                filledSlots[next] = ix;
                emptySlots[next] = ix + 1;
            }

            /* every other full slot is a tombstone, when its offset ÷2 is an odd number */
            for (int ix = 0, next = 0; ix < 256; ix += 2) {
                if ((ix % 2) != 0) {
                    tombstoneSlots[next] = ix;
                    next++;   // only incremented after each odd number, so matched pairs of tombstones and lives 
                }
                else {
                    liveSlots[next] = ix;
                }
            }

            PrivateObject pQuad = this.PrivateIntIntQuadruple(int.MinValue, int.MinValue);

            int expectedArraySize = 256;   // next power of 2 from 225 (size arg) 
            int actualArraySize;

            int expectedFilledsCount = 128;
            int expectedEmptiesCount = 128;
            int expectedTombstonesCount = 64;
            int expectedLivesCount = 64;

            int actualFilledsCount = 0;
            int actualEmptiesCount = 0;
            int actualTombstonesCount = 0;
            int actualLivesCount = 0;


            //**  exercising the code  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(225);
            PrivateObject pHash = new PrivateObject(target);


            //**  testing  **//
            actualArraySize = target.Size;
            Assert.AreEqual(expectedArraySize, actualArraySize);


            /* gathering counts for other asserts */
            for (int offset = 0; offset < 256; offset++) {
                object element = pHash.GetArrayElement("_table", offset);

                if (element == null) {
                    actualEmptiesCount++;
                }

                if (element != null) {
                    actualFilledsCount++;

                    pQuad.Target = element;

                    SlotState state = (SlotState)pQuad.GetFieldOrProperty("State");

                    if (state != SlotState.Tombstone) {
                        actualLivesCount++;
                    }
                    else {
                        actualTombstonesCount++;
                    }
                }
            }

            /* by-type asserts */
            Assert.AreEqual(expectedFilledsCount, actualFilledsCount);
            Assert.AreEqual(expectedEmptiesCount, actualEmptiesCount);
            Assert.AreEqual(expectedLivesCount, actualLivesCount);
            Assert.AreEqual(expectedTombstonesCount, actualTombstonesCount);

        }

        #endregion BuildEmptyHashTableHalfFullHalfOfThoseTombstones()

        #endregion Friendlies_Tests


        //**  *** *** *** friendlies above, code below *** *** ***  **//


        #region GetProbeHash() vs. GetHashCode()

        [TestMethod()]
        public void GetProbeHash_Test__Variety_Of_Inputs__Assert_Hashes_Are_Different()  /* working */  {
            //**  groundwork  **//
            string targetString = "target";
            int targetInt = 707070707;
            long targetLong = 505050505050505;
            char targetChar = 't';

            object[] targets = new object[] { targetString, targetInt, targetLong, targetChar };

            int expected;
            int actual;


            /* looped for easier testing */
            for (int ix = 0; ix < 4; ix++) {


                //**  exercising the code  **//
                expected = targets[ix].GetHashCode();
                actual = targets[ix].GetProbeHash();


                //**  testing  **//
                Assert.AreNotEqual(expected, actual);

            }
        }

        #endregion GetProbeHash() vs. GetHashCode()


        #region Internal Methods

        #region MakeHashOddNumber()

        [TestMethod()]
        public void MakeHashOddNumber_Test__Even_Hash_Input__Assert_Odd_Output()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(100, "fail value");   // needed only to allow invoking private method 
            PrivateObject p = new PrivateObject(target);

            /*  although the tested method is for hash codes, actually hashing a value is not necessary; 
             *  I'm using a real hash code, altered for the test's needs, for its length  */

            int subject = 1236128812;

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            int result = (int)p.Invoke("MakeHashOddNumber", subject);


            //**  testing  **//
            actual = this.IsAnOddNumber(result);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MakeHashOddNumber_Test__Odd_Hash_Input__Assert_Odd_Output()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(100, "fail value");   // needed only to allow invoking private method 
            PrivateObject p = new PrivateObject(target);

            /*  although the tested method is for hash codes, actually hashing a value is not necessary; 
             *  I'm using a real hash code, altered for the test's needs, for its length  */

            int subject = 1236128815;

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            int result = (int)p.Invoke("MakeHashOddNumber", subject);


            //**  testing  **//
            actual = this.IsAnOddNumber(result);
            Assert.AreEqual(expected, actual);
        }

        #endregion MakeHashOddNumber()


        #region SlotIsAvailable()

        [TestMethod()]
        public void SlotIsAvailable_Test__Slot_As_Inited__Assert_Returns_True()  /* working */  {
            /* nothing has been put into the hash table, so any targeted slot should be null, and therefore usable */

            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(10, 0);
            PrivateObject pHash = new PrivateObject(target);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = (bool)pHash.Invoke("SlotIsAvailable", 6);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotIsAvailable_Test__Slot_Is_Null__Assert_Returns_True()  /* working */  {
            /* nothing has been put into the hash table, so any targeted slot should be null, and therefore usable */

            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(10, 0);
            PrivateObject pHash = new PrivateObject(target);

            pHash.SetArrayElement("_table", null, 6);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = (bool)pHash.Invoke("SlotIsAvailable", 6);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        /* internal, private object raw syntax here, with using a private property of the enclosing class */
        public void SlotIsAvailable_Test__Slot_Is_Tombstone__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(10, 0);
            PrivateObject pHash = new PrivateObject(target);

            /*  the following disabled lines are needed to use a type that's private to another, with that internal, private object's 
             *  reflected metadata coming from a private property in the enclosing class, now disabled; I've moved this stuff to a friendly, 
             *  and I've switched to using literals that I originally got using the private property and the reflection here */
            {
                //string assemblyFullName = typeof(HashTable_<int, int>).Assembly.FullName;
                //string classFullName = (string)pHash.GetFieldOrProperty("QuadrupleFullName");

                //PrivateObject pQuadruple = new PrivateObject(assemblyFullName, classFullName, 1, 1);
            }

            PrivateObject pQuadruple = this.PrivateIntIntQuadruple(1, 1);
            pQuadruple.SetFieldOrProperty("State", SlotState.Tombstone);   // default .State after init is .Full 

            pHash.SetArrayElement("_table", pQuadruple.Target, 6);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = (bool)pHash.Invoke("SlotIsAvailable", 6);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotIsAvailable_Test__Slot_Is_Full__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(10, 0);
            PrivateObject pHash = new PrivateObject(target);

            PrivateObject pQuadruple = this.PrivateIntIntQuadruple(1, 1);
            pHash.SetArrayElement("_table", pQuadruple.Target, 6);


            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = (bool)pHash.Invoke("SlotIsAvailable", 6);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion SlotIsAvailable()


        #region SlotResult()

        [TestMethod()]
        public void SlotResult_Test__Slot_As_Inited__Assert_Returns_Enum_Failed()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(10, "fail");
            PrivateObject pHash = new PrivateObject(target);

            string key = "key";


            SearchResult expected = SearchResult.Failed;
            SearchResult actual;


            //**  exercising the code  **//
            actual = (SearchResult)pHash.Invoke("SlotResult", 7, key);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotResult_Test__Slot_Is_Null__Assert_Returns_Enum_Failed()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(10, "fail");
            PrivateObject pHash = new PrivateObject(target);

            string key = "key";

            pHash.SetArrayElement("_table", null, 7);


            SearchResult expected = SearchResult.Failed;
            SearchResult actual;


            //**  exercising the code  **//
            actual = (SearchResult)pHash.Invoke("SlotResult", 7, key);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotResult_Test__Slot_Has_Same_Key__Assert_Returns_Enum_Succeeded()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(10, "fail");
            PrivateObject pHash = new PrivateObject(target);

            string key = "key";
            string value = "value";

            PrivateObject pQuadruple = this.PrivateStringStringQuadruple(key, value);
            pHash.SetArrayElement("_table", pQuadruple.Target, 7);


            SearchResult expected = SearchResult.Succeeded;
            SearchResult actual;


            //**  exercising the code  **//
            actual = (SearchResult)pHash.Invoke("SlotResult", 7, key);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotResult_Test__Slot_Is_Tombstone__Assert_Returns_Enum_Continue()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(10, "fail");
            PrivateObject pHash = new PrivateObject(target);

            string key = "key";
            string value = "value";

            /*  matching key but tombstone, so should be a .Continue */
            PrivateObject pQuadruple = this.PrivateStringStringQuadruple(key, value);
            pQuadruple.SetFieldOrProperty("State", SlotState.Tombstone);

            pHash.SetArrayElement("_table", pQuadruple.Target, 7);


            SearchResult expected = SearchResult.Continue;
            SearchResult actual;


            //**  exercising the code  **//
            actual = (SearchResult)pHash.Invoke("SlotResult", 7, key);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SlotResult_Test__Slot_Has_Different_Key__Assert_Returns_Enum_Continue()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(10, "fail");
            PrivateObject pHash = new PrivateObject(target);

            string sought = "key";
            string key = "different";
            string value = "value";

            PrivateObject pQuadruple = this.PrivateStringStringQuadruple(key, value);
            pHash.SetArrayElement("_table", pQuadruple.Target, 7);


            SearchResult expected = SearchResult.Continue;
            SearchResult actual;


            //**  exercising the code  **//
            actual = (SearchResult)pHash.Invoke("SlotResult", 7, sought);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion SlotResult()


        #region SearchForKey()

        [TestMethod()]
        public void SearchForKey_Test__At_Home_Position__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;

            // quadruples not needed, as those are created from the entries in the friendly 
            TableElementStringString homePositionElement = new TableElementStringString(key, value, homePosition, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, homePositionElement);   // 512 slots 
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = homePosition;
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);   // no casting necessary, as .Target is Object (but never null) 


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__At_First_Probe_Slot__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }


            TableElementStringString homePositionElement 
                = new TableElementStringString(different, value, homePosition, SlotState.Full);
            TableElementStringString firstProbePositionElement 
                = new TableElementStringString(key, value, (homePosition + offset) % 512, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, homePositionElement, firstProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = (homePosition + offset) % 512;   // first slot in probe sequence 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);   // no casting necessary, as .Target is Object (but never an assigned null) 


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__At_Second_Probe_Slot_Home_Different_First_Probe_Tombstone__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            // % 512 must be applied at every step to keep values in table's range 
            int probePosition1 = (homePosition + offset) % 512;
            int probePosition2 = (probePosition1 + offset) % 512;

            TableElementStringString homePositionElement = new TableElementStringString(different, value, homePosition, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(key, value, probePosition1, SlotState.Tombstone);
            TableElementStringString secondProbePositionElement = new TableElementStringString(key, value, probePosition2, SlotState.Full);


            HashTable_<string, string> target 
                = this.BuildHashTableWithGivenElements(500, homePositionElement, firstProbePositionElement, secondProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = (homePosition + offset + offset) % 512;   // second slot in probe sequence (0) 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__At_Fourth_Probe_Slot_All_Priors_Tombstones__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Tombstone);
            TableElementStringString firstProbePositionElement = new TableElementStringString(key, value, 249, SlotState.Tombstone);
            TableElementStringString secondProbePositionElement = new TableElementStringString(key, value, 84, SlotState.Tombstone);
            TableElementStringString thirdProbePositionElement = new TableElementStringString(key, value, 431, SlotState.Tombstone);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(key, value, 266, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                thirdProbePositionElement,
                fourthProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = 266;   // fourth slot in probe sequence 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__No_Matching_Key__Assert_Result_Failed_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            /* a mix of tombstones and fulls; a matching key at a tombstone just in case */
            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(different, value, 249, SlotState.Tombstone);
            TableElementStringString secondProbePositionElement = new TableElementStringString(different, value, 84, SlotState.Full);
            TableElementStringString thirdProbePositionElement = new TableElementStringString(key, value, 431, SlotState.Tombstone);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(different, value, 266, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                thirdProbePositionElement,
                fourthProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Failed;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__No_Matching_Key_All_Slots_Used__Assert_Result_Failed_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /* all elements full and different (not the sought key) */
            TableElementStringString[] elements = new TableElementStringString[512];

            for (int ix = 0; ix < 512; ix++) {
                TableElementStringString currentElement = new TableElementStringString(different, value, ix, SlotState.Full);
                elements[ix] = currentElement;
            }

            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, elements);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Failed;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForKey_Test__At_Fourth_Probe_Slot_Empty_Prior__Assert_Result_Failed_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Tombstone);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(key, value, 266, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, homePositionElement, fourthProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Failed;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForKey", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        #endregion SearchForKey()


        #region SearchForAvailableSlot()

        [TestMethod()]
        public void SearchForAvailableSlot_Test__No_Matching_Key_All_Slots_Used__Assert_Result_Impossible_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /* all elements full and different (not the sought key) */
            TableElementStringString[] elements = new TableElementStringString[512];

            for (int ix = 0; ix < 512; ix++) {
                TableElementStringString currentElement = new TableElementStringString(different, value, ix, SlotState.Full);
                elements[ix] = currentElement;
            }

            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, elements);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Impossible;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Repeated_Key_Home_Position__Assert_Result_Repeated_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(key, value, 414, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(different, value, 249, SlotState.Tombstone);
            TableElementStringString secondProbePositionElement = new TableElementStringString(different, value, 84, SlotState.Full);
            TableElementStringString thirdProbePositionElement = new TableElementStringString(different, value, 431, SlotState.Tombstone);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(different, value, 266, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                thirdProbePositionElement,
                fourthProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Repeated;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Repeated_Key_At_Fourth_Probe_Slot_Full_Priors__Assert_Result_Repeated_And_None_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(different, value, 249, SlotState.Full);
            TableElementStringString secondProbePositionElement = new TableElementStringString(different, value, 84, SlotState.Full);
            TableElementStringString thirdProbePositionElement = new TableElementStringString(different, value, 431, SlotState.Full);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(key, value, 266, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(
                500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                thirdProbePositionElement,
                fourthProbePositionElement
                );
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Repeated;
            SearchResult actualHashResult;

            int expectedSlot = -1;   // -1 same as HashTable_<>.None 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Repeated_Key_At_Fourth_Probe_Slot_Empty_Third__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(different, value, 249, SlotState.Full);
            TableElementStringString secondProbePositionElement = new TableElementStringString(different, value, 84, SlotState.Full);
            // no third element 
            TableElementStringString fourthProbePositionElement = new TableElementStringString(key, value, 266, SlotState.Full);

            /* no third element added, so that should be available */
            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(
                500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                fourthProbePositionElement
                );
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = 431;   // third probe slot 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            /* since the home position has a different key, the result and slot should be at the first slot in the probe sequence */
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Empty_Home_Position__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;

            /* no elements added to the table, so home position should be free */

            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500);   // 512 slots 
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = homePosition;
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);   // no casting necessary, as .Target is Object (but never null) 


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Empty_First_Probe_Slot__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }


            TableElementStringString homePositionElement = new TableElementStringString(different, value, homePosition, SlotState.Full);

            /* table is empty except for home position, so first probe slot should be free */
            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, homePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = (homePosition + offset) % 512;   // first slot in probe sequence, with % of table size 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);   // no casting necessary, as .Target is Object (but never an assigned null) 


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Home_Different_First_Probe_Tombstone__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            // % 512 must be applied at every step to keep values in table's range 
            int probePosition1 = (homePosition + offset) % 512;
            int probePosition2 = (probePosition1 + offset) % 512;

            TableElementStringString homePositionElement = new TableElementStringString(different, value, homePosition, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(key, value, probePosition1, SlotState.Tombstone);
            TableElementStringString secondProbePositionElement = new TableElementStringString(key, value, probePosition2, SlotState.Full);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500, homePositionElement, firstProbePositionElement, secondProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = (homePosition + offset) % 512;   // first slot in probe sequence 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        [TestMethod()]
        public void SearchForAvailableSlot_Test__Tombstone_Fourth_Probe_Slot_All_Priors_Full__Assert_Result_Succeeded_And_Expected_Slot()  /* working */  {
            //**  groundwork  **//
            string key = "key";
            string value = "value";
            string different = "different";

            int homePosition = Math.Abs(key.GetHashCode()) % 512;
            int offset = Math.Abs(key.GetProbeHash()) % 512;

            /* offsets must always be odd numbers to be relatively prime, in the implementation I'm using */
            if ((offset % 2) == 0) {
                offset++;
            }

            /*  home: 414            = 414 ; 
             *  1st: (1st +347) %512 = 249 ; 
             *  2nd: (2nd +347) %512 = 84  ; 
             *  3rd: (3rd +347) %512 = 431 ; 
             *  4th: (4th +347) %512 = 266 ; 
             *  */

            TableElementStringString homePositionElement = new TableElementStringString(different, value, 414, SlotState.Full);
            TableElementStringString firstProbePositionElement = new TableElementStringString(different, value, 249, SlotState.Full);
            TableElementStringString secondProbePositionElement = new TableElementStringString(different, value, 84, SlotState.Full);
            TableElementStringString thirdProbePositionElement = new TableElementStringString(different, value, 431, SlotState.Full);
            TableElementStringString fourthProbePositionElement = new TableElementStringString(different, value, 266, SlotState.Tombstone);


            HashTable_<string, string> target = this.BuildHashTableWithGivenElements(500,
                homePositionElement,
                firstProbePositionElement,
                secondProbePositionElement,
                thirdProbePositionElement,
                fourthProbePositionElement);
            PrivateObject p = new PrivateObject(target);

            PrivateObject dupleResult = this.PrivateStringStringEmptyDuple();


            SearchResult expectedHashResult = SearchResult.Succeeded;
            SearchResult actualHashResult;

            int expectedSlot = 266;   // fourth slot in probe sequence 
            int actualSlot;


            //**  exercising the code  **//
            dupleResult.Target = p.Invoke("SearchForAvailableSlot", key);


            //**  testing  **//
            actualHashResult = (SearchResult)dupleResult.GetFieldOrProperty("Result");
            actualSlot = (int)dupleResult.GetFieldOrProperty("Slot");

            Assert.AreEqual(expectedHashResult, actualHashResult);
            Assert.AreEqual(expectedSlot, actualSlot);
        }

        #endregion SearchForAvailableSlot()


        #region Rehash()

        [TestMethod()]
        public void Rehash_Test__Size_Of_512__Assert_Rehashed_Original_HashTable_Has_Expected_New_Size()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(256);
            this.AssignKeysAndTombstonesCount(target, 64, 64);   // original is half full, so 128 total, half keys and half tombstones, so 64 of each 
            PrivateObject p = new PrivateObject(target);

            int newSize = 512;
            int expected = 512;
            int actual;


            //**  exercising the code  **//
            p.Invoke("Rehash", newSize);


            //**  testing  **//
            /* the existing object has been given a different backing array of the new size, so .Size should now be the new value  */
            actual = target.Size;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Rehash_Test__Known_Counts__Two_Asserts__Rehashed_Expected_Number_Of_Non_Nulls_One_Metadata_One_Observed()  /* working */  {
            /*  this test checks for the number of elements copied, replacing the default nulls; 
             *  some of these could in theory be tombstones, which is checked separately  */

            //**  groundwork  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(256);
            this.AssignKeysAndTombstonesCount(target, 64, 64);   // original is half full, so 128 total, half keys and half tombstones, so 64 of each 
            PrivateObject p = new PrivateObject(target);

            int newSize = 512;
            int expected = 64;   // same number of keys in rehashed table 


            int actualFromMetadata = 0;
            int actualFromObservation = 0;


            //**  exercising the code  **//
            p.Invoke("Rehash", newSize);


            //**  testing  **//
            actualFromMetadata = (int)p.GetField("_keys");
            Assert.AreEqual(expected, actualFromMetadata);

            for (int ix = 0; ix < newSize; ix++) {
                object element = p.GetArrayElement("_table", ix);
                if (element != null) {
                    actualFromObservation++;
                }
            }

            Assert.AreEqual(expected, actualFromObservation);
        }

        [TestMethod()]
        public void Rehash_Test__Known_Counts__Assert_Rehashed_Has_No_Tombstones()  /* working */  {
            /* this test checks for the number of tombstones, which should be zero */

            //**  groundwork  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(256);
            this.AssignKeysAndTombstonesCount(target, 64, 64);   // original is half full, so 128 total, half keys and half tombstones, so 64 of each 
            PrivateObject pHash = new PrivateObject(target);

            PrivateObject pQuad = this.PrivateIntIntQuadruple(int.MaxValue, int.MaxValue);

            int newSize = 512;


            bool expectedAnyTombstonesPresent = false;
            bool actualAnyTombstonesPresent = true;   // anti-default 


            //**  exercising the code  **//
            pHash.Invoke("Rehash", newSize);


            //**  testing  **//
            for (int ix = 0; ix < newSize; ix++) {
                object element = pHash.GetArrayElement("_table", ix);
                if (element != null) {
                    pQuad.Target = element;

                    SlotState state = (SlotState)pQuad.GetFieldOrProperty("State");
                    actualAnyTombstonesPresent = (state == SlotState.Tombstone);

                    // stop at any first tombstone; even one indicates that rehashing ops are wrong 
                    if (actualAnyTombstonesPresent) {
                        break;
                    }
                }
            }

            Assert.AreEqual(expectedAnyTombstonesPresent, actualAnyTombstonesPresent);
        }

        [TestMethod()]
        public void Rehash_Test__Known_Counts__Assert_Rehashed_Has_All_Keys()  /* working */  {
            /* this test checks for the number of full / non-tombstone values, which should be 64; if so, all keys correctly copied */

            //**  groundwork  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(256);
            this.AssignKeysAndTombstonesCount(target, 64, 64);   // original is half full, so 128 total, half keys and half tombstones, so 64 of each 
            PrivateObject pHash = new PrivateObject(target);

            PrivateObject pQuad = this.PrivateIntIntQuadruple(int.MaxValue, int.MaxValue);

            int newSize = 512;


            bool expectedAllAreKeys = true;
            bool actualAllAreKeys = false;   // anti-default 


            //**  exercising the code  **//
            pHash.Invoke("Rehash", newSize);


            //**  testing  **//
            for (int ix = 0; ix < newSize; ix++) {
                object element = pHash.GetArrayElement("_table", ix);
                if (element != null) {
                    pQuad.Target = element;

                    SlotState state = (SlotState)pQuad.GetFieldOrProperty("State");
                    actualAllAreKeys = (state == SlotState.Full);

                    // stop at any first non-full; even one indicates that rehashing ops are wrong 
                    if (!actualAllAreKeys) {
                        break;
                    }
                }
            }

            Assert.AreEqual(expectedAllAreKeys, actualAllAreKeys);
        }

        [TestMethod()]
        public void Rehash_Test__Known_Counts__Assert_Rehashed_Has_Expected_Keys()  /* working */  {
            /* this test checks for the number of full / non-tombstone values, which should be 64; if so, all keys correctly copied */

            //**  groundwork  **//
            HashTable_<int, int> target = this.BuildEmptyHashTableHalfFullHalfOfThoseTombstones(256);
            this.AssignKeysAndTombstonesCount(target, 64, 64);   // original is half full, so 128 total, half keys and half tombstones, so 64 of each 
            PrivateObject pHash = new PrivateObject(target);

            PrivateObject pQuad = this.PrivateIntIntQuadruple(int.MaxValue, int.MaxValue);

            /* get keys to compare against */
            int[] passer = new int[64];

            for (int ix = 0, next = 0; ix < target.Size; ix++) {
                object subject = pHash.GetArrayElement("_table", ix);

                if (subject != null) {
                    pQuad.Target = subject;

                    if ((SlotState)pQuad.GetFieldOrProperty("State") == SlotState.Full) {
                        passer[next] = (int)pQuad.GetFieldOrProperty("Key");
                        next++;
                    }
                }

            }

            int newSize = 512;


            int[] expectedKeys = passer;
            int[] actualKeys = new int[64];   // all are 0 after init 


            //**  exercising the code  **//
            pHash.Invoke("Rehash", newSize);


            //**  testing  **//
            for (int ix = 0, next = 0; ix < newSize; ix++) {
                object element = pHash.GetArrayElement("_table", ix);
                if (element != null) {
                    pQuad.Target = element;
                    actualKeys[next] = (int)pQuad.GetFieldOrProperty("Key");
                    next++;
                }
            }

            /* rehashed keys not guaranteed to be in order; doing both as precaution */
            SortingAlgorithms_<int>.Sort_Quick_Insertion_(expectedKeys);
            SortingAlgorithms_<int>.Sort_Quick_Insertion_(actualKeys);

            /* the actual test */
            CollectionAssert.AreEqual(expectedKeys, actualKeys);
        }

        #endregion Rehash()


        #region AnyRehashNow() ; Testing Disabled ; See Remarks Next In Expansion

        //**  ***  these are disabled by commenting out the test-method attribute   ***  **//
        //**  ***  so they don't run automatically without preceding groundwork;    ***  **//
        //**  ***  these *must* be enabled again and used if anything connected to  ***  **//
        //**  ***  rehashing, or starting it, changes at all;                       ***  **//
        //**  ***  to run these again, elements in .AnyRehashNow() and .Rehash(),   ***  **//
        //**  ***  the field _rehashBegun, and property .RehashBegun must be        ***  **//
        //**  ***  enabled again in HashTable_<>                                    ***  **//

        //[TestMethod()]
        public void AnyRehashNow_Test__Metadata_Zero_Keys_Zero_Tombstones__Not_Rehashed___Test_Only_Flag_False()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(512, int.MaxValue);
            PrivateObject p = new PrivateObject(target);

            /*  only metadata counts need to be changed as long as actual rehashing is disabled; it will fail if mismatched counts; 
             *  in real use, metadata must match, and should be maintained correctly by .Insert() and .Delete()  */
            this.AssignKeysAndTombstonesCount(target, 0, 0);


            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            p.Invoke("AnyRehashNow");


            //**  testing  **//
            actual = (bool)p.GetProperty("RehashBegun");
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        public void AnyRehashNow_Test__Metadata_Zero_Keys_Nonzero_Tombstones__Not_Rehashed___Test_Only_Flag_False()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(512, int.MaxValue);
            PrivateObject p = new PrivateObject(target);

            /*  only metadata counts need to be changed as long as actual rehashing is disabled; it will fail if mismatched counts; 
             *  in real use, metadata must match, and should be maintained correctly by .Insert() and .Delete()  */
            this.AssignKeysAndTombstonesCount(target, 0, 100);


            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            p.Invoke("AnyRehashNow");


            //**  testing  **//
            actual = (bool)p.GetProperty("RehashBegun");
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        public void AnyRehashNow_Test__Metadata_Below_Half_Keys_Zero_Tombstones__Not_Rehashed___Test_Only_Flag_False()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(512, int.MaxValue);
            PrivateObject p = new PrivateObject(target);

            /*  only metadata counts need to be changed as long as actual rehashing is disabled; it will fail if mismatched counts; 
             *  in real use, metadata must match, and should be maintained correctly by .Insert() and .Delete()  */
            this.AssignKeysAndTombstonesCount(target, 200, 0);


            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            p.Invoke("AnyRehashNow");


            //**  testing  **//
            actual = (bool)p.GetProperty("RehashBegun");
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        public void AnyRehashNow_Test__Metadata_Below_Half_Keys_But_Above_Half_Proportion_Tombstones__Rehashed___Test_Only_Flag_True()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(512, int.MaxValue);
            PrivateObject p = new PrivateObject(target);

            /*  only metadata counts need to be changed as long as actual rehashing is disabled; it will fail if mismatched counts; 
             *  in real use, metadata must match, and should be maintained correctly by .Insert() and .Delete()  */
            this.AssignKeysAndTombstonesCount(target, 100, 75);


            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            p.Invoke("AnyRehashNow");


            //**  testing  **//
            actual = (bool)p.GetProperty("RehashBegun");
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        public void AnyRehashNow_Test__Metadata_Above_Half_Keys_Zero_Tombstones__Rehashed___Test_Only_Flag_True()  /* working */  {
            //**  groundwork  **//
            HashTable_<int, int> target = new HashTable_<int, int>(512, int.MaxValue);
            PrivateObject p = new PrivateObject(target);

            /*  only metadata counts need to be changed as long as actual rehashing is disabled; it will fail if mismatched counts; 
             *  in real use, metadata must match, and should be maintained correctly by .Insert() and .Delete()  */
            this.AssignKeysAndTombstonesCount(target, 400, 0);


            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            p.Invoke("AnyRehashNow");


            //**  testing  **//
            actual = (bool)p.GetProperty("RehashBegun");
            Assert.AreEqual(expected, actual);
        }

        #endregion AnyRehashNow() ; Testing Disabled ; See Remarks Next In Expansion

        #endregion Internal Methods


        #region Insert()

        [TestMethod()]
        public void Insert_Test__All_New_Key__Assert_Succeeded_Flag()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 

            SearchResult expected = SearchResult.Succeeded;
            SearchResult actual;


            //**  exercising the code  **//
            actual = target.Insert("new key", "new value");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Insert_Test__Several_All_New_Keys__Assert_Succeeded_Flags_Every_Time()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 

            SearchResult local;


            bool expectedAllAreSucceeded = true;
            bool actualAllAreSucceeded = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < 4; ix++) {
                local = target.Insert(string.Format("new key {0}", ix + 1), string.Format("new value {0}", ix + 1));

                actualAllAreSucceeded = (local == SearchResult.Succeeded);

                // one not .Succeeded is a fail 
                if (!actualAllAreSucceeded) {
                    break;
                }
            }


            //**  testing  **//
            Assert.AreEqual(expectedAllAreSucceeded, actualAllAreSucceeded);
        }

        [TestMethod()]
        public void Insert_Test__Several_All_New_Keys__Assert_Correct_Keys_Count_Every_Time()  /* working */  {
            /* not testing contents of array directly, but instead crucial metadata */

            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 
            PrivateObject p = new PrivateObject(target);

            int local = 0;


            bool expectedAllKeyCountsAreCorrect = true;
            bool actualAllKeyCountsAreCorrect = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < 4; ix++) {
                target.Insert(string.Format("new key {0}", ix + 1), string.Format("new value {0}", ix + 1));

                local = (int)p.GetField("_keys");

                actualAllKeyCountsAreCorrect = (local == (ix + 1));

                // one not .Succeeded is a fail 
                if (!actualAllKeyCountsAreCorrect) {
                    break;
                }
            }


            //**  testing  **//
            Assert.AreEqual(expectedAllKeyCountsAreCorrect, actualAllKeyCountsAreCorrect);
        }

        [TestMethod()]
        public void Insert_Test__Several_All_New_Keys_Same_Hash_Code__Assert_Succeeded_Flags_Every_Time()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 

            /* these all have a hash value of 7 in a table of 8 slots, as do some others */
            string[] keys = { "fsg", "ivj", "nbo", "qer" };

            SearchResult local;

            bool expectedAllAreSucceeded = true;
            bool actualAllAreSucceeded = false;



            //**  exercising the code  **//
            for (int ix = 0; ix < 4; ix++) {
                local = target.Insert(keys[ix], string.Format("new value {0}", ix + 1));

                actualAllAreSucceeded = (local == SearchResult.Succeeded);

                // one not .Succeeded is a fail 
                if (!actualAllAreSucceeded) {
                    break;
                }
            }


            //**  testing  **//
            Assert.AreEqual(expectedAllAreSucceeded, actualAllAreSucceeded);
        }

        [TestMethod()]
        public void Insert_Test__One_Repeated_Key__Assert_Repeated_Flag()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(string.Format("new key {0}", ix + 1), string.Format("new value {0}", ix + 1));
            }

            SearchResult expected = SearchResult.Repeated;
            SearchResult actual;


            //**  exercising the code  **//
            actual = target.Insert("new key 2", "new value 2");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Insert_Test__More_Than_Half_All_New_Keys__Assert_Table_Rehashed_Resized_After_Last()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 
            PrivateObject p = new PrivateObject(target);

            int expectedSize = 16;
            int actualSize = 0;


            //**  exercising the code  **//
            for (int ix = 0; ix < 6; ix++) {
                /* at the 5th insert, .AnyRehashNow() should call .Rehash(), and size of backing array should be doubled */
                target.Insert(string.Format("new key {0}", ix + 1), string.Format("new value {0}", ix + 1));
            }


            //**  testing  **//
            actualSize = target.Size;
            Assert.AreEqual(expectedSize, actualSize);
        }

        [TestMethod()]
        public void Insert_Test__Several_All_New_Keys_Same_Hash_Code__Assert_Array_Contents_As_Expected()  /* working */  {
            /* checking actual contents, not metadata; should not be necessary, but is reassuring */

            //**  groundwork  **//
            HashTable_<string, string> target = new HashTable_<string, string>(7, string.Empty);   // size is 8 
            PrivateObject p = new PrivateObject(target);

            /* these all have a hash value of 7 in a table of 8 slots, as do some others */
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };


            List<string> expected = subjectKeys.ToList();
            List<string> actual = new List<string>();



            //**  exercising the code  **//
            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], string.Format("new value {0}", ix + 1));
            }


            //**  testing  **//
            /* testing to see if all values are present, order not important */
            for (int ix = 0; ix < target.Size; ix++) {
                object topic = p.GetArrayElement("_table", ix);

                if (topic != null) {
                    PrivateObject pQuad = new PrivateObject(topic);
                    string local = (string)pQuad.GetField("Key");
                    actual.Add(local);
                }
            }

            /* actual has no guaranteed order, and expected wasn't ordered */
            expected.Sort();
            actual.Sort();

            /* the actual test */
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Insert()


        #region Find()

        /* these tests are only valid if .Insert() passes its correct tests, as they rely on it to build table before searching */

        [TestMethod()]
        public void Find_Test__Existing_Key__Assert_Expected_Value_Returned()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MinValue);   // size is 8 
            PrivateObject p = new PrivateObject(target);

            /* these all have a hash value of 7 in a table of 8 slots, as do some others */
            string[] keys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(keys[ix], ix);
            }

            string sought = "nbo";

            int expected = 2;   // input offset and value for 3rd key 
            int actual;


            //**  exercising the code  **//
            actual = target.Find(sought);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Find_Test__Existing_Keys__Assert_Expected_Access_Counts()  /* working */  {
            /* finding keys different numbers of times, and making sure their access counts match */

            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MinValue);   // size is 8 
            PrivateObject pHash = new PrivateObject(target);

            /* these all have a hash value of 7 in a table of 8 slots, as do some others */
            string[] keys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(keys[ix], ix);
            }

            string[] soughts = { "nbo", "fsg", "qer", "nbo", "fsg", "nbo" };
            int[] expectedCounts = { 3, 2, 1, 0 };   // "nbo", "fsg", "qer", "ivj" 
            int[] actualCounts = new int[4];

            //**  exercising the code  **//
            for (int ix = 0; ix < soughts.Length; ix++) {
                target.Find(soughts[ix]);
            }

            //**  testing  **//
            this.GatherCountsByKeysForAllElements(target, pHash, actualCounts);

            /* the actual test */
            CollectionAssert.AreEqual(expectedCounts, actualCounts);
        }

        #region Local Friendlies

        private void GatherCountsByKeysForAllElements(HashTable_<string, int> target, PrivateObject pHash, int[] actualCounts) {
            /* exfactored */
            for (int ix = 0; ix < target.Size; ix++) {
                object topic = pHash.GetArrayElement("_table", ix);

                if (topic != null) {
                    PrivateObject pQuad = new PrivateObject(topic);
                    this.AssignCountToActualsByKey(actualCounts, pQuad);
                }
            }
        }

        private void AssignCountToActualsByKey(int[] actualCounts, PrivateObject pQuad) {
            /* exfactored */
            string localKey = (string)pQuad.GetFieldOrProperty("Key");
            int localCount = (int)pQuad.GetFieldOrProperty("Count");

            switch (localKey) {
                case "nbo":
                    actualCounts[0] = localCount;
                    break;
                case "fsg":
                    actualCounts[1] = localCount;
                    break;
                case "qer":
                    actualCounts[2] = localCount;
                    break;
                case "ivj":
                    actualCounts[3] = localCount;
                    break;
                default:
                    throw new Exception("Unexpected key found during test.");
            }
        }

        #endregion Local Friendlies

        [TestMethod()]
        public void Find_Test__Missing_Key__Assert_Expected_Fail_Value_Returned()  /* working */  {
            //**  groundwork  **//
            const int FAIL_VALUE = int.MinValue;
            HashTable_<string, int> target = new HashTable_<string, int>(7, FAIL_VALUE);   // size is 8 
            PrivateObject p = new PrivateObject(target);

            /* these all have a hash value of 7 in a table of 8 slots, as do some others */
            string[] keys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(keys[ix], ix);
            }

            string sought = "apd";   // no such key in table 

            int expected = FAIL_VALUE;
            int actual;


            //**  exercising the code  **//
            actual = target.Find(sought);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Find_Test__Some_Tombstones__Assert_Expected_Keys_Are_Found_And_Values_Returned()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(12, int.MinValue);   // size is 16 

            /* offsets:        0      1      2      3      4      5      6 */
            string[] keys = { "fsg", "ivj", "cpd", "nbo", "dqe", "qer", "kxl" };
            string[] tombstones = { "ivj", "nbo", "qer" };

            this.InsertKeysAndConvertSomeToTombstones(target, keys, tombstones);

            string[] soughts = { "fsg", "cpd", "dqe", "kxl" };
            int[] values = { 0, 2, 4, 6 };


            bool expectedAllFound = true;
            bool actualAllFound = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < soughts.Length; ix++) {
                int found = target.Find(soughts[ix]);

                actualAllFound = (found == values[ix]);

                /* fail test at first `false` */
                if (!actualAllFound) {
                    break;
                }
            }

            //**  testing  **//
            Assert.AreEqual(expectedAllFound, actualAllFound);
        }

        [TestMethod()]
        public void Find_Test__Some_Tombstones__Assert_Tombstones_So_Not_Found_Fail_Value_Returned()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(12, int.MinValue);   // size is 16 

            /* offsets:        0      1      2      3      4      5      6 */
            string[] keys = { "fsg", "ivj", "cpd", "nbo", "dqe", "qer", "kxl" };
            string[] tombstones = { "ivj", "nbo", "qer" };

            this.InsertKeysAndConvertSomeToTombstones(target, keys, tombstones);

            string[] soughts = { "ivj", "nbo", "qer" };
            int failValue = int.MinValue;


            bool expectedNoneFound = true;
            bool actualNoneFound = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < soughts.Length; ix++) {
                int found = target.Find(soughts[ix]);

                actualNoneFound = (found == failValue);

                /* fail test at first `false` */
                if (!actualNoneFound) {
                    break;
                }
            }

            //**  testing  **//
            Assert.AreEqual(expectedNoneFound, actualNoneFound);
        }

        #region Local Friendlies

        private void InsertKeysAndConvertSomeToTombstones(HashTable_<string, int> target, string[] keys, string[] tombstones)  /* verified */  {
            PrivateObject pHash = new PrivateObject(target);

            /* inserting */
            for (int ix = 0; ix < keys.Length; ix++) {
                target.Insert(keys[ix], ix);
            }

            for (int ix = 0; ix < target.Size; ix++) {
                object topic = pHash.GetArrayElement("_table", ix);

                if (topic != null) {
                    PrivateObject pQuad = new PrivateObject(topic);
                    string key = (string)pQuad.GetFieldOrProperty("Key");

                    if (tombstones.Contains(key)) {
                        pQuad.SetFieldOrProperty("State", SlotState.Tombstone);
                    }
                }
            }
        }

        #endregion Local Friendlies

        #endregion Find()


        #region Indexer

        /* indexer uses [] and a key value to get something from the hash table, using Find() internally; nice syntax */

        [TestMethod()]
        public void Indexer_Test__Some_Tombstones__Assert_Expected_Keys_Are_Found_And_Values_Returned()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(12, int.MinValue);   // size is 16 

            /* offsets:        0      1      2      3      4      5      6 */
            string[] keys = { "fsg", "ivj", "cpd", "nbo", "dqe", "qer", "kxl" };
            string[] tombstones = { "ivj", "nbo", "qer" };

            this.InsertKeysAndConvertSomeToTombstones(target, keys, tombstones);

            string[] soughts = { "fsg", "cpd", "dqe", "kxl" };
            int[] values = { 0, 2, 4, 6 };


            bool expectedAllFound = true;
            bool actualAllFound = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < soughts.Length; ix++) {
                string key = soughts[ix];

                /* same as Find(), but easier syntax */
                int found = target[key];

                actualAllFound = (found == values[ix]);

                /* fail test at first `false` */
                if (!actualAllFound) {
                    break;
                }
            }

            //**  testing  **//
            Assert.AreEqual(expectedAllFound, actualAllFound);
        }

        [TestMethod()]
        public void Indexer_Test__Some_Tombstones__Assert_Tombstones_So_Not_Found_Fail_Value_Returned()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(12, int.MinValue);   // size is 16 

            /* offsets:        0      1      2      3      4      5      6 */
            string[] keys = { "fsg", "ivj", "cpd", "nbo", "dqe", "qer", "kxl" };
            string[] tombstones = { "ivj", "nbo", "qer" };

            this.InsertKeysAndConvertSomeToTombstones(target, keys, tombstones);

            string[] soughts = { "ivj", "nbo", "qer" };
            int failValue = int.MinValue;


            bool expectedNoneFound = true;
            bool actualNoneFound = false;


            //**  exercising the code  **//
            for (int ix = 0; ix < soughts.Length; ix++) {
                string key = soughts[ix];

                int found = target[key];

                actualNoneFound = (found == failValue);

                /* fail test at first `false` */
                if (!actualNoneFound) {
                    break;
                }
            }

            //**  testing  **//
            Assert.AreEqual(expectedNoneFound, actualNoneFound);
        }
        #endregion Indexer


        #region Delete()

        [TestMethod()]
        public void Delete_Test__Existing_Key_At_Home_Slot__Assert_Succeeded_Flag()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MaxValue);   // size is 8 

            /* these all have same hash value in 8-slot table; only first ("fsg") will be at home position */
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], ix);
            }

            string keyToDelete = "fsg";


            SearchResult expected = SearchResult.Succeeded;
            SearchResult actual = SearchResult.None;


            //**  exercising the code  **//
            actual = target.Delete(keyToDelete);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Delete_Test__Existing_Key_Along_Probe_Sequence__Assert_Succeeded_Flag()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MaxValue);   // size is 8 

            /* these all have same hash value in 8-slot table; only first ("fsg") will be at home position */
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], ix);
            }

            string keyToDelete = "ivj";


            SearchResult expected = SearchResult.Succeeded;
            SearchResult actual = SearchResult.None;


            //**  exercising the code  **//
            actual = target.Delete(keyToDelete);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Delete_Test__Existing_Key__Assert_Element_Has_Tombstone_State()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MaxValue);
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], ix);
            }

            string keyToDelete = "fsg";


            SlotState expected = SlotState.Tombstone;
            SlotState actual = SlotState.None;


            //**  exercising the code  **//
            target.Delete(keyToDelete);


            //**  testing  **//
            actual = this.GetSlotStateOfDeletedElement(target, keyToDelete);

            /* the actual test */
            Assert.AreEqual(expected, actual);
        }

        #region Local Friendlies

        private SlotState GetSlotStateOfDeletedElement(HashTable_<string, int> target, string subject)  /* verified */  {
            SlotState result = SlotState.None;

            PrivateObject pHash = new PrivateObject(target);

            for (int ix = 0; ix < target.Size; ix++) {
                object topic = pHash.GetArrayElement("_table", ix);

                if (topic != null) {
                    PrivateObject pQuad = new PrivateObject(topic);
                    string key = (string)pQuad.GetField("Key");

                    if (key == subject) {
                        result = (SlotState)pQuad.GetField("State");
                    }
                }
            }

            return result;
        }

        #endregion Local Friendlies

        [TestMethod()]
        public void Delete_Test__Existing_Key__Assert_Expected_Tombstones_Count()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MaxValue);
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], ix);
            }

            string keyToDelete = "ivj";


            int expected = 1;
            int actual;


            //**  exercising the code  **//
            target.Delete(keyToDelete);


            //**  testing  **//
            PrivateObject p = new PrivateObject(target);
            actual = (int)p.GetField("_tombstones");

            /* the actual test */
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Delete_Test__Nonexistent_Key__Assert_Failed_Flag()  /* working */  {
            //**  groundwork  **//
            HashTable_<string, int> target = new HashTable_<string, int>(7, int.MaxValue);
            string[] subjectKeys = { "fsg", "ivj", "nbo", "qer" };

            for (int ix = 0; ix < 4; ix++) {
                target.Insert(subjectKeys[ix], ix);
            }

            string keyToDelete = "pcy";


            SearchResult expected = SearchResult.Failed;
            SearchResult actual = SearchResult.None;


            //**  exercising the code  **//
            actual = target.Delete(keyToDelete);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Delete()

    }
}

/*                                  *** String Hashes Same in 8-Slot Table ***                                  

 * each 3-letter string below has the same hash code as the others in its group, if hashed to an 8-slot table * 

 * these are listed from lowest slot indices / offsets to highest, but some slots are skipped * 


lyn
per
    * 
    * 
boc
jwk
man
uiv
    * 
    * 
gth
hui
ocp
pdq
xly
    * 
    * 
fsh
jxl
vjx
vkx
yma
    * 
    * 
cpd
dqe
kxl
lym
sgt
thu
wkx
    * 
    * 
cpe
mbo
shu
    * 
    * 
anb
erf
fsg
ivj
nbo
qer
rfs
vjq
ymb
ynb
    * 
    * 

 */
