﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class MinHeap_T_Tests
    {
        #region Friendlies

        public static char[] PreHeap()  /* passed */  {
            char[] array = new char[11];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'h';
            array[8] = 'i';
            array[9] = 'j';
            array[10] = 'k';

            return array;
        }

        public static char[] PreHeapLateLetters()  /* ok */  {
            char[] array = new char[11];

            array[0] = 'j';
            array[1] = 'k';
            array[2] = 'l';
            array[3] = 'm';
            array[4] = 'n';
            array[5] = 'o';
            array[6] = 'p';
            array[7] = 'q';
            array[8] = 'r';
            array[9] = 's';
            array[10] = 't';

            return array;
        }

        public static char[] PreHeapFewer() {
            /* two siblings have L > R, at 7 and 8, to test R-side swapping */

            char[] array = new char[10];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'i';
            array[8] = 'h';
            array[9] = 'j';

            return array;
        }

        public static char[] PreHeapLateLettersFewer()  /* ok */  {
            /* two siblings have L > R, at 7 and 8, to test R-side swapping */

            char[] array = new char[10];

            array[0] = 'j';
            array[1] = 'k';
            array[2] = 'l';
            array[3] = 'm';
            array[4] = 'n';
            array[5] = 'o';
            array[6] = 'p';
            array[7] = 'r';
            array[8] = 'q';
            array[9] = 's';

            return array;
        }

        public object GatherContents(object topic_object, char content)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(content);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void PreHeap_Test_()  /* working */  {
            //**  groundwork  **//
            MinHeap_<char> preHeap = new MinHeap_<char>(PreHeap(), 16);

            string expected = "hdibjekafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PreHeapFewer_Test_()  /* working */  {
            //**  groundwork  **//
            MinHeap_<char> preHeap = new MinHeap_<char>(PreHeapFewer(), 16);

            string expected = "idhbjeafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        /*  test heap:                                                                                                                                      
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */

        /*  inorder traversal of test heap: 
         *      hdibjekafcg 
        */


        /*  test heap / fewer:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h             @9: j                                                                                             
         *                                                                                                                                                      
        */

        /*  inorder traversal of test heap / fewer:
         *  idhbjeafcg */


        // testing doesn't address problem of missing L or R children when using .RemoveHeapNode(); 
        // I'm skipping this because it isn't conceptually crucial 


        #region Heap Tests

        [TestMethod()]
        public void SwapDown_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap:                                                                                                                                      
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b >x >>d                                                     @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d >>x >>>h                             @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h >>>x                   @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: d                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: h                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: x                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /* inorder traversal of expected heap:  xhidjekafcg */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( b ) with testing contents ( x ); 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeContent = 'x';

            string expected = "xhidjekafcg";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 1);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapDown_Test_Addressing_Missing_Children_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap / fewer:                                                                                                                              
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h             @9: j                                                                                             
         *                                                                                                                                                      
        */

            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b >x >>d                                                     @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d >>x >>>h                             @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h >>>x        @9: j                                                                                             
         *                                                                                                                                                      
        */

            /*  resulting heap:                                                                                                                                
        *                                                                                                                                                      
        *                                                                            @0: a                                                                     
        *                                                                               |                                                                      
        *                                               ------------------------------------------------------------------                                     
        *                                               |                                                                |                                     
        *                                            @1: d                                                            @2: c                                    
        *                                               |                                                                |                                     
        *                       ---------------------------------------------                         -----------------------------------------                
        *                       |                                           |                         |                                       |                
        *                    @3: h                                      @4: e                      @5: f                                   @6: g               
        *                       |                                           |                                                                                  
        *        ------------------------------                 -------------                                                                                  
        *        |                            |                 |                                                                                              
        *     @7: i                        @8: x             @9: j                                                                                             
        *                                                                                                                                                      
*/

            /* inorder traversal of expected heap:  ihxdjeafcg */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeapFewer(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( b ) with testing contents ( x ); 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeContent = 'x';

            string expected = "ihxdjeafcg";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 1);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapUp_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  start heap:                                                                                                                                     
         *                                                                                                                                                      
         *                                                                            @0: j                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: k                                                            @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: m                                       @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: q                        @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */

            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: j >>>>b                                                               
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: k >>>b >>>>j                                                 @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: m >>b >>>k                              @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: q >b >>m                 @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */

            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: b                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: j                                                            @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: k                                       @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: m                        @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */


            /* inorder traversal of this expected heap:  mkrjsntbolp */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeapLateLetters(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 3 ( d ) with testing contents ( x ) 
            target.MoveToRoot();
            target.MoveToLeftChild();   // k 
            target.MoveToLeftChild();   // m 
            target.MoveToLeftChild();   // q 
            target.NodeContent = 'b';

            string expected = "mkrjsntbolp";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapUp", 7);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OrderHeap_Test_Assert_Expected_Relationships()   // working  
        {
            // the testing checks to see if each node is larger than either of its child nodes; 
            // not checking using a traversal because I can't easily predict what it should be; 
            //     it could be different for the same values if given in a different random order 

            //**  groundwork  **//

            // disordered array to make into binary-tree heap
            char[] array = new char[11];

            array[0] = 'f';
            array[1] = 'a';
            array[2] = 'b';
            array[3] = 'k';
            array[4] = 'c';
            array[5] = 'h';
            array[6] = 'd';
            array[7] = 'j';
            array[8] = 'g';
            array[9] = 'e';
            array[10] = 'i';

            MinHeap_<char> target = new MinHeap_<char>(array, 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            target.OrderHeap();


            //**  testing  **//

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test_Assert_Expected_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            target.AddHeapNode('x');


            //**  testing  **//

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of R child of root and its L child: indices 5 and 11; no R child
            target.MoveToRoot();
            target.MoveToRightChild();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;

            actual = (parentKey < leftChildKey);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveHeapNode_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.RemoveHeapNode(1);   // removing 400/b


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'a';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.PopHeapRoot();   // removing 500/a


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PolyMethod_RemoveHeapNode_PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeap_<char> target = new MinHeap_<char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.RemoveHeapNode(1);   // removing 400/b
            target.PopHeapRoot();
            target.PopHeapRoot();


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion Heap Tests


        #region Tree Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()  // working  
        {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;


            //**  exercising the code  **//
            // a complete tree, so elements are added left to right at each level before next level 
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  testing  **//
            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L sibling of R child of root ( == L child of root ), an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf 
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // parent of previous node ( L child of root ), an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;

            // a complete tree, so elements are added left to right at each level before next level
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  exercising the code  **//
            // after two nodes are removed, nodes c, d, and e should be leaves 
            target.RemoveTopNode();
            target.RemoveTopNode();


            //**  testing  **//
            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, now a leaf 
            expectedIsLeaf = true;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds() {
            //**  groundwork  **//
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level 
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');

            // last (leaf) node isn't added, so a complete, but not full, binary tree; 
            // now, node c should have only a L child 

            // target.AddNode('g');


            // each method is in a try-catch block to test for expected exception messages 


            //**  exercising the code  **//
            try {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX 
                target.MoveToRightChild();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child
                target.MoveToRoot();

                // CRUX
                target.MoveToParent();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToLeftSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToRightSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Tree Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char> target = new MinHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }

    [TestClass]
    public class MinHeapSharedArray_T_Tests
    {
        #region Friendlies

        public static char[] PreHeap()  /* passed */  {
            char[] array = new char[11];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'h';
            array[8] = 'i';
            array[9] = 'j';
            array[10] = 'k';

            return array;
        }

        public static char[] PreHeapLateLetters()  /* ok */  {
            char[] array = new char[11];

            array[0] = 'j';
            array[1] = 'k';
            array[2] = 'l';
            array[3] = 'm';
            array[4] = 'n';
            array[5] = 'o';
            array[6] = 'p';
            array[7] = 'q';
            array[8] = 'r';
            array[9] = 's';
            array[10] = 't';

            return array;
        }

        public static char[] PreHeapFewer() {
            /* two siblings have L > R, at 7 and 8, to test R-side swapping */

            char[] array = new char[10];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'i';
            array[8] = 'h';
            array[9] = 'j';

            return array;
        }

        public static char[] PreHeapLateLettersFewer()  /* ok */  {
            /* two siblings have L > R, at 7 and 8, to test R-side swapping */

            char[] array = new char[10];

            array[0] = 'j';
            array[1] = 'k';
            array[2] = 'l';
            array[3] = 'm';
            array[4] = 'n';
            array[5] = 'o';
            array[6] = 'p';
            array[7] = 'r';
            array[8] = 'q';
            array[9] = 's';

            return array;
        }

        public object GatherContents(object topic_object, char content)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(content);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void PreHeap_Test_()  /* working */  {
            //**  groundwork  **//
            MinHeapSharedArray_<char> preHeap = new MinHeapSharedArray_<char>(PreHeap());

            string expected = "hdibjekafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PreHeapFewer_Test_()  /* working */  {
            //**  groundwork  **//
            MinHeapSharedArray_<char> preHeap = new MinHeapSharedArray_<char>(PreHeapFewer());

            string expected = "idhbjeafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        /*  test heap:                                                                                                                                      
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */

        /*  inorder traversal of test heap: 
         *      hdibjekafcg 
        */


        /*  test heap / fewer:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h             @9: j                                                                                             
         *                                                                                                                                                      
        */

        /*  inorder traversal of test heap / fewer:
         *  idhbjeafcg */


        // testing doesn't address problem of missing L or R children when using .RemoveHeapNode(); 
        // I'm skipping this because it isn't conceptually crucial 


        #region Heap Tests

        [TestMethod()]
        public void SwapDown_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap:                                                                                                                                      
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b >x >>d                                                     @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d >>x >>>h                             @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: h >>>x                   @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: d                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: h                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: x                        @8: i             @9: j                    @10: k                                                                   
         *                                                                                                                                                      
        */


            /* inorder traversal of expected heap:  xhidjekafcg */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeap());
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( b ) with testing contents ( x ); 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeContent = 'x';

            string expected = "xhidjekafcg";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 1);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapDown_Test_Addressing_Missing_Children_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap / fewer:                                                                                                                              
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b                                                            @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d                                      @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h             @9: j                                                                                             
         *                                                                                                                                                      
        */

            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: a                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: b >x >>d                                                     @2: c                                    
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: d >>x >>>h                             @4: e                      @5: f                                   @6: g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 -------------                                                                                  
         *        |                            |                 |                                                                                              
         *     @7: i                        @8: h >>>x        @9: j                                                                                             
         *                                                                                                                                                      
        */

            /*  resulting heap:                                                                                                                                
        *                                                                                                                                                      
        *                                                                            @0: a                                                                     
        *                                                                               |                                                                      
        *                                               ------------------------------------------------------------------                                     
        *                                               |                                                                |                                     
        *                                            @1: d                                                            @2: c                                    
        *                                               |                                                                |                                     
        *                       ---------------------------------------------                         -----------------------------------------                
        *                       |                                           |                         |                                       |                
        *                    @3: h                                      @4: e                      @5: f                                   @6: g               
        *                       |                                           |                                                                                  
        *        ------------------------------                 -------------                                                                                  
        *        |                            |                 |                                                                                              
        *     @7: i                        @8: x             @9: j                                                                                             
        *                                                                                                                                                      
*/

            /* inorder traversal of expected heap:  ihxdjeafcg */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeapFewer());
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( b ) with testing contents ( x ); 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeContent = 'x';

            string expected = "ihxdjeafcg";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 1);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapUp_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  start heap:                                                                                                                                     
         *                                                                                                                                                      
         *                                                                            @0: j                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: k                                                            @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: m                                       @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: q                        @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */

            /*  ops in heap:                                                                                                                                    
         *                                                                                                                                                      
         *                                                                            @0: j >>>>b                                                               
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: k >>>b >>>>j                                                 @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: m >>b >>>k                              @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: q >b >>m                 @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */

            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: b                                                                     
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: j                                                            @2: l                                    
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: k                                       @4: n                     @5: o                                   @6: p               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: m                        @8: r             @9: s                    @10: t                                                                   
         *                                                                                                                                                      
        */


            /* inorder traversal of this expected heap:  mkrjsntbolp */

            #endregion Expecteds

            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeapLateLetters());
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 3 ( d ) with testing contents ( x ) 
            target.MoveToRoot();
            target.MoveToLeftChild();   // k 
            target.MoveToLeftChild();   // m 
            target.MoveToLeftChild();   // q 
            target.NodeContent = 'b';

            string expected = "mkrjsntbolp";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapUp", 7);
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OrderHeap_Test_Assert_Expected_Relationships()  /* working */  {
            // the testing checks to see if each node is larger than either of its child nodes; 
            // not checking using a traversal because I can't easily predict what it should be; 
            //     it could be different for the same values if given in a different random order 

            //**  groundwork  **//

            // disordered array to make into binary-tree heap
            char[] array = new char[11];

            array[0] = 'f';
            array[1] = 'a';
            array[2] = 'b';
            array[3] = 'k';
            array[4] = 'c';
            array[5] = 'h';
            array[6] = 'd';
            array[7] = 'j';
            array[8] = 'g';
            array[9] = 'e';
            array[10] = 'i';

            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(array);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            target.OrderHeap();


            //**  testing  **//

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeContent;
            target.MoveToLeftChild();
            leftChildKey = target.NodeContent;
            target.MoveToRightSibling();
            rightChildKey = target.NodeContent;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test__Partly_Empty_Initial_Array__Assert_No_Exception_Thrown()  /* working */  {
            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');

            Exception expected = null;
            Exception actual = null;


            //**  exercising the code  **//
            try {
                target.AddHeapNode('c');
                target.AddHeapNode('q');
                target.AddHeapNode('x');
                target.AddHeapNode('p');
                target.AddHeapNode('d');
            }
            catch (Exception x) {
                actual = x;
            }
            
            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test__Full_Initial_Array__Assert_Expected_Exception()  /* working */  {
            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeap());

            string expected = "_treeTop == _arrayTop";
            string actual = null;


            //**  exercising the code  **//
            try {
                target.AddHeapNode('x');
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveHeapNode_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeap());

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.RemoveHeapNode(1);   // removing 400/b


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeap());

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'a';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.PopHeapRoot();   // removing 500/a


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PolyMethod_RemoveHeapNode_PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            //**  groundwork  **//
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>(PreHeap());

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            //**  exercising the code  **//
            actualElement = target.RemoveHeapNode(1);   // removing 400/b
            target.PopHeapRoot();
            target.PopHeapRoot();


            //**  testing  **//

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;
                target.MoveToRightSibling();
                rightChildKey = target.NodeContent;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeContent;
                target.MoveToLeftChild();
                leftChildKey = target.NodeContent;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion Heap Tests


        #region Sharing Tests

        [TestMethod()]
        public void MinHeapSharedArray_Constructor_Test__Known_Array_Values__Assert_Expected_MinHeap_Values()  /* working */  {
            //**  groundwork  **//
            int[] core = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, };


            //**  exercising the code  **//
            MinHeapSharedArray_<int> heap = new MinHeapSharedArray_<int>(core);
            PrivateObject p = new PrivateObject(heap);
            int[] array = (int[])p.GetField("_array");


            //**  testing  **//
            for (int i = 0; i < core.Length; i++) {
                Assert.AreEqual(core[i], array[i]);
            }
        }

        #endregion Sharing Tests


        #region Tree Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()  // working  
        {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;


            //**  exercising the code  **//
            // a complete tree, so elements are added left to right at each level before next level 
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  testing  **//
            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L sibling of R child of root ( == L child of root ), an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf 
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // parent of previous node ( L child of root ), an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;

            // a complete tree, so elements are added left to right at each level before next level
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  exercising the code  **//
            // after two nodes are removed, nodes c, d, and e should be leaves 
            target.RemoveTopNode();
            target.RemoveTopNode();


            //**  testing  **//
            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, now a leaf 
            expectedIsLeaf = true;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds() {
            //**  groundwork  **//
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level 
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');

            // last (leaf) node isn't added, so a complete, but not full, binary tree; 
            // now, node c should have only a L child 

            // target.AddNode('g');


            // each method is in a try-catch block to test for expected exception messages 


            //**  exercising the code  **//
            try {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX 
                target.MoveToRightChild();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child
                target.MoveToRoot();

                // CRUX
                target.MoveToParent();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToLeftSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToRightSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Tree Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeapSharedArray_<char> target = new MinHeapSharedArray_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherContents);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }

    [TestClass]
    public class MinHeap_K_T_Tests
    {
        #region Friendlies

        public static Tuple<int, char>[] PreHeap()   // passed  
        {
            Tuple<int, char>[] array = new Tuple<int, char>[11];

            array[0] = Tuple.Create(10, 'a');
            array[1] = Tuple.Create(20, 'b');
            array[2] = Tuple.Create(30, 'c');
            array[3] = Tuple.Create(40, 'd');
            array[4] = Tuple.Create(50, 'e');
            array[5] = Tuple.Create(60, 'f');
            array[6] = Tuple.Create(70, 'g');
            array[7] = Tuple.Create(80, 'h');
            array[8] = Tuple.Create(90, 'i');
            array[9] = Tuple.Create(100, 'j');
            array[10] = Tuple.Create(110, 'k');

            return array;
        }

        public static Tuple<int, char>[] PreHeapFewer()   // ok   
        {
            Tuple<int, char>[] array = new Tuple<int, char>[10];

            // unlike the ordering in PreHeap(), the R child of the root's L child is lower,
            // so that testing of SwapDown() against data with a missing R child will visit intended nodes;
            // this means that int key values are swapped for nodes 3 and 4 (but char elements are not)

            array[0] = Tuple.Create(10, 'a');
            array[1] = Tuple.Create(20, 'b');
            array[2] = Tuple.Create(30, 'c');
            array[3] = Tuple.Create(50, 'd');
            array[4] = Tuple.Create(40, 'e');
            array[5] = Tuple.Create(60, 'f');
            array[6] = Tuple.Create(70, 'g');
            array[7] = Tuple.Create(80, 'h');
            array[8] = Tuple.Create(90, 'i');
            array[9] = Tuple.Create(100, 'j');

            return array;
        }

        public object GatherElements(object topic_object, int key, char element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(element);

            return topic;
        }

        public object GatherKeys(object topic_object, char key, int element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(key);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void PreHeap_Test_()   // working  
        {
            // GROUNDWORK  
            MinHeap_<int, char> preHeap = new MinHeap_<int, char>(PreHeap(), 16);

            string expected = "hdibjekafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)preHeap.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        /*  test heap:                                                                                                                                          
         *                                                                                                                                                      
         *                                                                            @0: 10/a                                                                  
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: 20/b                                                         @2: 30/c                                 
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: 40/d                                   @4: 50/e                   @5: 60/f                                @6: 70/g            
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: 80/h                     @8: 90/i          @9: 100/j                @10: 110/k                                                               
         *                                                                                                                                                      
        */


        /*  inorder traversal of test heap:
         *      hdibjekafcg
        */


        // testing doesn't address problem of missing L or R children when using .RemoveHeapNode();
        // I'm skipping this because it isn't conceptually crucial


        #region Heap Tests

        [TestMethod()]
        public void SwapDown_Test_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: 10/a                                                                  
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: 40/d                                                         @2: 30/c                                 
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: 80/h                                    @4: 50/e                   @5: 60/f                                @6: 70/g           
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: 300/x                 @8: 90/i          @9: 100/j                @10: 110/k                                                                  
         *                                                                                                                                                      
        */

            #endregion Expecteds


            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test;
            // replacing contents of node at index 1 ( 20/b ) with testing contents ( 300/x );
            // should swap down so that index 1 is 50/e and index 10 is 300/x
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeKey = 300;
            target.NodeElement = 'x';

            string expected = "xhidjekafcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapDown", 1);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapDown_Test_Addressing_Missing_Children_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  test heap:                                                                                                                                          
             *                                                                                                                                                      
             *                                                                            @0: 10/a                                                                  
             *                                                                               |                                                                      
             *                                               ------------------------------------------------------------------                                     
             *                                               |                                                                |                                     
             *                                            @1: 20/b                                                         @2: 30/c                                 
             *                                               |                                                                |                                     
             *                       ---------------------------------------------                         -----------------------------------------                
             *                       |                                           |                         |                                       |                
             *                    @3: 50/d                                   @4: 40/e                   @5: 60/f                                @6: 70/g            
             *                       |                                           |                                                                                  
             *        ------------------------------                 -------------                                                                                  
             *        |                            |                 |                                                                                              
             *     @7: 80/h                     @8: 90/i          @9: 100/j                                                                                         
             *                                                                                                                                                      
            */

            /*  inorder traversal of test heap:
             *      hdibjeafcg
            */

            /*
             * node 1 (20/b) is replaced with a value that will sort to node 9 (100/j);
             * new node values are 250/x;
             *      swaps first with 4 (40/e), which ends up at node 1;
             *      swaps next with 9 (100/j), which ends up at node 4;
             *      final node 9 is 250/x;
             * 
             * expected fail when not yet fixed is on second swap, when there is no R child
             */

            /*  expected heap:                                                                                                                                      
             *                                                                                                                                                      
             *                                                                            @0: 10/a                                                                  
             *                                                                               |                                                                      
             *                                               ------------------------------------------------------------------                                     
             *                                               |                                                                |                                     
             *                                            @1: 40/e                                                         @2: 30/c                                 
             *                                               |                                                                |                                     
             *                       ---------------------------------------------                         -----------------------------------------                
             *                       |                                           |                         |                                       |                
             *                    @3: 50/d                                   @4: 100/j                  @5: 60/f                                @6: 70/g            
             *                       |                                           |                                                                                  
             *        ------------------------------                 -------------                                                                                  
             *        |                            |                 |                                                                                              
             *     @7: 80/h                     @8: 90/i          @9: 250/x                                                                                         
             *                                                                                                                                                      
            */

            /*  inorder traversal of expected heap:
             *      hdiexjafcg
            */

            #endregion Expecteds


            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeapFewer(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test;
            // replacing contents of node at index 1 ( 20/b ) with testing contents ( 250/x );
            // should swap down so that index 1 is 50/e and index 9 is 250/x
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeKey = 250;
            target.NodeElement = 'x';

            string expected = "hdiexjafcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapDown", 1);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapUp_Test_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  expected heap:                                                                                                                                  
         *                                                                                                                                                      
         *                                                                            @0: 3/x                                                                   
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: 10/a                                                         @2: 30/c                                 
         *                                               |                                                                |                                     
         *                       --------------------------------------------                          ----------------------------------------                 
         *                       |                                           |                         |                                       |                
         *                    @3: 20/b                                @4: 50/e                   @5: 60/f                                @6: 70/g               
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: 40/d                 @8: 90/i          @9: 100/j                @10: 110/k                                                                   
         *                                                                                                                                                      
        */

            #endregion Expecteds


            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test;
            // replacing contents of node at index 7 ( 80/h ) with testing contents ( 3/x );
            // should swap up so that index 0 is 3/x and index 7 is 40/d
            target.MoveToRoot();
            target.MoveToLeftChild();   // b
            target.MoveToLeftChild();   // d
            target.MoveToLeftChild();   // h
            target.NodeKey = 3;
            target.NodeElement = 'x';

            string expected = "dbiajekxfcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapUp", 7);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OrderHeap_Test_Assert_Expected_Relationships()   // working  
        {
            // the testing checks to see if each node is larger than either of its child nodes;
            // not checking using a traversal because I can't easily predict what it should be;
            //     it could be different for the same values if given in a different random order

            // GROUNDWORK  

            // disordered array to make into binary-tree heap
            Tuple<int, char>[] array = new Tuple<int, char>[11];

            array[0] = Tuple.Create(60, 'f');
            array[1] = Tuple.Create(10, 'a');
            array[2] = Tuple.Create(20, 'b');
            array[3] = Tuple.Create(110, 'k');
            array[4] = Tuple.Create(30, 'c');
            array[5] = Tuple.Create(80, 'h');
            array[6] = Tuple.Create(40, 'd');
            array[7] = Tuple.Create(100, 'j');
            array[8] = Tuple.Create(70, 'g');
            array[9] = Tuple.Create(50, 'e');
            array[10] = Tuple.Create(90, 'i');

            MinHeap_<int, char> target = new MinHeap_<int, char>(array, 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual = false;


            // EXERCISING THE CODE  
            target.OrderHeap();


            // TESTING  

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test_Assert_Expected_Relationships()   // working  
        {
            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;


            // EXERCISING THE CODE  
            target.AddHeapNode(3, 'x');


            // TESTING  

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of R child of root and its L child: indices 5 and 11; no R child
            target.MoveToRoot();
            target.MoveToRightChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;

            actual = (parentKey < leftChildKey);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveHeapNode_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.RemoveHeapNode(1);   // removing 400/b


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'a';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.PopHeapRoot();   // removing 500/a


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PolyMethod_RemoveHeapNode_PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MinHeap_<int, char> target = new MinHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.RemoveHeapNode(1);   // removing 400/b
            target.PopHeapRoot();
            target.PopHeapRoot();


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey < leftChildKey) && (parentKey < rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = (parentKey < leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion Heap Tests


        #region Tree Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // TESTING  

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L sibling of R child of root ( == L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // parent of previous node ( L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // EXERCISING THE CODE  
            // after two nodes are removed, nodes c, d, and e should be leaves
            target.RemoveTopNode();
            target.RemoveTopNode();


            // TESTING  
            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, now a leaf
            expectedIsLeaf = true;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);

            // last (leaf) node isn't added, so a complete, but not full, binary tree;
            // now, node c should have only a L child

            // target.AddNode('g', 175);


            // each method is in a try-catch block to test for expected exception messages


            // EXERCISING THE CODE  
            try {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX
                target.MoveToRightChild();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child
                target.MoveToRoot();

                // CRUX
                target.MoveToParent();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToLeftSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToRightSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Tree Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MinHeap_<char, int> target = new MinHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }
}
