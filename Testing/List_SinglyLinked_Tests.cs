﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataStructs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass()]
    public class List_SinglyLinked_Tests
    {
        #region Test Context

        private TestContext testContextInstance;

        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #endregion Test Context


        #region Class Methods

        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}

        #endregion Class Methods


        // according to refsite, GenericParameterHelper is a nonspecific T you can use for testing;
        // it wraps only Int32 values you want to provide for tests, accessible through the .Data member


        #region Constructors

        [TestMethod()]
        public void List_SinglyLinked_Constructor_With_Arg_Test()   // working  
        {
            // groundwork  
            int length = 9;

            int expectedLength = 0;
            int actualLength;


            // exercising the code  
            PrivateObject target = new PrivateObject(new List_SinglyLinked_<GenericParameterHelper>(length));
            actualLength = (int)target.GetProperty("Length");


            // testing  
            Assert.AreEqual(expectedLength, actualLength);
        }

        [TestMethod()]
        public void List_SinglyLinked_Constructor_No_Arg_Test()   // working  
        {
            // groundwork  
            int expectedLength = 0;
            int actualLength;


            // exercising the code  
            PrivateObject target = new PrivateObject(new List_SinglyLinked_<GenericParameterHelper>());
            actualLength = (int)target.GetProperty("Length");


            // testing  
            Assert.AreEqual(expectedLength, actualLength);
        }

        #endregion Constructors


        #region Append, Insert, Remove

        [TestMethod()]
        public void Append_Test()   // working  
        {
            // groundwork  
            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] expected = new GenericParameterHelper[] { element0, element1, element2, element3, element4 };


            // exercising the code  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();
            target.Append(element0);
            target.Append(element1);
            target.Append(element2);
            target.Append(element3);
            target.Append(element4);

            PrivateObject privateTarget = new PrivateObject(target);


            // testing  
            Assert.AreEqual(expected.Length, target.Length);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test()   // working  
        {
            // groundwork  
            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);   // skipped the first time; index 3
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            // it's useful to separately match just the contents, and then the entire (mostly empty) array
            GenericParameterHelper[] expected = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            // object init and other pre-exercising here
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();
            target.Append(element0);
            target.Append(element1);
            target.Append(element2);
            // element3 skipped here; added below
            target.Append(element4);    // at present, this is at index of 3
            target.Append(element5);


            // exercising the code  
            target.PositionAtIndex(3);
            target.InsertAtCurrent(element3);


            // testing  
            Assert.AreEqual(expected.Length, target.Length);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);


            // moving to the middle of the list
            target.PositionAtIndex(3);


            // exercising the code  
            GenericParameterHelper actualElement = target.RemoveAtCurrent();


            // testing  
            Assert.AreEqual(5, target.Length);
            Assert.AreEqual(element3, actualElement);
            Assert.AreEqual(element4.Data, target.CurrentValue.Data);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test__Assert_Stepwise_Traversal_As_Expected()  /* working */  {
            //**  groundwork  **//
            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);   // skipped the first time; index 3 
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            // it's useful to separately match just the contents, and then the entire (mostly empty) array 
            GenericParameterHelper[] expected = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            // object init and other pre-exercising here 
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();
            target.Append(element0);
            target.Append(element1);
            target.Append(element2);
            // element3 skipped here; added below 
            target.Append(element4);    // at present, this is at index of 3 
            target.Append(element5);


            //**  exercising the code  **//
            target.PositionAtIndex(3);
            target.InsertAtCurrent(element3);


            //**  testing  **//
            // traversing from list first to last; all elements should match 
            for (int i = 0; target.CurrentPosition < target.Length; target.Next(), i++) {
                if (i == 0) { target.PositionAtStart(); }
                GenericParameterHelper actual = target.CurrentValue;
                Assert.AreEqual(expected[i], actual);
            }

        }

        [TestMethod()]
        public void RemoveAtCurrent_Test__Assert_Stepwise_Traversal_As_Expected()  /* working */  {
            //**  groundwork  **//
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            // no (former) element 3 
            GenericParameterHelper[] expected = new GenericParameterHelper[] { element0, element1, element2, element4, element5 };


            //**  exercising the code  **//
            // moving to the middle of the list, then removing 
            target.PositionAtIndex(3);
            GenericParameterHelper actualElement = target.RemoveAtCurrent();


            //**  testing  **//
            // traversing from list first to last; all elements should match 
            for (int i = 0; target.CurrentPosition < target.Length; target.Next(), i++) {
                if (i == 0) { target.PositionAtStart(); }
                GenericParameterHelper actual = target.CurrentValue;
                Assert.AreEqual(expected[i], actual);
            }
        }

        #endregion Append, Insert, Remove


        #region Next, Previous, Position At Index/Start/End

        [TestMethod()]
        public void Next_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            int expected;
            int actual;


            // exercising the code AND testing
            {
                // test 1: element 0
                expected = element0.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 2: element 1
                target.Next();
                expected = element1.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 3: element 2
                target.Next();
                expected = element2.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void Previous_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            int expected;
            int actual;


            // exercising the code AND testing
            {
                // moving to the middle of the list
                target.PositionAtIndex(2);

                // test 1: element 2
                expected = element2.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 2: element 1
                target.Previous();
                expected = element1.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 3: element 0
                target.Previous();
                expected = element0.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PositionAtIndex_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            target.PositionAtStart();


            int expected = 2;
            int actual;


            // exercising the code  
            target.PositionAtIndex(2);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtStart_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            // moving away from index 0
            target.Next();
            target.Next();
            target.Next();


            // exercising the code  
            target.PositionAtStart();


            // testing  
            Assert.AreEqual(0, target.CurrentPosition);
            Assert.AreEqual(element0, target.CurrentValue);
        }

        [TestMethod()]
        public void PositionAtEnd_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);


            // exercising the code  
            target.PositionAtEnd();


            // testing  
            Assert.AreEqual(5, target.CurrentPosition);
            Assert.AreEqual(element5, target.CurrentValue);
        }

        #endregion Next, Previous, Position At Index/Start/End


        #region Current Value

        [TestMethod()]
        public void CurrentValue_Test_Get_One_Element_Assert_Expected()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);

            target.Append(element0);

            int expected = element0.Data;
            int actual;


            // exercising the code  
            actual = target.CurrentValue.Data;


            // testing  
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CurrentValue_Test_Set_One_Element_Assert_Expected()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);

            target.Append(element0);

            GenericParameterHelper newElement = new GenericParameterHelper(++ix);


            int expected = newElement.Data;
            int actual;


            // exercising the code  
            target.CurrentValue = newElement;
            actual = target.CurrentValue.Data;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        // * skipped handling error conditions in this class *

        //[TestMethod()]
        //public void CurrentValue_Test_No_List_Elements_Get_Assert_Expected_Exception()   // working  
        //{
        //   // groundwork  
        //   List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

        //   object content;

        //   string expected = "@CurrentValue-get: _currentIndex < 0 || _currentIndex > this.Top";
        //   string actual = "FAIL";


        //   // exercising the code  
        //   try
        //   {
        //       content = target.CurrentValue;
        //   }
        //   catch (Exception x)
        //   {
        //       actual = x.Message;
        //   }


        //   // testing  
        //   Assert.AreEqual(expected, actual);
        //}


        //[TestMethod()]
        //public void CurrentValue_Test_No_List_Elements_Set_Assert_Expected_Exception()   // working  
        //{
        //   // groundwork  
        //   List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

        //   GenericParameterHelper content = new GenericParameterHelper(9);

        //   string expected = "@CurrentValue-set: _currentIndex < 0 || _currentIndex > this.Top";
        //   string actual = "FAIL";


        //   // exercising the code  
        //   try
        //   {
        //       target.CurrentValue = content;
        //   }
        //   catch (Exception x)
        //   {
        //       actual = x.Message;
        //   }


        //   // testing  
        //   Assert.AreEqual(expected, actual);
        //}

        #endregion Current Value


        #region Clear

        [TestMethod()]
        public void Clear_Test()   // working  
        {
            // groundwork  
            List_SinglyLinked_<GenericParameterHelper> target = new List_SinglyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            foreach (GenericParameterHelper element in new GenericParameterHelper[] { 
                element0, element1, element2, element3, element4, element5 
            })
                target.Append(element);

            int expectedLength = 0;
            int expectedPosition = -1;


            // moving from 0 to accurately test results of later clearing
            target.Next();
            target.Next();
            target.Next();


            // exercising the code  
            target.Clear();

            int actualLength = target.Length;
            int actualPosition = target.CurrentPosition;

            // functionality is different here than in array-based; does not throw exception, instead returns default(T)
            object unused = target.CurrentValue;


            // testing  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedPosition, actualPosition);
        }

        #endregion Clear
    }
}
