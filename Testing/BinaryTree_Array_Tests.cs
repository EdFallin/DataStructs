﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BinaryTree_Array_Tests
    {
        #region Friendlies

        public object GatherKeys(object topic_object, char key, int element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(key);

            return topic;
        }

        #endregion Friendlies


        #region Basic Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()   // WORKING  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // TESTING  

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L sibling of R child of root ( == L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // parent of previous node ( L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()   // WORKING  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // EXERCISING THE CODE  
            // after two nodes are removed, nodes c, d, and e should be leaves
            target.RemoveTopNode();
            target.RemoveTopNode();


            // TESTING  
            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, now a leaf
            expectedIsLeaf = true;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds()   // WORKING  
        {
            // GROUNDWORK  
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);

            // last (leaf) node isn't added, so a complete, but not full, binary tree;
            // now, node c should have only a L child

            // target.AddNode('g', 175);


            // each method is in a try-catch block to test for expected exception messages


            // EXERCISING THE CODE  
            try
            {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX
                target.MoveToRightChild();
                // END CRUX
            }
            catch (Exception x)
            {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try
            {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child
                target.MoveToRoot();

                // CRUX
                target.MoveToParent();
                // END CRUX
            }
            catch (Exception x)
            {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try
            {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToLeftSibling();
                // END CRUX
            }
            catch (Exception x)
            {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try
            {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToRightSibling();
                // END CRUX
            }
            catch (Exception x)
            {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Basic Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()   // WORKING  
        {
            // GROUNDWORK  
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()   // WORKING  
        {
            // GROUNDWORK  
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()   // WORKING  
        {
            // GROUNDWORK  
            BinaryTree_Array_<char, int> target = new BinaryTree_Array_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }
}
