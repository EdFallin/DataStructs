﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class Stack_Tests
    {
        #region Fields

        static StackType stackType;

        #endregion Fields


        #region Test Class Methods

        [ClassInitialize()]
        public static void ClassInitialize(TestContext unused)
        {
            //System.Windows.Forms.MessageBox.Show(
            //   "To change which stack implementation is being tested, enable/disable lines in Stack_Tests' ClassInitialize().");

            stackType = StackType.Fail;         // creates Stack_Fails_ instances, which are designed only to verify the tests here
            stackType = StackType.ArrayBased;
            stackType = StackType.LinkedListBased;
        }

        #endregion Test Class Methods




        #region Push

        #region Length

        [TestMethod()]
        public void Push_Test_One_Push_Assert_Length_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 1;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            actual = target.Length;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Push_Test_Three_Pushes_Assert_Length_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 3;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            actual = target.Length;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Length


        #region Peek

        [TestMethod()]
        public void Push_Test_One_Push_Assert_Peek_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 4;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            actual = target.Peek;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Push_Test_Three_Pushes_Assert_Peek_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 12;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            actual = target.Peek;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Peek

        #endregion Push


        #region Pop

        #region Length

        [TestMethod()]
        public void Pop_Test_One_Push_One_Pop_Assert_Length_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 0;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Pop();
            actual = target.Length;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pop_Test_Three_Pushes_One_Pop_Assert_Length_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 2;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            target.Pop();
            actual = target.Length;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Length


        #region Element

        [TestMethod()]
        public void Pop_Test_One_Push_One_Pop_Assert_Element_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 4;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            actual = target.Pop();


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pop_Test_Three_Pushes_One_Pop_Assert_Element_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 12;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            actual = target.Pop();


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pop_Test_Three_Pushes_Two_Pops_Assert_Element_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 8;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            target.Pop();
            actual = target.Pop();


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Element


        #region Peek

        [TestMethod()]
        public void Pop_Test_Three_Pushes_One_Pop_Assert_Peek_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 8;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            target.Pop();
            actual = target.Peek;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Pop_Test_Three_Pushes_Two_Pops_Assert_Peek_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 4;
            int actual;


            // EXERCISING THE CODE  
            target.Push(4);
            target.Push(8);
            target.Push(12);
            target.Pop();
            target.Pop();
            actual = target.Peek;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Peek

        #endregion Pop


        #region Clear

        [TestMethod()]
        public void Clear_Test_Assert_Length_Expected()   // WORKING  
        {
            // GROUNDWORK  
            Stack_<int> target = Stack_<int>.SupplyStack_T(stackType);

            int expected = 0;
            int actual;


            // EXERCISING THE CODE  
            target.Clear();
            actual = target.Length;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Clear

    }
}
