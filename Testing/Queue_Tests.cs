﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;


namespace Testing
{
    [TestClass]
    public class Queue_Tests
    {
        #region Fields

        static QueueType queueType;

        #endregion Fields


        #region Test Class Methods

        [ClassInitialize()]
        public static void ClassInitialize(TestContext unused)  /* verified */  {
            /* to change which queue implementation is being tested, enable or disable lines here */

            queueType = QueueType.Fail;         // creates Queue_Fails_ instances, which are designed only to verify the tests here 
            queueType = QueueType.Array;
            queueType = QueueType.LinkedList;
        }

        #endregion Test Class Methods


        #region Friendlies

        public static Queue_<int> AssignFailValues(Queue_<int> queue)  /* verified */  {
            if (queueType == QueueType.Fail) {
                PrivateObject p = new PrivateObject(queue);
                p.SetField("_peek", int.MinValue);
                p.SetField("_dequeue", int.MinValue);
                p.SetField("_length", int.MinValue);
            }

            return queue;
        }

        #endregion Friendlies


        #region Peek

        [TestMethod()]
        public void Peek__Empty_Queue__Assert_Default_T()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = default(int);
            int actual;


            //**  exercising the code  **//
            actual = target.Peek;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Peek__With_Queued_Items__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);

            // .Peek returns the first (oldest) 
            int expected = 2;
            int actual;


            //**  exercising the code  **//
            actual = target.Peek;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Peek


        #region Length

        [TestMethod()]
        public void Length__Empty_Queue__Assert_Zero()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 0;
            int actual;


            //**  exercising the code  **//
            actual = target.Length;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Length__With_Queued_Items__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);

            int expected = 4;
            int actual;


            //**  exercising the code  **//
            actual = target.Length;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Length


        #region Clear

        [TestMethod()]
        public void Clear___Assert_Length_Is_Zero()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 0;
            int actual;


            //**  exercising the code  **//
            target.Clear();
            actual = target.Length;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Clear___Assert_Peek_Is_Default_T()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = default(int);
            int actual;


            //**  exercising the code  **//
            target.Clear();
            actual = target.Peek;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Clear


        #region Enqueue

        [TestMethod()]
        public void Enqueue___Assert_Peeks_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 2;   // value peeked at is *oldest* value queued 
            int actual;


            //**  exercising the code and testing  **//
            // 
            // value should be the same oldest each time 
            target.Enqueue(2);
            actual = target.Peek;
            Assert.AreEqual(expected, actual);

            target.Enqueue(4);
            actual = target.Peek;
            Assert.AreEqual(expected, actual);

            target.Enqueue(6);
            actual = target.Peek;
            Assert.AreEqual(expected, actual);

            target.Enqueue(8);
            actual = target.Peek;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Enqueue___Assert_Lengths_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected;
            int actual;


            //**  exercising the code and testing  **//
            expected = 0;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Enqueue(2);
            expected = 1;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Enqueue(4);
            expected = 2;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Enqueue(6);
            expected = 3;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Enqueue(8);
            expected = 4;
            actual = target.Length;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Enqueue__Full_Queue__Assert_Length_Is_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 8;
            int actual;


            //**  exercising the code and testing  **//
            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);
            target.Enqueue(10);
            target.Enqueue(12);
            target.Enqueue(14);
            target.Enqueue(16);

            actual = target.Length;
            Assert.AreEqual(expected, actual);
        }

        #endregion Enqueue


        #region Dequeue

        [TestMethod()]
        public void Dequeue___Assert_Returneds_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);


            int expected;   // value returned by dequeue is *oldest* value queued 
            int actual;


            //**  exercising the code and testing  **//
            // 
            // value should be the oldest each time 
            actual = target.Dequeue();
            expected = 2;
            Assert.AreEqual(expected, actual);

            actual = target.Dequeue();
            expected = 4;
            Assert.AreEqual(expected, actual);

            actual = target.Dequeue();
            expected = 6;
            Assert.AreEqual(expected, actual);

            actual = target.Dequeue();
            expected = 8;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Dequeue___Assert_Peekeds_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);


            int expected;   // value peeked at is *oldest* value still queued after dequeueing 
            int actual;


            //**  exercising the code and testing  **//
            // 
            // value should be the oldest remaining each time 
            expected = 4;
            target.Dequeue();
            actual = target.Peek;
            Assert.AreEqual(expected, actual);

            expected = 6;
            target.Dequeue();
            actual = target.Peek;
            Assert.AreEqual(expected, actual);

            expected = 8;
            target.Dequeue();
            actual = target.Peek;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Dequeue___Assert_Lengths_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);

            int expected;
            int actual;


            //**  exercising the code and testing  **//
            expected = 4;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Dequeue();
            expected = 3;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Dequeue();
            expected = 2;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Dequeue();
            expected = 1;
            actual = target.Length;
            Assert.AreEqual(expected, actual);

            target.Dequeue();
            expected = 0;
            actual = target.Length;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Dequeue__Alternating_Enqueue_And_Dequeue__Assert_Returneds_Are_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);

            target.Enqueue(1);


            //**  exercising the code  **//
            int actual1 = target.Dequeue();
            target.Enqueue(2);
            int actual2 = target.Dequeue();


            //**  testing  **//
            Assert.AreEqual(1, actual1);
            Assert.AreEqual(2, actual2);
        }


        #endregion Dequeue


        #region Shifting Queue

        // these tests are most meaningful when the queue is implemented with a (circular) array 

        [TestMethod()]
        public void Queue_And_Dequeue__Shifting_Full_Queue__Assert_No_Exception_Thrown()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            Exception expected = null;
            Exception actual = null;


            //**  exercising the code  **//
            try {
                target.Enqueue(2);
                target.Enqueue(4);
                target.Enqueue(6);
                target.Enqueue(8);
                target.Enqueue(10);
                target.Enqueue(12);
                target.Enqueue(14);
                target.Enqueue(16);
                // queue is full; dequeuing and enqueueing now shift array forward 

                target.Dequeue();
                target.Enqueue(18);
                target.Dequeue();
                target.Enqueue(20);
                target.Dequeue();
                target.Enqueue(22);
                target.Dequeue();
                target.Enqueue(24);
                // queue should have shifted halfway around here 

                target.Dequeue();
                target.Enqueue(26);
                target.Dequeue();
                target.Enqueue(28);
                target.Dequeue();
                target.Enqueue(30);
                target.Dequeue();
                target.Enqueue(32);
                // queue should have shifted entirely around again 

                target.Dequeue();
                target.Enqueue(34);
                target.Dequeue();
                target.Enqueue(36);
                target.Dequeue();
                target.Enqueue(38);
                target.Dequeue();
                target.Enqueue(40);
                // queue again should have shifted halfway around here 

                target.Dequeue();
                target.Enqueue(42);
                target.Dequeue();
                target.Enqueue(44);
                target.Dequeue();
                target.Enqueue(46);
                target.Dequeue();
                target.Enqueue(48);
                // queue should have shifted entirely around another time 
            }
            catch (Exception x) {
                actual = x;
            }


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Queue_And_Dequeue__Shifting_Full_Queue__Assert_Final_Dequeue__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 32;
            int actual;


            //**  exercising the code  **//
            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);
            target.Enqueue(10);
            target.Enqueue(12);
            target.Enqueue(14);
            target.Enqueue(16);
            // full queue 

            // shifting 
            target.Dequeue();       // 2 
            target.Enqueue(18);
            target.Dequeue();       // 4 
            target.Enqueue(20);
            target.Dequeue();       // 6 
            target.Enqueue(22);
            target.Dequeue();       // 8 
            target.Enqueue(24);
            target.Dequeue();       // 10 
            target.Enqueue(26);
            target.Dequeue();       // 12 
            target.Enqueue(28);
            target.Dequeue();       // 14 
            target.Enqueue(30);
            target.Dequeue();       // 16 
            target.Enqueue(32);
            target.Dequeue();       // 18 
            target.Enqueue(34);
            target.Dequeue();       // 20 
            target.Enqueue(36);
            target.Dequeue();       // 22 
            target.Enqueue(38);
            target.Dequeue();       // 24 
            target.Enqueue(40);
            target.Dequeue();       // 26 
            target.Enqueue(42);
            target.Dequeue();       // 28 
            target.Enqueue(44);
            target.Dequeue();       // 30 
            target.Enqueue(46);

            actual = target.Dequeue();   // 32  ( 8 behind latest ) 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Queue_And_Dequeue__Shifting_Full_Queue__Assert_Final_Peek__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 34;
            int actual;


            //**  exercising the code  **//
            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);
            target.Enqueue(10);
            target.Enqueue(12);
            target.Enqueue(14);
            target.Enqueue(16);
            // full queue 

            // shifting 
            target.Dequeue();       // 2 
            target.Enqueue(18);
            target.Dequeue();       // 4 
            target.Enqueue(20);
            target.Dequeue();       // 6 
            target.Enqueue(22);
            target.Dequeue();       // 8 
            target.Enqueue(24);
            target.Dequeue();       // 10 
            target.Enqueue(26);
            target.Dequeue();       // 12 
            target.Enqueue(28);
            target.Dequeue();       // 14 
            target.Enqueue(30);
            target.Dequeue();       // 16 
            target.Enqueue(32);
            target.Dequeue();       // 18 
            target.Enqueue(34);
            target.Dequeue();       // 20 
            target.Enqueue(36);
            target.Dequeue();       // 22 
            target.Enqueue(38);
            target.Dequeue();       // 24 
            target.Enqueue(40);
            target.Dequeue();       // 26 
            target.Enqueue(42);
            target.Dequeue();       // 28 
            target.Enqueue(44);
            target.Dequeue();       // 30 
            target.Enqueue(46);

            target.Dequeue();   // 32  ( 8 behind latest ) 
            target.Enqueue(48);

            actual = target.Peek;           // 34  ( next after 32; 7 behind latest ) 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Queue_And_Dequeue__Shifting_Half_Full_Queue__Assert_Final_Dequeue__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 18;
            int actual;


            //**  exercising the code  **//
            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);
            // half-full, should have no shifting yet 

            target.Dequeue();       // 2 
            target.Enqueue(3);
            target.Dequeue();       // 4 
            target.Enqueue(5);
            target.Dequeue();       // 6 
            target.Enqueue(7);
            target.Dequeue();       // 8 
            target.Enqueue(9);
            // should be shifted halfway now 

            target.Dequeue();       // 3 
            target.Enqueue(10);
            target.Dequeue();       // 5 
            target.Enqueue(12);
            target.Dequeue();       // 7 
            target.Enqueue(14);
            target.Dequeue();       // 9 
            target.Enqueue(16);
            // should be shifted all the way now 

            target.Dequeue();       // 10 
            target.Enqueue(11);
            target.Dequeue();       // 12 
            target.Enqueue(13);
            target.Dequeue();       // 14 
            target.Enqueue(15);
            target.Dequeue();       // 16 
            target.Enqueue(17);
            // should be shifted halfway again 

            target.Dequeue();       // 11 
            target.Enqueue(18);
            target.Dequeue();       // 13 
            target.Enqueue(20);
            target.Dequeue();       // 15 
            target.Enqueue(22);
            target.Dequeue();       // 17 
            target.Enqueue(24);
            // should be shifted all the way around again 

            actual = target.Dequeue();   // 18 ( 4 behind latest ) 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Queue_And_Dequeue__Shifting_Half_Full_Queue__Assert_Final_Peek__Assert_Expected()  /* working */  {
            //**  groundwork  **//
            Queue_<int> target = Queue_<int>.SupplyQueue_T(queueType);
            AssignFailValues(target);

            int expected = 20;
            int actual;


            //**  exercising the code  **//
            target.Enqueue(2);
            target.Enqueue(4);
            target.Enqueue(6);
            target.Enqueue(8);
            // half-full, should have no shifting yet 

            target.Dequeue();       // 2 
            target.Enqueue(3);
            target.Dequeue();       // 4 
            target.Enqueue(5);
            target.Dequeue();       // 6 
            target.Enqueue(7);
            target.Dequeue();       // 8 
            target.Enqueue(9);
            // should be shifted halfway now 

            target.Dequeue();       // 3 
            target.Enqueue(10);
            target.Dequeue();       // 5 
            target.Enqueue(12);
            target.Dequeue();       // 7 
            target.Enqueue(14);
            target.Dequeue();       // 9 
            target.Enqueue(16);
            // should be shifted all the way now 

            target.Dequeue();       // 10 
            target.Enqueue(11);
            target.Dequeue();       // 12 
            target.Enqueue(13);
            target.Dequeue();       // 14 
            target.Enqueue(15);
            target.Dequeue();       // 16 
            target.Enqueue(17);
            // should be shifted halfway again 

            target.Dequeue();       // 11 
            target.Enqueue(18);
            target.Dequeue();       // 13 
            target.Enqueue(20);
            target.Dequeue();       // 15 
            target.Enqueue(22);
            target.Dequeue();       // 17 
            target.Enqueue(24);
            // should be shifted all the way around again 

            target.Dequeue();   // 18 ( 4 behind latest ) 
            actual = target.Peek;           // 20 ( 3 behind latest ) 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Shifting Queue
    }
}
