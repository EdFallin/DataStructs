﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataStructs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass()]
    public class List_Array_Tests
    {
        #region Test Context

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion Test Context


        #region Class Methods

        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}

        #endregion Class Methods


        // according to refsite, GenericParameterHelper is a nonspecific T you can use for testing;
        // it wraps only Int32 values you want to provide for tests, accessible through the .Data member


        #region Constructors / Passed

        [TestMethod()]
        public void List_Array_Constructor_With_Arg_Test()   // WORKING  
        {
            // GROUNDWORK  
            int length = 9;

            int expectedLength = 0;
            int actualLength;
            int expectedMax = length;
            int actualMax;


            // EXERCISING THE CODE  
            PrivateObject target = new PrivateObject(new List_Array_<GenericParameterHelper>(length));
            actualLength = (int)target.GetProperty("Length");
            actualMax = (int)target.GetProperty("Max");


            // TESTING  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedMax, actualMax);
        }

        [TestMethod()]
        public void List_Array_Constructor_No_Arg_Test()   // WORKING  
        {
            // GROUNDWORK  
            int expectedLength = 0;
            int actualLength;
            int expectedMax = 16;
            int actualMax;


            // EXERCISING THE CODE  
            PrivateObject target = new PrivateObject(new List_Array_<GenericParameterHelper>());
            actualLength = (int)target.GetProperty("Length");
            actualMax = (int)target.GetProperty("Max");


            // TESTING  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedMax, actualMax);
        }

        #endregion Constructors / Passed


        #region Append, Insert, Remove / Passed

        [TestMethod()]
        public void Append_Test()   // WORKING  
        {
            // GROUNDWORK  
            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);

            // it's useful to separately match just the contents, and then the entire (mostly empty) array
            GenericParameterHelper[] expectedWithContents = new GenericParameterHelper[] { element0, element1, element2, element3, element4 };
            GenericParameterHelper[] expected = new GenericParameterHelper[] { 
                element0, element1, element2, element3, element4,
                null, null, null, null, null, null,
                null, null, null, null, null, };


            // EXERCISING THE CODE  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();
            target.Append(element0);
            target.Append(element1);
            target.Append(element2);
            target.Append(element3);
            target.Append(element4);

            PrivateObject privateTarget = new PrivateObject(target);
            GenericParameterHelper[] actual = (GenericParameterHelper[])privateTarget.GetField("_array");

            // TESTING  
            Assert.AreEqual(expectedWithContents.Length, target.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test()   // WORKING  
        {
            // GROUNDWORK  
            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);   // skipped the first time; index 3
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            // it's useful to separately match just the contents, and then the entire (mostly empty) array
            GenericParameterHelper[] expectedWithContents = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };
            GenericParameterHelper[] expected = new GenericParameterHelper[] { 
                element0, element1, element2, element3, element4, element5,
                null, null, null, null, null, 
                null, null, null, null, null, };

            // object init and other pre-exercising here
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();
            target.Append(element0);
            target.Append(element1);
            target.Append(element2);
            // element3 skipped here; added below
            target.Append(element4);    // at present, this is at index of 3
            target.Append(element5);

            // EXERCISING THE CODE  
            // ACT: add move-to-first, nexts or move-to-index, then  .Insert() here
            target.PositionAtIndex(3);
            target.InsertAtCurrent(element3);

            PrivateObject privateTarget = new PrivateObject(target);
            GenericParameterHelper[] actual = (GenericParameterHelper[])privateTarget.GetField("_array");

            // TESTING  
            Assert.AreEqual(expectedWithContents.Length, target.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);


            // moving to the middle of the list
            target.PositionAtIndex(3);

            // EXERCISING THE CODE  
            GenericParameterHelper actualElement = target.RemoveAtCurrent();

            // TESTING  
            Assert.AreEqual(5, target.Length);
            Assert.AreEqual(element3, actualElement);
            Assert.AreEqual(element4.Data, target.CurrentValue.Data);
        }

        #endregion Append, Insert, Remove / Passed


        #region Next, Previous, Position At Index/Start/End / Passed

        [TestMethod()]
        public void Next_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            int expected;
            int actual;


            // EXERCISING THE CODE AND TESTING
            {
                // test 1: element 0 
                expected = element0.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 2: element 1 
                target.Next();
                expected = element1.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 3: element 2 
                target.Next();
                expected = element2.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void Previous_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            int expected;
            int actual;


            // EXERCISING THE CODE AND TESTING
            {
                // moving to the middle of the list
                target.PositionAtIndex(2);

                // test 1: element 2
                expected = element2.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 2: element 1
                target.Previous();
                expected = element1.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);

                // test 3: element 0
                target.Previous();
                expected = element0.Data;
                actual = target.CurrentValue.Data;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PositionAtIndex_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            target.PositionAtStart();


            int expected = 2;
            int actual;


            // EXERCISING THE CODE  
            target.PositionAtIndex(2);
            actual = target.CurrentPosition;

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtStart_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            // moving away from index 0
            target.Next();
            target.Next();
            target.Next();

            // EXERCISING THE CODE  
            target.PositionAtStart();


            // TESTING  
            Assert.AreEqual(0, target.CurrentPosition);
            Assert.AreEqual(element0, target.CurrentValue);
        }

        [TestMethod()]
        public void PositionAtEnd_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);


            // EXERCISING THE CODE  
            target.PositionAtEnd();


            // TESTING  
            Assert.AreEqual(5, target.CurrentPosition);
            Assert.AreEqual(element5, target.CurrentValue);

        }

        #endregion Next, Previous, Position At Index/Start/End / Passed


        #region Current Value / Passed

        [TestMethod()]
        public void CurrentValue_Test_Get_One_Element_Assert_Expected()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);

            target.Append(element0);

            int expected = element0.Data;
            int actual;


            // EXERCISING THE CODE  
            actual = target.CurrentValue.Data;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CurrentValue_Test_Set_One_Element_Assert_Expected()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);

            target.Append(element0);

            GenericParameterHelper newElement = new GenericParameterHelper(++ix);


            int expected = newElement.Data;
            int actual;


            // EXERCISING THE CODE  
            target.CurrentValue = newElement;
            actual = target.CurrentValue.Data;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CurrentValue_Test_No_List_Elements_Get_Assert_Expected_Exception()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            object content;

            string expected = "@CurrentValue-get: _currentIndex < 0 || _currentIndex > this.Top";
            string actual = "FAIL";


            // EXERCISING THE CODE  
            try
            {
                content = target.CurrentValue;
            }
            catch (Exception x)
            {
                actual = x.Message;
            }


            // TESTING  
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void CurrentValue_Test_No_List_Elements_Set_Assert_Expected_Exception()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            GenericParameterHelper content = new GenericParameterHelper(9);

            string expected = "@CurrentValue-set: _currentIndex < 0 || _currentIndex > this.Top";
            string actual = "FAIL";


            // EXERCISING THE CODE  
            try
            {
                target.CurrentValue = content;
            }
            catch (Exception x)
            {
                actual = x.Message;
            }


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Current Value / Passed


        #region Clear / Passed

        [TestMethod()]
        public void Clear_Test()   // WORKING  
        {
            // GROUNDWORK  
            List_Array_<GenericParameterHelper> target = new List_Array_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            foreach (GenericParameterHelper element in new GenericParameterHelper[] { 
                element0, element1, element2, element3, element4, element5 
            })
                target.Append(element);

            int expectedLength = 0;
            int expectedPosition = -1;
            string expectedExceptionMessage = "@CurrentValue-get: _currentIndex < 0 || _currentIndex > this.Top";


            // moving from 0 to accurately test results of later clearing
            target.Next();
            target.Next();
            target.Next();


            // EXERCISING THE CODE  
            target.Clear();

            int actualLength = target.Length;
            int actualPosition = target.CurrentPosition;
            string actualExceptionMessage = "FAIL";

            try
            {
                object unused = target.CurrentValue;
            }
            catch (Exception x)
            {
                actualExceptionMessage = x.Message;
            }


            // TESTING  
            Assert.AreEqual(expectedLength, actualLength);
            Assert.AreEqual(expectedPosition, actualPosition);
            Assert.AreEqual(expectedExceptionMessage, actualExceptionMessage);
        }

        #endregion Clear / Passed

    }
}
