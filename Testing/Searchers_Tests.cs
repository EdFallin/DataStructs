﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class Searchers_Tests
    {
        /* for full testing, not-found paths should be tested for all search types */

        #region Linear search

        [TestMethod()]
        public void FindLinearInTuplesArray__Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            List<Tuple<int, string>> original = new List<Tuple<int, string>>();
            original.Add(Tuple.Create(5, "not it"));
            original.Add(Tuple.Create(8, "not it"));
            original.Add(Tuple.Create(4, "not it"));
            original.Add(Tuple.Create(11, "it"));
            original.Add(Tuple.Create(1, "not it"));
            original.Add(Tuple.Create(2, "not it"));

            Tuple<int, string>[] subject = original.ToArray();

            string expected = "it";
            string actual;


            //**  exercising the code  **//
            actual = Searchers_.FindLinearInTuplesArray<int, string>(11, subject, "failed");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLinearInTuplesArray__Sought_Not_Present__Assert_Expected_Fail_Value()  /* working */  {
            //**  groundwork  **//
            List<Tuple<int, string>> original = new List<Tuple<int, string>>();
            original.Add(Tuple.Create(5, "not it"));
            original.Add(Tuple.Create(8, "not it"));
            original.Add(Tuple.Create(4, "not it"));
            original.Add(Tuple.Create(18, "it"));
            original.Add(Tuple.Create(1, "not it"));
            original.Add(Tuple.Create(2, "not it"));

            Tuple<int, string>[] subject = original.ToArray();

            string expected = "failed";
            string actual;


            //**  exercising the code  **//
            // no 11 in keys 
            actual = Searchers_.FindLinearInTuplesArray<int, string>(11, subject, "failed");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLinearInArray__Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("not it");
            original.Add("not it");
            original.Add("not it");
            original.Add("it");
            original.Add("not it");
            original.Add("not it");

            string[] subject = original.ToArray();

            string expected = "it";
            string actual;


            //**  exercising the code  **//
            actual = Searchers_.FindLinearInArray<string>("it", subject, "failed");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLinearInTuplesEnumerable__Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            List<Tuple<int, string>> subject = new List<Tuple<int, string>>();
            subject.Add(Tuple.Create(5, "not it"));
            subject.Add(Tuple.Create(8, "not it"));
            subject.Add(Tuple.Create(4, "not it"));
            subject.Add(Tuple.Create(11, "it"));
            subject.Add(Tuple.Create(1, "not it"));
            subject.Add(Tuple.Create(2, "not it"));

            string expected = "it";
            string actual;


            //**  exercising the code  **//
            actual = Searchers_.FindLinearInTuplesEnumerable<int, string>(11, subject, "failed");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLinearInEnumerable__Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            List<string> subject = new List<string>();
            subject.Add("not it");
            subject.Add("not it");
            subject.Add("not it");
            subject.Add("it");
            subject.Add("not it");
            subject.Add("not it");

            string expected = "it";
            string actual;


            //**  exercising the code  **//
            actual = Searchers_.FindLinearInEnumerable<string>("it", subject, "failed");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Linear search


        #region Binary search

        #region FindBinary<K>()

        [TestMethod()]
        public void FindBinary__No_Such__Assert_None()  /* working */  {
            //**  groundwork  **//
            List<int> original = new List<int>();
            original.Add(10);
            original.Add(15);
            original.Add(20);
            original.Add(25);
            original.Add(30);
            original.Add(35);
            original.Add(40);
            original.Add(45);
            original.Add(50);
            original.Add(55);
            original.Add(60);
            original.Add(65);
            original.Add(70);
            original.Add(75);

            int[] subject = original.ToArray();

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Each_Of_Five_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a');
            int acOfbChar = Searchers_.FindBinary(subject, 'b');
            int acOfcChar = Searchers_.FindBinary(subject, 'c');
            int acOfdChar = Searchers_.FindBinary(subject, 'd');
            int acOfeChar = Searchers_.FindBinary(subject, 'e');


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(1, acOfbChar);
            Assert.AreEqual(2, acOfcChar);
            Assert.AreEqual(3, acOfdChar);
            Assert.AreEqual(4, acOfeChar);
        }

        [TestMethod()]
        public void FindBinary__Each_Of_Ten_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[10];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a');
            int acOfbChar = Searchers_.FindBinary(subject, 'b');
            int acOfcChar = Searchers_.FindBinary(subject, 'c');
            int acOfdChar = Searchers_.FindBinary(subject, 'd');
            int acOfeChar = Searchers_.FindBinary(subject, 'e');
            int acOffChar = Searchers_.FindBinary(subject, 'f');
            int acOfgChar = Searchers_.FindBinary(subject, 'g');
            int acOfhChar = Searchers_.FindBinary(subject, 'h');
            int acOfiChar = Searchers_.FindBinary(subject, 'i');
            int acOfjChar = Searchers_.FindBinary(subject, 'j');


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(1, acOfbChar);
            Assert.AreEqual(2, acOfcChar);
            Assert.AreEqual(3, acOfdChar);
            Assert.AreEqual(4, acOfeChar);
            Assert.AreEqual(5, acOffChar);
            Assert.AreEqual(6, acOfgChar);
            Assert.AreEqual(7, acOfhChar);
            Assert.AreEqual(8, acOfiChar);
            Assert.AreEqual(9, acOfjChar);
        }

        [TestMethod()]
        public void FindBinary__Odd_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a');
            int acOfbChar = Searchers_.FindBinary(subject, 'b');
            int acOfcChar = Searchers_.FindBinary(subject, 'c');
            int acOfdChar = Searchers_.FindBinary(subject, 'd');
            int acOfeChar = Searchers_.FindBinary(subject, 'e');
            int acOffChar = Searchers_.FindBinary(subject, 'f');
            int acOfgChar = Searchers_.FindBinary(subject, 'g');
            int acOfhChar = Searchers_.FindBinary(subject, 'h');
            int acOfiChar = Searchers_.FindBinary(subject, 'i');
            int acOfjChar = Searchers_.FindBinary(subject, 'j');
            int acOfkChar = Searchers_.FindBinary(subject, 'k');


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(0, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(1, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(2, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(3, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(4, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
        }

        [TestMethod()]
        public void FindBinary__Even_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[6];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a');
            int acOfbChar = Searchers_.FindBinary(subject, 'b');
            int acOfcChar = Searchers_.FindBinary(subject, 'c');
            int acOfdChar = Searchers_.FindBinary(subject, 'd');
            int acOfeChar = Searchers_.FindBinary(subject, 'e');
            int acOffChar = Searchers_.FindBinary(subject, 'f');
            int acOfgChar = Searchers_.FindBinary(subject, 'g');
            int acOfhChar = Searchers_.FindBinary(subject, 'h');
            int acOfiChar = Searchers_.FindBinary(subject, 'i');
            int acOfjChar = Searchers_.FindBinary(subject, 'j');
            int acOfkChar = Searchers_.FindBinary(subject, 'k');
            int acOflChar = Searchers_.FindBinary(subject, 'l');
            int acOfmChar = Searchers_.FindBinary(subject, 'm');


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(0, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(1, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(2, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(3, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(4, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
            Assert.AreEqual(5, acOflChar);
            Assert.AreEqual(expNone, acOfmChar);
        }

        [TestMethod()]
        public void FindBinary__Empty_Subject__Assert_None()  /* working */  {
            //**  groundwork  **//
            int[] subject = new int[0];

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 15);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__One_Item_No_Sought__Assert_None()  /* working */  {
            //**  groundwork  **//
            int[] subject = { 10 };

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Two_Items_No_Such__Assert_None()  /* working */  {
            //**  groundwork  **//
            int[] subject = { 10, 65 };

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__One_Item_Sought_Present__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            int[] subject = { 10 };

            int expected = 0;  // the only item 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 10);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Two_Items_Sought_Present__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            int[] subject = { 10, 65 };

            int expected = 1;  // second item 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 65);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sought_Present__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("ab");
            original.Add("be");
            original.Add("ca");
            original.Add("do");
            original.Add("fa");
            original.Add("ge");
            original.Add("hu");
            original.Add("it");         // @7 
            original.Add("je");
            original.Add("lo");
            original.Add("na");
            original.Add("pe");
            original.Add("ra");
            original.Add("to");

            string[] subject = original.ToArray();

            int expected = 7;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, "it");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion FindBinary<K>()


        #region FindBinaryRecursive<K>()

        [TestMethod()]
        public void FindBinaryRecursive__No_Such__Assert_None()  /* working */  {
            //**  groundwork  **//
            List<int> original = new List<int>();
            original.Add(10);
            original.Add(15);
            original.Add(20);
            original.Add(25);
            original.Add(30);
            original.Add(35);
            original.Add(40);
            original.Add(45);
            original.Add(50);
            original.Add(55);
            original.Add(60);
            original.Add(65);
            original.Add(70);
            original.Add(75);

            int[] subject = original.ToArray();

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinaryRecursive(subject, 56);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryRecursive__Each_Of_Five_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinaryRecursive(subject, 'a');
            int acOfbChar = Searchers_.FindBinaryRecursive(subject, 'b');
            int acOfcChar = Searchers_.FindBinaryRecursive(subject, 'c');
            int acOfdChar = Searchers_.FindBinaryRecursive(subject, 'd');
            int acOfeChar = Searchers_.FindBinaryRecursive(subject, 'e');


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(1, acOfbChar);
            Assert.AreEqual(2, acOfcChar);
            Assert.AreEqual(3, acOfdChar);
            Assert.AreEqual(4, acOfeChar);
        }

        [TestMethod()]
        public void FindBinaryRecursive__Each_Of_Ten_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[10];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinaryRecursive(subject, 'a');
            int acOfbChar = Searchers_.FindBinaryRecursive(subject, 'b');
            int acOfcChar = Searchers_.FindBinaryRecursive(subject, 'c');
            int acOfdChar = Searchers_.FindBinaryRecursive(subject, 'd');
            int acOfeChar = Searchers_.FindBinaryRecursive(subject, 'e');
            int acOffChar = Searchers_.FindBinaryRecursive(subject, 'f');
            int acOfgChar = Searchers_.FindBinaryRecursive(subject, 'g');
            int acOfhChar = Searchers_.FindBinaryRecursive(subject, 'h');
            int acOfiChar = Searchers_.FindBinaryRecursive(subject, 'i');
            int acOfjChar = Searchers_.FindBinaryRecursive(subject, 'j');


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(1, acOfbChar);
            Assert.AreEqual(2, acOfcChar);
            Assert.AreEqual(3, acOfdChar);
            Assert.AreEqual(4, acOfeChar);
            Assert.AreEqual(5, acOffChar);
            Assert.AreEqual(6, acOfgChar);
            Assert.AreEqual(7, acOfhChar);
            Assert.AreEqual(8, acOfiChar);
            Assert.AreEqual(9, acOfjChar);
        }

        [TestMethod()]
        public void FindBinaryRecursive__Odd_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinaryRecursive(subject, 'a');
            int acOfbChar = Searchers_.FindBinaryRecursive(subject, 'b');
            int acOfcChar = Searchers_.FindBinaryRecursive(subject, 'c');
            int acOfdChar = Searchers_.FindBinaryRecursive(subject, 'd');
            int acOfeChar = Searchers_.FindBinaryRecursive(subject, 'e');
            int acOffChar = Searchers_.FindBinaryRecursive(subject, 'f');
            int acOfgChar = Searchers_.FindBinaryRecursive(subject, 'g');
            int acOfhChar = Searchers_.FindBinaryRecursive(subject, 'h');
            int acOfiChar = Searchers_.FindBinaryRecursive(subject, 'i');
            int acOfjChar = Searchers_.FindBinaryRecursive(subject, 'j');
            int acOfkChar = Searchers_.FindBinaryRecursive(subject, 'k');


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(0, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(1, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(2, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(3, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(4, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
        }

        [TestMethod()]
        public void FindBinaryRecursive__Even_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            char[] subject = new char[6];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinaryRecursive(subject, 'a');
            int acOfbChar = Searchers_.FindBinaryRecursive(subject, 'b');
            int acOfcChar = Searchers_.FindBinaryRecursive(subject, 'c');
            int acOfdChar = Searchers_.FindBinaryRecursive(subject, 'd');
            int acOfeChar = Searchers_.FindBinaryRecursive(subject, 'e');
            int acOffChar = Searchers_.FindBinaryRecursive(subject, 'f');
            int acOfgChar = Searchers_.FindBinaryRecursive(subject, 'g');
            int acOfhChar = Searchers_.FindBinaryRecursive(subject, 'h');
            int acOfiChar = Searchers_.FindBinaryRecursive(subject, 'i');
            int acOfjChar = Searchers_.FindBinaryRecursive(subject, 'j');
            int acOfkChar = Searchers_.FindBinaryRecursive(subject, 'k');
            int acOflChar = Searchers_.FindBinaryRecursive(subject, 'l');
            int acOfmChar = Searchers_.FindBinaryRecursive(subject, 'm');


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(0, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(1, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(2, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(3, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(4, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
            Assert.AreEqual(5, acOflChar);
            Assert.AreEqual(expNone, acOfmChar);
        }

        [TestMethod()]
        public void FindBinaryRecursive__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("ab");
            original.Add("be");
            original.Add("ca");
            original.Add("do");
            original.Add("fa");
            original.Add("ge");
            original.Add("hu");
            original.Add("it");         // @7 
            original.Add("je");
            original.Add("lo");
            original.Add("na");
            original.Add("pe");
            original.Add("ra");
            original.Add("to");

            string[] subject = original.ToArray();

            int expected = 7;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinaryRecursive(subject, "it");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion FindBinaryRecursive<K>()


        #region FindBinary<K, T>()

        [TestMethod()]
        public void FindBinaryKT__No_Such__Assert_Fail_Value()  /* working */  {
            //**  groundwork  **//
            List<KeyValuePair<int, char>> original = new List<KeyValuePair<int, char>>();
            original.Add(new KeyValuePair<int, char>(1, 'a'));
            original.Add(new KeyValuePair<int, char>(3, 'b'));
            original.Add(new KeyValuePair<int, char>(5, 'c'));
            original.Add(new KeyValuePair<int, char>(7, 'd'));
            original.Add(new KeyValuePair<int, char>(9, 'e'));
            original.Add(new KeyValuePair<int, char>(11, 'f'));
            original.Add(new KeyValuePair<int, char>(13, 'g'));
            original.Add(new KeyValuePair<int, char>(15, 'h'));
            original.Add(new KeyValuePair<int, char>(17, 'i'));
            original.Add(new KeyValuePair<int, char>(19, 'j'));
            original.Add(new KeyValuePair<int, char>(21, 'k'));
            original.Add(new KeyValuePair<int, char>(23, 'l'));
            original.Add(new KeyValuePair<int, char>(25, 'm'));
            original.Add(new KeyValuePair<int, char>(27, 'n'));

            KeyValuePair<int, char>[] subject = original.ToArray();

            char expected = 'x';
            char actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 12, 'x');


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__Each_Of_Five_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            KeyValuePair<char, int>[] subject = new KeyValuePair<char, int>[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = new KeyValuePair<char, int>(key, (i * 3));
            }

            int failValue = -1;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a', failValue);
            int acOfbChar = Searchers_.FindBinary(subject, 'b', failValue);
            int acOfcChar = Searchers_.FindBinary(subject, 'c', failValue);
            int acOfdChar = Searchers_.FindBinary(subject, 'd', failValue);
            int acOfeChar = Searchers_.FindBinary(subject, 'e', failValue);


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(3, acOfbChar);
            Assert.AreEqual(6, acOfcChar);
            Assert.AreEqual(9, acOfdChar);
            Assert.AreEqual(12, acOfeChar);
        }

        [TestMethod()]
        public void FindBinaryKT__Each_Of_Ten_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            KeyValuePair<char, string>[] subject = new KeyValuePair<char, string>[10];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('a' + i);
                subject[i] = new KeyValuePair<char, string>(
                    key, new string(key, i + 1));  // 'i + 1' because i is zero-based 
            }

            string none = "failed";


            //**  exercising the code  **//
            string acOfaChar = Searchers_.FindBinary(subject, 'a', none);
            string acOfbChar = Searchers_.FindBinary(subject, 'b', none);
            string acOfcChar = Searchers_.FindBinary(subject, 'c', none);
            string acOfdChar = Searchers_.FindBinary(subject, 'd', none);
            string acOfeChar = Searchers_.FindBinary(subject, 'e', none);
            string acOffChar = Searchers_.FindBinary(subject, 'f', none);
            string acOfgChar = Searchers_.FindBinary(subject, 'g', none);
            string acOfhChar = Searchers_.FindBinary(subject, 'h', none);
            string acOfiChar = Searchers_.FindBinary(subject, 'i', none);
            string acOfjChar = Searchers_.FindBinary(subject, 'j', none);


            //**  testing  **//
            Assert.AreEqual(new string('a', 1), acOfaChar);
            Assert.AreEqual(new string('b', 2), acOfbChar);
            Assert.AreEqual(new string('c', 3), acOfcChar);
            Assert.AreEqual(new string('d', 4), acOfdChar);
            Assert.AreEqual(new string('e', 5), acOfeChar);
            Assert.AreEqual(new string('f', 6), acOffChar);
            Assert.AreEqual(new string('g', 7), acOfgChar);
            Assert.AreEqual(new string('h', 8), acOfhChar);
            Assert.AreEqual(new string('i', 9), acOfiChar);
            Assert.AreEqual(new string('j', 10), acOfjChar);
        }

        [TestMethod()]
        public void FindBinaryKT__Odd_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            KeyValuePair<char, string>[] subject = new KeyValuePair<char, string>[5];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = new KeyValuePair<char, string>(
                    key, new string(key, i + 1));  // 'i + 1' because i is zero-based 
            }

            string expNone = "failed";


            //**  exercising the code  **//
            string acOfaChar = Searchers_.FindBinary(subject, 'a', expNone);
            string acOfbChar = Searchers_.FindBinary(subject, 'b', expNone);
            string acOfcChar = Searchers_.FindBinary(subject, 'c', expNone);
            string acOfdChar = Searchers_.FindBinary(subject, 'd', expNone);
            string acOfeChar = Searchers_.FindBinary(subject, 'e', expNone);
            string acOffChar = Searchers_.FindBinary(subject, 'f', expNone);
            string acOfgChar = Searchers_.FindBinary(subject, 'g', expNone);
            string acOfhChar = Searchers_.FindBinary(subject, 'h', expNone);
            string acOfiChar = Searchers_.FindBinary(subject, 'i', expNone);
            string acOfjChar = Searchers_.FindBinary(subject, 'j', expNone);
            string acOfkChar = Searchers_.FindBinary(subject, 'k', expNone);


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(new string('b', 1), acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(new string('d', 2), acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(new string('f', 3), acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(new string('h', 4), acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(new string('j', 5), acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
        }

        [TestMethod()]
        public void FindBinaryKT__Even_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            KeyValuePair<char, string>[] subject = new KeyValuePair<char, string>[6];

            for (int i = 0; i < subject.Length; i++) {
                char key = (char)('b' + (i * 2));
                subject[i] = new KeyValuePair<char, string>(
                    key, new string(key, i + 1));  // 'i + 1' because i is zero-based 
            }

            string expNone = "failed";


            //**  exercising the code  **//
            string acOfaChar = Searchers_.FindBinary(subject, 'a', expNone);
            string acOfbChar = Searchers_.FindBinary(subject, 'b', expNone);
            string acOfcChar = Searchers_.FindBinary(subject, 'c', expNone);
            string acOfdChar = Searchers_.FindBinary(subject, 'd', expNone);
            string acOfeChar = Searchers_.FindBinary(subject, 'e', expNone);
            string acOffChar = Searchers_.FindBinary(subject, 'f', expNone);
            string acOfgChar = Searchers_.FindBinary(subject, 'g', expNone);
            string acOfhChar = Searchers_.FindBinary(subject, 'h', expNone);
            string acOfiChar = Searchers_.FindBinary(subject, 'i', expNone);
            string acOfjChar = Searchers_.FindBinary(subject, 'j', expNone);
            string acOfkChar = Searchers_.FindBinary(subject, 'k', expNone);
            string acOflChar = Searchers_.FindBinary(subject, 'l', expNone);
            string acOfmChar = Searchers_.FindBinary(subject, 'm', expNone);


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(new string('b', 1), acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(new string('d', 2), acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(new string('f', 3), acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(new string('h', 4), acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(new string('j', 5), acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
            Assert.AreEqual(new string('l', 6), acOflChar);
            Assert.AreEqual(expNone, acOfmChar);
        }

        [TestMethod()]
        public void FindBinaryKT__Empty_Subject__Assert_Fail_Value()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, bool>[] subject = new KeyValuePair<int, bool>[0];

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 15, false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__One_Item_No_Sought__Assert_Fail_Value()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, bool>[] subject = { new KeyValuePair<int, bool>(10, true) };

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__Two_Items_No_Such__Assert_Fail_Value()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, bool>[] subject = {
                new KeyValuePair<int,bool>(10, true),
                new KeyValuePair<int,bool>(65, true),
                };

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__One_Item_Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, bool>[] subject = { new KeyValuePair<int, bool>(10, true) };

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 10, false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__Two_Items_Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, bool>[] subject = {
                new KeyValuePair<int,bool>(10, true),
                new KeyValuePair<int,bool>(65, true),
                };

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 65, false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryKT__Sought_Present__Assert_Expected_Found_Item()  /* working */  {
            //**  groundwork  **//
            List<KeyValuePair<string, bool>> original = new List<KeyValuePair<string, bool>>();
            original.Add(new KeyValuePair<string, bool>("ab", false));
            original.Add(new KeyValuePair<string, bool>("be", false));
            original.Add(new KeyValuePair<string, bool>("ca", false));
            original.Add(new KeyValuePair<string, bool>("do", false));
            original.Add(new KeyValuePair<string, bool>("fa", false));
            original.Add(new KeyValuePair<string, bool>("ge", false));
            original.Add(new KeyValuePair<string, bool>("hu", false));
            original.Add(new KeyValuePair<string, bool>("it", true));
            original.Add(new KeyValuePair<string, bool>("je", false));
            original.Add(new KeyValuePair<string, bool>("lo", false));
            original.Add(new KeyValuePair<string, bool>("na", false));
            original.Add(new KeyValuePair<string, bool>("pe", false));
            original.Add(new KeyValuePair<string, bool>("ra", false));
            original.Add(new KeyValuePair<string, bool>("to", false));

            KeyValuePair<string, bool>[] subject = original.ToArray();

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, "it", false);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion FindBinary<K, T>()


        #region FindBinary<K>() sliced

        [TestMethod()]
        public void FindBinary__Sliced__No_Such__Assert_None()  /* working */  {
            //**  groundwork  **//
            List<int> original = new List<int>();

            // outside the slice 
            original.Add(-10);
            original.Add(0);

            // in the slice 
            original.Add(10);
            original.Add(15);
            original.Add(20);
            original.Add(25);
            original.Add(30);

            original.Add(35);
            original.Add(40);
            original.Add(45);
            original.Add(50);
            original.Add(55);

            original.Add(60);
            original.Add(65);
            original.Add(70);
            original.Add(75);

            // outside the slice 
            original.Add(80);
            original.Add(85);
            original.Add(90);

            int[] subject = original.ToArray();

            int bottom = 2;
            int top = 16;


            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Each_Of_Five_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            // 5 used, from 2 to 6 
            char[] subject = new char[9];
            int bottom = 2;
            int top = 6;

            // '<=' because slice includes top index; @added to preserve expected chars 
            for (int i = bottom, added = 0; i <= top; i++, added++) {
                char key = (char)('a' + added);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a', bottom, top);
            int acOfbChar = Searchers_.FindBinary(subject, 'b', bottom, top);
            int acOfcChar = Searchers_.FindBinary(subject, 'c', bottom, top);
            int acOfdChar = Searchers_.FindBinary(subject, 'd', bottom, top);
            int acOfeChar = Searchers_.FindBinary(subject, 'e', bottom, top);


            //**  testing  **//
            Assert.AreEqual(2, acOfaChar);
            Assert.AreEqual(3, acOfbChar);
            Assert.AreEqual(4, acOfcChar);
            Assert.AreEqual(5, acOfdChar);
            Assert.AreEqual(6, acOfeChar);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Each_Of_Ten_Searched__Assert_All_Found()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            // 10 used, from 0 to 9 
            char[] subject = new char[12];
            int bottom = 0;
            int top = 9;

            // '<=' because slice includes top index; @added to preserve expected chars 
            for (int i = bottom, added = 0; i <= top; i++, added++) {
                char key = (char)('a' + added);
                subject[i] = key;
            }


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a', bottom, top);
            int acOfbChar = Searchers_.FindBinary(subject, 'b', bottom, top);
            int acOfcChar = Searchers_.FindBinary(subject, 'c', bottom, top);
            int acOfdChar = Searchers_.FindBinary(subject, 'd', bottom, top);
            int acOfeChar = Searchers_.FindBinary(subject, 'e', bottom, top);
            int acOffChar = Searchers_.FindBinary(subject, 'f', bottom, top);
            int acOfgChar = Searchers_.FindBinary(subject, 'g', bottom, top);
            int acOfhChar = Searchers_.FindBinary(subject, 'h', bottom, top);
            int acOfiChar = Searchers_.FindBinary(subject, 'i', bottom, top);
            int acOfjChar = Searchers_.FindBinary(subject, 'j', bottom, top);


            //**  testing  **//
            Assert.AreEqual(0, acOfaChar);
            Assert.AreEqual(1, acOfbChar);
            Assert.AreEqual(2, acOfcChar);
            Assert.AreEqual(3, acOfdChar);
            Assert.AreEqual(4, acOfeChar);
            Assert.AreEqual(5, acOffChar);
            Assert.AreEqual(6, acOfgChar);
            Assert.AreEqual(7, acOfhChar);
            Assert.AreEqual(8, acOfiChar);
            Assert.AreEqual(9, acOfjChar);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Odd_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an odd number of elements checks 
             * for one class of indexing / bounding mistakes */

            //**  groundwork  **//
            // 5 used, from 5 to 9 
            char[] subject = new char[10];
            int bottom = 5;
            int top = 9;

            // '<=' because slice includes top index; @added to preserve expected chars 
            for (int i = bottom, added = 0; i <= top; i++, added++) {
                char key = (char)('b' + (added * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a', bottom, top);
            int acOfbChar = Searchers_.FindBinary(subject, 'b', bottom, top);
            int acOfcChar = Searchers_.FindBinary(subject, 'c', bottom, top);
            int acOfdChar = Searchers_.FindBinary(subject, 'd', bottom, top);
            int acOfeChar = Searchers_.FindBinary(subject, 'e', bottom, top);
            int acOffChar = Searchers_.FindBinary(subject, 'f', bottom, top);
            int acOfgChar = Searchers_.FindBinary(subject, 'g', bottom, top);
            int acOfhChar = Searchers_.FindBinary(subject, 'h', bottom, top);
            int acOfiChar = Searchers_.FindBinary(subject, 'i', bottom, top);
            int acOfjChar = Searchers_.FindBinary(subject, 'j', bottom, top);
            int acOfkChar = Searchers_.FindBinary(subject, 'k', bottom, top);


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(5, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(6, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(7, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(8, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(9, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Even_Alternating_Present_And_Not__Assert_All_Present_Found_All_Others_Not()  /* working */  {
            /* testing a subject with an even number of elements checks 
             * for another class of indexing / bounding mistakes */

            //**  groundwork  **//
            // 6 used, from 25 to 30 
            char[] subject = new char[40];
            int bottom = 25;
            int top = 30;

            // '<=' because slice includes top index; @added to preserve expected chars 
            for (int i = bottom, added = 0; i <= top; i++, added++) {
                char key = (char)('b' + (added * 2));
                subject[i] = key;
            }

            int expNone = Searchers_.None;


            //**  exercising the code  **//
            int acOfaChar = Searchers_.FindBinary(subject, 'a', bottom, top);
            int acOfbChar = Searchers_.FindBinary(subject, 'b', bottom, top);
            int acOfcChar = Searchers_.FindBinary(subject, 'c', bottom, top);
            int acOfdChar = Searchers_.FindBinary(subject, 'd', bottom, top);
            int acOfeChar = Searchers_.FindBinary(subject, 'e', bottom, top);
            int acOffChar = Searchers_.FindBinary(subject, 'f', bottom, top);
            int acOfgChar = Searchers_.FindBinary(subject, 'g', bottom, top);
            int acOfhChar = Searchers_.FindBinary(subject, 'h', bottom, top);
            int acOfiChar = Searchers_.FindBinary(subject, 'i', bottom, top);
            int acOfjChar = Searchers_.FindBinary(subject, 'j', bottom, top);
            int acOfkChar = Searchers_.FindBinary(subject, 'k', bottom, top);
            int acOflChar = Searchers_.FindBinary(subject, 'l', bottom, top);
            int acOfmChar = Searchers_.FindBinary(subject, 'm', bottom, top);


            //**  testing  **//
            Assert.AreEqual(expNone, acOfaChar);
            Assert.AreEqual(25, acOfbChar);
            Assert.AreEqual(expNone, acOfcChar);
            Assert.AreEqual(26, acOfdChar);
            Assert.AreEqual(expNone, acOfeChar);
            Assert.AreEqual(27, acOffChar);
            Assert.AreEqual(expNone, acOfgChar);
            Assert.AreEqual(28, acOfhChar);
            Assert.AreEqual(expNone, acOfiChar);
            Assert.AreEqual(29, acOfjChar);
            Assert.AreEqual(expNone, acOfkChar);
            Assert.AreEqual(30, acOflChar);
            Assert.AreEqual(expNone, acOfmChar);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Empty_Subject__Assert_None()  /* working */  {
            /* searching a zero-length slice, which requires illogical args */

            //**  groundwork  **//
            int[] subject = new int[10];  // all 0 

            // args of the same index produce a 1-item slice, so [7, 6] here instead of the expected [7, 7] 
            int bottom = 7;
            int top = 6;

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 15, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__One_Item_No_Sought__Assert_None()  /* working */  {
            /* searching a 1-item slice of a larger @subject */

            //**  groundwork  **//
            int[] subject = { 0, 10, 20 };

            // args of the same index produce a 1-item slice 
            int bottom = 1;
            int top = 1;

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Two_Items_No_Such__Assert_None()  /* working */  {
            /* searching a 2-item slice of a larger @subject */

            //**  groundwork  **//
            int[] subject = { 10, 54, 65, 79 };

            int bottom = 1;
            int top = 2;

            int expected = Searchers_.None;  // -1 
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__One_Item_Sought_Present__Assert_Expected_Index()  /* working */  {
            /* searching a 1-item slice of a larger @subject */

            //**  groundwork  **//
            int[] subject = { 0, 3, 10, 25 };

            // args of the same index produce a 1-item slice 
            int bottom = 2;
            int top = 2;

            int expected = 2;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 10, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Two_Items_Sought_Present__Assert_Expected_Index()  /* working */  {
            /* searching a 2-item slice of a larger @subject */

            //**  groundwork  **//
            int[] subject = { 10, 20, 33, 54, 56, 65, 71, 79, 85 };

            int bottom = 4;
            int top = 5;

            int expected = 4;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, 56, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Sought_Below_Slice__Assert_None()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("ab");
            original.Add("be");
            original.Add("ca");
            original.Add("do");
            original.Add("fa");
            original.Add("ge");
            original.Add("hu");
            original.Add("it");         // @7 
            original.Add("je");
            original.Add("lo");
            original.Add("na");
            original.Add("pe");
            original.Add("ra");
            original.Add("to");

            string[] subject = original.ToArray();

            int bottom = 8;
            int top = 12;

            int expected = Searchers_.None;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, "it", bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Sought_Above_Slice__Assert_None()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("ab");
            original.Add("be");
            original.Add("ca");
            original.Add("do");
            original.Add("fa");
            original.Add("ge");
            original.Add("hu");
            original.Add("it");         // @7 
            original.Add("je");
            original.Add("lo");
            original.Add("na");
            original.Add("pe");
            original.Add("ra");
            original.Add("to");

            string[] subject = original.ToArray();

            int bottom = 1;
            int top = 6;

            int expected = Searchers_.None;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, "it", bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinary__Sliced__Sought_Present__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            List<string> original = new List<string>();
            original.Add("ab");
            original.Add("be");
            original.Add("ca");
            original.Add("do");
            original.Add("fa");
            original.Add("ge");
            original.Add("hu");
            original.Add("it");         // @7 
            original.Add("je");
            original.Add("lo");
            original.Add("na");
            original.Add("pe");
            original.Add("ra");
            original.Add("to");

            string[] subject = original.ToArray();

            int bottom = 1;
            int top = 9;

            int expected = 7;
            int actual;


            //**  exercising the code  **//
            actual = Searchers_.FindBinary(subject, "it", bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion FindBinary<K>() sliced


        #region FindBinaryClosestBelow<K>()

        [TestMethod()]
        public void FindBinaryClosestBelow__Below_Any__Assert_Minus_One()  /* working */  {
            /* search returns index of exact match if any, else next-lower */

            //**  groundwork  **//
            char[] subject = new char[26];

            for (int i = 0; i < 26; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject[i] = letter;
            }

            /* '5' is lower than any letters, so no next-lower in subject */
            int expected = -1;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestBelow(subject, '5');


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Above_Any__Assert_Top()  /* working */  {
            /*  search returns index of exact match if any, else highest below; 
             *  when sought is above all, highest below is top  */

            //**  groundwork  **//
            int[] subject = new int[10];

            for (int i = 0; i < 10; i++) {
                subject[i] = i * 25;
            }

            int expected = 9;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestBelow(subject, 400);  // top is 225 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /* testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[20];

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject */
            for (int i = 9; i >= 0; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, i * 17);
                Assert.AreEqual(i, actual);
            }

            /* high half of subject */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, i * 17);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Closest_Below_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns highest el that is <= sought; 
             *  testing all for thoroughness  */

            //**  groundwork  **//
            int[] subject = new int[20];

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject; each sought is slightly higher than an el */
            for (int i = 9; i >= 0; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, (i * 17) + 2);
                Assert.AreEqual(i, actual);
            }

            /* below low half of subject; .None / -1 is correct result */
            actual = Searchers_.FindBinaryClosestBelow(subject, -3);
            Assert.AreEqual(-1, actual);

            /* high half of subject; each sought is slightly higher than an el */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, (i * 17) + 5);
                Assert.AreEqual(i, actual);
            }
        }

        #endregion FindBinaryClosestBelow<K>()


        #region FindBinaryClosestAbove<K>()

        [TestMethod()]
        public void FindBinaryClosestAbove__Above_Any__Assert_Minus_One()  /* working */  {
            /* search returns index of exact match if any, else next-higher */

            //**  groundwork  **//
            char[] subject = new char[10];

            for (int i = 0; i < 10; i++) {
                char letter = (char)('1' + i);   // C-style 
                subject[i] = letter;
            }

            /* all numbers are ASCII-lower than any letters, so no next-higher */
            int expected = -1;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestAbove(subject, 'c');


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Below_Any__Assert_Bottom()  /* working */  {
            /*  search returns index of exact match if any, else lowest above; 
             *  when sought is below all, lowest above is bottom el */

            //**  groundwork  **//
            int[] subject = new int[10];

            for (int i = 0; i < 10; i++) {
                subject[i] = (i * 25) + 20;
            }

            int expected = 0;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestAbove(subject, 10);   // lowest is 20 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /* testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[20];

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject */
            for (int i = 9; i >= 0; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, i * 17);
                Assert.AreEqual(i, actual);
            }

            /* high half of subject */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, i * 17);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Closest_Above_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns lowest el that is >= sought; 
             *  testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[20];

            for (int i = 0; i < 20; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject; each sought is slightly lower than an el */
            for (int i = 9; i >= 0; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, (i * 17) - 2);
                Assert.AreEqual(i, actual);
            }

            /* value higher than any; returns fail value */
            actual = Searchers_.FindBinaryClosestAbove(subject, 350);   // higher than top el, 340 
            Assert.AreEqual(-1, actual);

            /* high half of subject; each sought is slightly lower than an el */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, (i * 17) - 5);
                Assert.AreEqual(i, actual);
            }
        }

        #endregion FindBinaryClosestAbove<K>()


        #region FindBinaryClosestBelow<K>() sliced

        [TestMethod()]
        public void FindBinaryClosestBelow__Sliced__Below_Any__Assert_Minus_One()  /* working */  {
            /* search returns index within slice of exact match if any, 
             * else next-lower if sought would be within slice */

            //**  groundwork  **//
            char[] subject = new char[50];

            for (int i = 0; i < subject.Length; i++) {
                char letter = (char)('3' + i);   // C-style 
                subject[i] = letter;
            }

            // arbitrary non-entirety slice that doesn't include the sought 
            int bottom = 5;
            int top = subject.Length - 3;

            /* '5' is lower than any chars in slice, so no next-lower in subject */
            int expected = -1;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestBelow(subject, '5', bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Sliced__Above_Any__Assert_Top()  /* working */  {
            /*  search returns index of exact match if any, else highest below; 
             *  when sought is above all, highest below is top  */

            //**  groundwork  **//
            int[] subject = new int[25];

            for (int i = 0; i < subject.Length; i++) {
                subject[i] = i * 25;
            }

            // arbitrary non-entirety slice that doesn't include the sought 
            int bottom = 2;
            int top = 9;

            int expected = 9;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestBelow(subject, 400, bottom, top);  // top in slice is 225 


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Sliced__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /* testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[30];

            for (int i = 0; i < subject.Length; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }

            int bottom = 3;
            int top = 20;


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject */
            for (int i = 9; i >= bottom; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, i * 17, bottom, top);
                Assert.AreEqual(i, actual);
            }

            /* high half of subject */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, i * 17, bottom, top);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void FindBinaryClosestBelow__Sliced__Closest_Below_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns highest el that is <= sought; 
             *  testing all for thoroughness  */

            //**  groundwork  **//
            int[] subject = new int[30];

            for (int i = 0; i < subject.Length; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }

            int bottom = 5;
            int top = 25;


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject; each sought is slightly higher than an el */
            for (int i = 9; i >= bottom; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, (i * 17) + 2, bottom, top);
                Assert.AreEqual(i, actual);
            }

            /* below low half of subject; .None / -1 is correct result */
            actual = Searchers_.FindBinaryClosestBelow(subject, -3, bottom, top);
            Assert.AreEqual(-1, actual);

            /* high half of subject; each sought is slightly higher than an el */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestBelow(subject, (i * 17) + 5, bottom, top);
                Assert.AreEqual(i, actual);
            }
        }

        #endregion FindBinaryClosestBelow<K>() sliced


        #region FindBinaryClosestAbove<K>() sliced

        [TestMethod()]
        public void FindBinaryClosestAbove__Sliced__Above_Any__Assert_Minus_One()  /* working */  {
            /* search returns index of exact match if any, else next-higher */

            //**  groundwork  **//
            char[] subject = new char[26];

            for (int i = 0; i < subject.Length; i++) {
                char letter = (char)('a' + i);   // C-style 
                subject[i] = letter;
            }

            int bottom = 3;
            int top = 10;

            /* all letters in range are below 'x', so no next-higher */
            int expected = -1;


            //**  exercising the code  **//
            int actual = Searchers_.FindBinaryClosestAbove(subject, 'x', bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Sliced__Below_Any__Assert_Bottom()  /* working */  {
            /*  search returns index of exact match if any, else lowest above; 
             *  when sought is below all, lowest above is bottom el */

            //**  groundwork  **//
            int[] subject = new int[15];

            for (int i = 0; i < subject.Length; i++) {
                subject[i] = (i * 25) + 20;
            }

            int bottom = 3;
            int top = 12;

            int expected = 3;  // the lowest in the slice, instead of 0 over the whole sought 


            //**  exercising the code  **//
            // lowest in sought is 20, at 0, but lowest in slice is 95, at 3 
            int actual = Searchers_.FindBinaryClosestAbove(subject, 20, bottom, top);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Sliced__Exact_Match_All__Assert_Expected_Offsets()  /* working */  {
            /* testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[30];

            for (int i = 0; i < subject.Length; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }

            int bottom = 6;
            int top = 20;


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject */
            for (int i = 9; i >= bottom; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, i * 17, bottom, top);
                Assert.AreEqual(i, actual);
            }

            /* high half of subject */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, i * 17, bottom, top);
                Assert.AreEqual(i, actual);
            }
        }

        [TestMethod()]
        public void FindBinaryClosestAbove__Sliced__Closest_Above_All__Assert_Expected_Offsets()  /* working */  {
            /*  search returns lowest el that is >= sought; 
             *  testing all for thoroughness */

            //**  groundwork  **//
            int[] subject = new int[30];

            for (int i = 0; i < subject.Length; i++) {
                int element = i * 17;   // 0, 17, 34, 51, 68, ...
                subject[i] = element;
            }

            int bottom = 7;
            int top = 20;


            //**  exercising the code and testing  **//
            /* not in original order, to make me feel better */
            int actual;

            /* low half of subject; each sought is slightly lower than an el */
            for (int i = 9; i >= bottom; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, (i * 17) - 2, bottom, top);
                Assert.AreEqual(i, actual);
            }

            /* value higher than any; returns fail value */
            actual = Searchers_.FindBinaryClosestAbove(subject, 350, bottom, top);   // higher than top el in slice, 340 
            Assert.AreEqual(-1, actual);

            /* high half of subject; each sought is slightly lower than an el */
            for (int i = 19; i >= 10; i--) {
                actual = Searchers_.FindBinaryClosestAbove(subject, (i * 17) - 5, bottom, top);
                Assert.AreEqual(i, actual);
            }
        }

        #endregion FindBinaryClosestAbove<K>() sliced

        #endregion Binary search


        #region Self-organizing -list search

        [TestMethod()]
        public void FindSelfOrganize_Test_3_Asserts_2_Expected_Found_Items_And_Final_Internal_List_Order()  /* working */  {
            //**  groundwork  **//
            List<KeyValuePair<int, string>> original = new List<KeyValuePair<int, string>>();
            original.Add(new KeyValuePair<int, string>(8, "not it"));
            original.Add(new KeyValuePair<int, string>(4, "not it"));
            original.Add(new KeyValuePair<int, string>(25, "second and fourth"));
            original.Add(new KeyValuePair<int, string>(1, "not it"));
            original.Add(new KeyValuePair<int, string>(53, "not it"));
            original.Add(new KeyValuePair<int, string>(5, "first and third"));
            original.Add(new KeyValuePair<int, string>(9, "not it"));
            original.Add(new KeyValuePair<int, string>(3, "not it"));
            original.Add(new KeyValuePair<int, string>(48, "not it"));
            original.Add(new KeyValuePair<int, string>(125, "not it"));

            original.Add(new KeyValuePair<int, string>(15, "not it"));
            original.Add(new KeyValuePair<int, string>(18, "not it"));
            original.Add(new KeyValuePair<int, string>(14, "not it"));
            original.Add(new KeyValuePair<int, string>(11, "fifth"));
            original.Add(new KeyValuePair<int, string>(101, "not it"));
            original.Add(new KeyValuePair<int, string>(153, "not it"));
            original.Add(new KeyValuePair<int, string>(111, "not it"));
            original.Add(new KeyValuePair<int, string>(13, "not it"));
            original.Add(new KeyValuePair<int, string>(19, "not it"));
            original.Add(new KeyValuePair<int, string>(480, "not it"));

            KeyValuePair<int, string>[] subject = original.ToArray();

            string expectedFourthFind = "second and fourth";
            string expectedFifthFind = "fifth";
            int[] expectedOrdering = { 25, 5, 8, 4, 53, 1, 9, 3, 48, 125, 15, 11, 14, 18, 101, 153, 111, 13, 19, 480 };

            string actualFourthFind;
            string actualFifthFind;
            int[] actualOrdering = new int[20];


            #region Testing Algo

            /* before:  8, 4, 25, 1, 53, 5, 9, 3, 48, 125, 15, 18, 14, 11, 101, 153, 111, 13, 19, 480 */

            /* rearranges by 2 slots, 1/10 of total length (20); rearrangements are asterisked below */

            /* 1st:  8, 4, 25, *5, 53, *1, 9, 3, 48, 125, 15, 18, 14, 11, 101, 153, 111, 13, 19, 480 */
            /* 2nd:  *25, 4, *8, 5, 53, 1, 9, 3, 48, 125, 15, 18, 14, 11, 101, 153, 111, 13, 19, 480 */
            /* 3rd:  25, *5, 8, *4, 53, 1, 9, 3, 48, 125, 15, 18, 14, 11, 101, 153, 111, 13, 19, 480 */
            /* 4th:  *25, 5, 8, 4, 53, 1, 9, 3, 48, 125, 15, 18, 14, 11, 101, 153, 111, 13, 19, 480  (no changes) */
            /* 5th:  25, 5, 8, 4, 53, 1, 9, 3, 48, 125, 15, *11, 14, *18, 101, 153, 111, 13, 19, 480 */

            /* after:  25, 5, 8, 4, 53, 1, 9, 3, 48, 125, 15, 11, 14, 18, 101, 153, 111, 13, 19, 480 */

            #endregion Testing Algo


            //**  exercising the code  **//
            // 5 searches; return values from first 3 ignored; checking ordering results of these only 
            Searchers_.FindSelfOrganize(5, subject, "fail");
            Searchers_.FindSelfOrganize(25, subject, "fail");
            Searchers_.FindSelfOrganize(5, subject, "fail");
            actualFourthFind = Searchers_.FindSelfOrganize(25, subject, "fail");
            actualFifthFind = Searchers_.FindSelfOrganize(11, subject, "fail");


            //**  testing  **//
            Assert.AreEqual(expectedFourthFind, actualFourthFind);
            Assert.AreEqual(expectedFifthFind, actualFifthFind);

            for (int ix = 0; ix < subject.Length; ix++) {
                actualOrdering[ix] = subject[ix].Key;
            }

            CollectionAssert.AreEqual(expectedOrdering, actualOrdering);
        }

        #endregion Self-organizing -list search


        #region Bitmap search

        [TestMethod()]
        public void Digest_Test_Assert_Expected_BitArray_Values()  /* working */  {
            //**  groundwork  **//
            char[] subject = new char[] { 'a', 'K', 'l', 'F', 'j', 'M', 'H', 'c' };   // not in same order as `topics 
            char[] topics = new char[] { 'a', 'B', 'c', 'D', 'e', 'F', 'g', 'H' };

            BitArray expected = new BitArray(new bool[] { true, false, true, false, false, true, false, true });
            BitArray actual;


            //**  exercising the code  **//
            actual = Searchers_.Digest<char>(subject, topics);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindInDigest_Test_Topic_Present_Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            char[] subject = new char[] { 'a', 'K', 'l', 'F', 'j', 'M', 'H', 'c' };   // not in same order as `topics 
            char[] topics = new char[] { 'a', 'B', 'c', 'D', 'e', 'F', 'g', 'H' };
            /* presence in subject:  true, false, true, false, false, true, false, true */

            BitArray digest = Searchers_.Digest<char>(subject, topics);

            char sought = 'c';

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindInDigest<char>(digest, topics, sought);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindInDigest_Test_Topic_Not_Present_Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            char[] subject = new char[] { 'a', 'K', 'l', 'F', 'j', 'M', 'H', 'c' };   // not in same order as `topics 
            char[] topics = new char[] { 'a', 'B', 'c', 'D', 'e', 'F', 'g', 'H' };
            /* presence in subject:  true, false, true, false, false, true, false, true */

            BitArray digest = Searchers_.Digest<char>(subject, topics);

            char sought = 'B';

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = Searchers_.FindInDigest<char>(digest, topics, sought);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TopicsAreInDigest_Test_All_Topics_Present_Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            char[] subject = new char[] { 'a', 'K', 'l', 'F', 'j', 'M', 'H', 'c' };   // not in same order as `topics 
            char[] topics = new char[] { 'a', 'B', 'c', 'D', 'e', 'F', 'g', 'H' };
            BitArray digest = Searchers_.Digest<char>(subject, topics);
            /* presence in subject:  true, false, true, false, false, true, false, true */


            BitArray soughts = Searchers_.Digest<char>(new char[] { 'a', 'c', 'F' }, topics);   // all are present 

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            // `topics not needed here, because this is a bit-to-bit comparison of predigested content 
            actual = Searchers_.TopicsAreInDigest<char>(digest, soughts);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TopicsAreInDigest_Test_Not_All_Topics_Present_Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            char[] subject = new char[] { 'a', 'K', 'l', 'F', 'j', 'M', 'H', 'c' };   // not in same order as `topics 
            char[] topics = new char[] { 'a', 'B', 'c', 'D', 'e', 'F', 'g', 'H' };
            BitArray digest = Searchers_.Digest<char>(subject, topics);
            /* presence in subject:  true, false, true, false, false, true, false, true */

            BitArray soughts = Searchers_.Digest<char>(new char[] { 'a', 'c', 'g' }, topics);   // last is not present 

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            // `topics not needed here, because this is a bit-to-bit comparison of predigested content 
            actual = Searchers_.TopicsAreInDigest<char>(digest, soughts);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Bitmap search


        #region Text fixtures

        #region Friendlies

        public static int KeyValuePairComparer<K, T>(KeyValuePair<K, T> first, KeyValuePair<K, T> second)  /* passed */  {
            /* compares by keys, assumed to be comparable */

            IComparable firstKey = (IComparable)first.Key;
            IComparable secondKey = (IComparable)second.Key;

            return firstKey.CompareTo(secondKey);
        }

        #endregion Friendlies


        #region Tests of friendlies

        [TestMethod()]
        public void KeyValuePairComparer_Test_3_Asserts_Each_Expected_Outcome_Of_Comparing_Keys()  /* working */  {
            //**  groundwork  **//
            KeyValuePair<int, string> first = new KeyValuePair<int, string>(2, "two");
            KeyValuePair<int, string> second = new KeyValuePair<int, string>(3, "three");
            KeyValuePair<int, string> third = new KeyValuePair<int, string>(3, "three");

            int expected;
            int actual;


            //**  exercising the code and testing  **//
            // first arg < second arg 
            expected = -1;
            actual = Searchers_Tests.KeyValuePairComparer<int, string>(first, second);
            Assert.AreEqual(expected, actual);

            // first arg > second arg 
            expected = 1;
            actual = Searchers_Tests.KeyValuePairComparer<int, string>(second, first);
            Assert.AreEqual(expected, actual);

            // first arg == second arg 
            expected = 0;
            actual = Searchers_Tests.KeyValuePairComparer<int, string>(second, third);
            Assert.AreEqual(expected, actual);
        }

        #endregion Tests of friendlies

        #endregion Text fixtures

    }
}
