﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BinaryTree_KVPs_Plus_Tests
    {
        #region Friendlies

        public object GatherKeys(object topic_object, CompositePointerNode_<char, int> node)  /* verified */  {
            string topic = topic_object as string;

            // first invocation only 
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(node.Key);

            return topic;
        }

        #endregion Friendlies


        #region Basic Tests

        [TestMethod()]
        public void PolyMethod_AddNode_Children_NodeProperties_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            //**  exercising the code  **//
            // 
            // elements are added directly using nodes 
            BinaryTree_KVPs_Plus_<char, int> target = new BinaryTree_KVPs_Plus_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            // 4-arg constructor inits inodes; 2-arg constructor inits leaves 

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);


            //**  testing  **//
            current = null;

            // root, an inode 
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            current = target.Root;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode 
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            current = current.RightChild;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            current = target.Root;
            current = current.LeftChild;
            current = current.LeftChild;

            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        #endregion Basic Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            BinaryTree_KVPs_Plus_<char, int> target = new BinaryTree_KVPs_Plus_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);


            string expected = "abdecfg";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraversePreorder(GatherKeys);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            BinaryTree_KVPs_Plus_<char, int> target = new BinaryTree_KVPs_Plus_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);

            string expected = "dbeafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraverseInorder(GatherKeys);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            BinaryTree_KVPs_Plus_<char, int> target = new BinaryTree_KVPs_Plus_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);

            string expected = "debfgca";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraversePostorder(GatherKeys);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals


        #region Indexing By Offset

        /* --> let's get this unit and/or test working, or else trash the tested class, which is an extended learning */
        //[TestMethod()]
        //public void IndexingByInteger_Test_Assert_Expecteds()  /* working */  {
        //   //**  groundwork  **//
        //   BinaryTree_KVPs_Plus_<char, int> target = new BinaryTree_KVPs_Plus_<char, int>('a', 100);

        //   CompositePointerNode_<char, int> current;

        //   current = target.Root;
        //   current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

        //   current = current.LeftChild;
        //   current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
        //   current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

        //   current = target.Root;
        //   current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

        //   current = current.RightChild;
        //   current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
        //   current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);

        //   char[] indexedInOrderKeys = { 'd', 'b', 'e', 'a', 'f', 'c', 'g' };
        //   int[] indexedInOrderElements = { 25, 50, 75, 100, 125, 150, 175 };

        //   char[] expectedKeys = { 'd', 'e', 'f' };
        //   char[] actualKeys = new char[3];

        //   int[] expectedElements = { 25, 75, 125 };
        //   int[] actualElements = new int[3];

        //   CompositePointerNode_<char, int> actual = null;


        //   //**  exercising the code  **//
        //   for (int ix = 0, locator = 0; ix < 5; ix += 2, locator++) {
        //       actual = target[ix];
        //       actualKeys[locator] = actual.Key;
        //       actualElements[locator] = actual.Element;
        //   }


        //   //**  testing  **//
        //   CollectionAssert.AreEqual(expectedKeys, actualKeys);
        //   CollectionAssert.AreEqual(expectedElements, actualElements);
        //}

        #endregion Indexing By Offset


        #region Indexing By Key

        #endregion Indexing By Key


        /* not implementing enumeration: devolves into enumeration of a list after a full traversal */
    }
}
