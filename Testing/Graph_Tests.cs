﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DataStructs;

namespace Testing
{
    [TestClass]
    public class Graph_Tests
    {
        #region Fixtures

        public int _CountAt(PrivateObject p, int nearEnd)  /* verified */  {
            Graph_.Edge edge = (Graph_.Edge)p.GetArrayElement("_edges", nearEnd);

            int count = 0;

            while (edge != null) {
                count++;
                edge = edge.Next;
            }

            return count;
        }

        private T[] _ArrayFromVector<T>(List_Vector_<T> vector)  /* verified */  {
            T[] array = new T[vector.Length];

            for (int of = 0; of < vector.Length; of++) {
                vector.PositionAtIndex(of);
                array[of] = vector.CurrentValue;
            }

            return array;
        }

        #endregion Fixtures


        #region Graph_.Edge constructors

        [TestMethod()]
        public void Constructor_Int_Int__Undirected__Knowns__Assert_Correct_Properties()  /* working */  {
            //**  groundwork  **//
            int far = 7;
            int weight = 12;


            //**  exercising the code  **//
            Graph_.Edge actual = new Graph_.Edge(far, weight);


            //**  testing  **//
            Assert.AreEqual(far, actual.FarEnd);
            Assert.AreEqual(weight, actual.Weight);
        }

        [TestMethod()]
        public void Constructor_Int__Undirected__Knowns__Assert_Correct_Properties()  /* working */  {
            /* when no weight is provided, the graph is assumed to be unweighted, 
             * with the appropriate default "weight" as defined on Graph_ */

            //**  groundwork  **//
            int far = 7;


            //**  exercising the code  **//
            Graph_.Edge actual = new Graph_.Edge(far);


            //**  testing  **//
            Assert.AreEqual(far, actual.FarEnd);
            Assert.AreEqual(Graph_.Unweighted, actual.Weight);
        }

        #endregion Graph_.Edge constructors


        #region HasEdge()

        #region On adjacency-list representation

        /* these tests are written using undirected graphs, but it shouldn't matter the way they're written */

        [TestMethod()]
        public void HasEdge__Undirected__No_Such_Edge__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            int near = 3;
            int far = 6;

            Graph_ graph = new Graph_(11, false);
            PrivateObject p = new PrivateObject(graph);

            // adding two edges as a linked list, but neither is the targeted edge 
            Graph_.Edge otherEdge = new Graph_.Edge(12);
            Graph_.Edge targetEdge = new Graph_.Edge(9);
            otherEdge.Next = targetEdge;

            p.SetArrayElement("_edges", otherEdge, near);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HasEdge__Undirected__Does_Have_Edge__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            int near = 3;
            int far = 6;

            Graph_ graph = new Graph_(11, false);
            PrivateObject p = new PrivateObject(graph);

            // adding two edges as a linked list, so this is not the trivial case 
            Graph_.Edge otherEdge = new Graph_.Edge(12);
            Graph_.Edge targetEdge = new Graph_.Edge(far);
            otherEdge.Next = targetEdge;

            p.SetArrayElement("_edges", otherEdge, near);

            bool expected = true;


            //**  exercising the code  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion On adjacency-list representation


        #region On undirected graphs

        [TestMethod()]
        public void HasEdge__Undirected__No_Edges__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            int near = 3;
            int far = 6;

            Graph_ graph = new Graph_(11, false);

            /* no edges are set at all */

            bool expected = false;


            //**  exercising the code  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void HasEdge__Undirected__High_To_Low_Vertex_Order__Assert_True_In_Both_Vertex_Orders()  /* working */  {
            /* to simplify my life, edges in undirected graphs are only 
             * stored and accessed with vertices from low to high */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 10;
            int far = 7;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
            Assert.AreEqual(true, graph.HasEdge(far, near));
        }

        [TestMethod()]
        public void HasEdge__Undirected__Low_To_High_Vertex_Order__Assert_True_In_Both_Vertex_Orders()  /* working */  {
            /* to simplify my life, edges in undirected graphs are only 
             * stored and accessed with vertices from low to high */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 7;
            int far = 10;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
            Assert.AreEqual(true, graph.HasEdge(far, near));
        }

        #endregion On undirected graphs, and representation


        #region On directed graphs

        /* not retesting things here that are the same as on undirected graphs */

        [TestMethod()]
        public void HasEdge__Directed__Low_Vertex_First__Assert_True_Only_In_Same_Order()  /* working */  {
            /* in a directed graph, the vertex order given is used to establish edge direction, 
             * so edges defined from higher to lower vertices are kept in that order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, true);
            int near = 7;
            int far = 10;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
            Assert.AreEqual(false, graph.HasEdge(far, near));
        }

        [TestMethod()]
        public void HasEdge__Directed__High_Vertex_First__Assert_True_Only_In_Same_Order()  /* working */  {
            /* in a directed graph, the vertex order given is used to establish edge direction, 
             * so edges defined from higher to lower vertices are kept in that order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, true);
            int near = 10;
            int far = 7;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
            Assert.AreEqual(false, graph.HasEdge(far, near));
        }

        #endregion On directed graphs

        #endregion HasEdge()


        #region SetEdge(int, int, int)

        #region Adding a new edge

        #region On undirected graphs

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__First_New_Edge__Assert_Edge_Present()  /* working */  {
            /* first new edge must be set as head of list */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 3;
            int far = 11;
            int weight = 10;


            //**  exercising the code  **//
            graph.SetEdge(near, far, weight);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Third_New_Edge__Assert_Present()  /* working */  {
            /* second and later edges must exist down the linked list */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 3;
            int far = 11;
            int weight = 10;


            //**  exercising the code  **//
            graph.SetEdge(near, far - 5, weight);
            graph.SetEdge(near, far - 2, weight);
            graph.SetEdge(near, far, weight);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Out_Of_Order__Assert_Retained_In_Order()  /* working */  {
            /* second and later edges should be linked in ascending far-vertex order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(75, false);
            int near = 3;

            int far1 = 10;
            int far2 = 20;
            int far3 = 30;
            int far4 = 40;
            int far5 = 50;

            int weight = 10;


            //**  exercising the code  **//
            graph.SetEdge(near, far2, weight);
            graph.SetEdge(near, far5, weight);
            graph.SetEdge(near, far1, weight);
            graph.SetEdge(near, far4, weight);
            graph.SetEdge(near, far3, weight);


            //**  gathering results  **//
            PrivateObject p = new PrivateObject(graph);
            Graph_.Edge actual1 = (Graph_.Edge)p.GetArrayElement("_edges", near);

            Graph_.Edge actual2 = actual1.Next;
            Graph_.Edge actual3 = actual2.Next;
            Graph_.Edge actual4 = actual3.Next;
            Graph_.Edge actual5 = actual4.Next;
            Graph_.Edge actualNull = actual5.Next;  // last edge in list should have a null .Next 


            //**  testing  **//
            Assert.AreEqual(far1, actual1.FarEnd);
            Assert.AreEqual(far2, actual2.FarEnd);
            Assert.AreEqual(far3, actual3.FarEnd);
            Assert.AreEqual(far4, actual4.FarEnd);
            Assert.AreEqual(far5, actual5.FarEnd);
            Assert.AreEqual(null, actualNull);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__High_To_Low_Vertices__Assert_Low_To_High_Edge_Present()  /* working */  {
            /* to simplify my life, edges in undirected graphs 
             * are only stored with vertices from low to high */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 10;
            int far = 7;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(far, near));
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Several_Edges__Assert_Correct_Graph_Edge_Counts()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);


            //**  exercising the code and gathering results  **//
            graph.SetEdge(2, 5, 10);
            int actual1 = graph.EdgeCount;

            graph.SetEdge(3, 4, 12);
            int actual2 = graph.EdgeCount;

            graph.SetEdge(6, 3, 18);
            int actual3 = graph.EdgeCount;

            graph.SetEdge(4, 14, 56);
            graph.SetEdge(13, 3, 39);
            graph.SetEdge(8, 7, 56);
            int actual4 = graph.EdgeCount;


            //**  testing  **//
            Assert.AreEqual(1, actual1);
            Assert.AreEqual(2, actual2);
            Assert.AreEqual(3, actual3);
            Assert.AreEqual(6, actual4);
        }

        #endregion On undirected graphs


        #region On directed graphs

        /* not retesting things here that are the same as on undirected graphs */

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Directed__High_To_Low_Vertices__Assert_Only_High_To_Low_Edge_Present()  /* working */  {
            /* this test really just duplicates what's in one of the tests of HasEdge() */

            /* in a directed graph, the vertex order given is used to establish edge direction, 
             * so edges defined from higher to lower vertices are kept in that order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, true);
            int near = 10;
            int far = 7;


            //**  exercising the code  **//
            graph.SetEdge(near, far, 25);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
            Assert.AreEqual(false, graph.HasEdge(far, near));
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Directed__Out_Of_Order_And_Mixed_Directions__Assert_Retained_In_Order()  /* working */  {
            /* second and later edges should be linked in ascending far-vertex order, but at chosen near vertex */

            //**  groundwork  **//
            Graph_ graph = new Graph_(100, true);
            int lowNear = 3;
            int highNear = 70;

            int far1 = 10;
            int far2 = 20;
            int far3 = 30;
            int far4 = 40;
            int far5 = 50;
            int far6 = 60;

            int weight = 10;


            //**  exercising the code  **//
            // these should be in order of far vertex even when those are lower than near vertex 
            graph.SetEdge(lowNear, far2, weight);
            graph.SetEdge(lowNear, far5, weight);
            graph.SetEdge(lowNear, far1, weight);

            graph.SetEdge(highNear, far4, weight);
            graph.SetEdge(highNear, far3, weight);
            graph.SetEdge(highNear, far6, weight);


            //**  gathering results  **//
            PrivateObject p = new PrivateObject(graph);
            Graph_.Edge actual1 = (Graph_.Edge)p.GetArrayElement("_edges", lowNear);
            Graph_.Edge actual3 = (Graph_.Edge)p.GetArrayElement("_edges", highNear);

            Graph_.Edge actual2 = actual1.Next;
            Graph_.Edge actual5 = actual2.Next;
            Graph_.Edge actualLowNull = actual5.Next;  // last edge in list should have a null .Next 

            Graph_.Edge actual4 = actual3.Next;
            Graph_.Edge actual6 = actual4.Next;
            Graph_.Edge actualHighNull = actual6.Next;  // last edge in list should have a null .Next 


            //**  testing  **//
            Assert.AreEqual(far1, actual1.FarEnd);
            Assert.AreEqual(far2, actual2.FarEnd);
            Assert.AreEqual(far3, actual3.FarEnd);
            Assert.AreEqual(far4, actual4.FarEnd);
            Assert.AreEqual(far5, actual5.FarEnd);

            Assert.AreEqual(null, actualHighNull);
            Assert.AreEqual(null, actualHighNull);
        }

        #endregion On directed graphs

        #endregion Adding a new edge


        #region Changing an existing edge

        #region On undirected graphs

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Existing_Edge_New_Weight__Assert_Edge_Exists()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            int nearEnd = 3;
            int farEnd = 6;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);


            //**  pre-testing  **//
            Assert.AreEqual(true, graph.HasEdge(nearEnd, farEnd));


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            bool actual = graph.HasEdge(nearEnd, farEnd);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Existing_Edge_New_Weight__Assert_Edge_Count_Unchanged()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            int nearEnd = 3;
            int farEnd = 6;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);

            int expected = graph.EdgeCount;


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            int actual = graph.EdgeCount;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Existing_Edge_New_Weight__Assert_Correct_Edge_Weight()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            int nearEnd = 3;
            int farEnd = 6;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);


            //**  pre-testing  **//
            Assert.AreEqual(oldWeight, graph.WeightOfEdge(nearEnd, farEnd));


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            int actual = graph.WeightOfEdge(nearEnd, farEnd);


            //**  testing  **//
            Assert.AreEqual(newWeight, actual);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Existing_Edge_New_Weight__Assert_Correct_Reverse_Edge_Weight()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            int nearEnd = 3;
            int farEnd = 6;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);


            //**  pre-testing  **//
            Assert.AreEqual(oldWeight, graph.WeightOfEdge(nearEnd, farEnd));


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            int actual = graph.WeightOfEdge(farEnd, nearEnd);  // order of vertices reversed 


            //**  testing  **//
            Assert.AreEqual(newWeight, actual);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Undirected__Existing_Edge_New_Weight__Assert_Representation_Has_Same_Number_Of_Edges()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            PrivateObject p = new PrivateObject(graph);

            int nearEnd = 3;
            int farEnd = 6;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(nearEnd, 5, 10);
            graph.SetEdge(nearEnd, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);

            //**  getting expected value  **//
            int expected = _CountAt(p, nearEnd);


            //**  pre-testing  **//
            Assert.AreEqual(3, expected);


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            int actual = _CountAt(p, nearEnd);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Directed__New_Weight_To_Existing_Of_Two__Assert_Only_One_Changed()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            int nearEnd = 6;
            int farEnd = 3;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);  // topic edge 
            graph.SetEdge(farEnd, nearEnd, oldWeight);  // reverse edge 


            //**  pre-testing  **//
            Assert.AreEqual(true, graph.HasEdge(nearEnd, farEnd));


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            bool acPresence = graph.HasEdge(nearEnd, farEnd);
            int acTopicWeight = graph.WeightOfEdge(nearEnd, farEnd);
            int acReverseWeight = graph.WeightOfEdge(farEnd, nearEnd);


            //**  testing  **//
            Assert.AreEqual(true, acPresence);
            Assert.AreEqual(newWeight, acTopicWeight);
            Assert.AreEqual(oldWeight, acReverseWeight);
        }

        [TestMethod()]
        public void SetEdge_Int_Int_Int__Directed__New_Weight_To_Existing_Of_Two__Assert_Edge_Count_Unchanged()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            int nearEnd = 6;
            int farEnd = 3;
            int oldWeight = 5;
            int newWeight = nearEnd * farEnd;

            graph.SetEdge(2, 5, 10);
            graph.SetEdge(6, 7, 42);
            graph.SetEdge(nearEnd, farEnd, oldWeight);  // topic edge 
            graph.SetEdge(farEnd, nearEnd, oldWeight);  // reverse edge 

            int expected = graph.EdgeCount;  // this should also remain unchanged 


            //**  pre-testing  **//
            Assert.AreEqual(4, expected);


            //**  exercising the code  **//
            graph.SetEdge(nearEnd, farEnd, newWeight);


            //**  gathering results  **//
            int actual = graph.EdgeCount;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion On directed graphs

        #endregion Changing an existing edge

        #endregion SetEdge(int, int, int)


        #region SetEdge(int, int)

        #region On undirected graphs

        [TestMethod()]
        public void SetEdge_Int_Int__Undirected__First_New_Edge__Assert_Edge_Present()  /* working */  {
            /* first new edge must be set as head of list */

            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 3;
            int far = 11;


            //**  pre-testing  **//
            Assert.AreEqual(false, graph.HasEdge(near, far));


            //**  exercising the code  **//
            graph.SetEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(true, graph.HasEdge(near, far));
        }

        [TestMethod()]
        public void SetEdge_Int_Int__Undirected__Later_Edge__Assert_Edge_Present()  /* working */  {
            /* second and later edges should be linked in ascending far-vertex order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(75, false);
            int near = 3;
            int far = 30;


            //**  exercising the code  **//
            graph.SetEdge(near, far - 20);
            graph.SetEdge(near, far - 10);
            graph.SetEdge(near, far);
            graph.SetEdge(near, far + 10);
            graph.SetEdge(near, far + 20);


            //**  gathering results  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void SetEdge_Int_Int__Undirected__Knowns__Assert_Weight_Is_Unweighted()  /* working */  {
            /* second and later edges should be linked in ascending far-vertex order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(50, false);
            int near = 3;
            int far = 30;


            //**  exercising the code  **//
            graph.SetEdge(near, far - 10);
            graph.SetEdge(near, far);
            graph.SetEdge(near, far + 10);


            //**  gathering results  **//
            int actual = graph.WeightOfEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(Graph_.Unweighted, actual);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void SetEdge_Int_Int__Directed__One_Set__Assert_Edge_Only_In_One_Direction()  /* working */  {
            /* second and later edges should be linked in ascending far-vertex order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(75, true);
            int near = 30;
            int far = 3;


            //**  exercising the code  **//
            graph.SetEdge(near, far - 20);
            graph.SetEdge(near, far - 10);
            graph.SetEdge(near, far);
            graph.SetEdge(near, far + 10);
            graph.SetEdge(near, far + 20);


            //**  gathering results  **//
            bool acTopicEdgeExists = graph.HasEdge(near, far);
            bool acReverseExists = graph.HasEdge(far, near);


            //**  testing  **//
            Assert.AreEqual(true, acTopicEdgeExists);
            Assert.AreEqual(false, acReverseExists);
        }

        #endregion On directed graphs

        #endregion SetEdge(int, int)


        #region SetEdges((int, int, int), ...)

        /* since SetEdges() just calls SetEdge() internally, I'm not testing it on both undirected and directed graphs */

        [TestMethod()]
        public void SetEdges_Int_Int_Int_Tuples__Undirected__Knowns__Assert_Count_Presences_Weights_All_Correct()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // 3 new edges added, 1 changed (4 to 5) 
            int expCount = 3;

            int expWeight1To2 = 9;
            int expWeight4To5 = 75;
            int expWeight3To7 = 21;


            //**  exercising the code  **//
            // new edges, except (4, 5, expWeight4To5), which changes (4, 5, 20) 
            graph.SetEdges((1, 2, expWeight1To2), (4, 5, 20));
            graph.SetEdges((4, 5, expWeight4To5), (3, 7, expWeight3To7));


            //**  gathering results  **//
            int acCount = graph.EdgeCount;
            int acWeight1To2 = graph.WeightOfEdge(1, 2);
            int acWeight4To5 = graph.WeightOfEdge(4, 5);
            int acWeight3To7 = graph.WeightOfEdge(3, 7);


            //**  testing  **//
            Assert.AreEqual(expCount, acCount);

            Assert.AreEqual(true, graph.HasEdge(1, 2));
            Assert.AreEqual(true, graph.HasEdge(4, 5));
            Assert.AreEqual(true, graph.HasEdge(3, 7));

            Assert.AreEqual(expWeight1To2, acWeight1To2);
            Assert.AreEqual(expWeight4To5, acWeight4To5);
            Assert.AreEqual(expWeight3To7, acWeight3To7);
        }

        #endregion SetEdges((int, int, int), ...)


        #region SetEdges((int, int), ...)

        /* since SetEdges() just calls SetEdge() internally, I'm not testing it on both undirected and directed graphs */

        [TestMethod()]
        public void SetEdges_Int_Int_Tuples__Undirected__Knowns__Assert_Count_Presences_Unweighted_All_Correct()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // 3 new edges added, 1 repeated (4 to 5) 
            int expCount = 3;


            //**  exercising the code  **//
            // new edges, except the second (4, 5), which should have no effect 
            graph.SetEdges((1, 2), (4, 5));
            graph.SetEdges((4, 5), (3, 7));


            //**  gathering results  **//
            int acCount = graph.EdgeCount;
            int acWeight1To2 = graph.WeightOfEdge(1, 2);
            int acWeight4To5 = graph.WeightOfEdge(4, 5);
            int acWeight3To7 = graph.WeightOfEdge(3, 7);


            //**  testing  **//
            Assert.AreEqual(expCount, acCount);

            Assert.AreEqual(true, graph.HasEdge(1, 2));
            Assert.AreEqual(true, graph.HasEdge(4, 5));
            Assert.AreEqual(true, graph.HasEdge(3, 7));

            Assert.AreEqual(Graph_.Unweighted, acWeight1To2);
            Assert.AreEqual(Graph_.Unweighted, acWeight4To5);
            Assert.AreEqual(Graph_.Unweighted, acWeight3To7);
        }

        #endregion SetEdges((int, int), ...)


        #region CutEdge()

        #region On undirected graphs

        [TestMethod()]
        public void CutEdge__Undirected__First_Edge__Assert_Not_Present()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            int near = 4;
            int far = 6;
            graph.SetEdge(near, far, near * far);


            //**  exercising the code  **//
            graph.CutEdge(near, far);


            //**  gathering results  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CutEdge__Undirected__Several_Edges__Assert_Reverse_Edges_Not_Present()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(30, false);
            int far1 = 05;
            int far2 = 10;
            int far3 = 15;
            int far4 = 20;
            int far5 = 25;

            int near = 6;

            // added in reverse order, but the (near, far) edge should be at end of linked list 
            graph.SetEdge(near, far1, 25);
            graph.SetEdge(near, far2, 25);
            graph.SetEdge(near, far3, 25);
            graph.SetEdge(near, far4, 25);
            graph.SetEdge(near, far5, 25);


            //**  exercising the code and gathering results  **//
            graph.CutEdge(near, far3);
            int actual1 = graph.EdgeCount;

            graph.CutEdge(near, far1);
            int actual2 = graph.EdgeCount;

            graph.CutEdge(near, far5);
            int actual3 = graph.EdgeCount;

            graph.CutEdge(near, far2);
            graph.CutEdge(near, far4);
            int actual4 = graph.EdgeCount;


            //**  testing  **//
            Assert.AreEqual(false, graph.HasEdge(far1, near));
            Assert.AreEqual(false, graph.HasEdge(far2, near));
            Assert.AreEqual(false, graph.HasEdge(far3, near));
            Assert.AreEqual(false, graph.HasEdge(far4, near));
            Assert.AreEqual(false, graph.HasEdge(far5, near));
        }

        [TestMethod()]
        public void CutEdge__Undirected__LastEdge__Assert_Not_Present()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(30, false);
            int near = 20;
            int far = 25;

            // added in reverse order, but the (near, far) edge should be at end of linked list 
            graph.SetEdge(near, far, 25);
            graph.SetEdge(near, far - 2, 25);
            graph.SetEdge(near, far - 5, 25);
            graph.SetEdge(near, far - 10, 25);


            //**  exercising the code  **//
            graph.CutEdge(near, far);


            //**  gathering results  **//
            bool actual = graph.HasEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CutEdge__Undirected__Middle_Edge__Assert_Cut_Not_Present_Others_Present()  /* working */  {
            /* essentially, testing that linked-list splicing is correct */

            //**  groundwork  **//
            Graph_ graph = new Graph_(30, false);
            int far1 = 05;
            int far2 = 10;
            int far3 = 15;
            int far4 = 20;
            int far5 = 25;

            int near = 6;

            // added in reverse order, but the (near, far) edge should be at end of linked list 
            graph.SetEdge(near, far1, 25);
            graph.SetEdge(near, far2, 25);
            graph.SetEdge(near, far3, 25);
            graph.SetEdge(near, far4, 25);
            graph.SetEdge(near, far5, 25);


            //**  exercising the code  **//
            graph.CutEdge(near, far3);


            //**  testing  **//
            // these were not cut 
            Assert.AreEqual(true, graph.HasEdge(near, far1));
            Assert.AreEqual(true, graph.HasEdge(near, far2));

            // this was cut 
            Assert.AreEqual(false, graph.HasEdge(near, far3));

            // these were not cut 
            Assert.AreEqual(true, graph.HasEdge(near, far4));
            Assert.AreEqual(true, graph.HasEdge(near, far5));
        }

        [TestMethod()]
        public void CutEdge__Undirected__Several_Edges__Assert_Correct_Edge_Counts()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(30, false);
            int far1 = 05;
            int far2 = 10;
            int far3 = 15;
            int far4 = 20;
            int far5 = 25;

            int near = 6;

            // added in reverse order, but the (near, far) edge should be at end of linked list 
            graph.SetEdge(near, far1, 25);
            graph.SetEdge(near, far2, 25);
            graph.SetEdge(near, far3, 25);
            graph.SetEdge(near, far4, 25);
            graph.SetEdge(near, far5, 25);


            //**  exercising the code and gathering results  **//
            graph.CutEdge(near, far3);
            int actual1 = graph.EdgeCount;

            graph.CutEdge(near, far1);
            int actual2 = graph.EdgeCount;

            graph.CutEdge(near, far5);
            int actual3 = graph.EdgeCount;

            graph.CutEdge(near, far2);
            graph.CutEdge(near, far4);
            int actual4 = graph.EdgeCount;


            //**  testing  **//
            Assert.AreEqual(4, actual1);
            Assert.AreEqual(3, actual2);
            Assert.AreEqual(2, actual3);
            Assert.AreEqual(0, actual4);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void CutEdge__Directed__One_Of_Pair__Assert_Only_Cut_Direction_Not_Present()  /* working */  {
            /* essentially, testing that linked-list splicing is correct */

            //**  groundwork  **//
            Graph_ graph = new Graph_(30, true);
            int near = 15;
            int far = 6;

            // topic edge and reverse edge 
            graph.SetEdge(near, far, 25);
            graph.SetEdge(far, near, 25);


            //**  exercising the code  **//
            graph.CutEdge(near, far);


            //**  testing  **//
            // topic edge was cut 
            Assert.AreEqual(false, graph.HasEdge(near, far));

            // reverse was not cut 
            Assert.AreEqual(true, graph.HasEdge(far, near));
        }

        [TestMethod()]
        public void CutEdge__Directed__One_Of_Pair__Assert_Correct_Edge_Counts()  /* working */  {
            /* essentially, testing that linked-list splicing is correct */

            //**  groundwork  **//
            Graph_ graph = new Graph_(30, true);
            int near = 15;
            int far = 6;

            // topic edge and reverse edge 
            graph.SetEdge(near, far, 25);
            graph.SetEdge(far, near, 25);

            int expected = 1;


            //**  exercising the code  **//
            graph.CutEdge(near, far);


            //**  gathering results  **//
            int actual = graph.EdgeCount;

            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion On directed graphs

        #endregion CutEdge()


        #region WeightOfEdge()

        #region On undirected graphs

        [TestMethod()]
        public void WeightOfEdge__Undirected__No_Such_Edge__Assert_Value_None()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 5;
            int far = 12;

            /* no edges added, so weight for any vertex pair must be Graph_.None */


            //**  exercising the code  **//
            int actual = graph.WeightOfEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(Graph_.None, actual);
        }

        [TestMethod()]
        public void WeightOfEdge__Undirected__Known_Weight__Assert_Correct_Weight()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 5;
            int far = 12;
            int weight = 16;

            // added out of order as acid test 
            graph.SetEdge(near, far + 1, weight - 5);
            graph.SetEdge(near, far, weight);
            graph.SetEdge(near, far - 3, weight + 5);


            //**  exercising the code  **//
            int actual = graph.WeightOfEdge(near, far);


            //**  testing  **//
            Assert.AreEqual(weight, actual);
        }

        [TestMethod()]
        public void WeightOfEdge__Undirected__Reversed_Vertex_Order__Assert_Correct_Weight()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 12;
            int far = 5;
            int weight = 16;

            // added out of order as acid test 
            graph.SetEdge(near, far + 1, weight - 5);
            graph.SetEdge(near, far, weight);
            graph.SetEdge(near, far - 3, weight + 5);


            //**  exercising the code  **//
            int acReversed = graph.WeightOfEdge(near, far);
            int acSame = graph.WeightOfEdge(far, near);


            //**  testing  **//
            Assert.AreEqual(weight, acReversed);
            Assert.AreEqual(weight, acSame);
        }

        [TestMethod()]
        public void WeightOfEdge__Undirected__Knowns__Assert_Reversed_Vertices_Correct_Weight()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, false);
            int near = 5;
            int far = 12;
            int weight = 16;

            // added out of order as acid test 
            graph.SetEdge(near, far + 1, weight - 5);
            graph.SetEdge(near, far, weight);
            graph.SetEdge(near, far - 3, weight + 5);


            //**  exercising the code  **//
            int acReversed = graph.WeightOfEdge(near, far);
            int acSame = graph.WeightOfEdge(far, near);


            //**  testing  **//
            Assert.AreEqual(weight, acReversed);
            Assert.AreEqual(weight, acSame);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void WeightOfEdge__Directed__Known_Weight__Assert_Weight_In_One_Direction_And_None_In_Other()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(15, true);
            int near = 12;
            int far = 5;
            int weight = 16;

            // added out of order as acid test 
            graph.SetEdge(near, far + 1, weight - 5);
            graph.SetEdge(near, far, weight);
            graph.SetEdge(near, far - 3, weight + 5);


            //**  exercising the code  **//
            int acTopicWeight = graph.WeightOfEdge(near, far);
            int acReverseWeight = graph.WeightOfEdge(far, near);


            //**  testing  **//
            Assert.AreEqual(weight, acTopicWeight);
            Assert.AreEqual(Graph_.None, acReverseWeight);
        }

        #endregion On directed graphs

        #endregion WeightOfEdge()


        #region VertexDirectedDegrees()

        [TestMethod()]
        public void VertexDirectedDegrees__Known_Edges__Assert_Correct_Degrees()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(5, true);
            graph.SetEdge(0, 1);
            graph.SetEdge(0, 2);
            graph.SetEdge(0, 3);
            graph.SetEdge(1, 4);
            graph.SetEdge(1, 2);
            graph.SetEdge(3, 4);

            int[] expOutDegrees = { 3, 2, 0, 1, 0 };
            int[] expInDegrees = { 0, 1, 2, 1, 2 };


            //**  exercising the code  **//
            (int[] acOutDegrees, int[] acInDegrees) = graph.VertexDirectedDegrees();


            //**  testing  **//
            CollectionAssert.AreEqual(expOutDegrees, acOutDegrees);
            CollectionAssert.AreEqual(expInDegrees, acInDegrees);
        }

        #endregion VertexDirectedDegrees()


        #region DepthFirstSearch()

        #region On undirected graphs

        [TestMethod()]
        public void DepthFirstSearch__Undirected__Known_Edges__Assert_Correct_Visit_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(5, false);
            graph.SetEdges((1, 2), (2, 3), (3, 4));
            graph.SetEdges((1, 3), (2, 4));

            // traversal should be: 1 >> 2, 2 >> 3, 3 >> 4, 2 >> 4, 1 >> 3, 
            // but after the first three edges, all vertices have been reached, 
            // so 2 >> 4 and 1 >> 3 don't show up in @actual 
            (int, int)[] expected = { (1, 2), (2, 3), (3, 4) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DepthFirstSearch__Undirected__Reversed_Edges__Assert_Correct_Visit_Order()  /* working */  {
            /* on an undirected graph, edges inserted with vertices reversed should 
             * be traversed in the same order as if inserted in forward order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(5, false);
            graph.SetEdges((2, 1), (3, 2), (4, 3));
            graph.SetEdges((3, 1), (4, 2));

            // traversal should be: 1 >> 2, 2 >> 3, 3 >> 4, 2 >> 4, 1 >> 3, 
            // but after the first three edges, all vertices have been reached, 
            // so 2 >> 4 and 1 >> 3 don't show up in @actual 
            (int, int)[] expected = { (1, 2), (2, 3), (3, 4) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DepthFirstSearch__Undirected__Known_Edges__Assert_Correct_Moments()  /* working */  {
            /* DFS can be used to define parentage relationships based on the 
             * "time" / moment that recursion reaches each new vertex; 
             * in the DFS tree, child nodes / vertices have moments 
             * that are between those of their parent nodes / vertices */

            //**  groundwork  **//
            Graph_ graph = new Graph_(5, false);
            graph.SetEdges((1, 2), (2, 3), (3, 4));
            graph.SetEdges((1, 3), (2, 4));

            int none = Graph_.None;

            // traversal should be 1 >> 2, 2 >> 3, 3 >> 4; remaining edges "tried" but effectively skipped; 
            // DFS tree has one branch, so all parent-child moments can be compared together 
            int[] treeOrder = { 1, 2, 3, 4 };

            //     arrivals should move forward, and departure times backwards, along one path; 
            // 5 duples here because all vertices are included, including the first and unreached ones 
            (int Arrival, int Departure)[] expecteds = { (none, none), (0, 7), (1, 6), (2, 5), (3, 4) };
            (int Arrival, int Departure)[] actuals = { (none, none), (none, none), (none, none), (none, none), (none, none) };

            int moment = 0;  // used for both arrivals and departures 

            // delegates that implement arrive / depart tracking of moments; 
            // incremented both at arrival and departure so that moments for 
            // a child vertex are both between arrival and departure of parent 
            graph.AtArriveNewVertex = (int near) => {
                actuals[near].Arrival = moment;
                moment++;
            };

            graph.AtDepartNewVertex = (int near) => {
                actuals[near].Departure = moment;
                moment++;
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);

            // testing the parent-child moment relationships algos may rely on 
            for (int at = 1; at < treeOrder.Length; at++) {
                int ofParent = treeOrder[at - 1];
                int ofChild = treeOrder[at];

                (int parentArrival, int parentDeparture) = actuals[ofParent];
                (int childArrival, int childDeparture) = actuals[ofChild];

                Assert.IsTrue(childArrival > parentArrival);
                Assert.IsTrue(childDeparture < parentDeparture);
            }
        }

        [TestMethod()]
        public void DepthFirstSearch__Undirected__Reversed_Edges__Assert_Correct_Moments()  /* working */  {
            /* on an undirected graph, edges inserted with vertices reversed should 
           * be traversed in the same order as if inserted in forward order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(5, false);
            graph.SetEdges((2, 1), (3, 2), (4, 3));
            graph.SetEdges((3, 1), (4, 2));

            int none = Graph_.None;

            // traversal should be 1 >> 2, 2 >> 3, 3 >> 4; remaining edges "tried" but effectively skipped; 
            // DFS tree has one branch, so all parent-child moments can be compared together 
            int[] treeOrder = { 1, 2, 3, 4 };

            //     arrivals should move forward, and departure times backwards, along one path; 
            // 5 duples here because all vertices are included, including the first and unreached ones 
            (int Arrival, int Departure)[] expecteds = { (none, none), (0, 7), (1, 6), (2, 5), (3, 4) };
            (int Arrival, int Departure)[] actuals = { (none, none), (none, none), (none, none), (none, none), (none, none) };

            int moment = 0;  // used for both arrivals and departures 

            // delegates that implement arrive / depart tracking of moments; 
            // incremented both at arrival and departure so that moments for 
            // a child vertex are both between arrival and departure of parent 
            graph.AtArriveNewVertex = (int near) => {
                actuals[near].Arrival = moment;
                moment++;
            };

            graph.AtDepartNewVertex = (int near) => {
                actuals[near].Departure = moment;
                moment++;
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);

            // testing the parent-child moment relationships algos may rely on 
            for (int at = 1; at < treeOrder.Length; at++) {
                int ofParent = treeOrder[at - 1];
                int ofChild = treeOrder[at];

                (int parentArrival, int parentDeparture) = actuals[ofParent];
                (int childArrival, int childDeparture) = actuals[ofChild];

                Assert.IsTrue(childArrival > parentArrival);
                Assert.IsTrue(childDeparture < parentDeparture);
            }
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void DepthFirstSearch__Directed__Known_Edges__Assert_Correct_Visit_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(5, true);
            graph.SetEdges((1, 2), (3, 2), (3, 4));
            graph.SetEdges((1, 3), (2, 4));

            // traversal should be: 1 >> 2, 2 >> 4, 1 >> 3, 3 >> 2, 3 >> 4, 
            // but after the first three edges, all vertices have been reached, 
            // so 3 >> 2 and 3 >> 4 don't show up in @actual 
            (int, int)[] expected = { (1, 2), (2, 4), (1, 3) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DepthFirstSearch__Directed__Known_Edges__Assert_Correct_Moments()  /* working */  {
            /* DFS can be used to define parentage relationships based on the 
             * "time" / moment that recursion reaches each new vertex; 
             * in the DFS tree, child nodes / vertices have moments 
             * that are between those of their parent nodes / vertices */

            //**  groundwork  **//
            Graph_ graph = new Graph_(5, true);
            graph.SetEdges((1, 2), (3, 2), (3, 4));
            graph.SetEdges((1, 3), (2, 4));

            int none = Graph_.None;

            // traversal should be 1 >> 2, 2 >> 4, 1 >> 3; remaining edges "tried" but effectively skipped; 
            // DFS tree has two branches, whose parent-child moments must be compared separately 
            int[] firstBranchOrder = { 1, 2, 4 };
            int[] secondBranchOrder = { 1, 3 };

            // arrivals should move forward, and departure times backwards, along one path; 
            // 5 duples here because all vertices are included, including the first and unreached ones 
            (int Arrival, int Departure)[] expecteds = { (none, none), (0, 7), (1, 4), (5, 6), (2, 3) };
            (int Arrival, int Departure)[] actuals = { (none, none), (none, none), (none, none), (none, none), (none, none) };

            List_Vector_<int> acArrivals = new List_Vector_<int>();
            List_Vector_<int> acDepartures = new List_Vector_<int>();

            int moment = 0;  // used for both arrivals and departures 

            // delegates that implement arrive / depart tracking of moments; 
            // incremented both at arrival and departure so that moments for 
            // a child vertex are both between arrival and departure of parent 
            graph.AtArriveNewVertex = (int near) => {
                actuals[near].Arrival = moment;
                moment++;
            };

            graph.AtDepartNewVertex = (int near) => {
                actuals[near].Departure = moment;
                moment++;
            };


            //**  exercising the code  **//
            graph.DepthFirstSearch(1);


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);

            /* testing the parent-child moment relationships algos may rely on */

            // first branch of tree 
            for (int at = 1; at < firstBranchOrder.Length; at++) {
                int ofParent = firstBranchOrder[at - 1];
                int ofChild = firstBranchOrder[at];

                (int parentArrival, int parentDeparture) = actuals[ofParent];
                (int childArrival, int childDeparture) = actuals[ofChild];

                Assert.IsTrue(childArrival > parentArrival);
                Assert.IsTrue(childDeparture < parentDeparture);
            }

            // second branch of tree 
            for (int at = 1; at < secondBranchOrder.Length; at++) {
                int ofParent = secondBranchOrder[at - 1];
                int ofChild = secondBranchOrder[at];

                (int parentArrival, int parentDeparture) = actuals[ofParent];
                (int childArrival, int childDeparture) = actuals[ofChild];

                Assert.IsTrue(childArrival > parentArrival);
                Assert.IsTrue(childDeparture < parentDeparture);
            }
        }

        #endregion On directed graphs

        #endregion DepthFirstSearch()


        #region BreadthFirstSearch()

        #region On undirected graphs

        [TestMethod()]
        public void BreadthFirstSearch__Undirected__Known_Edges__Assert_Correct_Visit_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(6, false);
            graph.SetEdges((0, 1), (0, 2));
            graph.SetEdges((2, 1), (2, 3), (2, 4));
            graph.SetEdges((3, 4), (3, 5));
            graph.SetEdges((4, 5));

            // traversal should be: 0 >> 1, 0 >> 2, 2 >> 1, 2 >> 3, 2 >> 4, 3 >> 4, 3 >> 5, 4 >> 5, 
            // but far ends already reached at 2 >> 1, 3 >> 4, and 4 >> 5, 
            // so they shouldn't show up in @actual 
            (int, int)[] expected = { (0, 1), (0, 2), (2, 3), (2, 4), (3, 5) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.BreadthFirstSearch(0);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BreadthFirstSearch__Undirected__Reversed_Edges__Assert_Correct_Visit_Order()  /* working */  {
            /* on an undirected graph, edges inserted with vertices reversed should 
             * be traversed in the same order as if inserted in forward order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(6, false);
            graph.SetEdges((1, 0), (2, 0));
            graph.SetEdges((1, 2), (3, 2), (4, 2));
            graph.SetEdges((4, 3), (5, 3));
            graph.SetEdges((5, 4));

            // traversal should be: 0 >> 1, 0 >> 2, 2 >> 1, 2 >> 3, 2 >> 4, 3 >> 4, 3 >> 5, 4 >> 5, 
            // but far ends already reached at 2 >> 1, 3 >> 4, and 4 >> 5, 
            // so they shouldn't show up in @actual 
            (int, int)[] expected = { (0, 1), (0, 2), (2, 3), (2, 4), (3, 5) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.BreadthFirstSearch(0);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void BreadthFirstSearch__Directed__Known_Edges__Assert_Correct_Visit_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(6, true);
            graph.SetEdges((0, 1), (0, 2));
            graph.SetEdges((2, 1), (3, 2), (2, 4));
            graph.SetEdges((4, 3), (3, 5));
            graph.SetEdges((4, 5));

            // traversal should be: 0 >> 1, 0 >> 2, 2 >> 1, 2 >> 3, 2 >> 4, 3 >> 4, 3 >> 5, 4 >> 5, 
            // but far ends already reached at 2 >> 1, 3 >> 4, and 4 >> 5, 
            // so they shouldn't show up in @actual 
            (int, int)[] expected = { (0, 1), (0, 2), (2, 4), (4, 3), (4, 5) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.BreadthFirstSearch(0);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);


            //**  testing  **//
            Assert.AreEqual(expected.Length, actual.Length);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BreadthFirstSearch__Directed__Edge_Back_To_Root__Assert_Correct_Visit_Order()  /* working */  {
            /* when an edge exists back to the starting point of a BFS in a 
             * directed graph, it should not be visited during traversal */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            graph.SetEdges((0, 1), (1, 4));
            graph.SetEdges((0, 2), (2, 3), (3, 4));
            graph.SetEdges((4, 0));  // reverse edge back to root 

            // traversal should be: 0 >> 1, 0 >> 2, 1 >> 4, 2 >> 3, 3 >> 4, 4 >> 0, 
            // but far ends already reached at 3 >> 4 and 4 >> 0, 
            // so they shouldn't show up in @actual 
            (int, int)[] expected = { (0, 1), (0, 2), (1, 4), (2, 3) };
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };


            //**  exercising the code  **//
            graph.BreadthFirstSearch(0);


            //**  gathering results  **//
            (int, int)[] actual = _ArrayFromVector(acVector);

            //actual = new[] { (0, 1), (1, 2), (2, 3), (3, 4), (4, 5) };


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion On directed graphs

        #endregion BreadthFirstSearch()


        #region ResetSearch()

        [TestMethod()]
        public void ResetSearch__Undirected__Known_Searches__Assert_Correct_New_Search()  /* working */  {
            /* whatever its implementation, ResetSearch() makes it possible to search anew correctly; 
             * to test this, search is performed and should be correct, then performed again, and 
             * should _not_ be correct, then performed again after resetting and _should_ be correct */

            //**  groundwork  **//
            Graph_ graph = new Graph_(6, false);
            graph.SetEdges((0, 1), (0, 2));
            graph.SetEdges((2, 1), (2, 3), (2, 4));
            graph.SetEdges((3, 4), (3, 5));
            graph.SetEdges((4, 5));

            // traversal should be: 0 >> 1, 0 >> 2, 2 >> 1, 2 >> 3, 2 >> 4, 3 >> 4, 3 >> 5, 4 >> 5, 
            // but far ends already reached at 2 >> 1, 3 >> 4, and 4 >> 5, 
            // so they shouldn't show up in @actual 
            (int, int)[] expected = { (0, 1), (0, 2), (2, 3), (2, 4), (3, 5) };

            // reused throughput 
            List_Vector_<(int, int)> acVector = new List_Vector_<(int, int)>();

            // delegate that takes needed steps at the point a new edge is reached 
            graph.AtNewEdge = (int near, int far) => {
                acVector.Append((near, far));
            };

            // first search and results 
            graph.BreadthFirstSearch(0);
            (int, int)[] acFirst = _ArrayFromVector(acVector);  // copies data 

            // clearing accumulator, then second search without resetting first 
            acVector.Clear();
            graph.BreadthFirstSearch(0);
            (int, int)[] acNotReset = _ArrayFromVector(acVector);

            // re-clearing accumulator 
            acVector.Clear();


            //**  exercising the code  **//
            // resetting and third search only 
            graph.ResetSearch();
            graph.BreadthFirstSearch(0);
            (int, int)[] acWasReset = _ArrayFromVector(acVector);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, acFirst);
            CollectionAssert.AreNotEqual(expected, acNotReset);
            CollectionAssert.AreEqual(expected, acWasReset);
        }

        #endregion ResetSearch()


        #region ShortestUnweightedPath()

        #region On undirected graphs

        [TestMethod()]
        public void ShortestUnweightedPath__Undirected__Known_Two_Paths__Assert_Shorter_Path()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (1, 4));          // shortest path 
            graph.SetEdges((0, 2), (2, 3), (3, 4));  // longer path 

            int[] expected = { 0, 1, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(0, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ShortestUnweightedPath__Undirected__Two_Paths_Reversed_Edges__Assert_Shorter_Path()  /* working */  {
            /* on an undirected graph, edges inserted with vertices reversed should 
             * be traversed in the same order as if inserted in forward order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((1, 0), (4, 1));          // shortest path 
            graph.SetEdges((2, 0), (3, 2), (4, 3));  // longer path 

            int[] expected = { 0, 1, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(0, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ShortestUnweightedPath__Undirected__Known_Non_Root_Paths__Assert_Shorter_Path()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (1, 4));                   // other paths in graph 
            graph.SetEdges((2, 5), (5, 7), (7, 8), (8, 9));   // longer path from 2 to 4 
            graph.SetEdges((0, 2), (2, 3), (3, 4));           // shortest path from 2 to 4 

            int[] expected = { 2, 3, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(2, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ShortestUnweightedPath__Undirected__Non_Root_Paths_Reversed_Edges__Assert_Shorter_Path()  /* working */  {
            /* on an undirected graph, edges inserted with vertices reversed should 
             * be traversed in the same order as if inserted in forward order */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((1, 0), (4, 1));                   // other paths in graph 
            graph.SetEdges((5, 2), (7, 5), (8, 7), (9, 8));   // longer path from 2 to 4 
            graph.SetEdges((2, 0), (3, 2), (4, 3));           // shortest path from 2 to 4 

            int[] expected = { 2, 3, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(2, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion On undirected graphs


        #region On directed graphs

        [TestMethod()]
        public void ShortestUnweightedPath__Directed__Known_Three_Paths__Assert_Shorter_Path()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            graph.SetEdges((0, 1), (1, 4));          // shortest path 
            graph.SetEdges((0, 2), (2, 3), (3, 4));  // longer path 
            graph.SetEdges((4, 0));  // reverse path is not shortest, since directed graph 

            int[] expected = { 0, 1, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(0, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ShortestUnweightedPath__Directed__Known_Non_Root_Paths__Assert_Shorter_Path()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            graph.SetEdges((0, 1), (1, 4));                   // other paths in graph 
            graph.SetEdges((2, 5), (5, 7), (7, 8), (8, 9));   // longer path from 2 to 4 
            graph.SetEdges((0, 2), (2, 3), (3, 4));           // shortest path from 2 to 4 
            graph.SetEdges((4, 2));  // reverse path is not shortest, since directed graph 

            int[] expected = { 2, 3, 4 };


            //**  exercising the code  **//
            int[] actual = graph.ShortestUnweightedPath(2, 4);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion On directed graphs

        #endregion ShortestUnweightedPath()


        #region ConnectedComponents()

        /* the algorithm here is only well-defined on undirected graphs, but it also 
         * works on directed ones if components are completely unconnected; 
         * if they are weakly connected or strongly connected, its results 
         * are unreliable, and depend on where traversal starts */

        [TestMethod()]
        public void ConnectedComponents__Undirected__Known_Components__Assert_Correct_Contents_By_Index()  /* working */  {
            /* connected components are numbered, with each one listing all of its vertices */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // first component, with some back edges 
            graph.SetEdges((0, 3), (0, 5), (5, 3), (3, 6), (6, 7), (7, 9), (9, 5));

            // second component, with some back edges 
            graph.SetEdges((1, 2), (1, 4), (2, 4), (1, 8));

            (int component, int[] vertices)[] expecteds = {
                (0, new int[] { 0, 3, 5, 6, 9, 7 }),  // not in purely ascending order because BFS 
                (1, new int[] { 1, 2, 4, 8 })
            };


            //**  exercising the code  **//
            (int component, int[] vertices)[] actuals = graph.ConnectedComponents();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int of = 0; of < expecteds.Length; of++) {
                (int Component, int[] Vertices) expected = expecteds[of];
                (int Component, int[] Vertices) actual = actuals[of];

                Assert.AreEqual(expected.Component, actual.Component);
                CollectionAssert.AreEqual(expected.Vertices, actual.Vertices);
            }
        }

        [TestMethod()]
        public void ConnectedComponents__Undirected__One_Component_By_Reverse_Edges__Assert_Correct_Contents_By_Index()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (0, 4));  // B to W 
            graph.SetEdges((1, 6), (4, 2));  // W to B 
            graph.SetEdges((6, 5), (2, 3));  // B to W 
            graph.SetEdges((3, 7), (5, 7));  // W to B 


            //**  exercising the code  **//
            (int Component, int[] Vertices)[] actuals = graph.ConnectedComponents();


            //**  testing  **//
            Assert.AreEqual(1, actuals.Length);
        }

        [TestMethod()]
        public void ConnectedComponents__Directed__Completely_Unconnected_Components__Assert_Correct_Contents_By_Index()  /* working */  {
            /* connected components are numbered, with each one listing all of its vertices */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);

            // first component, with some back edges that affect traversal order 
            graph.SetEdges((0, 3), (0, 5), (5, 3), (3, 6), (6, 7), (7, 9), (9, 5));

            // second component, with some back edges 
            graph.SetEdges((1, 2), (1, 4), (2, 4), (1, 8));

            (int component, int[] vertices)[] expecteds = {
                (0, new int[] { 0, 3, 5, 6, 7, 9 }),  // in purely ascending order, even though BFS, because of back edges 
                (1, new int[] { 1, 2, 4, 8 })
            };


            //**  exercising the code  **//
            (int component, int[] vertices)[] actuals = graph.ConnectedComponents();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            for (int of = 0; of < expecteds.Length; of++) {
                (int Component, int[] Vertices) expected = expecteds[of];
                (int Component, int[] Vertices) actual = actuals[of];

                Assert.AreEqual(expected.Component, actual.Component);
                CollectionAssert.AreEqual(expected.Vertices, actual.Vertices);
            }
        }

        #endregion ConnectedComponents()


        #region TwoColored()

        /* Two-coloring is tested only on undirected graphs, because implementing it on a directed graph 
         * is best done by treating that graph as undirected, by one mechanism or another */

        [TestMethod()]
        public void TwoColored__Undirected__One_Component_Is_Two_Colored__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (0, 4));  // B to W 
            graph.SetEdges((1, 6), (4, 2));  // W to B 
            graph.SetEdges((6, 5), (2, 3));  // B to W 
            graph.SetEdges((3, 7), (5, 7));  // W to B 


            //**  exercising the code  **//
            (bool isTwoColored, Graph_.GraphColor[] coloring) = graph.TwoColored();
            bool actual = isTwoColored;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void TwoColored__Undirected__Two_Components_Is_Two_Colored__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // first component 
            graph.SetEdges((0, 1), (0, 4));  // B to W 
            graph.SetEdges((1, 6));          // W to B 
            graph.SetEdges((6, 5));          // B to W 

            // second component 
            graph.SetEdges((2, 3));          // B to W 
            graph.SetEdges((3, 7));          // W to B 


            //**  exercising the code  **//
            (bool isTwoColored, Graph_.GraphColor[] coloring) = graph.TwoColored();
            bool actual = isTwoColored;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void TwoColored__Undirected__One_Component_Not_Two_Colored__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (0, 4));  // B to W 
            graph.SetEdges((1, 4));          // not two-colored: W to W 
            graph.SetEdges((1, 6), (4, 2));  // W to B 
            graph.SetEdges((6, 5), (2, 3));  // B to W 
            graph.SetEdges((3, 7), (5, 7));  // W to B 


            //**  exercising the code  **//
            (bool isTwoColored, Graph_.GraphColor[] coloring) = graph.TwoColored();
            bool actual = isTwoColored;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void TwoColored__Undirected__Two_Components_Is_Two_Colored__Assert_Correct_Two_Coloring()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // first component 
            graph.SetEdges((0, 1), (0, 4));  // B to W 
            graph.SetEdges((1, 6));  // W to B 
            graph.SetEdges((6, 5));  // B to W 

            // second component 
            graph.SetEdges((2, 3));  // B to W 
            graph.SetEdges((3, 7));  // W to B 

            // localizing 
            Graph_.GraphColor black = Graph_.GraphColor.Black;
            Graph_.GraphColor white = Graph_.GraphColor.White;

            // @expecteds doesn't have any ×.None values because all vertices are colored .Black 
            // when traversed as a potential root of a new BFS tree / component 
            Graph_.GraphColor[] expecteds = {
                black,  white,  black,  white,  white,
                white,  black,  black,  black,  black
            };


            //**  exercising the code  **//
            (bool isTwoColored, Graph_.GraphColor[] coloring) = graph.TwoColored();
            Graph_.GraphColor[] actuals = coloring;


            //**  testing  **//
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        #endregion TwoColored()


        #region ContainsCycle()

        /* Cycle-finding is tested only on undirected graphs, because the reftext for it only defines it 
         * for undirected graphs, and an algorithm for directed graphs may be much more complicated */

        [TestMethod()]
        public void ContainsCycle__Undirected__One_Component_No_Cycle__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (0, 6), (6, 5));
            graph.SetEdges((1, 7), (7, 2), (2, 3), (3, 9));
            graph.SetEdges((7, 8), (8, 4));


            //**  exercising the code  **//
            bool actual = graph.ContainsCycle();


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void ContainsCycle__Undirected__One_Component_Has_Cycle__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);
            graph.SetEdges((0, 1), (0, 6), (6, 5));
            graph.SetEdges((1, 7), (7, 2), (2, 3), (3, 9));
            graph.SetEdges((7, 8), (8, 4));

            graph.SetEdge(2, 6);  // creating a cycle 


            //**  exercising the code  **//
            bool actual = graph.ContainsCycle();


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void ContainsCycle__Undirected__Known_Components_No_Cycle__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // first component 
            graph.SetEdges((0, 1), (0, 6), (6, 5));

            // second component 
            graph.SetEdges((7, 2), (2, 3), (3, 9));
            graph.SetEdges((7, 8), (8, 4));


            //**  exercising the code  **//
            bool actual = graph.ContainsCycle();


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void ContainsCycle__Undirected__Known_Components_Has_Cycle__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // first component 
            graph.SetEdges((0, 1), (0, 6), (6, 5));

            // second component 
            graph.SetEdges((7, 2), (2, 3), (3, 9));
            graph.SetEdges((7, 8), (8, 4));

            // cycle, in second component 
            graph.SetEdge(4, 9);


            //**  exercising the code  **//
            bool actual = graph.ContainsCycle();


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        #endregion ContainsCycle()


        #region TopologicalSortMono()

        /* topological sorting is only defined on directed graphs; 
         * the algo here also only works for one-component graphs */

        [TestMethod()]
        public void TopologicalSortMono__Directed__Known_Order__Assert_Returns_Correct_Vertex_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);

            // two paths from 3 to 0 
            graph.SetEdges((3, 1), (1, 0));
            graph.SetEdges((3, 7), (7, 0));

            // two paths from 0 to 5 
            graph.SetEdges((0, 8), (8, 4), (4, 5));
            graph.SetEdges((0, 6), (6, 2), (2, 5));

            // one path from 5 to 9 
            graph.SetEdge(5, 9);

            // as so often with graph traversals, this correct order is not intuitive; 
            // recursion proceeds on the highest-child path, with lower-child paths 
            // interpolated between vertices where paths split and converge again 
            int[] expected = { 3, 7, 1, 0, 8, 4, 6, 2, 5, 9 };


            //**  exercising the code  **//
            int[] actual = graph.TopologicalSortMono();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion TopologicalSortMono()


        #region TopologicalSortPoly()

        [TestMethod()]
        public void TopologicalSortPoly__Directed__Known_Order__Assert_Returns_Correct_Vertex_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);

            // two paths from 3 to 0 
            graph.SetEdges((3, 1), (1, 0));
            graph.SetEdges((3, 7), (7, 0));

            // two paths from 0 to 5 
            graph.SetEdges((0, 8), (8, 4), (4, 5));
            graph.SetEdges((0, 6), (6, 2), (2, 5));

            // one path from 5 to 9 
            graph.SetEdge(5, 9);

            // unusually for a graph-algo result, this correct order is semi-intuitive; 
            // this algorithm uses a queue and so is essentially breadth-first, 
            // so "adjacent" vertices on parallel paths appear roughly in alternation 
            int[] expected = { 3, 1, 7, 0, 6, 8, 2, 4, 5, 9 };


            //**  exercising the code  **//
            int[] actual = graph.TopologicalSortPoly();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TopologicalSortPoly__Directed__Two_Components_Known_Order__Assert_Returns_Correct_Vertex_Order()  /* working */  {
            //**  groundwork  **//
            Graph_ graph = new Graph_(10, true);
            graph.SetEdges((3, 2), (2, 1), (2, 0), (1, 4), (0, 4));  // first component 
            graph.SetEdges((5, 7), (7, 9), (9, 6), (9, 8), (8, 6));  // second component 

            // because this is much like two simultaneous breadth-first searches, 
            // the sorted vertices from each component are interleaved, 
            // with consecutive elements from a component only where it alone is branching; 
            // from 3, vertices are { 3, 2, 0, 1, 4 }; from 5, they are { 5, 7, 9, 8, 6 } 
            int[] expected = { 3, 5, 2, 7, 0, 1, 9, 4, 8, 6 };


            //**  exercising the code  **//
            int[] actual = graph.TopologicalSortPoly();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion TopologicalSortPoly()


        #region ShortestWeightedPath()

        [TestMethod()]
        public void ShortestWeightedPath__Undirected__Known_Edges__Assert_Returns_Correct_Shortest_Path() {
            Assert.Inconclusive("Implementation of tested method ShortestWeightedPath() is not yet complete.");
 
            /* this test is apparently working, if my original assumptions in it were correct */

            //**  groundwork  **//
            Graph_ graph = new Graph_(10, false);

            // shortest path from 2 to 9: 2, 1, 3, 4, 7, 9; total == 20 
            graph.SetEdges((2, 1, 2), (1, 3, 6), (3, 4, 4),(4,7,5),(7,9,3));

            // new edges of other (longest) path from 2 to 9: 2, 1, 0, 7, 9; total == 23 
            graph.SetEdges((1, 0, 6), (0, 7, 12));

            // expecteds 
            int[] expPath = { 2, 1, 3, 4, 7, 9 };
            int expLength = 20;


            //**  exercising the code  **//
            (int[] acPath, int acLength) = graph.ShortestWeightedPath(2, 9);


            //**  testing  **//
            CollectionAssert.AreEqual(expPath, acPath);
            Assert.AreEqual(expLength, acLength);
        }

        #endregion ShortestWeightedPath()

    }
}
