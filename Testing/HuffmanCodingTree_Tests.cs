﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class HuffmanCodingTree_Tests
    {
        #region Friendlies

        public Tuple<int, char>[] RankedLettersSet()   // passed  
        {
            List<Tuple<int, char>> results = new List<Tuple<int, char>>();

            string letters = "AabBcCdDeEfFgGHhIiJjkKLlMmNnOoPpqQRrSstTuUvVwWXxYyzZ";
            int[] ranks = { 32, 200, 109, 403, 1, 250, 25, 324, 544, 1000, 6, 321, 10, 341, 433, 822, 123, 524, 24, 235, 51, 337, 54, 342, 15, 717, 173, 353, 89, 617, 7, 491, 108, 132, 310, 794, 94, 953, 2, 101, 52, 91, 19, 28, 20, 873, 3, 623, 75, 685, 78, 81 };

            char[] characters = letters.ToCharArray();

            for (int i = 0; i < characters.Length; i++)
                results.Add(Tuple.Create(ranks[i], characters[i]));

            return results.ToArray();
        }

        public Tuple<int, char>[] RankedLettersSetShort()   // ok  
        {
            List<Tuple<int, char>> results = new List<Tuple<int, char>>();

            string letters = "AabBcC";
            int[] ranks = { 32, 324, 109, 403, 1, 250, };

            char[] characters = letters.ToCharArray();

            for (int i = 0; i < characters.Length; i++)
                results.Add(Tuple.Create(ranks[i], characters[i]));

            return results.ToArray();
        }

        public Tuple<int, char>[] RankedCharactersSet()  /* ok */  {
            List<Tuple<int, char>> results = new List<Tuple<int, char>>();

            string letters = "AabBcCdDeEfFgGHhIiJjkKLlMmNnOoPpqQRrSstTuUvVwWXxYyzZ.,' ";
            int[] ranks = { 32, 200, 109, 403, 1, 250, 25, 324, 544, 1000, 6, 321, 10, 
                              341, 433, 822, 123, 524, 24, 235, 51, 337, 54, 342, 15, 717, 173, 353, 89, 
                              617, 7, 491, 108, 132, 310, 794, 94, 953, 2, 101, 52, 91, 
                              19, 28, 20, 873, 3, 623, 75, 685, 78, 81,
                          42, 43, 44, 45};   // last line here is punctuation and space 

            char[] characters = letters.ToCharArray();

            for (int i = 0; i < characters.Length; i++)
                results.Add(Tuple.Create(ranks[i], characters[i]));

            return results.ToArray();
        }

        public object GatherKeys(object topic_object, int key, CompositePointerNode_<int, char> element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(key);

            return topic;
        }

        public object GatherElements(object topic_object, int key, CompositePointerNode_<int, char> element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(element.Element);

            return topic;
        }

        public object GatherNodeKeys(object topic_object, CompositePointerNode_<int, char> element)   // ok  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            // separating gathered values
            if (topic != string.Empty)
                topic += " - ";

            // gathering
            topic += Convert.ToString(element.Key);

            return topic;
        }

        public object GatherNodeElements(object topic_object, CompositePointerNode_<int, char> node)   // ok  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            // separating gathered values
            if (topic != string.Empty)
                topic += "-";

            // gathering
            topic += Convert.ToString(node.Element);

            return topic;
        }

        public bool IsMinOrdered(MinHeap_<int, CompositePointerNode_<int, char>> subject)   // passed  
        {
            // min-ordered means that a node is less than *or equal to* either of its children 

            bool isMinOrdered = true;
            List<bool> results = null;

            // get a list of booleans, true for each ordered pairing of 
            // parent and child, false for each disordered pairing 
            {
                subject.MoveToRoot();

                results = new List<bool>();
                results = MinOrderRecursor(subject, results);
            }

            // summarize results 
            foreach (bool finding in results) {
                // logical and + reassignment 
                isMinOrdered &= finding;

                // might as well stop at first false 
                if (!isMinOrdered)
                    break;
            }

            return isMinOrdered;
        }

        public List<bool> MinOrderRecursor(MinHeap_<int, CompositePointerNode_<int, char>> subject, List<bool> results)   // verified  
        {
            // .NodeKey for my minheap implemn is always the key of whichever node was most recently moved to 

            // this level 
            int currentKey = subject.NodeKey;

            // comparing with next levels (part of this level), 
            // and next levels immediately 
            if (!subject.NodeIsLeaf) {
                try {
                    subject.MoveToLeftChild();

                    // the remainder is never reached if .Move_ throws an exception 
                    {
                        results.Add(currentKey <= subject.NodeKey);

                        // already at L child, so recursion can continue here 
                        results = MinOrderRecursor(subject, results);

                        // *** return to current node ***
                        subject.MoveToParent();
                    }
                }
                catch { /* no operations */ }


                try {
                    subject.MoveToRightChild();

                    // the remainder is never reached if .Move_ throws an exception 
                    {
                        results.Add(currentKey <= subject.NodeKey);

                        results = MinOrderRecursor(subject, results);

                        // *** return to current node ***
                        subject.MoveToParent();
                    }
                }
                catch { /* no operations */ }
            }

            return results;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void RankedLettersSet_Test_Assert_Expecteds()   // working  
        {
            // no groundwork steps 


            // EXERCISING THE CODE  
            Tuple<int, char>[] subjects = this.RankedLettersSet();


            // TESTING  
            // 
            // randomly testing a few elements 
            Assert.AreEqual(Tuple.Create(32, 'A'), subjects[0]);
            Assert.AreEqual(Tuple.Create(200, 'a'), subjects[1]);
            Assert.AreEqual(Tuple.Create(109, 'b'), subjects[2]);
            Assert.AreEqual(Tuple.Create(310, 'R'), subjects[34]);
            Assert.AreEqual(Tuple.Create(78, 'z'), subjects[50]);
            Assert.AreEqual(Tuple.Create(81, 'Z'), subjects[51]);
        }

        [TestMethod()]
        public void IsMinOrdered_Test_Is_Min_Ordered_Assert_True()   // working  
        {
            // GROUNDWORK  
            // 
            // composite nodes are ignored, as they aren't addressed in comparing keys 
            MinHeap_<int, CompositePointerNode_<int, char>> heap = new MinHeap_<int, CompositePointerNode_<int, char>>(0, null);
            heap.AddHeapNode(1, null);
            heap.AddHeapNode(2, null);
            heap.AddHeapNode(3, null);

            bool expected = true;
            bool actual;


            // EXERCISING THE CODE  
            actual = this.IsMinOrdered(heap);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IsMinOrdered_Test_Is_Min_Ordered_All_Elements_Equal_Assert_True()   // working  
        {
            // GROUNDWORK  
            // 
            // composite nodes are ignored, as they aren't addressed in comparing keys 
            MinHeap_<int, CompositePointerNode_<int, char>> heap = new MinHeap_<int, CompositePointerNode_<int, char>>(5, null);
            heap.AddHeapNode(5, null);
            heap.AddHeapNode(5, null);
            heap.AddHeapNode(5, null);

            bool expected = true;
            bool actual;


            // EXERCISING THE CODE  
            actual = this.IsMinOrdered(heap);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IsMinOrdered_Test_Is_Not_Min_Ordered_Assert_False()   // working  
        {
            // GROUNDWORK  
            // 
            // composite nodes are ignored, as they aren't addressed in comparing keys 
            MinHeap_<int, CompositePointerNode_<int, char>> heap = new MinHeap_<int, CompositePointerNode_<int, char>>(0, null);

            // heap is constructed using private methods in order to avoid automatic sorting 
            {
                PrivateObject p = new PrivateObject(heap);

                Tuple<int, CompositePointerNode_<int, char>>[] array = new Tuple<int, CompositePointerNode_<int, char>>[]
                {
                    new Tuple<int, CompositePointerNode_<int, char>>(5, null),
                    new Tuple<int, CompositePointerNode_<int, char>>(4, null),
                    new Tuple<int, CompositePointerNode_<int, char>>(3, null),
                    new Tuple<int, CompositePointerNode_<int, char>>(2, null),
                    new Tuple<int, CompositePointerNode_<int, char>>(1, null),
                };

                p.SetField("_array", array);
                p.SetField("_treeTop", 4);
            }

            bool expected = false;
            bool actual;


            // EXERCISING THE CODE  
            actual = this.IsMinOrdered(heap);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        #region Cross-Compatibility Tests

        [TestMethod()]
        public void PolyMethod_Composites_And_Arrays_Compatibility_Test_Assert_Expected_Min_Ordered()   // working  
        {
            // min-ordered here will include two values that are equal 

            // GROUNDWORK  
            HuffmanCodingTree_<char> target = new HuffmanCodingTree_<char>(this.RankedLettersSet(), Usage.Debug);

            bool expected = true;
            bool actual = false;


            // EXERCISING THE CODE  
            target.Sorter.OrderHeap();

            // getting values and removing their nodes 
            CompositePointerNode_<int, char> firstRoot = target.Sorter.PopHeapRoot();
            CompositePointerNode_<int, char> secondRoot = target.Sorter.PopHeapRoot();

            // getting weighting metadata for inode 
            int newKey = firstRoot.Key + secondRoot.Key;

            // non-alphabetic character for easiest pinpointing 
            CompositePointerNode_<int, char> newRoot = CompositePointerNode_<int, char>.InitCompositePointerNode_(newKey, '@', firstRoot, secondRoot);

            // key Huffman step, returning partially built tree to heap 
            target.Sorter.AddHeapNode(newKey, newRoot);


            // TESTING  
            string strong = (string)target.Sorter.TraverseInorder(GatherElements);
            actual = this.IsMinOrdered(target.Sorter);
            Assert.AreEqual(expected, actual);
        }

        #endregion Cross-Compatibility Tests


        #region Construction Tests

        [TestMethod()]
        public void ConstructHuffmanFromHeap_Test_Assert_Expected_Key_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            HuffmanCodingTree_<char> target = new HuffmanCodingTree_<char>(RankedLettersSetShort(), Usage.Debug);
            PrivateObject p = new PrivateObject(target);

            string expected = "1119 - 403 - 716 - 324 - 392 - 142 - 33 - 1 - 32 - 109 - 250";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("ConstructHuffmanFromHeap");

            // TESTING  
            actual = (string)target.HuffmanTree.TraversePreorder(GatherNodeKeys);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ConstructHuffmanFromHeap_Test_Assert_Expected_Element_Traversal_Results()   // working  
        {
            // *** weird fact: null characters in a string ( \0 literals, for instance ) are treated as non-equal nulls! *** 

            // GROUNDWORK  
            HuffmanCodingTree_<char> target = new HuffmanCodingTree_<char>(RankedLettersSetShort(), Usage.Debug);
            PrivateObject p = new PrivateObject(target);

            string expected = @"0-B-0-a-0-0-0-c-A-b-C";     // \0 simplified to 0 to bypass non-equal nulls 
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("ConstructHuffmanFromHeap");

            // TESTING  
            actual = (string)target.HuffmanTree.TraversePreorder(GatherNodeElements);
            actual = actual.Replace('\0', '0');     // simplifying \0 to 0 to bypass non-equal nulls 

            Assert.AreEqual(expected, actual);
        }

        #endregion Construction Tests


        #region Encoding and Decoding Tests

        /* expected testing tree:                                                               
         *                                                                              
         *                                             1119                             
         *                                               |                              
         *                                      0---------------1                       
         *                                       |             |                        
         *           B: 0                        B           716                        
         *                                                     |                        
         *                                              0-------------1                 
         *                                               |           |                  
         *           a: 10                               a          392                 
         *                                                           |                  
         *                                                     0-------------1          
         *                                                      |           |           
         *           C: 111                                    142          C           
         *                                                      |                       
         *                                               0------------1                 
         *                                                |          |                  
         *           b: 1101                             33          b                  
         *                                                |                             
         *                                         0------------1                       
         *                                          |          |                        
         *           c: 11000                       c          A                        
         *           A: 11001                                                           
         *                                                                              
         */

        /* expected testing codes, in preorder: 
         *      B: 0 // a: 10 // c: 11000 // A: 11001 // b: 1101 // C: 111 
         */

        [TestMethod()]
        public void FillLookups_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            HuffmanCodingTree_<char> target = new HuffmanCodingTree_<char>(RankedLettersSetShort());
            PrivateObject p = new PrivateObject(target);

            // B: 0 // a: 10 // c: 11000 // A: 11001 // b: 1101 // C: 111 

            // testables
            Tuple<char, BitQueue>[] expected = new Tuple<char, BitQueue>[] {
                    Tuple.Create('B', new BitQueue(new bool[] { false })),                                  // 0 
                    Tuple.Create('a', new BitQueue(new bool[] { true, false})),                             // 10 
                    Tuple.Create('c', new BitQueue(new bool[] { true, true, false, false, false })),        // 11000 
                    Tuple.Create('A', new BitQueue(new bool[] { true, true, false, false, true })),         // 11001 
                    Tuple.Create('b', new BitQueue(new bool[] { true, true, false, true })),                // 1101 
                    Tuple.Create('C', new BitQueue(new bool[] { true, true, true }))                        // 111 
                };
            Tuple<char, BitQueue>[] actual;


            // EXERCISING THE CODE  
            p.Invoke("FillLookups");


            // TESTING  
            actual = target.Lookups;

            /* for this to work correctly, all classes in elements being compared must have an .Equals(object) override defined */
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PolyMethod_Test_Encode_Decode_Assert_Expected()   // working  
        {
            /* groundwork */
            string target = "McDougal";

            string expected = "McDougal";
            string actual;


            /* exercising the code */
            HuffmanCodingTree_<char> hct = new HuffmanCodingTree_<char>(RankedLettersSet());

            // lookup table is generated internally when first needed, retained, and applied 
            BitQueue encodedText = hct.Encode(target);

            // text is reconstructed using the HCT itself 
            actual = hct.Decode(encodedText);


            /* testing */
            Assert.AreEqual(expected, actual);


        }

        [TestMethod()]
        public void PolyMethod_Test_Long_Encode_Decode_Assert_Expected()  /* working */  {
            /* groundwork */
            string target = "McDougal and O'Malley went looking for fun.";

            string expected = "McDougal and O'Malley went looking for fun.";
            string actual;


            /* exercising the code */
            HuffmanCodingTree_<char> hct = new HuffmanCodingTree_<char>(RankedCharactersSet());

            // lookup table is generated internally when first needed, retained, and applied 
            BitQueue encodedText = hct.Encode(target);

            // text is reconstructed using the HCT itself 
            actual = hct.Decode(encodedText);


            /* testing */
            Assert.AreEqual(expected, actual);
        }

        #endregion Encoding and Decoding Tests
    }
}

/*
letter	rank
c	1
t	2
X	3
f	6
P	7
g	10
M	15
v	19
w	20
J	24
d	25
V	28
A	32
k	51
u	52
L	54
Y	75
z	78
Z	81
O	89
U	91
S	94
T	101
q	108
b	109
I	123
Q	132
N	173
a	200
j	235
C	250
R	310
F	321
D	324
K	337
G	341
l	342
n	353
B	403
H	433
p	491
i	524
e	544
o	617
x	623
y	685
m	717
r	794
h	822
W	873
s	953
E	1000
*/
