﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BinaryTree_PointerNodes_Tests
    {
        // I think just adapt the tests I wrote for the array-backed trees,
        // including for the traversals; I can review my notes first, though

        #region Friendlies

        public object GatherKeys(object topic_object, PointerNode_<char, int> node)
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(node.Key);

            return topic;
        }

        #endregion Friendlies


        #region Basic Tests

        [TestMethod()]
        public void PolyMethod_AddNode_Children_NodeProperties_Test_Assert_Expecteds()   // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // elements are added directly using nodes
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);

            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);
            current.RightChild = new PointerNode_<char, int>('g', 175);


            // TESTING  

            current = null;

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            current = target.Root;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            current = current.RightChild;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            current = target.Root;
            current = current.LeftChild;
            current = current.LeftChild;

            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        #endregion Basic Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);

            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);
            current.RightChild = new PointerNode_<char, int>('g', 175);


            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);

            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);
            current.RightChild = new PointerNode_<char, int>('g', 175);

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);

            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);
            current.RightChild = new PointerNode_<char, int>('g', 175);

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals


        #region Breadth-First Traversals

        [TestMethod()]
        public void TraverseBreadthFirst_Preorder_Test_Assert_Expected()   // working  
        {
            /* groundwork */
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);

            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);
            current.RightChild = new PointerNode_<char, int>('g', 175);


            string expected = "abcdefg";   // level 0 is "a", level 1 is "bc", level 2 is "defg" 
            string actual = string.Empty;


            /* exercising the code */
            actual = (string)target.TraverseBreadthFirstPreorder(GatherKeys);


            /* testing */
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseBreadthFirst_Postorder_Test_Assert_Expected()   // working  
        {
            /* groundwork */
            BinaryTree_PointerNodes_<char, int> target = new BinaryTree_PointerNodes_<char, int>('a', 100);
            PointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = new PointerNode_<char, int>('b', 50);

            current = current.LeftChild;
            current.LeftChild = new PointerNode_<char, int>('d', 25);
            current.RightChild = new PointerNode_<char, int>('e', 75);

            current = target.Root;
            current.RightChild = new PointerNode_<char, int>('c', 150);

            current = current.RightChild;
            current.LeftChild = new PointerNode_<char, int>('f', 125);

            current.RightChild = new PointerNode_<char, int>('g', 175);

            string expected = "defgbca";   // level 2 is "defg", level 1 is "bc", level 0 is "a" 
            string actual = string.Empty;


            /* exercising the code */
            actual = (string)target.TraverseBreadthFirstPostorder(GatherKeys);


            /* testing */
            Assert.AreEqual(expected, actual);
        }

        #endregion Breadth-First Traversals

    }
}
