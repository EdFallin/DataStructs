﻿using System;
using System.Collections;
using System.Collections.Generic;

using DataStructs;
using Advanceds;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass]
    public class BPlusTreeTests
    {
        #region Fixtures and their tests

        /* see image files in the Ancillaries subfolder to see what these trees look like */

        /* 'verified / passed' == overall verified, some ops passed tests (here, the linked-list ops) */

        public BPlusTree<char> FixtureTestTreeChars()  /* verified / passed */  {
            /* forced construction of a simple, narrow tree; starting at leaves and working rootward */

            BPlusTree<char> tree = new BPlusTree<char>(4);  // narrow tree 


            /* leaves; must have keys, just as forks do */
            LeafBpt<char> _abc = new LeafBpt<char>('$');
            _abc.Keys[0] = '$'; _abc.Keys[1] = 'a'; _abc.Keys[2] = 'b'; _abc.Keys[3] = 'c';
            _abc.Contents[0] = new ElementBpt<char>('$'); _abc.Contents[1] = new ElementBpt<char>('a');
            _abc.Contents[2] = new ElementBpt<char>('b'); _abc.Contents[3] = new ElementBpt<char>('c');
            _abc.TopKey = 3;

            LeafBpt<char> de = new LeafBpt<char>('d');
            de.Keys[0] = 'd'; de.Keys[1] = 'e';
            de.Contents[0] = new ElementBpt<char>('d'); de.Contents[1] = new ElementBpt<char>('e');
            de.TopKey = 1;

            LeafBpt<char> fgh = new LeafBpt<char>('f');
            fgh.Keys[0] = 'f'; fgh.Keys[1] = 'g'; fgh.Keys[2] = 'h';
            fgh.Contents[0] = new ElementBpt<char>('f'); fgh.Contents[1] = new ElementBpt<char>('g');
            fgh.Contents[2] = new ElementBpt<char>('h');
            fgh.TopKey = 2;

            LeafBpt<char> ijkl = new LeafBpt<char>('i');
            ijkl.Keys[0] = 'i'; ijkl.Keys[1] = 'j'; ijkl.Keys[2] = 'k'; ijkl.Keys[3] = 'l';
            ijkl.Contents[0] = new ElementBpt<char>('i'); ijkl.Contents[1] = new ElementBpt<char>('j');
            ijkl.Contents[2] = new ElementBpt<char>('k'); ijkl.Contents[3] = new ElementBpt<char>('l');
            ijkl.TopKey = 3;

            LeafBpt<char> mno = new LeafBpt<char>('m');
            mno.Keys[0] = 'm'; mno.Keys[1] = 'n'; mno.Keys[2] = 'o';
            mno.Contents[0] = new ElementBpt<char>('m'); mno.Contents[1] = new ElementBpt<char>('n');
            mno.Contents[2] = new ElementBpt<char>('o');
            mno.TopKey = 2;

            LeafBpt<char> pqrs = new LeafBpt<char>('p');
            pqrs.Keys[0] = 'p'; pqrs.Keys[1] = 'q'; pqrs.Keys[2] = 'r'; pqrs.Keys[3] = 's';
            pqrs.Contents[0] = new ElementBpt<char>('p'); pqrs.Contents[1] = new ElementBpt<char>('q');
            pqrs.Contents[2] = new ElementBpt<char>('r'); pqrs.Contents[3] = new ElementBpt<char>('s');
            pqrs.TopKey = 3;

            LeafBpt<char> tuv = new LeafBpt<char>('t');
            tuv.Keys[0] = 't'; tuv.Keys[1] = 'u'; tuv.Keys[2] = 'v';
            tuv.Contents[0] = new ElementBpt<char>('t'); tuv.Contents[1] = new ElementBpt<char>('u');
            tuv.Contents[2] = new ElementBpt<char>('v');
            tuv.TopKey = 2;

            LeafBpt<char> wx = new LeafBpt<char>('w');
            wx.Keys[0] = 'w'; wx.Keys[1] = 'x';
            wx.Contents[0] = new ElementBpt<char>('w'); wx.Contents[1] = new ElementBpt<char>('x');
            wx.TopKey = 1;

            LeafBpt<char> yz = new LeafBpt<char>('y');
            yz.Keys[0] = 'y'; yz.Keys[1] = 'z';
            yz.Contents[0] = new ElementBpt<char>('y');
            yz.Contents[1] = new ElementBpt<char>('z');
            yz.TopKey = 1;


            /* linked list */
            LeafBpt<char>[] leaves = { _abc, de, fgh, ijkl, mno, pqrs, tuv, wx, yz };

            ElementBpt<char> lastOfPriorLeaf = null;

            for (int atLeaf = 0; atLeaf < leaves.Length; atLeaf++) {
                LeafBpt<char> leaf = leaves[atLeaf];

                /* first el in leaf; distinct handling */
                ElementBpt<char> first = leaf.Contents[0] as ElementBpt<char>;
                first.Previous = lastOfPriorLeaf;  // null at Lmost leaf 

                /* .Next of last el */
                if (lastOfPriorLeaf != null) {
                    lastOfPriorLeaf.Next = first;
                }

                /* other els in leaf, except .Next of last el */
                for (int atEl = 1; atEl < leaf.Count; atEl++) {
                    ElementBpt<char> current = leaf.Contents[atEl] as ElementBpt<char>;
                    ElementBpt<char> previous = leaf.Contents[atEl - 1] as ElementBpt<char>;

                    previous.Next = current;
                    current.Previous = previous;
                }

                lastOfPriorLeaf = leaf.Contents[leaf.TopKey] as ElementBpt<char>;
            }


            /* forks */
            ForkBpt<char> _df = new ForkBpt<char>('$');
            _df.Keys[0] = '$'; _df.Keys[1] = 'd'; _df.Keys[2] = 'f';  // unkey @0 
            _df.Contents[0] = _abc; _df.Contents[1] = de; _df.Contents[2] = fgh;
            _df.TopKey = 2;

            ForkBpt<char> impt = new ForkBpt<char>('i');
            impt.Keys[0] = 'i'; impt.Keys[1] = 'm'; impt.Keys[2] = 'p'; impt.Keys[3] = 't';  // unkey @0 
            impt.Contents[0] = ijkl; impt.Contents[1] = mno; impt.Contents[2] = pqrs; impt.Contents[3] = tuv;
            impt.TopKey = 3;

            ForkBpt<char> wy = new ForkBpt<char>('w');
            wy.Keys[0] = 'w'; wy.Keys[1] = 'y';  // unkey @0 
            wy.Contents[0] = wx; wy.Contents[1] = yz;
            wy.TopKey = 1;


            /* root */
            ForkBpt<char> root = new ForkBpt<char>('\0');  // root's key never used 
            root.Keys[0] = '$'; root.Keys[1] = 'i'; root.Keys[2] = 'w';  // unkey @0 
            root.Contents[0] = _df; root.Contents[1] = impt; root.Contents[2] = wy;
            root.TopKey = 2;

            tree.Root = root;


            /* convey */
            return tree;
        }

        public void FixtureAddRmostLeaf(BPlusTree<char> tree)  /* verified */  {
            ForkBpt<char> root = tree.Root as ForkBpt<char>;
            ForkBpt<char> rMostFork = root.Contents[2] as ForkBpt<char>;

            LeafBpt<char> lastLeaf = rMostFork.Contents[1] as LeafBpt<char>;
            ElementBpt<char> lastEl = lastLeaf.Contents[1] as ElementBpt<char>;

            LeafBpt<char> newLeaf = new LeafBpt<char>('{');

            char[] chars = { '{', '|', '}' };

            for (int i = 0; i < chars.Length; i++) {
                /* keys and then contents of new leaf */
                newLeaf.Keys[i] = chars[i];

                ElementBpt<char> newEl = new ElementBpt<char>(chars[i]);
                newLeaf.Contents[i] = newEl;

                /* linked list */
                newEl.Previous = lastEl;
                lastEl.Next = newEl;

                /* previous of each new el */
                lastEl = newEl;
            }

            /* vital metadata */
            newLeaf.Count = 3;

            /* adding leaf to structure, with metadata */
            rMostFork.Contents[2] = newLeaf;
            rMostFork.Keys[2] = newLeaf.Key;
            rMostFork.Count++;
        }

        public BPlusTree<int> FixtureTestTreeSevens()  /* verified / passed */  {
            /* forced construction of a simple, narrow tree; starting at leaves and working rootward */

            BPlusTree<int> tree = new BPlusTree<int>(4);  // narrow tree 


            /* leaves; must have keys, just as forks do */
            LeafBpt<int> from0to21 = new LeafBpt<int>(0);
            from0to21.Keys[0] = 0; from0to21.Keys[1] = 7; from0to21.Keys[2] = 14; from0to21.Keys[3] = 21;
            from0to21.Contents[0] = new ElementBpt<int>(0); from0to21.Contents[1] = new ElementBpt<int>(7);
            from0to21.Contents[2] = new ElementBpt<int>(14); from0to21.Contents[3] = new ElementBpt<int>(21);
            from0to21.TopKey = 3;

            LeafBpt<int> from28to35 = new LeafBpt<int>(28);
            from28to35.Keys[0] = 28; from28to35.Keys[1] = 35;
            from28to35.Contents[0] = new ElementBpt<int>(28); from28to35.Contents[1] = new ElementBpt<int>(35);
            from28to35.TopKey = 1;

            LeafBpt<int> from42to56 = new LeafBpt<int>(42);
            from42to56.Keys[0] = 42; from42to56.Keys[1] = 49; from42to56.Keys[2] = 56;
            from42to56.Contents[0] = new ElementBpt<int>(42); from42to56.Contents[1] = new ElementBpt<int>(49);
            from42to56.Contents[2] = new ElementBpt<int>(56);
            from42to56.TopKey = 2;

            LeafBpt<int> from63to84 = new LeafBpt<int>(63);
            from63to84.Keys[0] = 63; from63to84.Keys[1] = 70; from63to84.Keys[2] = 77; from63to84.Keys[3] = 84;
            from63to84.Contents[0] = new ElementBpt<int>(63); from63to84.Contents[1] = new ElementBpt<int>(70);
            from63to84.Contents[2] = new ElementBpt<int>(77); from63to84.Contents[3] = new ElementBpt<int>(84);
            from63to84.TopKey = 3;

            LeafBpt<int> from91to105 = new LeafBpt<int>(91);
            from91to105.Keys[0] = 91; from91to105.Keys[1] = 98; from91to105.Keys[2] = 105;
            from91to105.Contents[0] = new ElementBpt<int>(91); from91to105.Contents[1] = new ElementBpt<int>(98);
            from91to105.Contents[2] = new ElementBpt<int>(105);
            from91to105.TopKey = 2;

            LeafBpt<int> from112to133 = new LeafBpt<int>(112);
            from112to133.Keys[0] = 112; from112to133.Keys[1] = 119; from112to133.Keys[2] = 126; from112to133.Keys[3] = 133;
            from112to133.Contents[0] = new ElementBpt<int>(112); from112to133.Contents[1] = new ElementBpt<int>(119);
            from112to133.Contents[2] = new ElementBpt<int>(126); from112to133.Contents[3] = new ElementBpt<int>(133);
            from112to133.TopKey = 3;

            LeafBpt<int> from140to154 = new LeafBpt<int>(140);
            from140to154.Keys[0] = 140; from140to154.Keys[1] = 147; from140to154.Keys[2] = 154;
            from140to154.Contents[0] = new ElementBpt<int>(140); from140to154.Contents[1] = new ElementBpt<int>(147);
            from140to154.Contents[2] = new ElementBpt<int>(154);
            from140to154.TopKey = 2;

            LeafBpt<int> from161to168 = new LeafBpt<int>(161);
            from161to168.Keys[0] = 161; from161to168.Keys[1] = 168;
            from161to168.Contents[0] = new ElementBpt<int>(161); from161to168.Contents[1] = new ElementBpt<int>(168);
            from161to168.TopKey = 1;

            LeafBpt<int> from175to182 = new LeafBpt<int>(175);
            from175to182.Keys[0] = 175; from175to182.Keys[1] = 182;
            from175to182.Contents[0] = new ElementBpt<int>(175);
            from175to182.Contents[1] = new ElementBpt<int>(182);
            from175to182.TopKey = 1;


            /* linked list */
            LeafBpt<int>[] leaves = {
                from0to21, from28to35, from42to56,
                from63to84, from91to105, from112to133,
                from140to154, from161to168, from175to182 };

            ElementBpt<int> lastOfPriorLeaf = null;

            for (int atLeaf = 0; atLeaf < leaves.Length; atLeaf++) {
                LeafBpt<int> leaf = leaves[atLeaf];

                /* first el in leaf; distinct handling */
                ElementBpt<int> first = leaf.Contents[0] as ElementBpt<int>;
                first.Previous = lastOfPriorLeaf;  // null at Lmost leaf 

                /* .Next of last el */
                if (lastOfPriorLeaf != null) {
                    lastOfPriorLeaf.Next = first;
                }

                /* other els in leaf, except .Next of last el */
                for (int atEl = 1; atEl < leaf.Count; atEl++) {
                    ElementBpt<int> current = leaf.Contents[atEl] as ElementBpt<int>;
                    ElementBpt<int> previous = leaf.Contents[atEl - 1] as ElementBpt<int>;

                    previous.Next = current;
                    current.Previous = previous;
                }

                lastOfPriorLeaf = leaf.Contents[leaf.TopKey] as ElementBpt<int>;
            }


            /* forks */
            ForkBpt<int> from0to42 = new ForkBpt<int>(0);
            from0to42.Keys[0] = 0; from0to42.Keys[1] = 28; from0to42.Keys[2] = 42;  // unkey @0 
            from0to42.Contents[0] = from0to21; from0to42.Contents[1] = from28to35; from0to42.Contents[2] = from42to56;
            from0to42.TopKey = 2;

            ForkBpt<int> from63to140 = new ForkBpt<int>(63);
            from63to140.Keys[0] = 63; from63to140.Keys[1] = 91; from63to140.Keys[2] = 112; from63to140.Keys[3] = 140;  // unkey @0 
            from63to140.Contents[0] = from63to84; from63to140.Contents[1] = from91to105; from63to140.Contents[2] = from112to133; from63to140.Contents[3] = from140to154;
            from63to140.TopKey = 3;

            ForkBpt<int> from161to175 = new ForkBpt<int>(161);
            from161to175.Keys[0] = 161; from161to175.Keys[1] = 175;  // unkey @0 
            from161to175.Contents[0] = from161to168; from161to175.Contents[1] = from175to182;
            from161to175.TopKey = 1;


            /* root */
            ForkBpt<int> root = new ForkBpt<int>(0);  // root's key never used 
            root.Keys[0] = 0; root.Keys[1] = 63; root.Keys[2] = 161;  // unkey @0 
            root.Contents[0] = from0to42; root.Contents[1] = from63to140; root.Contents[2] = from161to175;
            root.TopKey = 2;

            tree.Root = root;


            /* convey */
            return tree;
        }

        public BPlusTree<double> FixtureTestTreeDoubles()  /* passed */  {
            /* build tree with whole-number doubles at leaves */

            BPlusTree<double> tree = new BPlusTree<double>(4);  // size must be set first here, for all objects 

            ForkBpt<double> root = new ForkBpt<double>();

            ElementBpt<double> last = null;

            for (int atFork = 0, forkKey = 0; atFork < 4; atFork++, forkKey += 16) {
                ForkBpt<double> fork = new ForkBpt<double>();
                fork.Key = forkKey;  // implicit conversion; same later here 

                for (int atLeaf = 0, leafKey = forkKey; atLeaf < 4; atLeaf++, leafKey += 4) {
                    LeafBpt<double> leaf = new LeafBpt<double>();
                    leaf.Key = leafKey;

                    for (int atEl = 0, elKey = leafKey; atEl < 4; atEl++, elKey++) {
                        ElementBpt<double> el = new ElementBpt<double>(elKey);

                        leaf.Keys[atEl] = el.Key;
                        leaf.Contents[atEl] = el;
                        leaf.Count++;  // vital metadata 

                        /* linked list */
                        el.Previous = last;

                        if (last != null) {
                            last.Next = el;
                        }

                        last = el;
                    }

                    fork.Keys[atLeaf] = leaf.Key;
                    fork.Contents[atLeaf] = leaf;
                    fork.Count++;  // vital metadata 
                }

                root.Keys[atFork] = fork.Key;
                root.Contents[atFork] = fork;
                root.Count++;  // vital metadata 
            }

            /* output */
            tree.Root = root;

            return tree;
        }

        private void FixtureTruncateLmostLeaf(BPlusTree<int> tree)  /* verified */  {
            /* drop 2 els from leaf 0 so that removing from leaf 1 will cause sloshing from R (leaf 2) */

            /* get to correct structure point */
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;

            /* dropping from leaf */
            lmostLeaf.Keys[2] = -1;
            lmostLeaf.Keys[3] = -1;
            lmostLeaf.Contents[2] = null;
            lmostLeaf.Contents[3] = null;
            lmostLeaf.TopKey -= 2;  // vital metadata 

            /* dropping from linked list */
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;  // 0 
            ElementBpt<int> leftOf = el.Next;  // 7; before the two to skip 
            ElementBpt<int> rightOf = leftOf.Next.Next.Next;  // 28; across the two to skip and to the one after 

            leftOf.Next = rightOf;
            rightOf.Previous = leftOf;
        }

        private void FixtureTruncate3rdLeaf(BPlusTree<int> tree)  /* verified */  {
            /* drop 1 el (the last, 56) from leaf 2 so that removing from leaf 1 will cause merging from L or R */

            /* get to correct structure points */
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> targetLeaf = lmostFork.Contents[2] as LeafBpt<int>;

            ElementBpt<int> el = targetLeaf.Contents[2] as ElementBpt<int>;


            /* dropping from leaf */
            targetLeaf.Keys[2] = -1;
            targetLeaf.Contents[2] = null;
            targetLeaf.TopKey--;  // vital metadata 


            /* dropping from linked list */
            ElementBpt<int> leftOf = el.Previous;  // 49 
            ElementBpt<int> rightOf = el.Next;     // 63 

            leftOf.Next = rightOf;
            rightOf.Previous = leftOf;
        }

        public BPlusTree<double> FixtureTestTreeHalfFullDoubles()  /* verified */  {
            /* build tree of consecutive whole doubles, but only half-full to test node-merging to root */

            BPlusTree<double> tree = new BPlusTree<double>(8);  // size must be set first here, for all objects 

            ForkBpt<double> root = new ForkBpt<double>();

            ElementBpt<double> last = null;

            for (int atFork = 0, forkKey = 0; atFork < 2; atFork++, forkKey += 16) {
                ForkBpt<double> fork = new ForkBpt<double>();
                fork.Key = forkKey;  // implicit conversion; same later here 

                for (int atLeaf = 0, leafKey = forkKey; atLeaf < 4; atLeaf++, leafKey += 4) {
                    LeafBpt<double> leaf = new LeafBpt<double>();
                    leaf.Key = leafKey;

                    for (int atEl = 0, elKey = leafKey; atEl < 4; atEl++, elKey++) {
                        ElementBpt<double> el = new ElementBpt<double>(elKey);

                        leaf.Keys[atEl] = el.Key;
                        leaf.Contents[atEl] = el;
                        leaf.Count++;  // vital metadata 

                        /* linked list */
                        el.Previous = last;

                        if (last != null) {
                            last.Next = el;
                        }

                        last = el;
                    }

                    fork.Keys[atLeaf] = leaf.Key;
                    fork.Contents[atLeaf] = leaf;
                    fork.Count++;  // vital metadata 
                }

                root.Keys[atFork] = fork.Key;
                root.Contents[atFork] = fork;
                root.Count++;  // vital metadata 
            }

            /* output */
            tree.Root = root;

            return tree;
        }


        #region Tests of these fixtures

        [TestMethod()]
        public void FixtureTestTreeChars_Test__Links()  /* working */  {
            //**  groundwork  **//
            /* no groundwork */


            //**  exercising the code  **//
            BPlusTree<char> tree = FixtureTestTreeChars();


            //**  gathering data  **//
            NodeBpt<char> root = tree.Root as NodeBpt<char>;
            NodeBpt<char> lmostFork = root.Contents[0] as NodeBpt<char>;
            NodeBpt<char> lmostLeaf = lmostFork.Contents[0] as NodeBpt<char>;

            ElementBpt<char> element = lmostLeaf.Contents[0] as ElementBpt<char>;


            //**  testing  **//
            Assert.AreEqual('$', element.Key);
            element = element.Next;
            Assert.AreEqual('a', element.Key);
            Assert.AreEqual('$', element.Previous.Key);

            for (int i = 0; i < 26; i++) {
                /* testing forward reference */
                Assert.AreEqual('a' + i, element.Key);  // .Next of last tested 
                element = element.Next;

                /* last el has null forward reference */
                if (i == 25) {
                    Assert.AreEqual(null, element);
                    break;
                }

                /* testing backward reference */
                Assert.AreEqual('a' + i, element.Previous.Key);
            }
        }

        [TestMethod()]
        public void FixtureTestTreeSevens_Test__Links()  /* working */  {
            //**  groundwork  **//
            /* no groundwork */


            //**  exercising the code  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  gathering data  **//
            NodeBpt<int> root = tree.Root as NodeBpt<int>;
            NodeBpt<int> lmostFork = root.Contents[0] as NodeBpt<int>;
            NodeBpt<int> lmostLeaf = lmostFork.Contents[0] as NodeBpt<int>;

            ElementBpt<int> element = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i <= 182; i += 7) {
                /* testing this element */
                Assert.AreEqual(i, element.Key);

                /* testing backward reference; null at first el */
                if (i == 0) {
                    Assert.AreEqual(null, element.Previous);
                }
                else {
                    Assert.AreEqual(i - 7, element.Previous.Key);
                }


                /* stepping forward */
                element = element.Next;


                /* testing forward reference; null at last el */
                if (i == 182) {
                    Assert.AreEqual(null, element);
                }
                else {
                    Assert.AreEqual(i + 7, element.Key);
                }
            }
        }

        [TestMethod()]
        public void FixtureTestTreeDoubles_Test__Structure()  /* working */  {
            //**  groundwork : none  **//


            //**  exercising the code  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();


            //**  defining recursive testing lambda  **//
            Action<IKeyedObject<double>, double> test = null;  /* if not in its own line, can't rcall */

            test = (IKeyedObject<double> at, double of) => {
                if (at is ForkBpt<double>) {
                    ForkBpt<double> asFork = at as ForkBpt<double>;

                    Assert.AreEqual(4, asFork.Count);
                    Assert.AreEqual(of, asFork.Key);

                    for (int i = 0; i < 4; i++) {
                        Assert.AreEqual(of + (i * 4), asFork.Keys[i]);
                        test(asFork.Contents[i], of + (i * 4));  // rcall to leaves 
                    }
                }

                if (at is LeafBpt<double>) {
                    LeafBpt<double> asLeaf = at as LeafBpt<double>;

                    Assert.AreEqual(4, asLeaf.Count);
                    Assert.AreEqual(of, asLeaf.Key);

                    for (int i = 0; i < 4; i++) {
                        Assert.AreEqual(of + i, asLeaf.Keys[i]);
                        test(asLeaf.Contents[i], of + i);  // rcall to els 
                    }
                }

                if (at is ElementBpt<double>) {
                    ElementBpt<double> asEl = at as ElementBpt<double>;
                    Assert.AreEqual(of, asEl.Key);
                }
            };


            //**  testing  **//
            ForkBpt<double> root = tree.Root as ForkBpt<double>;
            Assert.AreEqual(4, root.Count);

            for (int i = 0; i < 4; i++) {
                Assert.AreEqual(i * 16, root.Keys[i]);
                test(root.Contents[i], i * 16);
            }
        }

        [TestMethod()]
        public void FixtureTestTreeDoubles_Test__Links()  /* working */  {
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            /* get Lmost el of Lmost leaf */
            ForkBpt<double> fork = tree.Root.Contents[0] as ForkBpt<double>;
            LeafBpt<double> leaf = fork.Contents[0] as LeafBpt<double>;
            ElementBpt<double> el = leaf.Contents[0] as ElementBpt<double>;

            /* traverse linked list */
            for (int i = 0; i < 64; i++) {
                Assert.AreEqual(i, el.Key);  // int treated as double auto'ly 
                el = el.Next;
            }
        }

        #endregion Tests of these fixtures

        #endregion Fixtures and their tests


        /* *** tests follow *** */


        #region Search()

        [TestMethod()]
        public void Search_Test__Sought_Is_Char_i__Assert_Found()  /* working */  {
            //**  groundwork  **//
            BPlusTree<char> target = FixtureTestTreeChars();


            //**  exercising the code  **//
            ElementBpt<char> actual = target.Search('i');


            //**  testing  **//
            Assert.AreEqual('i', actual?.Key);
        }

        [TestMethod()]
        public void Search_Test__Sought_Is_Char_p__Assert_Found()  /* working */  {
            //**  groundwork  **//
            BPlusTree<char> target = FixtureTestTreeChars();


            //**  exercising the code  **//
            ElementBpt<char> actual = target.Search('p');


            //**  testing  **//
            Assert.AreEqual('p', actual?.Key);
        }

        [TestMethod()]
        public void Search_Test__Sought_Is_Char_2__Assert_Not_Found()  /* working */  {
            //**  groundwork  **//
            BPlusTree<char> target = FixtureTestTreeChars();


            //**  exercising the code  **//
            ElementBpt<char> actual = target.Search('2');  // valid but not present 


            //**  testing  **//
            Assert.AreEqual(null, actual?.Key);  // '?': no object, no .Key 
        }

        #endregion Search()


        #region SearchUpward()

        [TestMethod()]
        public void SearchUpward_Test__Known_Range__Assert_Corrects()  /* working */  {
            /* whole doubles between 6.5 and 17.5: 7.0 to 17.0 */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                7, 8, 9, 10, 11, 12,
                13, 14, 15, 16, 17 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchUpward(6.5, 17.5);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchUpward_Test__Exact_Matches__Assert_Corrects()  /* working */  {
            /* whole doubles between 7.0 and 17.0: 7.0 to 17.0 */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                7, 8, 9, 10, 11, 12,
                13, 14, 15, 16, 17 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchUpward(7.0, 17.0);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchUpward_Test__Excess_Range__Assert_Through_Top()  /* working */  {
            /* ~highest > top key in tree; should return all through end of tree */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                55, 56, 57, 58, 59,
                60, 61, 62, 63 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchUpward(54.73, 150.28);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchUpward_Test__Beyond_Range__Assert_Empty()  /* working */  {
            /* ~lowest > top key in tree; should return empty vector */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = { };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchUpward(99.88, 137);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);
        }

        #endregion SearchUpward()


        #region SearchDownward()

        [TestMethod()]
        public void SearchDownward_Test__Known_Range__Assert_Corrects()  /* working */  {
            /* whole doubles between 48.8 and 39.9: 48.0 to 40.0 */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                48, 47, 46, 45, 44,
                43, 42, 41, 40 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchDownward(48.8, 39.9);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchDownward_Test__Exact_Matches__Assert_Corrects()  /* working */  {
            /* whole doubles between 48.0 and 40.0: 48.0 to 40.0 */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                48, 47, 46, 45, 44,
                43, 42, 41, 40 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchDownward(48.0, 40.0);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchDownward_Test__Excess_Range__Assert_Through_Bottom()  /* working */  {
            /* ~lowest < bottom key in tree; should return all through start of tree */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = {
                17, 16, 15, 14, 13, 12, 11, 10,
                9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchDownward(17.52, -3.3);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);

            /* no CollectionAssert(): ~actuals not ICollection */
            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Key);
            }
        }

        [TestMethod()]
        public void SearchDownward_Test__Beyond_Range__Assert_Empty()  /* working */  {
            /* ~highest < bottom key in tree; should return empty vector */

            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();

            double[] expecteds = { };


            //**  exercising the code  **//
            List_Vector_Extended_<ElementBpt<double>> actuals = tree.SearchDownward(-0.5, -10);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Length);
        }

        #endregion SearchDownward()


        #region Insert()


        #region No restructuring

        [TestMethod()]
        public void Insert_Test__At_Last_El__No_Restruct__Assert_Correct_Key_Pointer()  /* working */  {
            /* insert el with key of 39; it ends up in leaf ~from28to35, 
             * which is child of ~from0to42, which is child of root */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();

            int pointer = int.MaxValue / 2;

            ElementBpt<int> insertable = new ElementBpt<int>(39, pointer);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            ForkBpt<int> fork = tree.Root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> leaf = fork.Contents[1] as LeafBpt<int>;
            ElementBpt<int> element = leaf.Contents[2] as ElementBpt<int>;


            //**  testing  **//
            Assert.AreEqual(insertable.Key, leaf.Keys[2]);
            Assert.AreEqual(insertable.Pointer, element.Pointer);
        }

        [TestMethod()]
        public void Insert_Test__Amid_Els__No_Restruct__Assert_Correct_Key_Pointer()  /* working */  {
            /* insert el with key of 102; it ends up in leaf ~from91to105, 
             * which is child of ~from63to140, which is child of root */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();

            int pointer = int.MaxValue / 3;

            ElementBpt<int> insertable = new ElementBpt<int>(102, pointer);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            ForkBpt<int> fork = tree.Root.Contents[1] as ForkBpt<int>;
            LeafBpt<int> leaf = fork.Contents[1] as LeafBpt<int>;
            ElementBpt<int> element = leaf.Contents[2] as ElementBpt<int>;  // prior @2 shifted 1 R 


            //**  testing  **//
            Assert.AreEqual(insertable.Key, leaf.Keys[2]);
            Assert.AreEqual(insertable.Pointer, element.Pointer);
        }

        [TestMethod()]
        public void Insert_Test__At_Last_El__No_Restruct__Assert_Continuous_LinkedList()  /* working */  {
            /* insert el with key of 39; it ends up in leaf ~from28to35, 
             * which is child of ~from0to42, which is child of root */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();

            int pointer = int.MaxValue / 2;

            ElementBpt<int> insertable = new ElementBpt<int>(39, pointer);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            /* leftmost element of whole tree */
            ForkBpt<int> fork = tree.Root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> leaf = fork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> element = leaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0, key = -1, lastKey = -1; i < 28; i++) {
                #region Calculating correct ~key, accounting for insertion

                if (i < 6) {
                    key = 7 * i;
                }

                if (i == 6) {
                    key = 39;
                }

                if (i > 6) {
                    key = 7 * (i - 1);
                }

                #endregion Calculating correct ~key, accounting for insertion

                /* testing forward links, in effect */
                Assert.AreEqual(key, element.Key);

                /* testing backward links, except Lmost */
                if (i > 0) {
                    Assert.AreEqual(lastKey, element.Previous.Key);
                }

                lastKey = key;

                /* stepping forward; null after Rmost */
                element = element.Next;
            }
        }

        /* there's no test for inserting at the first el ( @0 ) without restruct, 
                 * because such a key will be inserted at R end of L sibling or cousin */

        #endregion No restructuring


        #region Restructuring at leaves only

        [TestMethod()]
        public void Insert_Test__Amid_Els__Restruct_Leaves_Only__Assert_Correct_Data()  /* working */  {
            /* insert el with key of 25; it should split leaf ~from0to21 
             *     into [from-0-to-14] and [from 21-to-25], 
             * but fork ~from0to42 should only acquire new item, 
             *     key 21 with child; 
             * root unchanged */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();

            int pointer = int.MaxValue / 4;

            ElementBpt<int> insertable = new ElementBpt<int>(25, pointer);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            ForkBpt<int> fork = tree.Root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> leftSibling = fork.Contents[0] as LeafBpt<int>;
            LeafBpt<int> leaf = fork.Contents[1] as LeafBpt<int>;
            LeafBpt<int> rightSibling = fork.Contents[2] as LeafBpt<int>;
            ElementBpt<int> element = leaf.Contents[1] as ElementBpt<int>;  // prior @2 shifted 1 R 


            //**  testing  **//
            /* fork */
            Assert.AreEqual(4, fork.Count);
            Assert.AreEqual(0, fork.Keys[0]);  // 1 L of insert site; original Lmost leaf @0 
            Assert.AreEqual(21, fork.Keys[1]);  // _new_ child, inserted in fork @1 with existing .Key 
            Assert.AreEqual(28, fork.Keys[2]);  // 1 R of insert site; prior leaf @1, now @2 

            /* leaf that was split (now L sibling) */
            Assert.AreEqual(3, leftSibling.Count);  // split-from has [½ +1] items here 
            Assert.AreEqual(14, leftSibling.Keys[2]);  // last item L of ~insertable 

            /* new leaf, split from now- L-sibling, ~insertable added */
            Assert.AreEqual(2, leaf.Count);
            Assert.AreEqual(21, leaf.Keys[0]);  // R-transferred prior item 

            /* inserted item on new leaf */
            Assert.AreEqual(insertable.Key, leaf.Keys[1]);  // new item is Rmost in new leaf 
            Assert.AreEqual(insertable.Pointer, element.Pointer);

            /* right sibling; relocated, but should be unchanged */
            Assert.AreEqual(28, rightSibling.Key);
            Assert.AreEqual(2, rightSibling.Count);
            Assert.AreEqual(28, rightSibling.Keys[0]);
        }

        #endregion Restructuring at leaves only


        #region Restructuring to mid-depth forks

        [TestMethod()]
        public void Insert_Test__Amid_Els__Restruct_Leaf_And_Fork__Assert_Correct_Data()  /* working */  {
            /* insert el with key of 67; 
             * leaf ~from63to84 should split into [from-63-to-77] and [from 84-to-84]; 
             * fork ~from63to140 should split into [from-x-to-y] and [from-x-to-y]; 
             * root should end up with 4 items */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            ElementBpt<int> insertable = new ElementBpt<int>(67, int.MaxValue / 9);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//
            #region Root

            Assert.AreEqual(4, root.Count);
            Assert.AreEqual(0, root.Keys[0]);  // unchanged subtree 
            Assert.AreEqual(63, root.Keys[1]);  // original subtree, split, same key 
            Assert.AreEqual(140, root.Keys[2]);  // new sibling of original 
            Assert.AreEqual(161, root.Keys[3]);  // unchanged but R-shifted subtree 

            #endregion Root

            ForkBpt<int> fork;
            LeafBpt<int> leaf;

            #region Split-from subtree: fork

            fork = root.Contents[1] as ForkBpt<int>;
            Assert.AreEqual(4, fork.Count);

            Assert.AreEqual(63, fork.Keys[0]);
            Assert.AreEqual(84, fork.Keys[1]);
            Assert.AreEqual(91, fork.Keys[2]);
            Assert.AreEqual(112, fork.Keys[3]);

            #endregion Split-from subtree: fork


            #region Split-from subtree: split-from leaf

            leaf = fork.Contents[0] as LeafBpt<int>;
            Assert.AreEqual(4, leaf.Count);

            /* keys */
            Assert.AreEqual(63, leaf.Keys[0]);
            Assert.AreEqual(67, leaf.Keys[1]);
            Assert.AreEqual(70, leaf.Keys[2]);
            Assert.AreEqual(77, leaf.Keys[3]);

            /* contents, by key */
            Assert.AreEqual(63, leaf.Contents[0].Key);
            Assert.AreEqual(67, leaf.Contents[1].Key);
            Assert.AreEqual(70, leaf.Contents[2].Key);
            Assert.AreEqual(77, leaf.Contents[3].Key);

            #endregion Split-from subtree: split-from leaf


            #region Split-from subtree: split-to leaf

            leaf = fork.Contents[1] as LeafBpt<int>;
            Assert.AreEqual(1, leaf.Count);

            /* keys */
            Assert.AreEqual(84, leaf.Keys[0]);

            /* contents, by key */
            Assert.AreEqual(84, leaf.Contents[0].Key);

            #endregion Split-from subtree: split-to leaf


            #region Split-from subtree: R sibling of split-to leaf

            /* R sibling of split-to leaf: skeleton testing */
            leaf = fork.Contents[2] as LeafBpt<int>;
            Assert.AreEqual(91, leaf.Key);
            Assert.AreEqual(3, leaf.Count);

            #endregion Split-from subtree: R sibling of split-to leaf


            #region Split-from subtree: Rmost sibling of split-to leaf

            /* skeleton testing */
            leaf = fork.Contents[3] as LeafBpt<int>;
            Assert.AreEqual(112, leaf.Key);
            Assert.AreEqual(4, leaf.Count);

            #endregion Split-from subtree: Rmost sibling of split-to leaf


            #region Split-to subtree: fork

            fork = root.Contents[2] as ForkBpt<int>;
            Assert.AreEqual(1, fork.Count);
            Assert.AreEqual(140, fork.Key);

            #endregion Split-to subtree: fork


            #region Split-to subtree: solo leaf

            /* this is an existing leaf, now moved to new fork */
            leaf = fork.Contents[0] as LeafBpt<int>;
            Assert.AreEqual(3, leaf.Count);

            /* keys */
            Assert.AreEqual(140, leaf.Keys[0]);
            Assert.AreEqual(147, leaf.Keys[1]);
            Assert.AreEqual(154, leaf.Keys[2]);

            /* contents, by key */
            Assert.AreEqual(140, leaf.Contents[0].Key);
            Assert.AreEqual(147, leaf.Contents[1].Key);
            Assert.AreEqual(154, leaf.Contents[2].Key);

            #endregion Split-to subtree: solo leaf


            #region Rmost subtree, unchanged: fork

            /* skeleton testing */
            fork = root.Contents[3] as ForkBpt<int>;
            Assert.AreEqual(2, fork.Count);
            Assert.AreEqual(161, fork.Key);

            #endregion Rmost subtree, unchanged: fork


            #region Rmost subtree, L leaf

            /* skeleton testing */
            leaf = fork.Contents[0] as LeafBpt<int>;
            Assert.AreEqual(2, leaf.Count);

            /* keys */
            Assert.AreEqual(161, leaf.Keys[0]);

            /* contents, by key */
            Assert.AreEqual(161, leaf.Contents[0].Key);

            #endregion Rmost subtree, L leaf


            #region Rmost subtree, R leaf

            /* skeleton testing */
            leaf = fork.Contents[1] as LeafBpt<int>;
            Assert.AreEqual(2, leaf.Count);

            /* keys */
            Assert.AreEqual(175, leaf.Keys[0]);

            /* contents, by key */
            Assert.AreEqual(175, leaf.Contents[0].Key);

            #endregion Rmost subtree, R leaf
        }

        [TestMethod()]
        public void Insert_Test__Amid_Els__Restruct_Leaf_And_Fork__Assert_Continuous_LinkedList()  /* working */  {
            /* insert el with key of 67; 
             * leaf ~from63to84 should split into [from-63-to-77] and [from 84-to-84]; 
             * fork ~from63to140 should split into [from-x-to-y] and [from-x-to-y]; 
             * root should end up with 4 items */

            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            ElementBpt<int> insertable = new ElementBpt<int>(67, int.MaxValue / 9);


            //**  exercising the code  **//
            tree.Insert(insertable);


            //** gathering results **//
            /* leftmost element of whole tree */
            ForkBpt<int> fork = tree.Root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> leaf = fork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> element = leaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0, key = -1, lastKey = -1; i < 28; i++) {
                #region Calculating correct ~key, accounting for insertion

                if (i < 10) {
                    key = 7 * i;
                }

                if (i == 10) {
                    key = 67;
                }

                if (i > 10) {
                    key = 7 * (i - 1);
                }

                #endregion Calculating correct ~key, accounting for insertion

                /* testing forward links, in effect */
                Assert.AreEqual(key, element.Key);

                /* testing backward links, except Lmost */
                if (i > 0) {
                    Assert.AreEqual(lastKey, element.Previous.Key);
                }

                lastKey = key;

                /* stepping forward; null after Rmost */
                element = element.Next;
            }
        }

        #endregion Restructuring to mid-depth forks


        #region Restructuring to root

        [TestMethod()]
        public void Insert_Test__Amid_Els__Restruct_To_Root__Assert_Correct_Data()  /* working */  {
            //** groundwork **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();
            ElementBpt<double> insertable = new ElementBpt<double>(2.5);


            //** exercising the code **//
            tree.Insert(insertable);


            //** gathering data **//
            NodeBpt<double> root = tree.Root;


            //** testing **//
            /* root should now have 2 children: L and R halves of prior root */
            Assert.AreEqual(2, root.Count);

            /* tree should have a depth of 4 +1 now: [root, fork, fork, leaf] + element */
            IKeyedObject<double> topic = root;

            #region Lmost branchings

            for (int i = 0; i < 5; i++) {
                if (topic != root) /* .Key not set at root, since never needed */ {
                    Assert.AreEqual(0, topic.Key);
                }

                if (i < 3) {
                    Assert.AreEqual(typeof(ForkBpt<double>), topic.GetType());
                }

                if (i == 3) {
                    Assert.AreEqual(typeof(LeafBpt<double>), topic.GetType());
                }

                if (i == 4) {
                    Assert.AreEqual(typeof(ElementBpt<double>), topic.GetType());
                    break;  // otherwise fails at 'advancing' next 
                }

                /* advancing leafward */
                topic = (topic as NodeBpt<double>).Contents[0];
            }

            #endregion Lmost branchings


            #region Split leaf and parent

            #region Parent (L2) fork

            /* 0 to 0, then 0 and 1 */
            ForkBpt<double> lmostForkAtL2 = (root.Contents[0] as ForkBpt<double>).Contents[0] as ForkBpt<double>;

            Assert.AreEqual(4, lmostForkAtL2.Count);

            double[] lmostForkAtL2Keys = { 0, 2.5, 4, 8 };

            for (int i = 0; i < lmostForkAtL2Keys.Length; i++) {
                Assert.AreEqual(lmostForkAtL2Keys[i], lmostForkAtL2.Keys[i]);
                Assert.AreEqual(lmostForkAtL2Keys[i], lmostForkAtL2.Contents[i].Key);
            }

            #endregion Parent (L2) fork


            #region Split-from leaf (Lmost)

            LeafBpt<double> lmostLeaf = lmostForkAtL2.Contents[0] as LeafBpt<double>;

            Assert.AreEqual(3, lmostLeaf.Count);

            for (int i = 0; i < lmostLeaf.Count; i++) {
                Assert.AreEqual(i, lmostLeaf.Keys[i]);
                Assert.AreEqual(i, lmostLeaf.Contents[i].Key);
            }

            #endregion Split-from leaf (Lmost)


            #region Split-to leaf

            LeafBpt<double> newLeaf = lmostForkAtL2.Contents[1] as LeafBpt<double>;

            Assert.AreEqual(2, newLeaf.Count);

            double[] newLeafKeys = { 2.5, 3 };

            for (int i = 0; i < 2; i++) {
                Assert.AreEqual(newLeafKeys[i], newLeaf.Keys[i]);
                Assert.AreEqual(newLeafKeys[i], newLeaf.Contents[i].Key);
            }

            #endregion Split-to leaf


            #region R sibling of split-to, originally R sibling of split-from

            LeafBpt<double> rSibling = lmostForkAtL2.Contents[2] as LeafBpt<double>;

            Assert.AreEqual(4, rSibling.Count);

            for (int i = 0; i < 4; i++) {
                Assert.AreEqual(i + 4, rSibling.Keys[i]);
                Assert.AreEqual(i + 4, rSibling.Contents[i].Key);
            }

            #endregion R sibling of split-to, originally R sibling of split-from

            #endregion Split leaf and parent


            #region Split-to L2 fork and child leaf

            #region Parent (L1) fork

            /* 0 to 0, then 0 and 1 */
            ForkBpt<double> lmostForkAtL1 = (root.Contents[0] as ForkBpt<double>);

            Assert.AreEqual(4, lmostForkAtL1.Count);

            double[] lmostForkAtL1Keys = { 0, 12, 16, 32 };

            for (int i = 0; i < lmostForkAtL1Keys.Length; i++) {
                Assert.AreEqual(lmostForkAtL1Keys[i], lmostForkAtL1.Keys[i]);
                Assert.AreEqual(lmostForkAtL1Keys[i], lmostForkAtL1.Contents[i].Key);
            }

            #endregion Parent (L1) fork

            /* Lmost fork at L2 already tested */

            #region Split-to L2 fork

            ForkBpt<double> newL2Fork = lmostForkAtL1.Contents[1] as ForkBpt<double>;

            Assert.AreEqual(1, newL2Fork.Count);
            Assert.AreEqual(12, newL2Fork.Key);

            #endregion Split-to L2 fork


            #region Single child of split-to fork

            LeafBpt<double> soleChild = newL2Fork.Contents[0] as LeafBpt<double>;

            Assert.AreEqual(4, soleChild.Count);

            for (int i = 0; i < 4; i++) {
                Assert.AreEqual(i + 12, soleChild.Keys[i]);
                Assert.AreEqual(i + 12, soleChild.Contents[i].Key);
            }

            #endregion R sibling of split-to, originally R sibling of split-from


            #region Right siblings of split-to L2 fork

            /* R-adjacent */
            ForkBpt<double> firstRSibling = lmostForkAtL1.Contents[2] as ForkBpt<double>;

            Assert.AreEqual(4, firstRSibling.Count);
            Assert.AreEqual(16, firstRSibling.Key);
            Assert.AreEqual(16, firstRSibling.Keys[0]);
            Assert.AreEqual(16, firstRSibling.Contents[0].Key);

            /* R-adjacent of R-adjacent (last in subtree) */
            ForkBpt<double> secondRSibling = lmostForkAtL1.Contents[3] as ForkBpt<double>;

            Assert.AreEqual(4, secondRSibling.Count);
            Assert.AreEqual(32, secondRSibling.Key);
            Assert.AreEqual(32, secondRSibling.Keys[0]);
            Assert.AreEqual(32, secondRSibling.Contents[0].Key);

            #endregion Right siblings of split-to L2 fork

            #endregion Split-to L2 fork and child leaf


            #region Split-to L1 fork and child fork

            #region Split-to L1 fork

            /* 0 to 0, then 0 and 1 */
            ForkBpt<double> rSubtreeFork = (root.Contents[1] as ForkBpt<double>);

            Assert.AreEqual(1, rSubtreeFork.Count);
            Assert.AreEqual(48, rSubtreeFork.Key);
            Assert.AreEqual(48, rSubtreeFork.Keys[0]);
            Assert.AreEqual(48, rSubtreeFork.Contents[0].Key);

            #endregion Split-to L1 fork

            /* Lmost fork at L2 already tested */

            #region Child fork

            ForkBpt<double> rmostL2Fork = rSubtreeFork.Contents[0] as ForkBpt<double>;

            Assert.AreEqual(4, rmostL2Fork.Count);
            Assert.AreEqual(48, rmostL2Fork.Key);

            #endregion Child fork

            #endregion Split-to L1 fork and child fork
        }

        [TestMethod()]
        public void Insert_Test__Amid_Els__Restruct_To_Root__Assert_Continuous_Linked_List()  /* working */  {
            //** groundwork **//
            BPlusTree<double> tree = FixtureTestTreeDoubles();
            ElementBpt<double> insertable = new ElementBpt<double>(2.5);


            //** exercising the code **//
            tree.Insert(insertable);


            //** gathering data **//
            NodeBpt<double> root = tree.Root;
            ForkBpt<double> lmostAtL1 = root.Contents[0] as ForkBpt<double>;
            ForkBpt<double> lmostAtL2 = lmostAtL1.Contents[0] as ForkBpt<double>;
            LeafBpt<double> lmostLeaf = lmostAtL2.Contents[0] as LeafBpt<double>;


            //** testing **//
            ElementBpt<double> el = lmostLeaf.Contents[0] as ElementBpt<double>;

            for (int i = 0; i < 65; i++) {
                double key = -1.0;

                #region Calculating key, adjusted for inserted value

                if (i < 3) {
                    key = i;
                }

                if (i == 3) {
                    key = 2.5;
                }

                if (i > 3) {
                    key = i - 1;
                }

                #endregion Calculating key, adjusted for inserted value

                /* current leaf; in effect, forward testing */
                Assert.AreEqual(key, el.Key);


                /* backwards testing */
                if (el.Next != null) {
                    ElementBpt<double> nextsBackLink = el.Next.Previous;  // same as ~el 
                    Assert.AreEqual(key, nextsBackLink.Key);
                }

                /* stepping forward */
                el = el.Next;
            }

        }

        #endregion Restructuring to root

        #endregion Insert()


        #region Remove()

        #region No restructuring

        [TestMethod()]
        public void Remove_Test__Amid_Els__No_Restruct__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(119));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i < 26; i++) {
                /* 16 normal, 17th and on 1 more */

                int key = -1;

                /* calc'ing ~key; starting at removal, next-higher should be found */
                if (i < 17) {
                    key = i * 7;
                }
                else {
                    key = (i + 1) * 7;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* step forward */
                el = el.Next;

                /* backward test, except for Lmost and Rmost els */
                if (el != null) {
                    if (el.Previous != null) {
                        Assert.AreEqual(key, el.Previous.Key);
                    }
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__No_Restruct__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(133));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i < 26; i++) {
                /* 16 normal, 17th and on 1 more */

                int key = -1;

                /* calc'ing ~key; starting at removal, next-higher should be found */
                if (i < 19) {
                    key = i * 7;
                }
                else {
                    key = (i + 1) * 7;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* step forward */
                el = el.Next;

                /* backward test, except for Lmost and Rmost els */
                if (el != null) {
                    if (el.Previous != null) {
                        Assert.AreEqual(key, el.Previous.Key);
                    }
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__Amid_Els__No_Restruct__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(119));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//
            int[] atRoot = { 0, 63, 161 };
            int[][] atForks = {
                new int[] { 0, 28, 42 },
                new int[] { 63, 91, 112, 140 },
                new int[] { 161, 175 }
            };

            int[][] atLeaves = {
                new int[] { 0, 7, 14, 21 },
                new int[] { 28, 35 },
                new int[] { 42, 49, 56 },
                new int[] { 63, 70, 77, 84 },
                new int[] { 91, 98, 105 },
                new int[] { 112, /* 119, */ 126, 133 },  // the removed item 
                new int[] { 140, 147, 154 },
                new int[] { 161, 168 },
                new int[] { 175, 182 },
            };

            Assert.AreEqual(3, root.Count);

            for (int toFork = 0, acrossLeaves = 0; toFork < 4; toFork++) {
                Assert.AreEqual(atRoot[toFork], root.Keys[toFork]);
                Assert.AreEqual(atRoot[toFork], root.Contents[toFork].Key);

                ForkBpt<int> fork = root.Contents[toFork] as ForkBpt<int>;

                for (int toLeaf = 0; toLeaf < 4; toLeaf++) {
                    int[] forkLocal = atForks[toFork];
                    Assert.AreEqual(forkLocal.Length, fork.Count);

                    Assert.AreEqual(forkLocal[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(forkLocal[toLeaf], fork.Contents[toLeaf].Key);

                    LeafBpt<int> leaf = fork.Contents[toLeaf] as LeafBpt<int>;
                    int[] leafLocal = atLeaves[acrossLeaves];
                    Assert.AreEqual(leafLocal.Length, leaf.Count);

                    for (int toEls = 0; toEls < 4; toEls++) {
                        Assert.AreEqual(leafLocal[toEls], leaf.Keys[toEls]);
                        Assert.AreEqual(leafLocal[toEls], leaf.Contents[toEls].Key);

                        /* exit this loop when no more at this leaf */
                        if (toEls == -1 + leafLocal.Length) {
                            break;
                        }
                    }

                    acrossLeaves++;

                    /* exit this loop when no more at this fork */
                    if (toLeaf == -1 + forkLocal.Length) {
                        break;
                    }
                }

                /* exit overall loop when no more at root */
                if (toFork == -1 + atRoot.Length) {
                    break;
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__No_Restruct__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(56));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//
            int[] atRoot = { 0, 63, 161 };
            int[][] atForks = {
                new int[] { 0, 28, 42 },
                new int[] { 63, 91, 112, 140 },
                new int[] { 161, 175 }
            };

            int[][] atLeaves = {
                new int[] { 0, 7, 14, 21 },
                new int[] { 28, 35 },
                new int[] { 42, 49, /* 56 */ },  // the removed item 
                new int[] { 63, 70, 77, 84 },
                new int[] { 91, 98, 105 },
                new int[] { 112, 119, 126, 133 },
                new int[] { 140, 147, 154 },
                new int[] { 161, 168 },
                new int[] { 175, 182 },
            };

            Assert.AreEqual(3, root.Count);

            for (int toFork = 0, acrossLeaves = 0; toFork < 4; toFork++) {
                Assert.AreEqual(atRoot[toFork], root.Keys[toFork]);
                Assert.AreEqual(atRoot[toFork], root.Contents[toFork].Key);

                ForkBpt<int> fork = root.Contents[toFork] as ForkBpt<int>;

                for (int toLeaf = 0; toLeaf < 4; toLeaf++) {
                    int[] forkLocal = atForks[toFork];
                    Assert.AreEqual(forkLocal.Length, fork.Count);

                    Assert.AreEqual(forkLocal[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(forkLocal[toLeaf], fork.Contents[toLeaf].Key);

                    LeafBpt<int> leaf = fork.Contents[toLeaf] as LeafBpt<int>;
                    int[] leafLocal = atLeaves[acrossLeaves];
                    Assert.AreEqual(leafLocal.Length, leaf.Count);

                    for (int toEls = 0; toEls < 4; toEls++) {
                        Assert.AreEqual(leafLocal[toEls], leaf.Keys[toEls]);
                        Assert.AreEqual(leafLocal[toEls], leaf.Contents[toEls].Key);

                        /* exit this loop when no more at this leaf */
                        if (toEls == -1 + leafLocal.Length) {
                            break;
                        }
                    }

                    acrossLeaves++;

                    /* exit this loop when no more at this fork */
                    if (toLeaf == -1 + forkLocal.Length) {
                        break;
                    }
                }

                /* exit overall loop when no more at root */
                if (toFork == -1 + atRoot.Length) {
                    break;
                }
            }
        }

        #endregion No restructuring


        #region Restructuring at leaves only : sloshing

        [TestMethod()]
        public void Remove_Test__At_Last_El__Slosh_From_L_Leaves_Only__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(35));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i < 26; i++) {
                int key = -1;

                /* calc'ing ~key; starting at removal, next-higher should be found */
                if (i < 5) {
                    key = i * 7;
                }
                else {
                    key = (i + 1) * 7;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* step forward */
                el = el.Next;

                /* backward test, except for Lmost and Rmost els */
                if (el != null) {
                    if (el.Previous != null) {
                        Assert.AreEqual(key, el.Previous.Key);
                    }
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Slosh_From_L_Leaves_Only__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(35));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//
            int[] atRoot = { 0, 63, 161 };
            int[][] atForks = {
                new int[] { 0, 21, 42 },  // key of middle leaf changed from 28 to 21 by sloshing 
                new int[] { 63, 91, 112, 140 },
                new int[] { 161, 175 }
            };

            int[][] atLeaves = {
                new int[] { 0, 7, 14 },  // 21 sloshed to next 
                new int[] { 21, 28, /* 35 */ },  // the removed item (35) and the sloshed-in item (21) 
                new int[] { 42, 49, 56 },
                new int[] { 63, 70, 77, 84 },
                new int[] { 91, 98, 105 },
                new int[] { 112, 119, 126, 133 },
                new int[] { 140, 147, 154 },
                new int[] { 161, 168 },
                new int[] { 175, 182 },
            };

            Assert.AreEqual(3, root.Count);

            for (int toFork = 0, acrossLeaves = 0; toFork < 4; toFork++) {
                Assert.AreEqual(atRoot[toFork], root.Keys[toFork]);
                Assert.AreEqual(atRoot[toFork], root.Contents[toFork].Key);

                ForkBpt<int> fork = root.Contents[toFork] as ForkBpt<int>;

                for (int toLeaf = 0; toLeaf < 4; toLeaf++) {
                    int[] forkLocal = atForks[toFork];
                    Assert.AreEqual(forkLocal.Length, fork.Count);

                    Assert.AreEqual(forkLocal[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(forkLocal[toLeaf], fork.Contents[toLeaf].Key);

                    LeafBpt<int> leaf = fork.Contents[toLeaf] as LeafBpt<int>;
                    int[] leafLocal = atLeaves[acrossLeaves];
                    Assert.AreEqual(leafLocal.Length, leaf.Count);

                    for (int toEls = 0; toEls < 4; toEls++) {
                        Assert.AreEqual(leafLocal[toEls], leaf.Keys[toEls]);
                        Assert.AreEqual(leafLocal[toEls], leaf.Contents[toEls].Key);

                        /* exit this loop when no more at this leaf */
                        if (toEls == -1 + leafLocal.Length) {
                            break;
                        }
                    }

                    acrossLeaves++;

                    /* exit this loop when no more at this fork */
                    if (toLeaf == -1 + forkLocal.Length) {
                        break;
                    }
                }

                /* exit overall loop when no more at root */
                if (toFork == -1 + atRoot.Length) {
                    break;
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Slosh_From_R_Leaves_Only__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            FixtureTruncateLmostLeaf(tree);


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(35));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i < 26; i++) {
                int key = -1;

                /* calc'ing ~key : 0, 7, [--], 28, [-], 42, 49, ..., 182 */
                if (i < 2) {
                    key = i * 7;  // before truncating fixture and removal 
                }
                if (i == 2) {
                    key = (i + 2) * 7;  // after truncating fixture, before removal 
                }
                if (i > 2) {
                    key = (i + 3) * 7;  // after truncating fixture and removal 
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* step forward */
                el = el.Next;

                /* exit at last el */
                if (el == null) {
                    break;
                }

                /* backward test, except for Lmost and Rmost els */
                if (el.Previous != null) {
                    Assert.AreEqual(key, el.Previous.Key);
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Slosh_From_R_Leaves_Only__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            FixtureTruncateLmostLeaf(tree);


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(35));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//

            /* keys on leaves : [ 0, 7 ] , [ 28, 42 ] , [ 49, 56 ] , [ 63, ..., 84], ... ; 
            * keys on forks, : [ 0, 28, 49 ] , [ 63, 91, ..., 140] , ... ; 
            * root unchanged */

            int[] atRoot = { 0, 63, 161 };
            int[][] atForks = {
                new int[] { 0, 28, 49 },  // key of middle leaf changed from 28 to 21 by sloshing 
                new int[] { 63, 91, 112, 140 },
                new int[] { 161, 175 }
            };

            int[][] atLeaves = {
                new int[] { 0, 7 },  // no 14 or 21 at all in this tree 
                new int[] { 28, /* 35, */ 42 },  // the removed item (35) and the sloshed-in item (42) 
                new int[] { /* 42, */ 49, 56 },  // the sloshed item 
                new int[] { 63, 70, 77, 84 },
                new int[] { 91, 98, 105 },
                new int[] { 112, 119, 126, 133 },
                new int[] { 140, 147, 154 },
                new int[] { 161, 168 },
                new int[] { 175, 182 },
            };

            Assert.AreEqual(3, root.Count);

            for (int toFork = 0, acrossLeaves = 0; toFork < 4; toFork++) {
                Assert.AreEqual(atRoot[toFork], root.Keys[toFork]);
                Assert.AreEqual(atRoot[toFork], root.Contents[toFork].Key);

                ForkBpt<int> fork = root.Contents[toFork] as ForkBpt<int>;

                for (int toLeaf = 0; toLeaf < 4; toLeaf++) {
                    int[] forkLocal = atForks[toFork];
                    Assert.AreEqual(forkLocal.Length, fork.Count);

                    Assert.AreEqual(forkLocal[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(forkLocal[toLeaf], fork.Contents[toLeaf].Key);

                    LeafBpt<int> leaf = fork.Contents[toLeaf] as LeafBpt<int>;
                    int[] leafLocal = atLeaves[acrossLeaves];
                    Assert.AreEqual(leafLocal.Length, leaf.Count);

                    for (int toEls = 0; toEls < 4; toEls++) {
                        Assert.AreEqual(leafLocal[toEls], leaf.Keys[toEls]);
                        Assert.AreEqual(leafLocal[toEls], leaf.Contents[toEls].Key);

                        /* exit this loop when no more at this leaf */
                        if (toEls == -1 + leafLocal.Length) {
                            break;
                        }
                    }

                    acrossLeaves++;

                    /* exit this loop when no more at this fork */
                    if (toLeaf == -1 + forkLocal.Length) {
                        break;
                    }
                }

                /* exit overall loop when no more at root */
                if (toFork == -1 + atRoot.Length) {
                    break;
                }
            }
        }

        #endregion Restructuring at leaves only : sloshing


        #region Restructuring at leaves only : merging

        [TestMethod()]
        public void Remove_Test__At_Last_El__Merge_With_L__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            FixtureTruncateLmostLeaf(tree);
            FixtureTruncate3rdLeaf(tree);

            int[] expecteds = { 0, 7, 28, 35, 42, 63 };


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(49));  // last el in truncated 3rd leaf overall 


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;


            //**  testing  **//
            for (int i = 0; i < 26; i++) {
                int key = -1;

                /* calc'ing ~key : 0, 7, [-], 28, 35, 42, [--], 63, ..., 182 */
                if (i < 6) {
                    key = expecteds[i];
                }
                else {
                    key = (i + 4) * 7;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* step forward */
                el = el.Next;

                /* exit at last el */
                if (el == null) {
                    break;
                }

                /* backward test, except for Lmost and Rmost els */
                if (el.Previous != null) {
                    Assert.AreEqual(key, el.Previous.Key);
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Merge_With_L__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<int> tree = FixtureTestTreeSevens();
            FixtureTruncateLmostLeaf(tree);
            FixtureTruncate3rdLeaf(tree);


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<int>(49));


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;


            //**  testing  **//
            int[] atRoot = { 0, 63, 161 };
            int[][] atForks = {
                new int[] { 0, 28 },  // key of 42 and its leaf dropped by merging 
                new int[] { 63, 91, 112, 140 },
                new int[] { 161, 175 }
            };

            int[][] atLeaves = {
                new int[] { 0, 7 },
                new int[] { 28, 35, 42 },  // the merged-to leaf; 42 added 
                /* new int[] { 49 }, */    // the removed item (49) and its entire leaf; 42 merged to preceding 
                new int[] { 63, 70, 77, 84 },
                new int[] { 91, 98, 105 },
                new int[] { 112, 119, 126, 133 },
                new int[] { 140, 147, 154 },
                new int[] { 161, 168 },
                new int[] { 175, 182 },
            };

            Assert.AreEqual(3, root.Count);

            for (int toFork = 0, acrossLeaves = 0; toFork < 4; toFork++) {
                Assert.AreEqual(atRoot[toFork], root.Keys[toFork]);
                Assert.AreEqual(atRoot[toFork], root.Contents[toFork].Key);

                ForkBpt<int> fork = root.Contents[toFork] as ForkBpt<int>;

                for (int toLeaf = 0; toLeaf < 4; toLeaf++) {
                    int[] forkLocal = atForks[toFork];
                    Assert.AreEqual(forkLocal.Length, fork.Count);

                    Assert.AreEqual(forkLocal[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(forkLocal[toLeaf], fork.Contents[toLeaf].Key);

                    LeafBpt<int> leaf = fork.Contents[toLeaf] as LeafBpt<int>;
                    int[] leafLocal = atLeaves[acrossLeaves];
                    Assert.AreEqual(leafLocal.Length, leaf.Count);

                    for (int toEls = 0; toEls < 4; toEls++) {
                        Assert.AreEqual(leafLocal[toEls], leaf.Keys[toEls]);
                        Assert.AreEqual(leafLocal[toEls], leaf.Contents[toEls].Key);

                        /* exit this loop when no more at this leaf */
                        if (toEls == -1 + leafLocal.Length) {
                            break;
                        }
                    }

                    acrossLeaves++;

                    /* exit this loop when no more at this fork */
                    if (toLeaf == -1 + forkLocal.Length) {
                        break;
                    }
                }

                /* exit overall loop when no more at root */
                if (toFork == -1 + atRoot.Length) {
                    break;
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Merge_With_R__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<char> tree = FixtureTestTreeChars();
            FixtureAddRmostLeaf(tree);

            /* expecteds */
            char[] keys = new char[] {
                '$', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                't', 'u', 'v', 'w', /* removed: 'x', */ 'y', 'z', '{', '|', '}'
            };

            int actualEls = 0;


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<char>('x'));


            //**  gathering data  **//
            ForkBpt<char> root = tree.Root as ForkBpt<char>;
            ForkBpt<char> lmostFork = root.Contents[0] as ForkBpt<char>;
            LeafBpt<char> lmostLeaf = lmostFork.Contents[0] as LeafBpt<char>;
            ElementBpt<char> el = lmostLeaf.Contents[0] as ElementBpt<char>;


            //**  testing  **//
            while (el != null) {
                /* checking .Next pointers, in effect */
                Assert.AreEqual(keys[actualEls], el.Key);

                /* here, I incr a counter; before the following because .Next can be ~null */
                actualEls++;

                /* traversing */
                el = el.Next;

                /* checking .Previous pointers */
                if (el != null) {
                    Assert.AreEqual(keys[actualEls - 1], el.Previous.Key);
                }
            }

            /* here, I check for reached against total expected */
            Assert.AreEqual(keys.Length, actualEls);
        }

        [TestMethod()]
        public void Remove_Test__At_Last_El__Merge_With_R__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<char> tree = FixtureTestTreeChars();
            FixtureAddRmostLeaf(tree);

            /* expecteds */
            char[] rootKeys = { '$', 'i', 'w' };

            char[][] forkKeys = {
                new char[] { '$', 'd', 'f' },
                new char[] { 'i', 'm', 'p', 't' },
                new char[] { 'w', /* 'y', */ '{' }  // removed leaf and its key 
                };

            char[][][] leafKeys = {
                /* left fork */
                new char[][] {
                    new char[] { '$', 'a', 'b', 'c' }, new char[] { 'd', 'e' }, new char[] { 'f', 'g', 'h' }
                    },

                /* middle fork */
                new char[][] {
                    new char[] { 'i', 'j', 'k', 'l' }, new char[] { 'm', 'n', 'o' },
                    new char[] { 'p', 'q', 'r', 's' }, new char[] { 't', 'u', 'v' }
                    },

                /* right fork; last leaf added by second fixture */
                new char[][]{
                    new char[] { 'w', /* removed: 'x' */ /* merged to:  }, new char[] { */ 'y', 'z' }, new char[] { '{', '|', '}' }
                    }
            };


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<char>('x'));


            //**  gathering data  **//
            ForkBpt<char> root = tree.Root as ForkBpt<char>;


            //**  testing  **//
            Assert.AreEqual(rootKeys.Length, root.Count);  // root's metadata 

            for (int toFork = 0; toFork < rootKeys.Length; toFork++) {
                /* verify each key / link in root */
                Assert.AreEqual(rootKeys[toFork], root.Keys[toFork]);
                Assert.AreEqual(rootKeys[toFork], root.Contents[toFork].Key);

                /* verify each fork's metadata, then its contents in loop */
                ForkBpt<char> fork = root.Contents[toFork] as ForkBpt<char>;
                char[] thisForksKeys = forkKeys[toFork];

                Assert.AreEqual(thisForksKeys.Length, fork.Count);  // fork metadata 

                for (int toLeaf = 0; toLeaf < thisForksKeys.Length; toLeaf++) {
                    Assert.AreEqual(thisForksKeys[toLeaf], fork.Keys[toLeaf]);
                    Assert.AreEqual(thisForksKeys[toLeaf], fork.Contents[toLeaf].Key);

                    /* verify each leaf's metadata, then its contents in loop */
                    LeafBpt<char> leaf = fork.Contents[toLeaf] as LeafBpt<char>;
                    char[] thisLeafsKeys = leafKeys[toFork][toLeaf];

                    Assert.AreEqual(thisLeafsKeys.Length, leaf.Count);  // leaf metadata 

                    for (int toEl = 0; toEl < thisLeafsKeys.Length; toEl++) {
                        Assert.AreEqual(thisLeafsKeys[toEl], leaf.Keys[toEl]);
                        Assert.AreEqual(thisLeafsKeys[toEl], leaf.Contents[toEl].Key);
                    }
                }
            }
        }

        #endregion Restructuring at leaves only : merging


        #region Restructuring through forks to root : merging

        [TestMethod()]
        public void Remove_Test__Amid_Els__Merge_Rward__To_Root__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeHalfFullDoubles();

            int actualEls = 0;


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<double>(10));


            //**  gathering data  **//
            ForkBpt<double> root = tree.Root as ForkBpt<double>;
            LeafBpt<double> lmostLeaf = root.Contents[0] as LeafBpt<double>;
            ElementBpt<double> el = lmostLeaf.Contents[0] as ElementBpt<double>;


            //**  testing  **//
            for (int i = 0; i < 32; i++) {
                int key = -1;

                /* calc'ing ~key : 0, ..., 9, [-], 11, ..., 31 */
                if (i < 10) {
                    key = i;
                }
                else {
                    key = i + 1;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* for testing total els traversed */
                actualEls++;

                /* step forward */
                el = el.Next;

                /* exit at last el */
                if (el == null) {
                    break;
                }

                /* backward test, except for Lmost and Rmost els */
                if (el.Previous != null) {
                    Assert.AreEqual(key, el.Previous.Key);
                }
            }

            /* here, I check for reached against total expected */
            Assert.AreEqual(31, actualEls);
        }

        [TestMethod()]
        public void Remove_Test__Amid_Els__Merge_Lward__Merge_To_Root__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeHalfFullDoubles();

            /* expecteds */
            double[] atRoot = { 0, 4, 8, 12, 16, 20, 28 };
            double[][] atLeaves = {
                new double[] { 0, 1, 2, 3 },
                new double[] { 4, 5, 6, 7 },
                new double[] { 8, 9, 10, 11 },
                new double[] { 12, 13, 14, 15 },
                new double[] { 16, 17, 18, 19 },
                new double[] { 20, 22, 23, 24, 25, 26, 27 },
                new double[] { 28, 29, 30, 31 },
            };


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<double>(21));


            //**  gathering data  **//
            ForkBpt<double> root = tree.Root as ForkBpt<double>;


            //**  testing  **//
            Assert.AreEqual(7, root.Count);  // root's metadata 

            for (int toLeaf = 0; toLeaf < atRoot.Length; toLeaf++) {
                /* root's data */
                Assert.AreEqual(atRoot[toLeaf], root.Keys[toLeaf]);
                Assert.AreEqual(atRoot[toLeaf], root.Contents[toLeaf].Key);

                /* each leaf's metadata */
                LeafBpt<double> leaf = root.Contents[toLeaf] as LeafBpt<double>;
                double[] atThisLeaf = atLeaves[toLeaf];
                Assert.AreEqual(atThisLeaf.Length, leaf.Count);

                /* each leaf's data */
                for (int toEl = 0; toEl < atThisLeaf.Length; toEl++) {
                    Assert.AreEqual(atThisLeaf[toEl], leaf.Keys[toEl]);
                    Assert.AreEqual(atThisLeaf[toEl], leaf.Contents[toEl].Key);
                }
            }
        }

        [TestMethod()]
        public void Remove_Test__Amid_Els__Merge_Lward__Merge_To_Root__Assert_Continuous_Linked_List()  /* working */  {
            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeHalfFullDoubles();

            int actualEls = 0;


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<double>(21));


            //**  gathering data  **//
            ForkBpt<double> root = tree.Root as ForkBpt<double>;
            LeafBpt<double> lmostLeaf = root.Contents[0] as LeafBpt<double>;
            ElementBpt<double> el = lmostLeaf.Contents[0] as ElementBpt<double>;


            //**  testing  **//
            for (int i = 0; i < 32; i++) {
                int key = -1;

                /* calc'ing ~key : 0, ..., 20, [-], 22, ..., 31 */
                if (i < 21) {
                    key = i;
                }
                else {
                    key = i + 1;
                }

                /* this el; in effect a forward test */
                Assert.AreEqual(key, el.Key);

                /* for testing total els traversed */
                actualEls++;

                /* step forward */
                el = el.Next;

                /* exit at last el */
                if (el == null) {
                    break;
                }

                /* backward test, except for Lmost and Rmost els */
                if (el.Previous != null) {
                    Assert.AreEqual(key, el.Previous.Key);
                }
            }

            /* here, I check for reached against total expected */
            Assert.AreEqual(31, actualEls);
        }

        [TestMethod()]
        public void Remove_Test__Amid_Els__Merge_Rward__To_Root__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            BPlusTree<double> tree = FixtureTestTreeHalfFullDoubles();

            /* expecteds */
            double[] atRoot = { 0, 4, 8, 16, 20, 24, 28 };
            double[][] atLeaves = {
                new double[] { 0, 1, 2, 3 },
                new double[] { 4, 5, 6, 7 },
                new double[] { 8, 9, 11, 12, 13, 14, 15 },
                new double[] { 16, 17, 18, 19 },
                new double[] { 20, 21, 22, 23 },
                new double[] { 24, 25, 26, 27 },
                new double[] { 28, 29, 30, 31 },
            };


            //**  exercising the code  **//
            tree.Remove(new ElementBpt<double>(10));


            //**  gathering data  **//
            ForkBpt<double> root = tree.Root as ForkBpt<double>;


            //**  testing  **//
            Assert.AreEqual(7, root.Count);  // root's metadata 

            for (int toLeaf = 0; toLeaf < atRoot.Length; toLeaf++) {
                /* root's data */
                Assert.AreEqual(atRoot[toLeaf], root.Keys[toLeaf]);
                Assert.AreEqual(atRoot[toLeaf], root.Contents[toLeaf].Key);

                /* each leaf's metadata */
                LeafBpt<double> leaf = root.Contents[toLeaf] as LeafBpt<double>;
                double[] atThisLeaf = atLeaves[toLeaf];
                Assert.AreEqual(atThisLeaf.Length, leaf.Count);

                /* each leaf's data */
                for (int toEl = 0; toEl < atThisLeaf.Length; toEl++) {
                    Assert.AreEqual(atThisLeaf[toEl], leaf.Keys[toEl]);
                    Assert.AreEqual(atThisLeaf[toEl], leaf.Contents[toEl].Key);
                }
            }
        }


        #endregion Restructuring through forks to root : merging


        #endregion Remove()


        #region MassConstruct()

        [TestMethod()]
        public void MassConstruct_Test__Knowns__Assert_Correct_Linked_List()  /* working */  {
            //**  groundwork  **//
            List<ElementBpt<int>> subject = new List<ElementBpt<int>>(100);

            /* out of order to test sorting step */
            for (int i = 40; i < 60; i++) {
                subject.Add(new ElementBpt<int>(i));
            }

            for (int i = 0; i < 40; i++) {
                subject.Add(new ElementBpt<int>(i));
            }

            for (int i = 60; i < 100; i++) {
                subject.Add(new ElementBpt<int>(i));
            }


            //**  exercising the code  **//
            BPlusTree<int> tree = BPlusTree<int>.MassConstruct(subject.ToArray(), 7);  // --> 5 children per parent 


            //**  gathering data  **//
            ForkBpt<int> root = tree.Root as ForkBpt<int>;
            ForkBpt<int> lmostFork = root.Contents[0] as ForkBpt<int>;
            LeafBpt<int> lmostLeaf = lmostFork.Contents[0] as LeafBpt<int>;


            //**  testing  **//
            ElementBpt<int> el = lmostLeaf.Contents[0] as ElementBpt<int>;

            for (int i = 0; i < 100; i++) {
                /* in effect a forward test */
                Assert.AreEqual(i, el.Key);

                /* traversing */
                el = el.Next;

                /* a backward test */
                if (el != null) {
                    Assert.AreEqual(i, el.Previous.Key);
                }
            }
        }

        [TestMethod()]
        public void MassConstruct_Test__Knowns__Assert_Correct_Structure()  /* working */  {
            //**  groundwork  **//
            List<ElementBpt<int>> subject = new List<ElementBpt<int>>(100);

            #region Tree contents

            /* out of order to test sorting step */
            for (int i = 40; i < 60; i++) {
                subject.Add(new ElementBpt<int>(i));
            }

            for (int i = 0; i < 40; i++) {
                subject.Add(new ElementBpt<int>(i));
            }

            for (int i = 60; i < 100; i++) {
                subject.Add(new ElementBpt<int>(i));
            }

            #endregion Tree contents

            #region Expecteds

            int[] atRoot = {
                0,
                25,
                50,
                75
            };

            int[][] atForks = {
                new int[] { 0, 5, 10, 15, 20 },
                new int[] { 25, 30, 35, 40, 45 },
                new int[] { 50, 55, 60, 65, 70 },
                new int[] { 75, 80, 85, 90, 95 }
            };

            int[][][] atLeaves = {
                new int[][] {
                    new int[] { 0, 1, 2, 3, 4 },
                    new int[] { 5, 6, 7, 8, 9 },
                    new int[] { 10, 11, 12, 13, 14 },
                    new int[] { 15, 16, 17, 18, 19 },
                    new int[] { 20, 21, 22, 23, 24 }
                },
                new int[][] {
                    new int[] { 25, 26, 27, 28, 29 },
                    new int[] { 30, 31, 32, 33, 34 },
                    new int[] { 35, 36, 37, 38, 39 },
                    new int[] { 40, 41, 42, 43, 44 },
                    new int[] { 45, 46, 47, 48, 49 }
                },
                new int[][] {
                    new int[] { 50, 51, 52, 53, 54 },
                    new int[] { 55, 56, 57, 58, 59 },
                    new int[] { 60, 61, 62, 63, 64 },
                    new int[] { 65, 66, 67, 68, 69 },
                    new int[] { 70, 71, 72, 73, 74 }
                },
                new int[][] {
                    new int[] { 75, 76, 77, 78, 79 },
                    new int[] { 80, 81, 82, 83, 84 },
                    new int[] { 85, 86, 87, 88, 89 },
                    new int[] { 90, 91, 92, 93, 94 },
                    new int[] { 95, 96, 97, 98, 99 }
                }
            };

            #endregion Expecteds


            //**  exercising the code  **//
            BPlusTree<int> actual = BPlusTree<int>.MassConstruct(subject.ToArray(), 7);  // --> 5 children on each node 


            //**  testing  **//
            #region Root, forks, and leaves

            /* root, its metadata */
            ForkBpt<int> root = actual.Root as ForkBpt<int>;
            Assert.AreEqual(atRoot.Length, root.Count);

            /* all nested data in matching loops */
            for (int onRoot = 0; onRoot < atRoot.Length; onRoot++) {
                /* at root */
                Assert.AreEqual(atRoot[onRoot], root.Keys[onRoot]);
                Assert.AreEqual(atRoot[onRoot], root.Contents[onRoot].Key);

                #region Forks and leaves

                /* fork, its metadata */
                ForkBpt<int> fork = root.Contents[onRoot] as ForkBpt<int>;
                int[] atFork = atForks[onRoot];
                Assert.AreEqual(atFork.Length, fork.Count);

                for (int onFork = 0; onFork < atFork.Length; onFork++) {
                    /* at fork */
                    Assert.AreEqual(atFork[onFork], fork.Keys[onFork]);
                    Assert.AreEqual(atFork[onFork], fork.Contents[onFork].Key);

                    #region Leaves

                    /* leaf, its metadata */
                    LeafBpt<int> leaf = fork.Contents[onFork] as LeafBpt<int>;
                    int[] atLeaf = atLeaves[onRoot][onFork];
                    Assert.AreEqual(atLeaf.Length, leaf.Count);

                    for (int onLeaf = 0; onLeaf < atLeaf.Length; onLeaf++) {
                        /* at leaf */
                        Assert.AreEqual(atLeaf[onLeaf], leaf.Keys[onLeaf]);
                        Assert.AreEqual(atLeaf[onLeaf], leaf.Contents[onLeaf].Key);
                    }

                    #endregion Leaves
                }

                #endregion Forks and leaves
            }

            #endregion Root, forks, and leaves
        }


        #endregion MassConstruct()

    }


    #region Secondary-class tests

    [TestClass]
    public class ElementBptTests
    {
        #region Equals()

        [TestMethod()]
        public void Equals_Test__Right_Null__Assert_False()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> left = new ElementBpt<int>(5, 500);
            ElementBpt<int> right = null;


            //**  exercising the code  **//
            bool actual = left.Equals(right);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void Equals_Test__Right_Same__Assert_True()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> left = new ElementBpt<int>(5, 500);
            ElementBpt<int> right = new ElementBpt<int>(5, 500);


            //**  exercising the code  **//
            bool actual = left.Equals(right);


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void Equals_Test__Right_Different_Key__Assert_False()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> left = new ElementBpt<int>(5, 500);
            ElementBpt<int> right = new ElementBpt<int>(14, 500);


            //**  exercising the code  **//
            bool actual = left.Equals(right);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void Equals_Test__Right_Different_Pointer__Assert_False()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> left = new ElementBpt<int>(5, 500);
            ElementBpt<int> right = new ElementBpt<int>(5, 140);


            //**  exercising the code  **//
            bool actual = left.Equals(right);


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        #endregion Equals()


        #region CompareTo()

        [TestMethod()]
        public void CompareTo_Test__Second_Key_Less__Assert_One()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> first = new ElementBpt<int>(3, -1);
            ElementBpt<int> second = new ElementBpt<int>(2, -1);


            //**  exercising the code  **//
            int actual = first.CompareTo(second);


            //**  testing  **//
            Assert.AreEqual(1, actual);
        }

        [TestMethod()]
        public void CompareTo_Test__Second_Key_Greater__Assert_Minus_One()  /* working */  {
            //**  groundwork  **//
            ElementBpt<int> first = new ElementBpt<int>(3, -1);
            ElementBpt<int> second = new ElementBpt<int>(6, -1);


            //**  exercising the code  **//
            int actual = first.CompareTo(second);


            //**  testing  **//
            Assert.AreEqual(-1, actual);
        }

        [TestMethod()]
        public void CompareTo_Test__Second_Key_Equal__Second_Pointer_Less__Assert_One()  /* working */  {
            /* when keys are equal, pointers are compared */

            //**  groundwork  **//
            ElementBpt<int> first = new ElementBpt<int>(6, 8);
            ElementBpt<int> second = new ElementBpt<int>(6, 2);


            //**  exercising the code  **//
            int actual = first.CompareTo(second);


            //**  testing  **//
            Assert.AreEqual(1, actual);
        }

        [TestMethod()]
        public void CompareTo_Test__Second_Key_Equal__Second_Pointer_Greater__Assert_Minus_One()  /* working */  {
            /* when keys are equal, pointers are compared */

            //**  groundwork  **//
            ElementBpt<int> first = new ElementBpt<int>(6, 3);
            ElementBpt<int> second = new ElementBpt<int>(6, 10);


            //**  exercising the code  **//
            int actual = first.CompareTo(second);


            //**  testing  **//
            Assert.AreEqual(-1, actual);
        }

        #endregion CompareTo()
    }

    #endregion Secondary-class tests

}
