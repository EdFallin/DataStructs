﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class GeneralTree_Tests
    {
        #region Overview

        /* units to test:
AddChild
RemoveFirstChild
RemoveNextChild
RemoveLastChild
FindAndRemoveNode ( *optional* )
TraversePreOrder
TraversePostOrder
*/

        /* descriptions by unit:
            AddChild
            // add a first child to `parent if no children;
            // otherwise, add new child at the end
         * 
            RemoveFirstChild
            // remove first child, dropping its subtree, and make
            // its any right sibling the new first child
         * 
            RemoveNextChild
            // remove the right sibling after the arg node
            // and point to any right sibling beyond it
         * 
            RemoveLastChild
            // remove the last child added and still present, dropping its subtree;
            // if the last present is the first child, drop it
         * 
            FindAndRemoveNode ( *optional* )
            // find the first node with the value in `content,
            // then use one of the removal methods to drop it
         * 
            TraversePreOrder
            // -
            // -
         * 
            TraversePostOrder
            // -
            // -
         * 
         */

        /* test tree for traversals         
                                            
            a							    
            |                               
            -----------------               
            b				c			    
            |               |               
            -------------   ---------       
            d	e	f	g	h	i	j	    
            |       |           |   |       
            |       |           |   -----   
            k		n			t	u	v   
            |       |                       
            -----   ---------               
            l	m	o	p	q			    
                            |               
                            -----           
                            r	s		    
                                            
         */

        /* preorder traversal of test tree:  a, b, d, k, l, m, e, f, n, o, p, q, r, s, g, c, h, i, t, j, u, v */

        /* postorder traversal of test tree:  l, m, k, d, e, o, p, r, s, q, n, f, g, b, h, t, i, u, v, j, c, a */

        #endregion Overview


        #region Friendlies

        public GeneralTree_<char> TestTree()
        {
            GeneralTree_Node_<char> node;

            #region Diagram of Test Tree

            /* test tree for traversals / find-and-remove ( if coded )
                                                
                a							    
                |                               
                -----------------               
                b				c			    
                |               |               
                -------------   ---------       
                d	e	f	g	h	i	j	    
                |       |           |   |       
                |       |           |   -----   
                k		n			t	u	v   
                |       |                       
                -----   ---------               
                l	m	o	p	q			    
                                |               
                                -----           
                                r	s		    
                                                
            */

            #endregion Diagram of Test Tree

            GeneralTree_<char> tree = new GeneralTree_<char>('a');
            node = tree.Root;

            tree.AddChild(node, 'b');
            tree.AddChild(node, 'c');

            node = node.FirstChild;   // 'b'
            tree.AddChild(node, 'd');
            tree.AddChild(node, 'e');
            tree.AddChild(node, 'f');
            tree.AddChild(node, 'g');

            node = tree.Root;
            node = node.FirstChild.RightSibling;   // 'c'
            tree.AddChild(node, 'h');
            tree.AddChild(node, 'i');
            tree.AddChild(node, 'j');

            node = tree.Root;
            node = node.FirstChild.FirstChild;   // 'd'
            tree.AddChild(node, 'k');
            node = node.FirstChild;   // 'k'
            tree.AddChild(node, 'l');
            tree.AddChild(node, 'm');

            node = tree.Root;
            node = node.FirstChild.FirstChild.RightSibling.RightSibling;    // 'f'
            tree.AddChild(node, 'n');
            node = node.FirstChild;   // 'n'
            tree.AddChild(node, 'o');
            tree.AddChild(node, 'p');
            tree.AddChild(node, 'q');

            node = node.FirstChild.RightSibling.RightSibling;   // 'q'
            tree.AddChild(node, 'r');
            tree.AddChild(node, 's');

            node = tree.Root;
            node = node.FirstChild.RightSibling.FirstChild.RightSibling;   // 'i'
            tree.AddChild(node, 't');

            node = node.RightSibling;   // 'j'
            tree.AddChild(node, 'u');
            tree.AddChild(node, 'v');




            return tree;
        }

        #endregion Friendlies


        #region Tests


        #region AddChild()

        [TestMethod()]
        public void AddChild_Test_No_Existing_Child_Assert_Expected_Child_And_Sibling()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');

            char expectedContent = 'b';
            char actualContent;

            GeneralTree_Node_<char> expectedSibling = null;
            GeneralTree_Node_<char> actualSibling;


            // EXERCISING THE CODE  
            target.AddChild(target.Root, 'b');


            // TESTING  
            {
                // expected child, via content
                actualContent = target.Root.FirstChild.Content;
                Assert.AreEqual(expectedContent, actualContent);

                // expected sibling ( of root )
                actualSibling = target.Root.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);

                // expected sibling ( of child )
                actualSibling = target.Root.FirstChild.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);
            }
        }

        [TestMethod()]
        public void AddChild_Test_With_Existing_Child_Assert_Expected_Child_And_Siblings()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');

            char expectedContent = 'c';
            char actualContent;

            GeneralTree_Node_<char> expectedSibling = null;
            GeneralTree_Node_<char> actualSibling;


            // EXERCISING THE CODE  
            target.AddChild(target.Root, 'c');


            // TESTING  
            {
                // expected first child, via content
                actualContent = target.Root.FirstChild.Content;
                Assert.AreEqual('b', actualContent);

                // expected second child, via content
                actualContent = target.Root.FirstChild.RightSibling.Content;
                Assert.AreEqual(expectedContent, actualContent);

                // expected sibling ( of a child )
                actualSibling = target.Root.FirstChild.RightSibling.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);
            }
        }

        [TestMethod()]
        public void AddChild_Test_With_Two_Existing_Children_Assert_Expected_Child_And_Siblings()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');

            char expectedContent = 'd';
            char actualContent;

            GeneralTree_Node_<char> expectedSibling = null;
            GeneralTree_Node_<char> actualSibling;


            // EXERCISING THE CODE  
            target.AddChild(target.Root, 'd');


            // TESTING  
            {
                // expected first child, via content
                actualContent = target.Root.FirstChild.Content;
                Assert.AreEqual('b', actualContent);

                // expected second child, via content
                actualContent = target.Root.FirstChild.RightSibling.Content;
                Assert.AreEqual('c', actualContent);

                // expected third child, via content
                actualContent = target.Root.FirstChild.RightSibling.RightSibling.Content;
                Assert.AreEqual(expectedContent, actualContent);

                // expected sibling ( of last child )
                actualSibling = target.Root.FirstChild.RightSibling.RightSibling.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);
            }
        }

        #endregion AddChild()


        #region RemoveFirstChild()

        [TestMethod()]
        public void RemoveFirstChild_Test_No_Siblings_Assert_No_Child()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');

            GeneralTree_Node_<char> expected = null;
            GeneralTree_Node_<char> actual = null;


            // EXERCISING THE CODE  
            target.RemoveFirstChild(target.Root);   // removing child node 'b' of 'a'


            // TESTING  
            actual = target.Root.FirstChild;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveFirstChild_Test_With_Siblings_Assert_Next_Sibling()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');

            char expected = 'c';
            char actual;


            // EXERCISING THE CODE  
            target.RemoveFirstChild(target.Root);   // removing node 'b' of 'a', replacing it with node 'c'


            // TESTING  
            actual = target.Root.FirstChild.Content;
            Assert.AreEqual(expected, actual);
        }

        #endregion RemoveFirstChild()


        #region RemoveNextChild()

        [TestMethod()]
        public void RemoveNextChild_Test_Starting_At_Second_Child_Assert_Expected_Sibling()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');
            target.AddChild(target.Root, 'e');

            GeneralTree_Node_<char> leftSibling = target.Root.FirstChild.RightSibling;   // node 'c'

            char expected = 'e';
            char actual;


            // EXERCISING THE CODE  
            target.RemoveNextChild(leftSibling);    // removing node 'd'


            // TESTING  
            {
                // . root == 'a'. first child == 'b'. right sibling == 'c'. right sibling == 'e'
                actual = target.Root.FirstChild.RightSibling.RightSibling.Content;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void RemoveNextChild_Test_Starting_At_Second_Child_Next_Null_Assert_Expected_Null()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');

            GeneralTree_Node_<char> leftSibling = target.Root.FirstChild.RightSibling;   // node 'c'

            GeneralTree_Node_<char> expected = null;
            GeneralTree_Node_<char> actual;


            // EXERCISING THE CODE  
            target.RemoveNextChild(leftSibling);    // removing node 'd'


            // TESTING  
            {
                // . root == 'a'. first child == 'b'. right sibling == 'c'. right sibling == null
                actual = target.Root.FirstChild.RightSibling.RightSibling;
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void RemoveNextChild_Test_Starting_At_First_Child_Assert_Expected_Sibling()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');

            GeneralTree_Node_<char> leftSibling = target.Root.FirstChild;   // node 'b'

            char expected = 'd';
            char actual;


            // EXERCISING THE CODE  
            target.RemoveNextChild(leftSibling);    // removing node 'c'


            // TESTING  
            {
                // . root == 'a'. first child == 'b'. right sibling == 'd'
                actual = target.Root.FirstChild.RightSibling.Content;
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion RemoveNextChild()


        #region RemoveLastChild()

        [TestMethod()]
        public void RemoveLastChild_Test_Single_Assert_Expected_Subtree()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');
            target.AddChild(target.Root, 'e');

            char expectedContent = 'd';
            char actualContent;

            GeneralTree_Node_<char> expectedSibling = null;
            GeneralTree_Node_<char> actualSibling;


            // EXERCISING THE CODE  
            target.RemoveLastChild(target.Root);    // removing node 'e'


            // TESTING  
            {
                // expected child, via content
                // . root == 'a'. first child == 'b'. right sibling == 'c'. right sibling == 'd'
                actualContent = target.Root.FirstChild.RightSibling.RightSibling.Content;
                Assert.AreEqual(expectedContent, actualContent);

                // expected sibling
                actualSibling = target.Root.FirstChild.RightSibling.RightSibling.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);
            }
        }

        [TestMethod()]
        public void RemoveLastChild_Test_Repeated_Assert_Expected_Subtree()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');
            target.AddChild(target.Root, 'e');

            char expectedContent = 'b';
            char actualContent;

            GeneralTree_Node_<char> expectedSibling = null;
            GeneralTree_Node_<char> actualSibling;


            // EXERCISING THE CODE  
            target.RemoveLastChild(target.Root);    // removing node 'e'
            target.RemoveLastChild(target.Root);    // removing node 'd'
            target.RemoveLastChild(target.Root);    // removing node 'c'


            // TESTING  
            {
                // expected child, via content
                // . root == 'a'. first child == 'b'
                actualContent = target.Root.FirstChild.Content;
                Assert.AreEqual(expectedContent, actualContent);

                // expected sibling
                actualSibling = target.Root.FirstChild.RightSibling;
                Assert.AreEqual(expectedSibling, actualSibling);
            }
        }

        [TestMethod()]
        public void RemoveLastChild_Test_Only_First_Child_Assert_Expected_Null()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');

            GeneralTree_Node_<char> expectedChild = null;
            GeneralTree_Node_<char> actualChild;


            // EXERCISING THE CODE  
            target.RemoveLastChild(target.Root);    // removing node 'b'


            // TESTING  
            {
                actualChild = target.Root.FirstChild;
                Assert.AreEqual(expectedChild, actualChild);
            }
        }

        [TestMethod()]
        public void RemoveLastChild_Test_All_Children_Assert_Expected_Null()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = new GeneralTree_<char>('a');
            target.AddChild(target.Root, 'b');
            target.AddChild(target.Root, 'c');
            target.AddChild(target.Root, 'd');
            target.AddChild(target.Root, 'e');

            GeneralTree_Node_<char> expectedChild = null;
            GeneralTree_Node_<char> actualChild;


            // EXERCISING THE CODE  
            target.RemoveLastChild(target.Root);    // removing node 'e'
            target.RemoveLastChild(target.Root);    // removing node 'd'
            target.RemoveLastChild(target.Root);    // removing node 'c'
            target.RemoveLastChild(target.Root);    // removing node 'b'


            // TESTING  
            {
                // expected sibling
                actualChild = target.Root.FirstChild;
                Assert.AreEqual(expectedChild, actualChild);
            }
        }

        #endregion RemoveLastChild()


        #region Traversals

        [TestMethod()]
        public void TraversePreOrder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = this.TestTree();

            char[] expected = new char[] { 'a', 'b', 'd', 'k', 'l', 'm', 'e', 'f', 'n', 'o', 'p', 'q', 'r', 's', 'g', 'c', 'h', 'i', 't', 'j', 'u', 'v' };
            char[] actual;


            // EXERCISING THE CODE  
            actual = target.TraversePreOrder(22);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostOrder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            GeneralTree_<char> target = this.TestTree();

            char[] expected = new char[] { 'l', 'm', 'k', 'd', 'e', 'o', 'p', 'r', 's', 'q', 'n', 'f', 'g', 'b', 'h', 't', 'i', 'u', 'v', 'j', 'c', 'a' };
            char[] actual;


            // EXERCISING THE CODE  
            actual = target.TraversePostOrder(22);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Traversals

        [TestMethod()]
        public void IsALeaf_Test_Assert_Expecteds()   // working  
        {

            #region Diagram of Test Tree

            /* test tree for traversals         
                                                
a							    
|                               
-----------------               
b				c			    
|               |               
-------------   ---------       
d	e	f	g	h	i	j	    
|       |           |   |       
|       |           |   -----   
k		n			t	u	v   
|       |                       
-----   ---------               
l	m	o	p	q			    
                |               
                -----           
                r	s		    
                                                
*/

            #endregion Diagram of Test Tree


            // GROUNDWORK  
            GeneralTree_<char> target = this.TestTree();
            GeneralTree_Node_<char> node = null;

            // testables
            // no `expected, for simplicity
            bool actual;


            // EXERCISING THE CODE && TESTING  
            {
                // checking each time to make sure the node is the right one

                // 'a', not a leaf
                actual = target.Root.IsALeaf;
                Assert.AreEqual(false, actual);

                // 'b', not a leaf
                node = target.Root.FirstChild;
                if (node.Content != 'b')
                    throw new Exception("node.Content != 'b'");
                actual = node.IsALeaf;
                Assert.AreEqual(false, actual);

                // 'e', a leaf
                node = node.FirstChild.RightSibling;   // starting at 'b'
                if (node.Content != 'e')
                    throw new Exception("node.Content != 'e'");
                actual = node.IsALeaf;
                Assert.AreEqual(true, actual);

                // 'g', a leaf; using `local to skip backtracking here
                GeneralTree_Node_<char> local = node.RightSibling.RightSibling;   // starting at 'e'
                if (local.Content != 'g')
                    throw new Exception("local.Content != 'g'");
                actual = local.IsALeaf;
                Assert.AreEqual(true, actual);

                // 'q', not a leaf
                node = node.RightSibling.FirstChild.FirstChild.RightSibling.RightSibling;   // starting at 'e'
                if (node.Content != 'q')
                    throw new Exception("node.Content != 'q'");
                actual = node.IsALeaf;
                Assert.AreEqual(false, actual);

                // 'r', a leaf
                node = node.FirstChild;   // starting at 'q'
                if (node.Content != 'r')
                    throw new Exception("node.Content != 'r'");
                actual = node.IsALeaf;
                Assert.AreEqual(true, actual);

                // 'h', a leaf
                node = target.Root.FirstChild.RightSibling.FirstChild;   // starting back at 'a'
                if (node.Content != 'h')
                    throw new Exception("node.Content != 'h'");
                actual = node.IsALeaf;
                Assert.AreEqual(true, actual);

                // 'v', a leaf
                node = node.RightSibling.RightSibling.FirstChild.RightSibling;   // starting at 'h'
                if (node.Content != 'v')
                    throw new Exception("node.Content != 'v'");
                actual = node.IsALeaf;
                Assert.AreEqual(true, actual);
            }
         }



        #endregion Tests

    }
}
