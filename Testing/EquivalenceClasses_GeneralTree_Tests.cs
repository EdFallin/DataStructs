﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class EquivalenceClasses_GeneralTree_Tests
    {
        #region Boilerplate

        public EquivalenceClasses_GeneralTree_Tests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #endregion Boilerplate


        #region Definitions

        const int ALPHA_RANGE = 97;

        #endregion Definitions


        #region ListAllEquivalenceClasses()

        [TestMethod()]    // ready  
        public void ListAllEquivalenceClasses_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            PrivateObject p = new PrivateObject(target);

            // 3 trees, each with three elements

            EquivalenceNode_<char>[] array = new EquivalenceNode_<char>[64];  // same as default size

            // filling content array with three trees
            for (int i = 0; i < 9; i++)
            {
                array[i] = new EquivalenceNode_<char>();

                array[i].Content = Convert.ToChar(i + ALPHA_RANGE);

                if ((i % 3) == 0)
                    array[i].Parent = EquivalenceClasses_GeneralTree_<char>.NO_PARENT;
                else
                    array[i].Parent = ((i / 3) * 3);    // use of int division reminiscent of % here
            }

            // setting data used in determining trees present
            array[0].NodeCount = 3;
            array[3].NodeCount = 3;
            array[6].NodeCount = 3;

            // assigning values to target private members
            p.SetField("_array", array);
            p.SetField("topNodeIndex", 8);

            // testables
            List<List<char>> expected = new List<List<char>>()
            {
                new List<char>() { 'a', 'b', 'c' },
                new List<char>() { 'd', 'e', 'f' },
                new List<char>() { 'g', 'h', 'i' },
            };

            List<List<char>> actual = null;


            // EXERCISING THE CODE  
            actual = target.ListAllEquivalenceClasses();


            // TESTING  
            for (int i = 0; i < 3; i++)
            {
                List<char> localExpected = expected[i];
                List<char> localActual = actual[i];

                CollectionAssert.AreEqual(localExpected, localActual);
            }
        }

        #endregion ListAllEquivalenceClasses()


        #region AddRoot()

        [TestMethod()]
        public void AddRoot_Test_Assert_Expected_ListAll_Results()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>(5);

            List<List<char>> expected = new List<List<char>>()
            {
                new List<char>() { 'a', },
                new List<char>() { 'b', },
                new List<char>() { 'c', },
            };

            List<List<char>> actual;


            // EXERCISING THE CODE  
            target.AddRoot('a');
            target.AddRoot('b');
            target.AddRoot('c');


            // TESTING  
            actual = target.ListAllEquivalenceClasses();

            for (int i = 0; i < 3; i++)
            {
                List<char> localExpected = expected[i];
                List<char> localActual = actual[i];

                CollectionAssert.AreEqual(localExpected, localActual);
            }
        }

        #endregion AddRoot()


        #region AddChild()

        [TestMethod()]
        public void AddChild_Test_Assert_Expected_ListAll_Results()   // working  
        {
            // this test doesn't deal with metadata, which is handled using FindRoot() internally

            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            target.AddRoot('a');
            target.AddRoot('b');
            target.AddRoot('c');

            List<List<char>> expected = new List<List<char>>()
            {
                new List<char>() { 'a', 'd', 'e', },
                new List<char>() { 'b', 'f', },
                new List<char>() { 'c', },
            };

            List<List<char>> actual;


            // EXERCISING THE CODE  
            // 
            // adding 2 children to 1st tree and 1 child to 2nd tree
            target.AddChild('d', 0);
            target.AddChild('e', 0);
            target.AddChild('f', 1);


            // TESTING  
            actual = target.ListAllEquivalenceClasses();

            for (int i = 0; i < 3; i++)
            {
                List<char> localExpected = expected[i];
                List<char> localActual = actual[i];

                CollectionAssert.AreEqual(localExpected, localActual);
            }
        }

        /* no separate test of metadata */

        #endregion AddChild()


        #region FindRoot()

        [TestMethod()]
        public void FindRoot_Test_Assert_Expecteds()   // working  
        {
            // looking for two different roots

            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            target.AddRoot('a');
            target.AddRoot('b');
            target.AddRoot('c');

            target.AddChild('d', 0);
            target.AddChild('e', 0);
            target.AddChild('f', 1);

            int expectedFirst = 0;
            int actualFirst;
            int expectedSecond = 1;
            int actualSecond;


            // EXERCISING THE CODE  
            actualFirst = target.FindRoot(4);    // 'e'; root is 'a'/0
            actualSecond = target.FindRoot(5);   // 'f'; root is 'b'/1

            // TESTING  
            Assert.AreEqual(expectedFirst, actualFirst);
            Assert.AreEqual(expectedSecond, actualSecond);
        }

        [TestMethod()]
        public void FindRoot_Test_Assert_Expected_Compressed_Paths()   // working  
        {
            // looking for two different roots

            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            PrivateObject p = new PrivateObject(target);

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1

            int actual;


            // EXERCISING THE CODE  
            target.FindRoot(4);     // 'e'; root is 'a'/0
            target.FindRoot(7);     // 'h'; root is 'b'/1; if instead FindRoot() is run against [5], [7] still points to a non-root node
            target.FindRoot(2);     // 'c'; root is itself, 'c'/2

            // TESTING  
            // 
            // the parent of each node should now be its root

            EquivalenceNode_<char>[] array = (EquivalenceNode_<char>[])p.GetField("_array");

            // first tree / equivalence class
            // nodes at 0, 3, and 4
            {
                // given true
                actual = array[0].Parent;
                Assert.AreEqual(EquivalenceClasses_GeneralTree_<char>.NO_PARENT, actual);

                actual = array[3].Parent;
                Assert.AreEqual(0, actual);

                actual = array[4].Parent;
                Assert.AreEqual(0, actual);

                // given true
                actual = array[6].Parent;
                Assert.AreEqual(0, actual);
            }

            // second tree / equivalence class;
            // nodes at 1 and 5
            {
                // given true
                actual = array[1].Parent;
                Assert.AreEqual(EquivalenceClasses_GeneralTree_<char>.NO_PARENT, actual);

                // given true
                actual = array[5].Parent;
                Assert.AreEqual(1, actual);

                actual = array[7].Parent;
                Assert.AreEqual(1, actual);
            }

            // third tree / equivalence class;
            // one node, at 2
            {
                // given true
                actual = array[2].Parent;
                Assert.AreEqual(EquivalenceClasses_GeneralTree_<char>.NO_PARENT, actual);
            }
        }

        #endregion FindRoot()


        #region DoShareRoot()

        [TestMethod()]
        public void DoShareRoot_Test_Do_Share_Root_Assert_True()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1

            bool expected = true;
            bool actual;


            // EXERCISING THE CODE  
            actual = target.DoShareRoot(3, 4);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DoShareRoot_Test_Do_Not_Share_Root_Assert_False()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1

            bool expected = false;
            bool actual;


            // EXERCISING THE CODE  
            actual = target.DoShareRoot(3, 7);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion DoShareRoot()


        #region MergeTrees()

        [TestMethod()]
        public void MergeTrees_Test_Do_Share_Root_Assert_True()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1

            bool expected = true;
            bool actual;

            if (target.DoShareRoot(3, 7))
                throw new AssertFailedException("target.DoShareRoot(3, 7) returned `true before trees were merged");


            // EXERCISING THE CODE  
            target.MergeTrees(3, 7);   // can be any nodes in trees


            // TESTING  
            // 
            // these didn't share a root before, but they should now
            actual = target.DoShareRoot(3, 7);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]   // ready  
        public void MergeTrees_Test_Correct_Counts_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            PrivateObject p = new PrivateObject(target);
            EquivalenceNode_<char>[] array = (EquivalenceNode_<char>[])p.GetField("_array");

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1


            int expectedAtFinalRoot = 7;   // count of nodes in post-merge first tree
            int actualAtFinalRoot = array[0].NodeCount;
            int expectedAtMergedRoot = 0;   // count of nodes in former root of now-merged second tree
            int actualAtMergedRoot = array[1].NodeCount;

            if (actualAtFinalRoot == expectedAtFinalRoot)
                throw new AssertFailedException("actualAtFinalRoot == expectedAtFinalRoot before trees were merged");

            if (expectedAtMergedRoot == actualAtMergedRoot)
                throw new AssertFailedException("expectedAtMergedRoot == actualAtMergedRoot before trees were merged");

            // EXERCISING THE CODE  
            target.MergeTrees(3, 7);   // can be any nodes in trees


            // TESTING  
            {
                // post-merge first tree
                actualAtFinalRoot = array[0].NodeCount;
                Assert.AreEqual(expectedAtFinalRoot, actualAtFinalRoot);

                // post-merge ex- second tree
                actualAtMergedRoot = array[1].NodeCount;
                Assert.AreEqual(expectedAtMergedRoot, actualAtMergedRoot);
            }
        }

        [TestMethod()]
        public void MergeTrees_Test_Assert_Expected_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>();
            PrivateObject p = new PrivateObject(target);

            target.AddRoot('a');    // @ 0; root of 'a'/0, 'd'/3, 'e'/4, 'g'/6
            target.AddRoot('b');    // @ 1; root of 'b'/1, 'f'/5, 'h'/7
            target.AddRoot('c');    // @ 2; root of 'c'/2

            target.AddChild('d', 6);    // @ 3; points at node that points at 'a'/0
            target.AddChild('e', 3);    // @ 4; points at node that points at node that points at 'a'/0
            target.AddChild('f', 1);    // @ 5; points at 'b'/1
            target.AddChild('g', 0);    // @ 6; points at 'a'/0
            target.AddChild('h', 5);    // @ 7; points at node that points at 'b'/1

            // testables
            List<List<char>> expected = new List<List<char>>()
            {
                // after joining via 3 and 7, 1st tree should comprise 'a'/0, 'b'/1, 'd'/3, 'e'/4, 'f'/5, 'g'/6, and 'h'/7
                new List<char>() { 'a', 'b', 'd', 'e', 'f', 'g', 'h' },

                // after joining, this should be empty
                //new List<char>() { },

                // after joinint, this should be the same as before, with one element, 'c'/2
                new List<char>() { 'c' },
            };

            List<List<char>> actual = null;


            // EXERCISING THE CODE  
            target.MergeTrees(3, 7);   // can be any nodes in trees
            actual = target.ListAllEquivalenceClasses();


            // TESTING  
            // 
            for (int i = 0; i < 2; i++)     // 2, not 3, because the now-empty 2nd tree is skipped by ListAll__()
            {
                List<char> localExpected = expected[i];
                List<char> localActual = actual[i];

                localActual.Sort();   // chars, should sort alphabetically

                CollectionAssert.AreEqual(localExpected, localActual);
            }

        }

        #endregion MergeTrees()


        [TestMethod()]
        public void MakeEquivalent_Test_Assert_Expected_Equivalence_Classes()   // working  
        {
            // GROUNDWORK  
            EquivalenceClasses_GeneralTree_<char> target = new EquivalenceClasses_GeneralTree_<char>(16);

            // adding 16 individual trees:
            // 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p'
            for (int i = 0; i < 16; i++)
                target.AddRoot(Convert.ToChar(i + ALPHA_RANGE));

            // testables
            List<char> classOne = new List<char> { 'a', 'b', 'c', 'd', 'l', 'm', 'n', 'o', 'p' };
            List<char> classTwo = new List<char> { 'e', 'f', 'g', 'h', 'i', 'j', 'k' };
            List<List<char>> expected = new List<List<char>> { classOne, classTwo };
            List<List<char>> actual = null;


            // EXERCISING THE CODE  
            // 
            // creating two equivalence classes;
            //
            // first equivalence class
            //     ( 'a','b','c','d','l','m','n','o','p'
            //        0    . . .   3 ; 11   . . .     15 )
            target.MakeEquivalent(0, 1);
            target.MakeEquivalent(1, 2);
            target.MakeEquivalent(2, 3);
            target.MakeEquivalent(11, 3);
            target.MakeEquivalent(12, 11);
            target.MakeEquivalent(13, 12);
            target.MakeEquivalent(14, 13);
            target.MakeEquivalent(15, 14);


            //     ( 'e','f','g','h','i','j','k'
            //        4         . . .         10 )
            target.MakeEquivalent(4, 5);
            target.MakeEquivalent(6, 5);
            target.MakeEquivalent(6, 7);
            target.MakeEquivalent(8, 7);
            target.MakeEquivalent(8, 9);
            target.MakeEquivalent(10, 9);


            // TESTING  
            // 
            // representative general equivalence tests
            {
                Assert.IsTrue(target.AreEquivalent(0, 3));
                Assert.IsTrue(target.AreEquivalent(4, 9));
                Assert.IsTrue(target.AreEquivalent(2, 14));
                Assert.IsFalse(target.AreEquivalent(0, 5));
            }

            // overall equivalence-classes test
            {
                actual = target.ListAllEquivalenceClasses();

                for (int i = 0; i < expected.Count; i++)
                {
                    // ensuring a reliable order
                    expected[i].Sort();
                    actual[i].Sort();

                    // the actual test
                    CollectionAssert.AreEqual(expected[i], actual[i]);
                }
            }
        }


    }
}
