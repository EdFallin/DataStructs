﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class List_DoublyLinked_Tests
    {
        public List_DoublyLinked_Tests() {
        }

        #region Test Context Instance

        private TestContext testContextInstance;

        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #endregion Test Context Instance


        #region Test Class Methods

        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }

        #endregion Test Class Methods


        #region Constructor / Completed

        [TestMethod()]
        public void Constructor_Test_Assert_Length_Zero()   // working  
        {
            // groundwork  
            int expected = 0;
            int actual;


            // exercising the code  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);
            actual = (int)p.GetField("_length");


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Constructor_Test_Assert_Position_Equals_Negative_One()   // working  
        {
            // groundwork  
            int expected = -1;
            int actual;


            // exercising the code  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);
            actual = (int)p.GetField("_position");


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Constructor_Test_Assert_Internal_Nodes_Initialized()   // working  
        {
            // groundwork  
            Type expectedType = typeof(TwoWayNode<int>);
            object actualStart;
            object actualEnd;

            // exercising the code  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            actualStart = p.GetField("_start");
            actualEnd = p.GetField("_end");


            // testing  
            Assert.AreEqual(expectedType, actualStart.GetType());
            Assert.AreEqual(expectedType, actualEnd.GetType());
        }

        [TestMethod()]
        public void Constructor_Test_Assert_Start_And_End_Nodes_Point_To_Each_Other()   // working  
        {
            // groundwork  
            TwoWayNode<int> start;
            TwoWayNode<int> end;

            bool expectedStartToEnd = true;
            bool expectedEndToStart = true;

            bool actualStartToEnd = true;
            bool actualEndToStart = true;


            // exercising the code  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            start = (TwoWayNode<int>)p.GetField("_start");
            end = (TwoWayNode<int>)p.GetField("_end");

            // retrieving values  
            actualStartToEnd = (start.Next == end);
            actualEndToStart = (end.Previous == start);

            // testing  
            Assert.AreEqual(expectedStartToEnd, actualStartToEnd);
            Assert.AreEqual(expectedEndToStart, actualEndToStart);
        }

        [TestMethod()]
        public void Constructor_Test_Assert_Pointed_Same_As_Start()   // working  
        {
            // groundwork  
            object expected;
            object actual;


            // exercising the code  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            expected = p.GetField("_start");
            actual = p.GetField("_pointed");

            // testing  
            Assert.AreEqual(expected, actual);
        }

        // * Disabled because top as a node is obsolete *
        //[TestMethod()]
        //public void Constructor_Test_Assert_Top_Same_As_Start()   // working  
        //{
        //   // groundwork  
        //   object expected;
        //   object actual;


        //   // exercising the code  
        //   List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
        //   PrivateObject p = new PrivateObject(target);

        //   expected = p.GetField("_start");
        //   actual = p.GetProperty("Top");

        //   // testing  
        //   Assert.AreEqual(expected, actual);
        //}

        #endregion Constructor / Completed


        #region Appending / Completed

        [TestMethod()]
        public void Append_Test_One_Append_Assert_Length_Is_Correct()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            int expected = 1;
            int actual;


            // exercising the code  
            target.Append(3);

            actual = (int)p.GetField("_length");


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Append_Test_Three_Appends_Assert_Length_Is_Correct()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            int expected = 3;
            int actual;


            // exercising the code  
            target.Append(3);
            target.Append(6);
            target.Append(9);

            actual = (int)p.GetField("_length");


            // testing  
            Assert.AreEqual(expected, actual);
        }

        // * Disabled because top as a node is obsolete *
        //[TestMethod()]
        //public void Append_Test_Assert_Latest_Is_Top()   // working  
        //{
        //   // groundwork  
        //   List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
        //   PrivateObject p = new PrivateObject(target);

        //   int expected = 9;
        //   int actual;


        //   // exercising the code  
        //   target.Append(3);
        //   target.Append(6);
        //   target.Append(9);

        //   actual = ((TwoWayNode<int>)p.GetProperty("Top")).Content;


        //   // testing  
        //   Assert.AreEqual(expected, actual);
        //}

        [TestMethod()]
        public void Append_Test_From_Empty_List_Assert_Latest_Is_Linked_Between_Start_And_End()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            object expectedStart;
            object expectedEnd;
            object actualPrevious;
            object actualNext;

            TwoWayNode<int> actual;


            // exercising the code  
            expectedStart = p.GetField("_start");
            expectedEnd = p.GetField("_end");

            target.Append(3);
            actual = ((TwoWayNode<int>)expectedEnd).Previous;
            actualPrevious = actual.Previous;
            actualNext = actual.Next;

            // testing  
            Assert.AreEqual(expectedStart, actualPrevious);
            Assert.AreEqual(expectedEnd, actualNext);

        }

        [TestMethod()]
        public void Append_Test_Assert_Latest_Is_Linked_Between_Prior_Top_And_End()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            object expectedTop;
            object expectedEnd;
            object actualPrevious;
            object actualNext;

            TwoWayNode<int> actual;


            // exercising the code  
            target.Append(3);
            target.Append(6);

            expectedEnd = p.GetField("_end");
            expectedTop = ((TwoWayNode<int>)expectedEnd).Previous;

            target.Append(9);
            actual = ((TwoWayNode<int>)p.GetField("_end")).Previous;
            actualPrevious = actual.Previous;
            actualNext = actual.Next;

            // testing  
            Assert.AreEqual(expectedTop, actualPrevious);
            Assert.AreEqual(expectedEnd, actualNext);

        }

        #endregion Appending / Completed


        #region Current Value / Completed

        [TestMethod()]
        public void CurrentValue_Test_First_Element_Get_Assert_Expected()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            int expected = 3;
            int actual;


            // exercising the code  
            target.Append(3);   // at this point, index is effectively -1
            target.PositionAtIndex(0);
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentValue_Test_MidList_Element_Get_Assert_Expected()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 9;
            int actual;


            // exercising the code  
            target.PositionAtIndex(2);
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentValue_Test_MidList_Element_Set_Assert_Get_Same_As_Assigned()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 19;
            int actual;


            // exercising the code  
            target.PositionAtIndex(2);
            target.CurrentValue = 19;
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Current Value / Completed


        #region Position At Index / Completed

        [TestMethod()]
        public void PositionAtIndex_Test_From_Initialized_List_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2

            int expected = 1;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            actual = (int)p.GetField("_position");

            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtIndex_Test_From_Initialized_List_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2

            int expected = 6;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            TwoWayNode<int> current = (TwoWayNode<int>)p.GetField("_pointed");
            actual = current.Content;

            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtIndex_Test_Index_Greater_Than_Current_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            p.SetField("_pointed", ((TwoWayNode<int>)p.GetField("_end")).Previous);   // at this point, _top points to node at index 1

            target.Append(9);   // 2

            // matching index to _pointed
            p.SetField("_position", 1);

            int expected = 2;
            int actual;


            // exercising the code  
            target.PositionAtIndex(2);
            actual = (int)p.GetField("_position");

            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtIndex_Test_Index_Greater_Than_Current_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            p.SetField("_pointed", ((TwoWayNode<int>)p.GetField("_end")).Previous);   // at this point, _top points to node at index 1

            target.Append(9);   // 2

            // matching index to _pointed
            p.SetField("_position", 1);

            int expected = 9;
            int actual;


            // exercising the code  
            target.PositionAtIndex(2);
            TwoWayNode<int> current = (TwoWayNode<int>)p.GetField("_pointed");
            actual = current.Content;

            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtIndex_Test_Index_Less_Than_Current_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2

            p.SetField("_position", 2);
            p.SetField("_pointed", ((TwoWayNode<int>)p.GetField("_end")).Previous);      // at this point, _top points to node at index 2

            int expected = 1;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            actual = (int)p.GetField("_position");

            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PositionAtIndex_Test_Index_Less_Than_Current_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();
            PrivateObject p = new PrivateObject(target);

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2

            p.SetField("_position", 2);
            p.SetField("_pointed", ((TwoWayNode<int>)p.GetField("_end")).Previous);      // at this point, _top points to node at index 2

            int expected = 6;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            TwoWayNode<int> current = (TwoWayNode<int>)p.GetField("_pointed");
            actual = current.Content;

            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Position At Index / Completed


        #region Position At Start, At End

        [TestMethod()]
        public void PositionAtStart_Test()  /* working */  {
            // groundwork  
            List_DoublyLinked_<GenericParameterHelper> target = new List_DoublyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);

            // moving away from index 0 
            target.Next();
            target.Next();
            target.Next();


            // exercising the code  
            target.PositionAtStart();


            // testing  
            Assert.AreEqual(0, target.CurrentPosition);
            Assert.AreEqual(element0, target.CurrentValue);
        }

        [TestMethod()]
        public void PositionAtEnd_Test()  /* working */  {
            // groundwork  
            List_DoublyLinked_<GenericParameterHelper> target = new List_DoublyLinked_<GenericParameterHelper>();

            int ix = -1;

            GenericParameterHelper element0 = new GenericParameterHelper(++ix);
            GenericParameterHelper element1 = new GenericParameterHelper(++ix);
            GenericParameterHelper element2 = new GenericParameterHelper(++ix);
            GenericParameterHelper element3 = new GenericParameterHelper(++ix);
            GenericParameterHelper element4 = new GenericParameterHelper(++ix);
            GenericParameterHelper element5 = new GenericParameterHelper(++ix);

            GenericParameterHelper[] elements = new GenericParameterHelper[] { element0, element1, element2, element3, element4, element5 };

            foreach (GenericParameterHelper element in elements)
                target.Append(element);


            // exercising the code  
            target.PositionAtEnd();


            // testing  
            Assert.AreEqual(5, target.CurrentPosition);
            Assert.AreEqual(element5, target.CurrentValue);
        }

        #endregion Position At Start, At End


        #region Current Position / Completed

        [TestMethod()]
        public void CurrentPosition_Test_Empty_List_Assert_Negative_One()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            int expected = -1;
            int actual;


            // exercising the code  
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentPosition_Test_MidList_Assert_Expected()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 2;
            int actual;


            // exercising the code  
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentPosition_Test_From_Empty_List_After_Appends_Assert_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = -1;
            int actual;


            // exercising the code  
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentPosition_Test_MidList_After_Appends_Assert_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 2;
            int actual;


            // exercising the code  
            target.Append(15);
            target.Append(18);
            target.Append(21);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CurrentPosition_Test_List_End_After_Appends_Assert_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 3;
            int actual;


            // exercising the code  
            target.Append(15);
            target.Append(18);
            target.Append(21);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Current Position / Completed


        #region Next / Completed

        [TestMethod()]
        public void Next_Test_Initial_To_First_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 0;
            int actual;


            // exercising the code  
            target.Next();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Next_Test_Initial_To_First_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 3;
            int actual;


            // exercising the code  
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Next_Test_Initial_To_Third_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 2;
            int actual;


            // exercising the code  
            target.Next();
            target.Next();
            target.Next();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Next_Test_Initial_To_Third_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 9;
            int actual;


            // exercising the code  
            target.Next();
            target.Next();
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Next_Test_MidList_To_Third_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 2;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            target.Next();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Next_Test_MidList_To_Third_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            int expected = 9;
            int actual;


            // exercising the code  
            target.PositionAtIndex(1);
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Next / Completed


        #region Previous / Completed

        [TestMethod()]
        public void Previous_Test_Second_To_First_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 0;
            int actual;


            // exercising the code  
            target.Previous();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Previous_Test_Second_To_First_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 3;
            int actual;


            // exercising the code  
            target.Previous();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Previous_Test_Last_To_Second_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 1;
            int actual;


            // exercising the code  
            target.Previous();
            target.Previous();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Previous_Test_Last_To_Second_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 6;
            int actual;


            // exercising the code  
            target.Previous();
            target.Previous();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Previous_Test_Last_To_First_Assert_Expected_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 0;
            int actual;


            // exercising the code  
            target.Previous();
            target.Previous();
            target.Previous();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Previous_Test_Last_To_First_Assert_Expected_Content()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 3;
            int actual;


            // exercising the code  
            target.Previous();
            target.Previous();
            target.Previous();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Previous / Completed


        #region Length / Completed

        [TestMethod()]
        public void Length_Test_Empty_List_Assert_Zero()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            int expected = 0;
            int actual;


            // exercising the code  
            actual = target.Length;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Length_Test_List_With_Elements_Assert_Expected()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);
            target.Append(6);
            target.Append(9);

            int expected = 3;
            int actual;


            // exercising the code  
            actual = target.Length;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Length / Completed


        #region Insert At Current / Completed

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Index_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 2;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Inserted_At_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 7;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Original_After_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 9;   // value of element originally at index of 2
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            target.Next();              // moving to the new index of 3
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Original_Before_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 6;   // value of element originally at index of 1 
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            target.Previous();              // moving to the index of 1 
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Inserted_Is_At_Next_Of_Previous()  /* working */  {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);       // 0  
            target.Append(6);       // 1  
            target.Append(9);       // 2  
            target.Append(12);      // 3  

            target.PositionAtIndex(2);

            int expected = 7;   // value of new element, now linked between prior elements 
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            target.Previous();              // moving to the index of 1 
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Inserted_Is_At_Previous_Of_Next()  /* working */  {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);       // 0  
            target.Append(6);       // 1  
            target.Append(9);       // 2  
            target.Append(12);      // 3  

            target.PositionAtIndex(2);

            int expected = 7;   // value of new element, now linked between prior elements 
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            target.Previous();              // moving to the index of 1 
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Assert_Expected_Length()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(2);

            int expected = 5;   // length after a 5th element inserted in middle
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.Length;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Inserting_At_End_Assert_Index_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 3;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Inserting_At_End_Assert_Inserted_At_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(3);

            int expected = 7;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Inserting_To_Empty_List_Assert_Index_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            int expected = -1;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertAtCurrent_Test_Inserting_To_Empty_List_Assert_Inserted_At_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            int expected = 7;
            int actual;


            // exercising the code  
            target.InsertAtCurrent(7);
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Insert At Current / Completed


        #region Remove At Current / Completed

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Index_Same()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 1;
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            actual = target.CurrentPosition;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Next_Of_Previous_Is_Originally_Following()  /* working */  {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);       // 0  
            target.Append(6);       // 1  
            target.Append(9);       // 2  
            target.Append(12);      // 3  

            target.PositionAtIndex(1);

            int expected = 9;
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            target.Previous();
            target.Next();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Previous_Of_Previous_Of_Next_Is_Originally_Before()  /* working */  {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);       // 0  
            target.Append(6);       // 1  
            target.Append(9);       // 2  
            target.Append(12);      // 3  

            target.PositionAtIndex(1);

            int expected = 3;
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            target.Next();
            target.Previous();
            target.Previous();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Returned_Value_Expected()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 6;   // value at index of 1 when it is removed
            int actual;


            // exercising the code  
            actual = target.RemoveAtCurrent();


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Original_Following_At_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 9;
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Original_Before_Index()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 3;   // value of element originally at index of 0
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            target.Previous();              // moving to the index of 0
            actual = target.CurrentValue;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveAtCurrent_Test_Assert_Expected_Length()   // working  
        {
            // groundwork  
            List_DoublyLinked_<int> target = new List_DoublyLinked_<int>();

            target.Append(3);   // 0
            target.Append(6);   // 1
            target.Append(9);   // 2
            target.Append(12);  // 3

            target.PositionAtIndex(1);

            int expected = 3;   // length after 4th element removed in middle
            int actual;


            // exercising the code  
            target.RemoveAtCurrent();
            actual = target.Length;


            // testing  
            Assert.AreEqual(expected, actual);
        }

        #endregion Remove At Current / Completed
    }
}
