﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BinaryTree_CompositePointerNodes_Tests
    {
        #region Friendlies

        public object GatherKeys(object topic_object, CompositePointerNode_<char, int> node)
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(node.Key);

            return topic;
        }

        #endregion Friendlies


        #region Basic Tests

        [TestMethod()]
        public void PolyMethod_AddNode_Children_NodeProperties_Test_Assert_Expecteds()   // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // elements are added directly using nodes
            BinaryTree_CompositePointerNodes_<char, int> target = new BinaryTree_CompositePointerNodes_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            // 4-arg constructor inits inodes; 2-arg constructor inits leaves

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);


            // TESTING  

            current = null;

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            current = target.Root;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            current = current.RightChild;
            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            current = target.Root;
            current = current.LeftChild;
            current = current.LeftChild;

            actualIsLeaf = current.IsLeaf;
            actualKey = current.Key;
            actualElement = current.Element;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        #endregion Basic Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_CompositePointerNodes_<char, int> target = new BinaryTree_CompositePointerNodes_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);


            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_CompositePointerNodes_<char, int> target = new BinaryTree_CompositePointerNodes_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinaryTree_CompositePointerNodes_<char, int> target = new BinaryTree_CompositePointerNodes_<char, int>('a', 100);

            CompositePointerNode_<char, int> current;

            current = target.Root;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('b', 50, null, null);

            current = current.LeftChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('d', 25);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('e', 75);

            current = target.Root;
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('c', 150, null, null);

            current = current.RightChild;
            current.LeftChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('f', 125);
            current.RightChild = CompositePointerNode_<char, int>.InitCompositePointerNode_('g', 175);

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }
}
