﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using DataStructs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    using ExternSortRS_Int_ = SortingExternal_ReplacementSelection_<int>;
    using TQueue_ = Queue_Array_<int[]>;
    using RFQueue_ = Queue_Array_<SortingExternal_ReplacementSelection_<int>.RunFile>;
    using RunBlock_ = SortingExternal_ReplacementSelection_<int>.RunBlock;
    using RunFile_ = SortingExternal_ReplacementSelection_<int>.RunFile;

    [TestClass()]
    public class SortingExternal_ReplacementSelection_Tests
    {
        /*  testing stack: 
         *      whole sort works with real data on disk; 
         *      finished: when I have data in an array, it's the same data as in the buffer (force-change through buffer, check array); 
         *      finished: when I have data in an array, base offset other than zero, it's the same data as in the buffer (force-change through buffer, check array); 
         *      finished: buffer block, indexer returns right values 
         *      finished: buffer block, .Size returns right value 
         *      finished: buffer block, .Exhausted returns right values 
         *      finished: buffer block, .Advance() and .Datum return right values 
         *      finished: number-of-Ts in block past end of core, but block is sized to end of core 
         *      finished: merge with blocks sharing parts of core buffer 
         *      finished: determining when sort (specifically, all merges) is done 
         *      finished: that a RunFile object is returned, in effect with a random file name 
         *      finished: that named file in RunFile has the expected nonzero length, at least the size of the total Ts times sizeof(T) 
         *      finished: that named file in RunFile contains the T[]s passed to it 
         *      finished: converting vectors to queues 
         *      finished: building min-heap in core; testing that core and min-heap are same using offset syntax 
         *      finished: min-heap algo, the core part of it outputting Ts 
         *      finished: min-heap algo, successful sorting with iteration 
         *      finished: min-heap algo, successful inserting into free core with iteration 
         *      --> drawing first cache of T[]s from subject to core 
         *      --> drawing caches of T[]s for min-heap algo 
         *      --> min-heap method, repeating and outputting T[]s to queue for run files; 
         *              maybe this + drawing more T[]s separately 
         *      --> adding run files to the queue of nexts-to-merge 
         *      --> swapping nexts-to-merge queues 
         *      --> testing interlocks added on queues, etc., if possible 
         *      and more...  */


        #region WriteRunToDisk()

        [TestMethod]
        public void WriteRunToDisk__Empty_Input__Assert_RunFile_Returned_With_Random_Name()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            TQueue_ queue = new TQueue_();
            queue.Enqueue(new int[64]);
            queue.Enqueue(new int[64]);
            queue.Enqueue(new int[64]);

            int totalBlocks = 3;


            //**  exercising the code  **//
            RunFile_ runFile = sort.WriteRunToDisk(queue, totalBlocks);


            //**  cleaning up; done before testing as ops stop at a failed test  **//
            DeleteFileCycle(runFile);


            //**  testing  **//
            Assert.IsNotNull(runFile.FileName);
        }

        [TestMethod]
        public void WriteRunToDisk__Full_Input__Assert_RunFile_Returned_Has_At_Least_Calculated_Length()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            TQueue_ queue = new TQueue_();
            queue.Enqueue(new int[64]);
            queue.Enqueue(new int[64]);
            queue.Enqueue(new int[64]);
            queue.Enqueue(new int[64]);

            // number of items times number of bytes in each 
            long sizeOfTs = queue.Length * 64;

            RunFile_ runFile = sort.WriteRunToDisk(queue, queue.Length);
            FileInfo fileMeta = new FileInfo(runFile.FileName);

            long sizeOfFile = fileMeta.Length;   // in bytes 


            //**  cleaning up; done before testing as ops stop at a failed test  **//
            DeleteFileCycle(runFile);


            //**  testing  **//
            Assert.IsTrue(sizeOfFile >= sizeOfTs);
        }

        [TestMethod()]
        public void WriteRunToDisk_Test__Meaningful_Input__Assert_RunFile_Returned_Contains_Input_Data()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            TQueue_ queue = new TQueue_();

            int[] blockOne = { 2, 10, 5, 17, 8, 19, 6, 7 };
            int[] blockTwo = { 3, 11, 6, 18, 9, 20, 7, 8 };

            queue.Enqueue(blockOne);
            queue.Enqueue(blockTwo);

            int[] originalData = blockOne.Concat(blockTwo).ToArray();
            int[] dataFromFile = new int[originalData.Length];


            //**  exercising the code  **//
            RunFile_ runFile = sort.WriteRunToDisk(queue, queue.Length);


            //**  getting data to test  **//
            using (BinaryReader reader = new BinaryReader(new FileStream(runFile.FileName, FileMode.Open))) {
                for (int i = 0; i < dataFromFile.Length; i++) {
                    int datum = reader.ReadInt32();
                    dataFromFile[i] = datum;
                }
            }


            //**  cleaning up  **//
            DeleteFileCycle(runFile);


            //**  testing  **//
            CollectionAssert.AreEqual(originalData, dataFromFile);
        }

        #region Local Friendlies

        private static void DeleteFileCycle(RunFile_ runFile) {
            while (File.Exists(runFile.FileName)) {
                try {
                    File.Delete(runFile.FileName);
                }
                finally {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        #endregion Local Friendlies

        #endregion WriteRunToDisk()


        #region RunBlock.ElementByOffset()

        [TestMethod]
        public void ElementByOffset_Test__Zero_Initial_Offset_Into_Buffer__Expected_Values_At_Offsets_Given()  /* working */  {
            //**  groundwork  **//
            int[] buffer = new int[512];

            buffer[0] = 100;
            buffer[1] = 15;
            buffer[2] = 7170;
            buffer[3] = 0;
            buffer[4] = 0;
            buffer[5] = 812;
            /* remainder not inited */

            int startOffset = 0;
            int lengthOfBlock = buffer.Length;
            RunBlock_ block = new RunBlock_(buffer, startOffset, lengthOfBlock);
            PrivateObject p = new PrivateObject(block);

            /* 1st targeted number is 1 byte; others are at least 2 bytes */
            int expectedFirst = 15;
            int expectedSecond = 7170;
            int expectedThird = 812;


            //**  exercising the code  **//
            int actualFirst = (int)p.Invoke("ElementByOffset", 1);
            int actualSecond = (int)p.Invoke("ElementByOffset", 2);
            int actualThird = (int)p.Invoke("ElementByOffset", 5);


            //**  testing  **//
            Assert.AreEqual(expectedFirst, actualFirst);
            Assert.AreEqual(expectedSecond, actualSecond);
            Assert.AreEqual(expectedThird, actualThird);
        }

        [TestMethod]
        public void ElementByOffset_Test__Nonzero_Initial_Offset_Into_Buffer__Expected_Values_At_Offsets_Given()  /* working */  {
            /* testing local offsets within a block of the entire buffer ( int[] ) and related math */

            //**  groundwork  **//
            int[] buffer = new int[512];

            // raw    local 
            buffer[0] = 100;      // 0       --   
            buffer[1] = 15;       // 1       --   
            buffer[2] = 7170;     // 2       --   
            buffer[3] = 0;        // 3       --   
            buffer[4] = 0;        // 4        0   
            buffer[5] = 42678;    // 5        1   
            buffer[6] = 0;        // 6        2   
            buffer[7] = 811;      // 7        3   
            buffer[8] = 0;        // 8        4   
            buffer[9] = 92;       // 9        5   
            buffer[10] = 812;     // 10       6   

            int startOffset = 4;   // first element in this RunBlock is at offset / index 4 
            int lengthOfBlock = buffer.Length;

            RunBlock_ block = new RunBlock_(buffer, startOffset, lengthOfBlock);
            PrivateObject p = new PrivateObject(block);

            /* 1st targeted number is 1 byte; others are at least 2 bytes */
            int expectedFirst = 42678;
            int expectedSecond = 0;
            int expectedThird = 92;


            //**  exercising the code  **//
            int actualFirst = (int)p.Invoke("ElementByOffset", 1);
            int actualSecond = (int)p.Invoke("ElementByOffset", 2);
            int actualThird = (int)p.Invoke("ElementByOffset", 5);


            //**  testing  **//
            Assert.AreEqual(expectedFirst, actualFirst);
            Assert.AreEqual(expectedSecond, actualSecond);
            Assert.AreEqual(expectedThird, actualThird);
        }

        #endregion RunBlock.ElementByOffset()


        #region RunBlock.Advance(), RunBlock.Datum

        [TestMethod()]
        public void RunBlock_Advance_Test__Buffered_Data_Known_Inputs__Assert_Expected_Next_Current()  /* working */  {
            /* in effect, this also tests .Datum, which internally calls .ElementByOffset() */
            //**  groundwork  **//
            int[] buffer = new int[8];
            int startOffset = 2;   // leave values 7 and 9 out of the block in the buffer 

            buffer[0] = 7;
            buffer[1] = 9;
            buffer[2] = 303;
            buffer[3] = 33247;
            buffer[4] = 8559;
            buffer[5] = 23;
            buffer[6] = 11;
            buffer[7] = 807;

            int lengthOfBlock = 8;
            RunBlock_ target = new RunBlock_(buffer, startOffset, lengthOfBlock);

            int expectedFirstAtTotalOffset3 = 33247;
            int expectedSecondAtTotalOffset4 = 8559;

            //**  exercising the code  **//
            /* at init, before any .Step() calls, at 0 in the block, 2 in the overall buffer (value of 303); each step moves forward one */
            target.Advance();   // at 33247 
            int actualFirst = target.Datum;

            target.Advance();   // at 8559 
            int actualSecond = target.Datum;


            //**  testing  **//
            Assert.AreEqual(expectedFirstAtTotalOffset3, actualFirst);
            Assert.AreEqual(expectedSecondAtTotalOffset4, actualSecond);
        }

        #endregion RunBlock.Advance(), RunBlock.Datum


        #region RunBlock.BracketedIndexer()

        [TestMethod]
        public void RunBlock_BracketedIndexer_Test__Buffered_Data_Known_Values__Assert_Expected_Results()  /* working */  {
            //**  groundwork  **//
            int[] buffer = new int[128];

            /* first 5 elements only */
            buffer[0] = 76;
            buffer[1] = 57;
            buffer[2] = 81;
            buffer[3] = 0;
            buffer[4] = 5;
            /* remainder not inited; all are 0 */

            int lengthOfBlock = buffer.Length;
            int startOffset = 0;

            RunBlock_ block = new RunBlock_(buffer, startOffset, lengthOfBlock);

            int expectedFirst = 76;
            int expectedSecond = 57;
            int expectedFifth = 5;


            //**  exercising the code  **//
            int actualFirst = block[0];
            int actualSecond = block[1];
            int actualFifth = block[4];


            //**  testing  **//
            Assert.AreEqual(expectedFirst, actualFirst);
            Assert.AreEqual(expectedSecond, actualSecond);
            Assert.AreEqual(expectedFifth, actualFifth);
        }

        #endregion RunBlock.BracketedIndexer()


        #region RunBlock.Size

        [TestMethod]
        public void RunBlock_Size_Test__Buffered_Data_Zero_Offset_Known_Length__Assert_Expected_Size()  /* working */  {
            /* this test is really of the buffer-based constructor, since .Size depends on metadata being set then */

            //**  groundwork  **//
            int[] buffer = new int[32];
            int offset = 0;

            int lengthOfBlock = buffer.Length / 2;   // only half of the core buffer is logically in the block 
            RunBlock_ block = new RunBlock_(buffer, offset, lengthOfBlock);

            int expectedSize = 16;


            //**  exercising the code  **//
            int actualSize = block.Size;


            //**  testing  **//
            Assert.AreEqual(expectedSize, actualSize);
        }

        [TestMethod]
        public void RunBlock_Size_Test__Buffered_Data_Known_Offset_Known_Length_And_Core_Size__Assert_Expected_Size()  /* working */  {
            /*  when the offset means that the buffer isn't full up to its arg size, the smaller size is calculated and used; 
             *  this test is really of the buffer-based constructor, since .Size depends on metadata being set then  */

            //**  groundwork  **//
            int[] buffer = new int[32];

            /* the entire core's length is provided, but only half of it is available after the offset */
            int lengthOfBlock = buffer.Length;
            int offset = buffer.Length / 2;
            RunBlock_ block = new RunBlock_(buffer, offset, lengthOfBlock);

            int expectedSize = 16;


            //**  exercising the code  **//
            int actualSize = block.Size;


            //**  testing  **//
            Assert.AreEqual(expectedSize, actualSize);
        }

        #endregion RunBlock.Size


        #region Runblock.Exhausted

        [TestMethod()]
        public void RunBlock_Exhausted_Test__Buffered_Data_Not_Yet_Exhausted__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            int[] buffer = new int[4];
            int offset = 0;

            int lengthOfBlock = buffer.Length;
            RunBlock_ target = new RunBlock_(buffer, offset, lengthOfBlock);


            // ^target's index starts at offset 0 
            target.Advance();   // now at offset 1 
            target.Advance();   // now at offset 2 
            target.Advance();   // now at offset 3 

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = target.Exhausted;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RunBlock_Exhausted_Test__Buffered_Data_Is_Exhausted__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            int[] buffer = new int[4];
            int offset = 0;

            int lengthOfBlock = buffer.Length;
            RunBlock_ target = new RunBlock_(buffer, offset, lengthOfBlock);


            // ^target's index starts at offset 0 
            target.Advance();   // now at offset 1 
            target.Advance();   // now at offset 2 
            target.Advance();   // now at offset 3 
            target.Advance();   // now past top (at index == 4), invalid, so exhausted 

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            actual = target.Exhausted;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Runblock.Exhausted


        #region LeastDatumDuple()

        [TestMethod()]
        public void LeastDatumDuple_Test__First_Is_Least__Assert_Expected_Datum_And_RunBlock()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ target = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(target);

            ExternSortRS_Int_.DatumBlockIndexDuple first = new ExternSortRS_Int_.DatumBlockIndexDuple(2, 4);
            ExternSortRS_Int_.DatumBlockIndexDuple second = new ExternSortRS_Int_.DatumBlockIndexDuple(3, 2);


            ExternSortRS_Int_.DatumBlockIndexDuple result;

            int expectedDatum = 2;
            int actualDatum;

            int expectedRunBlockIndex = 4;
            int actualRunBlockIndex;


            //**  exercising the code  **//
            result = (ExternSortRS_Int_.DatumBlockIndexDuple)p.Invoke("LeastDatumDuple", first, second);


            //**  testing  **//
            actualDatum = result.Datum;
            actualRunBlockIndex = result.RunBlockIndex;

            /* the actual testing */
            Assert.AreEqual(expectedDatum, actualDatum);
            Assert.AreEqual(expectedRunBlockIndex, actualRunBlockIndex);
        }

        [TestMethod()]
        public void LeastDatumDuple_Test__Second_Is_Least__Assert_Expected_Datum_And_RunBlock()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ target = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(target);

            ExternSortRS_Int_.DatumBlockIndexDuple first = new ExternSortRS_Int_.DatumBlockIndexDuple(3, 2);
            ExternSortRS_Int_.DatumBlockIndexDuple second = new ExternSortRS_Int_.DatumBlockIndexDuple(2, 4);


            ExternSortRS_Int_.DatumBlockIndexDuple result;

            int expectedDatum = 2;
            int actualDatum;

            int expectedRunBlockIndex = 4;
            int actualRunBlockIndex;


            //**  exercising the code  **//
            result = (ExternSortRS_Int_.DatumBlockIndexDuple)p.Invoke("LeastDatumDuple", first, second);


            //**  testing  **//
            actualDatum = result.Datum;
            actualRunBlockIndex = result.RunBlockIndex;

            /* the actual testing */
            Assert.AreEqual(expectedDatum, actualDatum);
            Assert.AreEqual(expectedRunBlockIndex, actualRunBlockIndex);
        }

        #endregion LeastDatumDuple()


        #region MergeRuns()

        [TestMethod()]
        public void MergeRuns_Test__Short_Shared_Buffered_Data_Output_To_List_Expected_Output_Blocks_And_Ordered_Elements()  /* working */  {
            /* runs are always depleted unevenly, so my minimum test of this method includes code handling run-block exhaustion */

            //**  groundwork  **//
            ExternSortRS_Int_ target = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(target);

            RunBlock_[] blocks = Make4RunBlocksSharingCoreOf12SemiSortedInt32s();

            TQueue_ mergedVector = new TQueue_();
            int outputBlockSize = 6;   // size in Ts ( here, Int32s ) 


            //**  exercising the code  **//
            p.Invoke("MergeRuns", blocks, mergedVector, outputBlockSize);


            //**  testing  **//
            /* first, the size of the output */
            int expectedBlocksCount = 2;   // 2 output blocks, both 8 Int32s long 
            Assert.AreEqual(expectedBlocksCount, mergedVector.Length);

            /* second, the data in the output */
            for (int blockIx = 0; blockIx < expectedBlocksCount; blockIx++) {
                int[] block = mergedVector.Dequeue();

                /* compare all, starting with 1 because -1 used in comparing */
                for (int datumIx = 1; datumIx < block.Length; datumIx++) {
                    Assert.IsTrue(Basics.AreOrdered(block[datumIx - 1], block[datumIx]));
                }
            }
        }

        #region Local Friendlies

        private RunBlock_[] Make4RunBlocksSharingCoreOf12SemiSortedInt32s()  /* verified */  {
            /* .Merge() only works if its inputs are themselves sorted */

            int[] coreBuffer = new int[12];

            /* first buffer: 2 elements */
            coreBuffer[0] = 25; coreBuffer[1] = 105;

            /* second buffer: 4 elements */
            coreBuffer[2] = 327; coreBuffer[3] = 939;
            coreBuffer[4] = 64405; coreBuffer[5] = 80807;

            /* third buffer: 2 elements */
            coreBuffer[6] = 1; coreBuffer[7] = 5;

            /* fourth buffer: 4 elements */
            coreBuffer[8] = 26; coreBuffer[9] = 288;
            coreBuffer[10] = 382; coreBuffer[11] = 417;


            int[] offsets = { 0, 2, 6, 8 };   /* start of each block is same as sum of previous blocks' lengths */
            int[] lengths = { 2, 4, 2, 4 };

            RunBlock_[] blocks = new RunBlock_[offsets.Length];

            for (int i = 0; i < blocks.Length; i++) {
                blocks[i] = new RunBlock_(coreBuffer, offsets[i], lengths[i]);
            }

            return blocks;
        }

        #endregion Local Friendlies


        [TestMethod()]
        public void MergeRuns_Test__Shared_Buffered_Data_Output_To_List_Expected_Output_Blocks_And_Ordered_Elements()  /* working */  {
            /* runs are always depleted unevenly, so I think the minimum test of this method includes code handling run-block exhaustion */

            //**  groundwork  **//
            ExternSortRS_Int_ target = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(target);

            RunBlock_[] blocks = Make4RunBlocksSharingCoreOf30SemiSortedInt32s();

            TQueue_ mergedVector = new TQueue_();
            int outputBlockSize = 15;   // size in Ts ( here, Int32s ) 


            //**  exercising the code  **//
            p.Invoke("MergeRuns", blocks, mergedVector, outputBlockSize);


            //**  testing  **//
            /* first, the size of the output */
            int expectedBlocksCount = 2;
            Assert.AreEqual(expectedBlocksCount, mergedVector.Length);

            /* second, the data in the output */
            for (int blockIx = 0; blockIx < expectedBlocksCount; blockIx++) {
                int[] block = mergedVector.Dequeue();

                /* compare all, starting with 1 because -1 used in comparing */
                for (int datumIx = 1; datumIx < block.Length; datumIx++) {
                    Assert.IsTrue(Basics.AreOrdered(block[datumIx - 1], block[datumIx]));
                }
            }
        }

        #region Local Friendlies

        private RunBlock_[] Make4RunBlocksSharingCoreOf30SemiSortedInt32s()  /* verified */  {
            /* .Merge() only works if its inputs are themselves sorted */

            int[] coreBuffer = new int[30];

            /* first buffer: 6 elements */
            coreBuffer[0] = 25; coreBuffer[1] = 105;
            coreBuffer[2] = 327; coreBuffer[3] = 939;
            coreBuffer[4] = 64405; coreBuffer[5] = 80807;

            /* second buffer: 8 elements */
            coreBuffer[6] = 1; coreBuffer[7] = 5;
            coreBuffer[8] = 26; coreBuffer[9] = 288;
            coreBuffer[10] = 382; coreBuffer[11] = 417;
            coreBuffer[12] = 1705; coreBuffer[13] = 50109;

            /* third buffer: 4 elements */
            coreBuffer[14] = 2; coreBuffer[15] = 9;
            coreBuffer[16] = 32905; coreBuffer[17] = 79265;

            /* fourth buffer: 12 elements */
            coreBuffer[18] = 3; coreBuffer[19] = 4;
            coreBuffer[20] = 18; coreBuffer[21] = 53;
            coreBuffer[22] = 100; coreBuffer[23] = 395;
            coreBuffer[24] = 1000; coreBuffer[25] = 10000;
            coreBuffer[26] = 17056; coreBuffer[27] = 45115;
            coreBuffer[28] = 66207; coreBuffer[29] = 93278;


            int[] offsets = new int[] { 0, 6, 14, 18 };   /* start of each block is same as sum of previous blocks' lengths */
            int[] lengths = new int[] { 6, 8, 4, 12 };   /* the last should be changed internally to 12 */

            RunBlock_[] blocks = new RunBlock_[offsets.Length];

            for (int i = 0; i < blocks.Length; i++) {
                blocks[i] = new RunBlock_(coreBuffer, offsets[i], lengths[i]);
            }

            return blocks;
        }

        #endregion Local Friendlies

        #endregion MergeRuns()


        #region SumOfSizesOfRunBlocks()

        [TestMethod]
        /* -->  I need to figure out why I can't convert this *fucking* thing to `private` in the class without a bullshit fail here, then to make the fix... */
        public void SumOfSizesOfRunBlocks_Test__Buffered_Data__Expected_Sum_Returned()  /* working */  {
            //**  groundwork  **//
            ExternSortRS_Int_ target = new ExternSortRS_Int_();

            int[] coreBuffer = new int[25];

            /* last arg in each initer is the size; actual size may be this or less (if arg would put size past end of buffer) */
            RunBlock_ one = new RunBlock_(coreBuffer, 0, 7);
            RunBlock_ two = new RunBlock_(coreBuffer, 7, 14);
            RunBlock_ three = new RunBlock_(coreBuffer, 21, 4);

            RunBlock_[] runBlocks = { one, two, three };


            //**  exercising the code & testing  **//
            Assert.AreEqual(25, target.SumOfSizesOfRunBlocks(runBlocks));
        }

        #endregion SumOfSizesOfRunBlocks()


        #region MergingIsDone()

        /* --> I can call this in a loop in coordinating method, stop when `true` */

        [TestMethod]
        public void MergingIsDone__Not_Done__Assert_Returns_False()  /* working */  {
            /* 2 or more items in queue is not done */

            //**  groundwork  **//
            ExternSortRS_Int_ sort = new ExternSortRS_Int_();

            RFQueue_ queue = new RFQueue_();
            queue.Enqueue(new ExternSortRS_Int_.RunFile("meaningless name"));
            queue.Enqueue(new ExternSortRS_Int_.RunFile("meaningless name"));

            //**  exercising code and testing  **//
            Assert.AreEqual(false, sort.MergingIsDone(queue));
        }

        [TestMethod]
        public void MergingIsDone__Done__Assert_Returns_True()  /* working */  {
            /* exactly 1 item in queue is done */

            //**  groundwork  **//
            ExternSortRS_Int_ sort = new ExternSortRS_Int_();

            RFQueue_ queue = new RFQueue_();
            queue.Enqueue(new ExternSortRS_Int_.RunFile("meaningless name"));

            //**  exercising code and testing  **//
            Assert.AreEqual(true, sort.MergingIsDone(queue));
        }

        #endregion MergingDone()


        #region CoreToSharedMinHeap()

        [TestMethod()]
        public void CoreToSharedMinHeap_Test__Known_Input__Assert_Elements_Of_Core_And_Heap_Match()  /* working */  {
            //**  groundwork  **//
            int[] topic = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, };

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject pSort = new PrivateObject(sort);
            pSort.SetField("_core", topic);


            //**  exercising the code  **//
            pSort.Invoke("CoreToSharedMinHeap");


            //**  testing  **//
            int[] core = (int[])pSort.GetField("_core");
            int[] array = ProvideArrayBackingMinHeap(pSort);

            for (int i = 0; i < topic.Length; i++) {
                Assert.AreEqual(core[i], array[i]);
            }
        }

        #region Local Friendlies

        private static int[] ProvideArrayBackingMinHeap(PrivateObject pSort)  /* verified */  {
            /* get array backing the heap */
            MinHeapSharedArray_<int> heap = (MinHeapSharedArray_<int>)pSort.GetField("_heap");
            PrivateObject pHeap = new PrivateObject(heap);
            int[] array = (int[])pHeap.GetField("_array");
            return array;
        }

        #endregion Local Friendlies

        #endregion CoreToSharedMinHeap()


        #region SelectReplace()

        [TestMethod()]
        public void SelectReplace_Test__Known_Input__Assert_Expected_Output()  /* working */  {
            /*  .SelectReplace() is the crux of the sort algo only, outputting one next element 
             *  and either adding the next one to the min-heap or to the free core  */

            //**  groundwork  **//
            int[] topic = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, 10 };

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int next = 100;

            int expected = 1;


            //**  exercising the code  **//
            int actual = sort.SelectReplace(next);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SelectReplace_Test__Into_Heap_Input__Assert_Same_Heap_Length()  /* working */  {
            /* when a new value goes into the heap, the heap's length does not decrease */

            //**  groundwork  **//
            int[] topic = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, 10 };   // 12 elements 

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int next = 100;

            int expected = 12;


            //**  exercising the code  **//
            int actual = sort.SelectReplace(next);


            //**  testing  **//
            MinHeapSharedArray_<int> heap = (MinHeapSharedArray_<int>)p.GetField("_heap");
            Assert.AreEqual(expected, heap.Count);
        }

        [TestMethod()]
        public void SelectReplace_Test__Into_Free_Core__Assert_Decremented_Heap_Length()  /* working */  {
            /* when a new value goes into the heap, the heap's length does not decrease */

            //**  groundwork  **//
            int[] topic = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, 10 };   // 12 elements 

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int next = -100;

            int expected = 11;


            //**  exercising the code  **//
            int actual = sort.SelectReplace(next);


            //**  testing  **//
            MinHeapSharedArray_<int> heap = (MinHeapSharedArray_<int>)p.GetField("_heap");
            Assert.AreEqual(expected, heap.Count);
        }

        [TestMethod()]
        public void SelectReplace_Test__Into_Free_Core__Assert_First_Element_In_Free_Core_Is_New_Element()  /* working */  {
            /* when a new value goes into the heap, the heap's length does not decrease */

            //**  groundwork  **//
            int[] topic = { 18, 20, 1, 19, 2, 16, 4, 5, 3, 17, 15, 10 };   // 12 elements 

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int next = -100;

            int expected = -100;


            //**  exercising the code  **//
            sort.SelectReplace(next);

            MinHeapSharedArray_<int> heap = (MinHeapSharedArray_<int>)p.GetField("_heap");
            int countInHeap = heap.Count;
            int[] core = (int[])p.GetField("_core");
            int actual = core[countInHeap];

            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion SelectReplace()


        #region SelectReplace() Iterated As Though In Main Algo

        [TestMethod()]
        public void SelectReplace_Test__Known_Inputs_Some_Into_Heap__Assert_Expected_Sorted_Outputs()  /* working */  {
            //**  groundwork  **//
            int[] topic = { 16, 1, 7, 4, 13, 10, };

            /* 2, 3, 5, and afters should not be output, as they should be lower than 10 after first three calls to .SelectReplace() */
            int[] nexts = { 19, 22, 25, 2, 3, 5, -1, -10, -100 };

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int[] expected = { 1, 4, 7, 10, 13, 16, 19, 22, 25 };
            int[] actual = new int[expected.Length];


            //**  exercising the code  **//
            /* ^nexts has to be same length as ^expected to get full expected length and also test into-heap and into-free */
            for (int i = 0; i < nexts.Length; i++) {
                int next = nexts[i];
                actual[i] = sort.SelectReplace(next);
            }

            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SelectReplace_Test__Known_Inputs_Some_Into_Heap__Assert_Expected_Free_Core_Elements()  /* working */  {
            //**  groundwork  **//
            int[] topic = { 16, 1, 7, 4, 13, 10, };

            /* 2, 3, 5, and afters should end up in the free core in reverse order */
            int[] nexts = { 19, 22, 25, 2, 3, 5, -1, -10, -100 };

            ExternSortRS_Int_ sort = new ExternSortRS_Int_();
            PrivateObject p = new PrivateObject(sort);
            p.SetField("_core", topic);
            p.Invoke("CoreToSharedMinHeap");

            int[] core = (int[])p.GetField("_core");

            int[] expected = { -100, -10, -1, 5, 3, 2 };
            int[] actual = new int[expected.Length];



            //**  exercising the code  **//
            /* ^nexts has to be same length as ^expected to get full expected length and also test into-heap and into-free */
            for (int i = 0; i < nexts.Length; i++) {
                int next = nexts[i];
                sort.SelectReplace(next);   // return values thrown out; not tested here 
            }

            //**  testing  **//
            /* metadata has to be retrieved after exercising code, or else it will be wrong */
            MinHeapSharedArray_<int> heap = (MinHeapSharedArray_<int>)p.GetField("_heap");
            int firstFree = heap.Count;

            for (int i = firstFree, outward = 0; i < core.Length; i++, outward++) {
                actual[outward] = core[i];
            }

            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion SelectReplace() Iterated As Though In Main Algo

    }
}
