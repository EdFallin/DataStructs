﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BinarySearchTree_Tests
    {

        // build a tree with adds, check structure node by node, and get count;
        // traverse inorder to verify it works and matches expected from node-by-node, so it can be used in later tests;
        // repeat, but with removals before traversal;
        // build a tree with adds, then do two find-returns;
        // remove several specific nodes, traversing tree each time to return structure;
        // remove-any several times, traversing and checking, then removing all and checking root and count;
        // possibly find-least by itself, with maybe deeper tree and two finds, with first least removed before second find;
        // finally, do all traversals as such


        #region Friendlies

        public object GatherElements(object topic_object, PointerNode_<int, char> node)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(node.Element);

            return topic;
        }

        #endregion Friendlies


        /*  test tree:                                      
         *                                                  
         *                    a/100                         
         *                      |                           
         *         ---------------------------              
         *        b/50                     c/150            
         *         |                         |              
         *    -----------               -----------         
         *  d/25       e/75         f/125       g/175       
         *                                                  
        */

        /*  extended test tree:                             
         *                                                  
         *                    a/100                         
         *                      |                           
         *         ---------------------------              
         *        b/50                     c/150            
         *         |                         |              
         *    -----------               -----------         
         *  d/25       e/75         f/125       g/175       
         *                            |                     
         *                        ----------                
         *                      h/111    i/142              
         *                                                  
        */


        #region Basic Tests

        [TestMethod()]
        public void PolyMethod_AddEntry_Count_Test_Assert_Expecteds()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            PointerNode_<int, char> current;

            char expected = '\0';
            char actual = '\0';

            // INTEGRATED EXERCISING THE CODE AND TESTING  
            expected = 'a';
            current = target.Root;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'b';
            current = current.LeftChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'd';
            current = current.LeftChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'e';
            current = target.Root;
            current = current.LeftChild;
            current = current.RightChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'c';
            current = target.Root;
            current = current.RightChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'f';
            current = current.LeftChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);

            expected = 'g';
            current = target.Root;
            current = current.RightChild;
            current = current.RightChild;
            actual = current.Element;

            Assert.AreEqual(expected, actual);


            // TESTING METADATA  
            Assert.AreEqual(7, target.Count);
        }

        [TestMethod()]
        public void PolyMethod_AddEntry_TraverseInorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindEntry_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            char expected = 'e';
            char actual;


            // EXERCISING THE CODE  
            actual = target.GetEntry(75);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }


        #region Invalid for now: using internal _swap

        //[TestMethod()]
        public void RemoveLeastRecursor_IsLeaf_Assert_Internal_Is_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c' ; least child is 125 / 'f'

            PrivateObject p = new PrivateObject(target);

            char expected = 'f';
            char actual = '\0';


            // EXERCISING THE CODE  
            p.Invoke("RemoveLeastRecursor", current);
            PointerNode_<int, char> topic = p.GetField("_swap") as PointerNode_<int, char>;

            if (topic != null)
                actual = topic.Element;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        public void RemoveLeastRecursor_INode_Assert_Internal_Is_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c' ; least child is 125 / 'f'

            PrivateObject p = new PrivateObject(target);

            char expected = 'c';
            char actual = '\0';

            // removes node 'f', so node 'c' is now least on R side of root ( 'a' )
            p.Invoke("RemoveLeastRecursor", current);


            // EXERCISING THE CODE  
            // removes node 'c' and makes node 'g' the R child of root ( 'a' )
            p.Invoke("RemoveLeastRecursor", current);
            PointerNode_<int, char> topic = p.GetField("_swap") as PointerNode_<int, char>;

            if (topic != null)
                actual = topic.Element;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Invalid for now: using internal _swap


        [TestMethod()]
        public void RemoveLeastRecursor_Leaf_Assert_Expected_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c' ; least child is 125 / 'f'

            PrivateObject p = new PrivateObject(target);

            string expected = "dbeacg";
            string actual;


            // EXERCISING THE CODE  
            // removing node 'f'
            p.Invoke("RemoveLeastRecursor", current);

            actual = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        //**  Structure of extended testing tree is pre-tested here  **//
        [TestMethod()]
        public void RemoveLeastRecursor_Leaf_Then_INode_Assert_Expected_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');
            target.AddEntry(111, 'h');
            target.AddEntry(142, 'i');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c'

            PrivateObject p = new PrivateObject(target);

            string expected;
            string actual;


            // PRE-TESTING  
            // 
            // ensuring that my new tree has the expected shape
            expected = "dbeahficg";
            actual = (string)target.TraverseInorder(GatherElements);
            Assert.AreEqual(expected, actual);


            // EXERCISING THE CODE  
            expected = "dbeaicg";

            // removing node 'h', then node 'f', leaving 'i' as L child of 'c'
            p.Invoke("RemoveLeastRecursor", current);
            p.Invoke("RemoveLeastRecursor", current);

            actual = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLeastRecursor_Leaf_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            // 
            // extended tree
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');
            target.AddEntry(111, 'h');
            target.AddEntry(142, 'i');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c' ; least child is 111 / 'h'

            PrivateObject p = new PrivateObject(target);

            char expected = 'h';
            char actual;


            // EXERCISING THE CODE  
            PointerNode_<int, char> topic = (PointerNode_<int, char>)p.Invoke("FindLeastRecursor", current);
            actual = topic.Element;
            

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void FindLeastRecursor_INode_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            // 
            // extended tree, minus node 'h'
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');
            target.AddEntry(142, 'i');

            PointerNode_<int, char> current = target.Root;
            current = current.RightChild;   // 150 / 'c' ; least child is 125 / 'f' despite its R child

            PrivateObject p = new PrivateObject(target);

            char expected = 'f';
            char actual;


            // EXERCISING THE CODE  
            PointerNode_<int, char> topic = (PointerNode_<int, char>)p.Invoke("FindLeastRecursor", current);
            actual = topic.Element;


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PolyMethod_AddEntry_DropEntry_Test_Assert_Expected_Elements_Counts_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');
            target.AddEntry(111, 'h');
            target.AddEntry(142, 'i');

            char expectedElementAfterFirstDrop = 'b';
            char expectedElementAfterSecondDrop = 'f';
            char actualElementAfterFirstDrop;
            char actualElementAfterSecondDrop;

            int expectedCountAfterFirstDrop = 8;
            int expectedCountAfterSecondDrop = 7;
            int actualCountAfterFirstDrop;
            int actualCountAfterSecondDrop;

            string expectedTraversal = "deahicg";
            string actualTraversal;


            // EXERCISING THE CODE  
            // 
            // removing node 'b' and node 'f', leaving this tree:  
            /*                                                      
            *                    a/100                              
            *                      |                                
            *         ---------------------------                   
            *        e/75                     c/150                 
            *         |                         |                   
            *    ------                   ------------              
            *  d/25                   i/142        g/175            
            *                            |                          
            *                        -----                          
            *                     h/111                             
            *                                                       
            */

            actualElementAfterFirstDrop = target.DropEntry(50);   // 'b'
            actualCountAfterFirstDrop = target.Count;

            actualElementAfterSecondDrop = target.DropEntry(125);   // 'f'
            actualCountAfterSecondDrop = target.Count;

            actualTraversal = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expectedElementAfterFirstDrop, actualElementAfterFirstDrop);
            Assert.AreEqual(expectedCountAfterFirstDrop, actualCountAfterFirstDrop);
            Assert.AreEqual(expectedElementAfterSecondDrop, actualElementAfterSecondDrop);
            Assert.AreEqual(expectedCountAfterSecondDrop, actualCountAfterSecondDrop);
            Assert.AreEqual(expectedTraversal, actualTraversal);
        }

        [TestMethod()]
        public void DropAnEntry_Test_Assert_Expected_Elements_Counts_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');
            target.AddEntry(111, 'h');
            target.AddEntry(142, 'i');

            char expectedElementAfterFirstDrop = 'a';
            char expectedElementAfterSecondDrop = 'h';
            char actualElementAfterFirstDrop;
            char actualElementAfterSecondDrop;

            int expectedCountAfterFirstDrop = 8;
            int expectedCountAfterSecondDrop = 7;
            int actualCountAfterFirstDrop;
            int actualCountAfterSecondDrop;

            int expectedCountAfterFourDrops = 5;
            int actualCountAfterFourDrops;

            char expectedRootElementAfterFourDrops = 'c';
            char actualRootElementAfterFourDrops;

            string expectedTraversal = "dbecg";
            string actualTraversal;


            // EXERCISING THE CODE  
            // 
            // removing root each time, with 4 removals:
            //     1st should swap `h for `a;
            //     2nd should swap `f for `h;
            //     3rd should swap `i for `f;
            //     4th should swap `c for `i;
            // leaving this tree:
            /*                                                  
             *                    c/150                         
             *                      |                           
             *         ---------------------------              
             *        b/50                     g/175            
             *         |                                        
             *    -----------                                   
             *  d/25       e/75                                 
             *                                                  
            */


            actualElementAfterFirstDrop = target.DropAnEntry();
            actualCountAfterFirstDrop = target.Count;

            actualElementAfterSecondDrop = target.DropAnEntry();
            actualCountAfterSecondDrop = target.Count;

            target.DropAnEntry();
            target.DropAnEntry();

            actualCountAfterFourDrops = target.Count;

            actualRootElementAfterFourDrops = target.Root.Element;

            actualTraversal = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expectedElementAfterFirstDrop, actualElementAfterFirstDrop);
            Assert.AreEqual(expectedCountAfterFirstDrop, actualCountAfterFirstDrop);
            Assert.AreEqual(expectedElementAfterSecondDrop, actualElementAfterSecondDrop);
            Assert.AreEqual(expectedCountAfterSecondDrop, actualCountAfterSecondDrop);
            Assert.AreEqual(expectedCountAfterFourDrops, actualCountAfterFourDrops);
            Assert.AreEqual(expectedRootElementAfterFourDrops, actualRootElementAfterFourDrops);
            Assert.AreEqual(expectedTraversal, actualTraversal);
        }

        [TestMethod()]
        public void SetEntry_Test_Assert_Expected_Traversal_Results()   // working  
        {
            // GROUNDWORK  
            // 
            // short tree
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            // 50 > 'x'
            // 125 > 'n'

            string expectedTraversal = "dxeancg";     // originally "dbeafcg"
            string actualTraversal;

            // EXERCISING THE CODE  
            target.SetEntry(50, 'x');
            target.SetEntry(125, 'n');


            // TESTING  
            actualTraversal = (string)target.TraverseInorder(GatherElements);
            Assert.AreEqual(expectedTraversal, actualTraversal);
        }

        #endregion Basic Tests


        #region Traversals

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            BinarySearchTree_<int, char> target = new BinarySearchTree_<int, char>(100, 'a');
            target.AddEntry(50, 'b');
            target.AddEntry(150, 'c');
            target.AddEntry(25, 'd');
            target.AddEntry(75, 'e');
            target.AddEntry(125, 'f');
            target.AddEntry(175, 'g');

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }
}
