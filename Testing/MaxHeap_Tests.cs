﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class MaxHeap_T_Tests
    {
        #region Friendlies

        public static char[] PreHeap()  /* passed */  {
            char[] array = new char[11];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'h';
            array[8] = 'i';
            array[9] = 'j';
            array[10] = 'k';

            return array;
        }

        public static char[] PreHeapFewer()  /* passed */  {
            char[] array = new char[10];

            array[0] = 'a';
            array[1] = 'b';
            array[2] = 'c';
            array[3] = 'd';
            array[4] = 'e';
            array[5] = 'f';
            array[6] = 'g';
            array[7] = 'h';
            array[8] = 'i';
            array[9] = 'j';

            return array;
        }

        public object GatherContents(object topic_object, char content) {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(content);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void PreHeap_Test_()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> preHeap = new MaxHeap_<char>(PreHeap(), 16);

            string expected = "hidjbeakfgc";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void PreHeapFewer_Test_()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> preHeap = new MaxHeap_<char>(PreHeapFewer(), 16);

            string expected = "ahdibejfgc";
            string actual;


            //**  exercising the code  **//
            actual = (string)preHeap.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        /*  test heap before any changes:                                                              
                                                              @0: k                                    
                                                                |                                      
                                         ----------------------------------------------                
                                         |                                            |                
                                      @1: j                                        @2: g               
                                         |                                            |                
                           --------------------------------                ------------------------    
                           |                              |                |                      |    
                        @3: i                         @4: e             @5: f                  @6: c   
                           |                              |                                            
                  -------------------            -------------------                                   
                  |                 |            |                 |                                   
               @7: h             @8: d        @9: b             @10: a                                 */

        /*  inorder traversal of test heap:
         *      hidjbeakfgc 
        */


        /*  test heap / fewer before any changes:                                              
                                                      @0: j                                    
                                                        |                                      
                                 ----------------------------------------------                
                                 |                                            |                
                              @1: i                                        @2: g               
                                 |                                            |                
                   --------------------------------                ------------------------    
                   |                              |                |                      |    
                @3: h                         @4: e             @5: f                  @6: c   
                   |                              |                                            
          -------------------            ----------                                            
          |                 |            |                                                     
       @7: a             @8: d        @9: b                                                    */

        /*  inorder traversal of test heap / fewer: 
         *  ahdibejfgc  */


        // testing doesn't address problem of missing L or R children when using .RemoveHeapNode(); 
        // I'm skipping this because it isn't conceptually crucial 


        #region Heap Tests

        [TestMethod()]
        public void SwapDown_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap before any changes:                                                              
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h             @8: d        @9: b             @10: a                                 */

            /*  test heap during any changes:                                                                
                                                                  @0: k                                      
                                                                    |                                        
                                             ------------------------------------------------                
                                             |                                              |                
                                          @1: j >@ >>i                                   @2: g               
                                             |                                              |                
                               ----------------------------------                ------------------------    
                               |                                |                |                      |    
                            @3: i >>@ >>>h                  @4: e             @5: f                  @6: c   
                               |                                |                                            
                      -------------------              -------------------                                   
                      |                 |              |                 |                                   
                   @7: h >>>@        @8: d          @9: b             @10: a                                 */

            /*  test heap after any changes:                                                               
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: i                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: h                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: @             @8: d        @9: b             @10: a                                 */


            /* @hdibeakfgc */

            #endregion Expecteds


            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( j ) with testing contents ( @ ) ( @ is less than all lowercase letters, codewise ); 
            // should swap down so that index 1 is e and index 10 is x 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeContent = '@';

            string expected = "@hdibeakfgc";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 1);


            //**  testing  **//
            // using results of inorder traversal 
            actual = (string)target.TraverseInorder(GatherContents);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapDown_Test_Addressing_Missing_Children_Assert_Expected_Traversal_Results()  /* working */  {
            /*  this test focuses on whether algo works when sibling of swap-subject is missing; 
             *  swapping limited to smaller part of table to bypass tendency to move in other direction  */

            #region Expecteds

            /*  test heap / fewer before any changes:                                                  
                                                              @0: j                                    
                                                                |                                      
                                         ----------------------------------------------                
                                         |                                            |                
                                      @1: i                                        @2: g               
                                         |                                            |                
                           --------------------------------                ------------------------    
                           |                              |                |                      |    
                        @3: h                         @4: e             @5: f                  @6: c   
                           |                              |                                            
                  -------------------            ----------                                            
                  |                 |            |                                                     
               @7: a             @8: d        @9: b                                                    */

            /*  test heap / fewer during any changes:                                                  
                                                              @0: j                                    
                                                                |                                      
                                         ----------------------------------------------                
                                         |                                            |                
                                      @1: i                                        @2: g               
                                         |                                            |                
                           --------------------------------                ------------------------    
                           |                              |                |                      |    
                        @3: h                         @4: e >@ >>b      @5: f                  @6: c   
                           |                              |                                            
                  -------------------            ----------                                            
                  |                 |            |                                                     
               @7: a             @8: d        @9: b >>@                                                */

            /*  test heap / fewer after any changes:                                                   
                                                              @0: j                                    
                                                                |                                      
                                         ----------------------------------------------                
                                         |                                            |                
                                      @1: i                                        @2: g               
                                         |                                            |                
                           --------------------------------                ------------------------    
                           |                              |                |                      |    
                        @3: h                         @4: b             @5: f                  @6: c   
                           |                              |                                            
                  -------------------            ----------                                            
                  |                 |            |                                                     
               @7: a             @8: d        @9: @                                                    */

            /* ahdi@bjfgc */

            #endregion Expecteds


            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeapFewer(), 16);   // 10 items, up to index 9 
            PrivateObject p = new PrivateObject(target);

            // replacing contents of node at index 4 ( e ) with testing contents ( @ ); 
            // should swap down so that index 4 is b and index 9 is @ 
            target.MoveToRoot();         // 'j' 
            target.MoveToLeftChild();    // 'i' 
            target.MoveToRightChild();   // 'e' 


            target.NodeContent = '@';

            string expected = "ahdi@bjfgc";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapDown", 4);


            //**  testing  **//
            // using results of inorder traversal 
            actual = (string)target.TraverseInorder(GatherContents);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapUp_Test_Assert_Expected_Traversal_Results()  /* working */  {
            #region Expecteds

            /*  test heap before any changes:                                                              
                                                              @0: k                                    
                                                                |                                      
                                         ----------------------------------------------                
                                         |                                            |                
                                      @1: j                                        @2: g               
                                         |                                            |                
                           --------------------------------                ------------------------    
                           |                              |                |                      |    
                        @3: i                         @4: e             @5: f                  @6: c   
                           |                              |                                            
                  -------------------            -------------------                                   
                  |                 |            |                 |                                   
               @7: h             @8: d        @9: b             @10: a                                 */

            /*  test heap during changes:                                                                  
                                                                  @0: k >>>>x                              
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j >>>x >>>>k                             @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i >>x >>>j                @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h >x >>i      @8: d        @9: b             @10: a                                 */


            /*  test heap after any changes:                                                               
                                                                  @0: x                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: k                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: j                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: i             @8: d        @9: b             @10: a                                 */


            /* ijdkbeaxfgc */

            #endregion Expecteds


            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 7 (h) with testing contents (x); 
            // should swap up so that index 0 is x and index 7 is i 
            target.MoveToRoot();
            target.MoveToLeftChild();   // j 
            target.MoveToLeftChild();   // i 
            target.MoveToLeftChild();   // h 
            target.NodeContent = 'x';

            string expected = "ijdkbeaxfgc";
            string actual;


            //**  exercising the code  **//
            p.Invoke("SwapUp", 7);


            //**  testing  **//
            // using results of inorder traversal 
            actual = (string)target.TraverseInorder(GatherContents);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OrderHeap_Test_Assert_Expected_Relationships()  /* working */  {
            /* the asserts test to see if each node is larger than either of its child nodes */

            //**  groundwork  **//

            // disordered array to make into binary-tree heap 
            char[] array = new char[11];

            array[0] = 'g';
            array[1] = 'b';
            array[2] = 'e';
            array[3] = 'j';
            array[4] = 'h';
            array[5] = 'c';
            array[6] = 'd';
            array[7] = 'i';
            array[8] = 'a';
            array[9] = 'k';
            array[10] = 'f';

            MaxHeap_<char> target = new MaxHeap_<char>(array, 16);

            char parentContent = '\0';
            char leftChildContent = '\0';
            char rightChildContent = '\0';

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            target.OrderHeap();


            //**  testing  **//
            // root and its 2 children: indices 0, 1, and 2 
            target.MoveToRoot();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4 
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6 
            target.MoveToRoot();
            target.MoveToRightChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test_Assert_Expected_Relationships()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);

            char parentContent = '\0';
            char leftChildContent = '\0';
            char rightChildContent = '\0';

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            target.AddHeapNode('x');


            //**  testing  **//
            // root and its 2 children: indices 0, 1, and 2 
            target.MoveToRoot();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4 
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6 
            target.MoveToRoot();
            target.MoveToRightChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;
            target.MoveToRightSibling();
            rightChildContent = target.NodeContent;

            actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
            Assert.AreEqual(expected, actual);

            // L child of R child of root and its L child: indices 5 and 11; no R child 
            target.MoveToRoot();
            target.MoveToRightChild();
            target.MoveToLeftChild();
            parentContent = target.NodeContent;
            target.MoveToLeftChild();
            leftChildContent = target.NodeContent;

            actual = (parentContent > leftChildContent);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveHeapNode_Test_Assert_Expected_Return_Value_And_Heap_Relationships()  /* working */  {
            #region Expecteds

            /*  test heap before any changes:                                                              
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h             @8: d        @9: b             @10: a                                 */

            /*  test heap during any changes:                                                              
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j >a >>i                                 @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i >>a >>>h                @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h >>>a         @8: d        @9: b             @10: a >empty                          */

            /*  test heap after any changes:                                                               
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: i                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: h                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            ----------                                            
                      |                 |            |                                                     
                   @7: a             @8: d        @9: b                                                    */

            #endregion Expecteds


            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);

            char parentContent = '\0';
            char leftChildContent = '\0';
            char rightChildContent = '\0';

            bool expected = true;
            bool actual;

            char expectedContent = 'j';
            char actualContent;


            //**  exercising the code  **//
            actualContent = target.RemoveHeapNode(1);   // removing 'j' and beginning a series of reheaping ops 


            //**  testing  **//
            // return value 
            {
                Assert.AreEqual(expectedContent, actualContent);
            }

            // this is the key test, the heap relationships, parent always greater than both children; 
            // technically, the specific values don't matter, although I also check those to see if my 
            // mentally calculated final heap is correct 
            {
                // root and its 2 children: indices 0, 1, and 2 
                target.MoveToRoot();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                {
                    Assert.AreEqual(parentContent, 'k');
                    Assert.AreEqual(leftChildContent, 'i');
                    Assert.AreEqual(rightChildContent, 'g');
                }

                // L child of root and its 2 children: indices 1, 3, and 4 
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                {
                    Assert.AreEqual(parentContent, 'i');
                    Assert.AreEqual(leftChildContent, 'h');
                    Assert.AreEqual(rightChildContent, 'e');
                }

                // R child of root and its 2 children: indices 2, 5, and 6 
                target.MoveToRoot();
                target.MoveToRightChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                {
                    Assert.AreEqual(parentContent, 'g');
                    Assert.AreEqual(leftChildContent, 'f');
                    Assert.AreEqual(rightChildContent, 'c');
                }

                // L child of L child of root and its 2 children: indices 3, 7, and 8 
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                {
                    Assert.AreEqual(parentContent, 'h');
                    Assert.AreEqual(leftChildContent, 'a');
                    Assert.AreEqual(rightChildContent, 'd');
                }

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child 
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                {
                    Assert.AreEqual(parentContent, 'e');
                    Assert.AreEqual(leftChildContent, 'b');
                }
            }
        }

        [TestMethod()]
        public void PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()  /* working */  {
            #region Expecteds

            /*  test heap before any changes:                                                              
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h             @8: d        @9: b             @10: a                                 */

            #endregion Expecteds


            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);

            char parentContent = '\0';
            char leftChildContent = '\0';
            char rightChildContent = '\0';

            bool expected = true;
            bool actual;

            char expectedContent = 'k';
            char actualContent;


            //**  exercising the code  **//
            actualContent = target.PopHeapRoot();   // removing 'k' 


            //**  testing  **//
            // return value 
            {
                Assert.AreEqual(expectedContent, actualContent);
            }

            // heap relationships; not testing exact values at each node, though those can be calculated and checked 
            {
                // root and its 2 children: indices 0, 1, and 2 
                target.MoveToRoot();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4 
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6 
                target.MoveToRoot();
                target.MoveToRightChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8 
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child 
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;

                actual = (parentContent > leftChildContent);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PolyMethod_RemoveHeapNode_PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()  /* working */  {
            #region Expecteds

            /*  test heap before any changes:                                                              
                                                                  @0: k                                    
                                                                    |                                      
                                             ----------------------------------------------                
                                             |                                            |                
                                          @1: j                                        @2: g               
                                             |                                            |                
                               --------------------------------                ------------------------    
                               |                              |                |                      |    
                            @3: i                         @4: e             @5: f                  @6: c   
                               |                              |                                            
                      -------------------            -------------------                                   
                      |                 |            |                 |                                   
                   @7: h             @8: d        @9: b             @10: a                                 */

            #endregion Expecteds

            
            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>(PreHeap(), 16);

            char parentContent = '\0';
            char leftChildContent = '\0';
            char rightChildContent = '\0';

            bool expected = true;
            bool actual;

            char expectedContent = 'j';
            char actualContent;


            //**  exercising the code  **//
            actualContent = target.RemoveHeapNode(1);   // removing 'j' 
            target.PopHeapRoot();
            target.PopHeapRoot();


            //**  testing  **//
            // return value 
            {
                Assert.AreEqual(expectedContent, actualContent);
            }

            // heap relationships 
            {
                // root and its 2 children: indices 0, 1, and 2 
                target.MoveToRoot();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4 
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6 
                target.MoveToRoot();
                target.MoveToRightChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;
                target.MoveToRightSibling();
                rightChildContent = target.NodeContent;

                actual = ((parentContent.CompareTo(leftChildContent) > 0) && (parentContent.CompareTo(rightChildContent) > 0));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8 
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentContent = target.NodeContent;
                target.MoveToLeftChild();
                leftChildContent = target.NodeContent;

                actual = (parentContent > leftChildContent);
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion Heap Tests


        #region Tree Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;


            //**  exercising the code  **//
            // 
            // a complete tree, so elements are added left to right at each level before next level
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  testing  **//
            // root, an inode
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L sibling of R child of root ( == L child of root ), an inode
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // parent of previous node ( L child of root ), an inode
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // root, an inode
            expectedIsLeaf = false;
            expectedContent = 'a';

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedContent;
            char actualContent;


            //**  exercising the code  **//
            // 
            // a complete tree, so elements are added left to right at each level before next level 
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');


            //**  exercising the code  **//
            // after two nodes are removed, nodes c, d, and e should be leaves 
            target.RemoveTopNode();
            target.RemoveTopNode();


            //**  testing  **//
            // root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'a';

            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of root, now a leaf 
            expectedIsLeaf = true;
            expectedContent = 'c';

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of root, an inode 
            expectedIsLeaf = false;
            expectedContent = 'b';

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // L child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'd';

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);

            // R child of L child of root, a leaf 
            expectedIsLeaf = true;
            expectedContent = 'e';

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualContent = target.NodeContent;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedContent, actualContent);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds()  /* working */  {
            //**  groundwork  **//
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level 
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');

            // last (leaf) node isn't added, so a complete, but not full, binary tree; 
            // now, node c should have only a L child 

            // target.AddNode('g'); 


            //**  exercising the code  **//
            // each method is in a try-catch block to test for expected exception messages 
            try {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child 
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX 
                target.MoveToRightChild();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);



            //**  exercising the code  **//
            try {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child 
                target.MoveToRoot();

                // CRUX 
                target.MoveToParent();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);



            //**  exercising the code  **//
            try {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling 
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX 
                target.MoveToLeftSibling();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);



            //**  exercising the code  **//
            try {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling 
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX 
                target.MoveToRightSibling();
                // END CRUX 
            }
            catch (Exception x) {
                actual = x.Message;
            }

            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Tree Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "abdecfg";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraversePreorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "dbeafcg";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraverseInorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  /* working */  {
            //**  groundwork  **//
            MaxHeap_<char> target = new MaxHeap_<char>('a');
            target.AddNode('b');
            target.AddNode('c');
            target.AddNode('d');
            target.AddNode('e');
            target.AddNode('f');
            target.AddNode('g');

            string expected = "debfgca";
            string actual;


            //**  exercising the code  **//
            actual = (string)target.TraversePostorder(GatherContents);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals
    }


    [TestClass]
    public class MaxHeap_K_T_Tests
    {
        #region Friendlies

        public static Tuple<int, char>[] PreHeap()   // verified  
        {
            Tuple<int, char>[] array = new Tuple<int, char>[11];

            array[0] = Tuple.Create(500, 'a');
            array[1] = Tuple.Create(400, 'b');
            array[2] = Tuple.Create(450, 'c');
            array[3] = Tuple.Create(350, 'd');
            array[4] = Tuple.Create(375, 'e');
            array[5] = Tuple.Create(425, 'f');
            array[6] = Tuple.Create(325, 'g');
            array[7] = Tuple.Create(200, 'h');
            array[8] = Tuple.Create(275, 'i');
            array[9] = Tuple.Create(225, 'j');
            array[10] = Tuple.Create(300, 'k');

            return array;
        }

        public static Tuple<int, char>[] PreHeapFewer()   // ok  
        {
            Tuple<int, char>[] array = new Tuple<int, char>[10];

            array[0] = Tuple.Create(500, 'a');
            array[1] = Tuple.Create(400, 'b');
            array[2] = Tuple.Create(450, 'c');
            array[3] = Tuple.Create(350, 'd');
            array[4] = Tuple.Create(375, 'e');
            array[5] = Tuple.Create(425, 'f');
            array[6] = Tuple.Create(325, 'g');
            array[7] = Tuple.Create(200, 'h');
            array[8] = Tuple.Create(275, 'i');
            array[9] = Tuple.Create(225, 'j');

            return array;
        }

        public object GatherElements(object topic_object, int key, char element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(element);

            return topic;
        }

        public object GatherKeys(object topic_object, char key, int element)   // verified  
        {
            string topic = topic_object as string;

            // first invocation only
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(key);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Tests

        [TestMethod()]
        public void PreHeap_Test_()   // working  
        {
            // GROUNDWORK  
            MaxHeap_<int, char> preHeap = new MaxHeap_<int, char>(PreHeap(), 16);

            string expected = "hdibjekafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)preHeap.TraverseInorder(GatherElements);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Tests


        /*  test heap:                                                                                                                                          
         *                                                                                                                                                      
         *                                                                            @0: 500/a                                                                 
         *                                                                               |                                                                      
         *                                               ------------------------------------------------------------------                                     
         *                                               |                                                                |                                     
         *                                            @1: 400/b                                                        @2: 450/c                                
         *                                               |                                                                |                                     
         *                       ---------------------------------------------                         -----------------------------------------                
         *                       |                                           |                         |                                       |                
         *                    @3: 350/d                                   @4: 375/e                 @5: 425/f                               @6: 325/g           
         *                       |                                           |                                                                                  
         *        ------------------------------                 ---------------------------                                                                    
         *        |                            |                 |                         |                                                                    
         *     @7: 200/h                    @8: 275/i         @9: 225/j                @10: 300/k                                                               
         *                                                                                                                                                      
        */


        /*  inorder traversal of test heap:
         *      hdibjekafcg
        */


        // testing doesn't address problem of missing L or R children when using .RemoveHeapNode();
        // I'm skipping this because it isn't conceptually crucial


        #region Heap Tests

        [TestMethod()]
        public void SwapDown_Test_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  expected heap:                                                                                                                                     
            *                                                                                                                                                      
            *                                                                            @0: 500/a                                                                 
            *                                                                               |                                                                      
            *                                               ------------------------------------------------------------------                                     
            *                                               |                                                                |                                     
            *                                            @1: 375/e                                                        @2: 450/c                                
            *                                               |                                                                |                                     
            *                       --------------------------------------------                          ----------------------------------------                 
            *                       |                                           |                         |                                       |                
            *                    @3: 350/d                                   @4: 300/k                 @5: 425/f                               @6: 325/g           
            *                       |                                           |                                                                                  
            *        ---------------------------                      --------------------------                                                                   
            *        |                         |                     |                         |                                                                   
            *     @7: 200/h                 @8: 275/i             @9: 225/j                @10: 100/x                                                              
            *                                                                                                                                                      
            */

            /*  inorder traversal of expected heap: 
             *      hdiejkxafcg 
            */

            #endregion Expecteds


            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test; 
            // replacing contents of node at index 1 ( 400/b ) with testing contents ( 100/x ); 
            // should swap down so that index 1 is 375/e and index 10 is 100/x 
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeKey = 100;
            target.NodeElement = 'x';

            string expected = "hdiejkxafcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapDown", 1);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapDown_Test_Addressing_Missing_Children_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  test heap:                                                                                                                                          
             *                                                                                                                                                      
             *                                                                            @0: 500/a                                                                 
             *                                                                               |                                                                      
             *                                               ------------------------------------------------------------------                                     
             *                                               |                                                                |                                     
             *                                            @1: 400/b                                                        @2: 450/c                                
             *                                               |                                                                |                                     
             *                       ---------------------------------------------                         -----------------------------------------                
             *                       |                                           |                         |                                       |                
             *                    @3: 350/d                                   @4: 375/e                 @5: 425/f                               @6: 325/g           
             *                       |                                           |                                                                                  
             *        ------------------------------                 -------------                                                                                  
             *        |                            |                 |                                                                                              
             *     @7: 200/h                    @8: 275/i         @9: 225/j                                                                                         
             *                                                                                                                                                      
            */

            /*  inorder traversal of test heap:
             *      hdibjeafcg
            */

            /*
             * node 1 (400/b) is replaced with a value that will sort to node 9 (225/j);
             * new node values are 100/x;
             *      swaps first with 4 (375/e), which ends up at node 1;
             *      swaps next with 9 (225/j), which ends up at node 4;
             *      final node 9 is 100/x;
             * 
             * expected fail when not yet fixed is on second swap, when there is no R child
             */

            /*  expected heap:                                                                                                                                      
             *                                                                                                                                                      
             *                                                                            @0: 500/a                                                                 
             *                                                                               |                                                                      
             *                                               ------------------------------------------------------------------                                     
             *                                               |                                                                |                                     
             *                                            @1: 375/e                                                        @2: 450/c                                
             *                                               |                                                                |                                     
             *                       ---------------------------------------------                         -----------------------------------------                
             *                       |                                           |                         |                                       |                
             *                    @3: 350/d                                   @4: 225/j                 @5: 425/f                               @6: 325/g           
             *                       |                                           |                                                                                  
             *        ------------------------------                 -------------                                                                                  
             *        |                            |                 |                                                                                              
             *     @7: 200/h                    @8: 275/i         @9: 100/x                                                                                         
             *                                                                                                                                                      
            */

            /*  inorder traversal of expected heap:
             *      hdiexjafcg
            */

            #endregion Expecteds


            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeapFewer(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test;
            // replacing contents of node at index 1 ( 400/b ) with testing contents ( 100/x );
            // should swap down so that index 1 is 375/e and index 9 is 100/x
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.NodeKey = 100;
            target.NodeElement = 'x';

            string expected = "hdiexjafcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapDown", 1);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SwapUp_Test_Assert_Expected_Traversal_Results()   // working  
        {
            #region Expecteds

            /*  expected heap:                                                                                                                                      
             *                                                                                                                                                      
             *                                                                            @0: 650/x                                                                
             *                                                                               |                                                                      
             *                                               ------------------------------------------------------------------                                     
             *                                               |                                                                |                                     
             *                                            @1: 500/a                                                        @2: 450/c                                
             *                                               |                                                                |                                     
             *                       --------------------------------------------                          ----------------------------------------                 
             *                       |                                           |                         |                                       |                
             *                    @3: 400/b                                   @4: 375/e                 @5: 425/f                               @6: 325/g           
             *                       |                                           |                                                                                  
             *        ------------------------------                 ---------------------------                                                                    
             *        |                            |                 |                         |                                                                    
             *     @7: 350/d                    @8: 275/i         @9: 225/j                @10: 300/k                                                               
             *                                                                                                                                                      
            */

            /*  inorder traversal of expected heap:
             *      dbiajekxfcg
            */

            #endregion Expecteds


            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);
            PrivateObject p = new PrivateObject(target);

            // using tree methods to change heap contents for swapping test;
            // replacing contents of node at index 7 ( 200/h ) with testing contents ( 650/x );
            // should swap up so that index 0 is 650/x and index 7 is 350/d
            target.MoveToRoot();
            target.MoveToLeftChild();   // b
            target.MoveToLeftChild();   // d
            target.MoveToLeftChild();   // h
            target.NodeKey = 650;
            target.NodeElement = 'x';

            string expected = "dbiajekxfcg";
            string actual;


            // EXERCISING THE CODE  
            p.Invoke("SwapUp", 7);


            // TESTING  

            // do inorder traversal
            actual = (string)target.TraverseInorder(GatherElements);

            // test
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OrderHeap_Test_Assert_Expected_Relationships()   // working  
        {
            // the testing checks to see if each node is larger than either of its child nodes;
            // not checking using a traversal because I can't easily predict what it should be;
            //     it could be different for the same values if given in a different random order

            // GROUNDWORK  

            // disordered array to make into binary-tree heap
            Tuple<int, char>[] array = new Tuple<int, char>[11];

            array[0] = Tuple.Create(325, 'g');
            array[1] = Tuple.Create(400, 'b');
            array[2] = Tuple.Create(375, 'e');
            array[3] = Tuple.Create(225, 'j');
            array[4] = Tuple.Create(200, 'h');
            array[5] = Tuple.Create(450, 'c');
            array[6] = Tuple.Create(350, 'd');
            array[7] = Tuple.Create(275, 'i');
            array[8] = Tuple.Create(500, 'a');
            array[9] = Tuple.Create(300, 'k');
            array[10] = Tuple.Create(425, 'f');

            MaxHeap_<int, char> target = new MaxHeap_<int, char>(array, 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual = false;


            // EXERCISING THE CODE  
            target.OrderHeap();


            // TESTING  

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddHeapNode_Test_Assert_Expected_Relationships()   // working  
        {
            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;


            // EXERCISING THE CODE  
            target.AddHeapNode(475, 'x');


            // TESTING  

            // root and its 2 children: indices 0, 1, and 2
            target.MoveToRoot();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of root and its 2 children: indices 1, 3, and 4
            target.MoveToRoot();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of root and its 2 children: indices 2, 5, and 6
            target.MoveToRoot();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of L child of root and its 2 children: indices 3, 7, and 8
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // R child of L child of root and its 2 children: indices 4, 9, and 10
            target.MoveToRoot();
            target.MoveToLeftChild();
            target.MoveToRightChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;
            target.MoveToRightSibling();
            rightChildKey = target.NodeKey;

            actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
            Assert.AreEqual(expected, actual);

            // L child of R child of root and its L child: indices 5 and 11; no R child
            target.MoveToRoot();
            target.MoveToRightChild();
            target.MoveToLeftChild();
            parentKey = target.NodeKey;
            target.MoveToLeftChild();
            leftChildKey = target.NodeKey;

            actual = (parentKey > leftChildKey);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveHeapNode_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.RemoveHeapNode(1);   // removing 400/b


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'a';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.PopHeapRoot();   // removing 500/a


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of L child of root and its 1, L child: indices 4 and 9; no R child
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = (parentKey > leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void PolyMethod_RemoveHeapNode_PopHeapRoot_Test_Assert_Expected_Return_Value_And_Heap_Relationships()   // working  
        {
            // GROUNDWORK  
            MaxHeap_<int, char> target = new MaxHeap_<int, char>(PreHeap(), 16);

            int parentKey = -1;
            int leftChildKey = -1;
            int rightChildKey = -1;

            bool expected = true;
            bool actual;

            char expectedElement = 'b';
            char actualElement;


            // EXERCISING THE CODE  
            actualElement = target.RemoveHeapNode(1);   // removing 400/b
            target.PopHeapRoot();
            target.PopHeapRoot();


            // TESTING  

            // return value
            {
                Assert.AreEqual(expectedElement, actualElement);
            }

            // heap relationships
            {
                // root and its 2 children: indices 0, 1, and 2
                target.MoveToRoot();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of root and its 2 children: indices 1, 3, and 4
                target.MoveToRoot();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // R child of root and its 2 children: indices 2, 5, and 6
                target.MoveToRoot();
                target.MoveToRightChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;
                target.MoveToRightSibling();
                rightChildKey = target.NodeKey;

                actual = ((parentKey > leftChildKey) && (parentKey > rightChildKey));
                Assert.AreEqual(expected, actual);

                // L child of L child of root and its 2 children: indices 3, 7, and 8
                target.MoveToRoot();
                target.MoveToLeftChild();
                target.MoveToLeftChild();
                parentKey = target.NodeKey;
                target.MoveToLeftChild();
                leftChildKey = target.NodeKey;

                actual = (parentKey > leftChildKey);
                Assert.AreEqual(expected, actual);
            }
        }

        #endregion Heap Tests


        #region Tree Tests

        [TestMethod()]
        public void PolyMethod_AddNode_MoveTos_NodeProperties_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // TESTING  

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L sibling of R child of root ( == L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R sibling of L child of L child of root ( R child of L child of root ), a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // parent of previous node ( L child of root ), an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToParent();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            target.MoveToRoot();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void RemoveTopNode_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            bool expectedIsLeaf;
            bool actualIsLeaf;

            char expectedKey;
            char actualKey;

            int expectedElement;
            int actualElement;


            // EXERCISING THE CODE  
            // 
            // a complete tree, so elements are added left to right at each level before next level
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);


            // EXERCISING THE CODE  
            // after two nodes are removed, nodes c, d, and e should be leaves
            target.RemoveTopNode();
            target.RemoveTopNode();


            // TESTING  
            // root, an inode
            expectedIsLeaf = false;
            expectedKey = 'a';
            expectedElement = 100;

            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of root, now a leaf
            expectedIsLeaf = true;
            expectedKey = 'c';
            expectedElement = 150;

            target.MoveToRightChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of root, an inode
            expectedIsLeaf = false;
            expectedKey = 'b';
            expectedElement = 50;

            target.MoveToLeftSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // L child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'd';
            expectedElement = 25;

            target.MoveToLeftChild();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);

            // R child of L child of root, a leaf
            expectedIsLeaf = true;
            expectedKey = 'e';
            expectedElement = 75;

            target.MoveToRightSibling();
            actualIsLeaf = target.NodeIsLeaf;
            actualKey = target.NodeKey;
            actualElement = target.NodeElement;

            Assert.AreEqual(expectedIsLeaf, actualIsLeaf);
            Assert.AreEqual(expectedKey, actualKey);
            Assert.AreEqual(expectedElement, actualElement);
        }

        [TestMethod()]
        public void PolyMethod_Exceptions_Test_Assert_Expecteds()  // working  
        {
            // GROUNDWORK  
            string expected = null;
            string actual = null;
            int index;

            // a complete tree, so elements are added left to right at each level before next level
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);

            // last (leaf) node isn't added, so a complete, but not full, binary tree;
            // now, node c should have only a L child

            // target.AddNode('g', 175);


            // each method is in a try-catch block to test for expected exception messages


            // EXERCISING THE CODE  
            try {
                index = 2;  // c   
                expected = "Node at index " + index.ToString() + " does not have a right child";

                // at R child of root; does not have a R child
                target.MoveToRoot();
                target.MoveToRightChild();

                // CRUX
                target.MoveToRightChild();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 0;  // a   
                expected = "Node at index " + index.ToString() + " is the root, so it does not have a parent";

                // at R child of root; does not have a R child
                target.MoveToRoot();

                // CRUX
                target.MoveToParent();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 1;  // b   
                expected = "Node at index " + index.ToString() + " is not a right child, so it does not have a left sibling";

                // at L child of root; does not have a L sibling
                target.MoveToRoot();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToLeftSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);



            // EXERCISING THE CODE  
            try {
                index = 5;  // f   
                expected = "Node at index " + index.ToString() + " does not have a right sibling";

                // at L child of R child of root; does not have a R sibling
                target.MoveToRoot();
                target.MoveToRightChild();
                target.MoveToLeftChild();

                // CRUX
                target.MoveToRightSibling();
                // END CRUX
            }
            catch (Exception x) {
                actual = x.Message;
            }

            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Tree Tests


        #region Traversals

        [TestMethod()]
        public void TraversePreorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "abdecfg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePreorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraverseInorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "dbeafcg";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraverseInorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TraversePostorder_Test_Assert_Expected()  // working  
        {
            // GROUNDWORK  
            MaxHeap_<char, int> target = new MaxHeap_<char, int>('a', 100);
            target.AddNode('b', 50);
            target.AddNode('c', 150);
            target.AddNode('d', 25);
            target.AddNode('e', 75);
            target.AddNode('f', 125);
            target.AddNode('g', 175);

            string expected = "debfgca";
            string actual;


            // EXERCISING THE CODE  
            actual = (string)target.TraversePostorder(GatherKeys);


            // TESTING  
            Assert.AreEqual(expected, actual);
        }

        #endregion Traversals

    }
}
