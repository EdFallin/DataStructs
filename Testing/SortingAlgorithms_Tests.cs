﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructs;
using System.Threading;
using Advanceds;

namespace Testing
{
    [TestClass]
    public class SortingAlgorithms_Tests
    {
        #region AreOrdered

        [TestMethod]
        public void AreOrdered__Ints__Assert_True_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            PrivateType sorting = new PrivateType(typeof(SortingAlgorithms_<int>));

            int earlier = 5;
            int later = 10;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = (bool)sorting.InvokeStatic("AreOrdered", earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered__Ints__Assert_True_Given_Equal()  /* working */  {
            //**  groundwork  **//
            PrivateType sorting = new PrivateType(typeof(SortingAlgorithms_<int>));

            int earlier = 100;
            int later = 100;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = (bool)sorting.InvokeStatic("AreOrdered", earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered__Ints__Assert_False_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            PrivateType sorting = new PrivateType(typeof(SortingAlgorithms_<int>));

            int earlier = 100;
            int later = 50;

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = (bool)sorting.InvokeStatic("AreOrdered", earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered__Strings__Assert_True_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            PrivateType sorting = new PrivateType(typeof(SortingAlgorithms_<string>));

            string earlier = "abc";
            string later = "pqr";

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = (bool)sorting.InvokeStatic("AreOrdered", earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered__Strings__Assert_False_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            PrivateType sorting = new PrivateType(typeof(SortingAlgorithms_<string>));

            string earlier = "pqr";
            string later = "abc";

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = (bool)sorting.InvokeStatic("AreOrdered", earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AreOrdered


        #region SwapByIndex

        [TestMethod()]
        public void SwapByIndex__Assert_Swapped()  /* working */  {
            //**  groundwork  **//
            PrivateType target = new PrivateType(typeof(SortingAlgorithms_<string>));
            string[] subject = new string[] { "first", "second" };

            string[] expected = new string[] { "second", "first" };
            string[] actual;


            //**  exercising the code  **//
            actual = (string[])target.InvokeStatic("SwapByIndex", subject, 0, 1);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion SwapByIndex


        #region Exchange Sorts: InsertionSort and SelectionSort

        [TestMethod()]
        public void Sort_Insertion__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Insertion_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Selection__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Selection_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Exchange Sorts: InsertionSort and SelectionSort


        #region Extended Exhange Sorts: InsertionSort

        [TestMethod()]
        public void Sort_Insertion_Partial___Full_Array__Assert_Ordered()  /* working */  {
            //**  groundwork  **// 
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Insertion_Partial_(26, subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Insertion_Partial___Partial_Array__Assert_Ordered_In_Original_Range()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                /* following items should get sorted */ 
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", 
                /* these should stay in their original locs */ 
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p" };

            /* "a", "b", "c", "d", "e", "v", "w", "x", "y", "z" */

            string[] expected = new string[] {
                /* following items should be sorted */ 
                "a", "b", "c", "d", "e", "v", "w", "x", "y", "z",
                /* these should be in their original locs */ 
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Insertion_Partial_(10, subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Extended Exhange Sorts: InsertionSort


        #region MergeSort Varieties

        [TestMethod()]
        public void Sort_Merge__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Merge_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Merge_Insertion__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Merge_Insertion_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion MergeSort Varieties


        #region QuickSort Varieties

        [TestMethod()]
        public void Sort_Quick__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Quick_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Quick__Ten_Ints__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            int[] subject = new int[] { 5, 3, 8, 6, 4, 2, 1, 7, 9, 0 };

            int[] expected = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<int>.Sort_Quick_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Quick_Insertion__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Quick_Insertion_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Quick_Insertion__Ten_Ints__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            int[] subject = new int[] { 5, 3, 8, 6, 4, 2, 1, 7, 9, 0 };

            int[] expected = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<int>.Sort_Quick_Insertion_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Sort_Quick_Insertion__Thousand_Ints__Assert_Ordered()  /* working */  {
            //**  groundwork  **//

            // setting up random and large set of ints 
            int[] subject = new int[1000];

            // creating a random set of 1000 ints 
            for (int i = 0; i < 1000; i++) {
                // making a new Random instance at each loop is faster than a Thread.Sleep() for different .Next() results 
                Random r = new Random(i);
                subject[i] = r.Next(0, 100000);
            }

            //**  exercising the code  **//
            int[] actual = SortingAlgorithms_<int>.Sort_Quick_Insertion_(subject);


            //**  testing  **//

            // verifying that all values are in order, whatever they are; "in order" means left <= right 
            for (int i = 1; i < 1000; i++) {
                Assert.IsTrue(actual[i - 1] <= actual[i],
                    string.Format("Value {0} at index {1} is not less than value {2} at index {3}.", actual[i - 1], i - 1, actual[i], i)
                    );
            }
        }

        #endregion QuickSort Varieties


        #region HeapSort

        [TestMethod()]
        public void Sort_Heap__Assert_Ordered()  /* working */  {
            //**  groundwork  **//
            string[] subject = new string[] {
                "a", "z", "b", "y", "c", "x", "d", "w", "e", "v", "f", "u", "g",
                "t", "h", "s", "i", "r", "j", "q", "k", "p", "l", "o", "m", "n" };

            string[] expected = new string[] {
                "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string[] actual;


            //**  exercising the code  **//
            actual = SortingAlgorithms_<string>.Sort_Heap_(subject);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion HeapSort


        #region IndexSort

        [TestMethod()]
        public void Sort_Index__Assert_Ordered()  /* working */  {
            /* groundwork */
            int[] arg = Permutations.RandomZeroUpPermutation(50);

            int[] expected = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                                 11, 12, 13, 14, 15, 16, 17, 18, 19,
                                 20, 21, 22, 23, 24, 25, 26, 27, 28,
                                 29, 30, 31, 32, 33, 34, 35, 36, 37,
                                 38, 39, 40, 41, 42, 43, 44, 45, 46,
                                 47, 48, 49 };
            int[] actual;


            /* exercising the code */
            actual = SortingAlgorithms_<int>.Sort_Index_(arg);
            //actual = arg;   // inbound argument should be sorted because passed by reference 

            /* testing */
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion IndexSort


        #region RadixSort and its Dependencies

        [TestMethod()]
        public void Sort_Radix__Thousand_Ints__Assert_Ordered()  /* working */  {
            /* groundwork */

            // setting up random and large set of ints 
            int[] subject = new int[1000];

            // creating a random set of 1000 ints, all with the same number of digits 
            int seed = (new Random().Next());

            for (int i = 0; i < 1000; i++) {
                // making a new Random instance at each loop is faster than a Thread.Sleep() for different .Next() results 
                Random r = new Random(i * seed);

                subject[i] = r.Next(1000, 9999);
            }


            /* exercising the code */
            int[] actual = SortingAlgorithms_<int>.Sort_Radix_(subject, 4);


            /* testing */

            // verifying that all values are in order, whatever they are; "in order" means left <= right 
            for (int i = 1; i < 1000; i++) {
                Assert.IsTrue(actual[i - 1] <= actual[i],
                    string.Format("Value {0} at index {1} is not less than value {2} at index {3}.", actual[i - 1], i - 1, actual[i], i)
                    );
            }
        }

        [TestMethod()]
        public void AtomModulo__Assert_Expecteds()  /* working */  {
            /* groundwork */

            /* two tests */

            /* 1050 / 10 --> 105 // 105 % 10 --> 5 */
            /* 2730 / 100 --> 27 // 27 % 10 --> 7 */
            int subjectOf5 = 1050;
            int subjectOf7 = 2730;
            PrivateType t = new PrivateType(typeof(SortingAlgorithms_<int>));


            int expectedOf5 = 5;
            int expectedOf7 = 7;
            int actualOf5;
            int actualOf7;


            /* exercising the code */
            actualOf5 = (int)t.InvokeStatic("AtomModulo", subjectOf5, 10);
            actualOf7 = (int)t.InvokeStatic("AtomModulo", subjectOf7, 100);


            /* testing */
            Assert.AreEqual(expectedOf5, actualOf5);
            Assert.AreEqual(expectedOf7, actualOf7);
        }

        #endregion RadixSort and its Dependencies


        #region Deprecated : Generalized RadixSort

        //[TestMethod()]
        //public void Sort_Radix_Generalized_Thousand_Ints_Same_Lengths__Assert_Ordered()    {
        //   /* groundwork */

        //   // setting up random and large set of ints 
        //   int[] subject = new int[1000];

        //   // creating a random set of 1000 ints, all with the same number of digits 
        //   int seed = (new Random().Next());

        //   for (int i = 0; i < 1000; i++) {
        //       // making a new Random instance at each loop is faster than a Thread.Sleep() for different .Next() results 
        //       Random r = new Random(i * seed);

        //       subject[i] = r.Next(1000, 9999);
        //   }


        //   /* exercising the code */
        //   int[] actual = SortingAlgorithms_<int>.Sort_Radix_Generalized_(subject, SortingAlgorithms_<int>.IntDigitsLength, SortingAlgorithms_<int>.IntAsDecimal);


        //   /* testing */

        //   // verifying that all values are in order, whatever they are; "in order" means left <= right 
        //   for (int i = 1; i < 1000; i++) {
        //       Assert.IsTrue(actual[i - 1] <= actual[i],
        //           string.Format("Value {0} at index {1} is not less than value {2} at index {3}.", actual[i - 1], i - 1, actual[i], i)
        //           );
        //   }
        //}

        //[TestMethod()]
        //public void Sort_Radix_Generalized_Thousand_Ints_Different_Lengths__Assert_Ordered()    {
        //   /* groundwork */

        //   // setting up random and large set of ints 
        //   int[] subject = new int[1000];

        //   // creating a random set of 1000 ints, all with the same number of digits 
        //   int seed = (new Random().Next());

        //   for (int i = 0; i < 1000; i++) {
        //       // making a new Random instance at each loop is faster than a Thread.Sleep() for different .Next() results 
        //       Random r = new Random(i * seed);

        //       subject[i] = r.Next(1, 99999);
        //   }


        //   /* exercising the code */
        //   int[] actual = SortingAlgorithms_<int>.Sort_Radix_Generalized_(subject, SortingAlgorithms_<int>.IntDigitsLength, SortingAlgorithms_<int>.IntAsDecimal);


        //   /* testing */

        //   // verifying that all values are in order, whatever they are; "in order" means left <= right 
        //   for (int i = 1; i < 1000; i++) {
        //       Assert.IsTrue(actual[i - 1] <= actual[i],
        //           string.Format("Value {0} at index {1} is not less than value {2} at index {3}.", actual[i - 1], i - 1, actual[i], i)
        //           );
        //   }
        //}

        //[TestMethod()]
        //public void Sort_Radix_Generalized_Strings_Different_Lengths__Assert_Ordered()    {
        //   /* groundwork */
        //   string[] subject = { "tuxa", "pqrst", "tuv", "abc", "tuxb", "xyz123", "def" };

        //   string[] expected = { "abc", "def", "pqrst", "tuv", "tuxa", "tuxb", "xyz123" };
        //   string[] actual;

        //   SortingAlgorithms_<string>.ReversePaddingInRadixSort = true;

        //   /* exercising the code */
        //   actual = SortingAlgorithms_<string>.Sort_Radix_Generalized_(subject, SortingAlgorithms_<string>.StringDecimalDigitsLength, SortingAlgorithms_<string>.StringAsDecimal);


        //   /* testing */

        //   // verifying that all values are in order, whatever they are; 
        //   // "in order" means left <= right; .CompareTo < 1 is the same as <= 
        //   for (int i = 1; i < subject.Length; i++) {
        //       Assert.IsTrue(actual[i - 1].CompareTo(actual[i]) < 1,
        //           string.Format("Value {0} at index {1} is not less than value {2} at index {3}.", actual[i - 1], i - 1, actual[i], i)
        //           );
        //   }
        //}

        //[TestMethod()]
        //public void IntDigitsLength__Assert_Expecteds()    {

        //   /* 5 tests, each for a different-length int */

        //   /* groundwork */
        //   int actual;


        //   /* exercising the code and testing in alternation */
        //   actual = SortingAlgorithms_<int>.IntDigitsLength(5);
        //   Assert.AreEqual(1, actual);

        //   actual = SortingAlgorithms_<int>.IntDigitsLength(15);
        //   Assert.AreEqual(2, actual);

        //   actual = SortingAlgorithms_<int>.IntDigitsLength(105);
        //   Assert.AreEqual(3, actual);

        //   actual = SortingAlgorithms_<int>.IntDigitsLength(10005);
        //   Assert.AreEqual(5, actual);

        //   actual = SortingAlgorithms_<int>.IntDigitsLength(10000005);
        //   Assert.AreEqual(8, actual);
        //}

        //[TestMethod()]
        //public void StringAsDecimal__Assert_Expecteds()    {

        //   /* 3 different tests; max string length for my 10s-based algo seems to stabilize at 27, 2 less than max decimal digits */

        //   /* groundwork */

        //   string argAbc = "abc";
        //   string argJkl = "jkl";
        //   string argTuvwxyz = "tuvwxyz";

        //   decimal expectedAbc = 10779;   // calculated right to left:  99 + (98 * 10) + (97 * 100) 
        //   decimal actualAbc;

        //   decimal expectedJkl = 11778;   // calculated right to left:  108 + (107 * 10) + (106 * 100) 
        //   decimal actualJkl;

        //   decimal expectedTuv = 129012332;   // calculated right to left:  122 + (121 * 10) + (120 * 100) + (119 * 1000) + (118 * 10000) + (117 * 100000) + (116 * 1000000)
        //   decimal actualTuvwxyz;


        //   /* exercising the code */
        //   actualAbc = SortingAlgorithms_<string>.StringAsDecimal(argAbc);
        //   actualJkl = SortingAlgorithms_<string>.StringAsDecimal(argJkl);
        //   actualTuvwxyz = SortingAlgorithms_<string>.StringAsDecimal(argTuvwxyz);


        //   /* testing */
        //   Assert.AreEqual(expectedAbc, actualAbc);
        //   Assert.AreEqual(expectedJkl, actualJkl);
        //   Assert.AreEqual(expectedTuv, actualTuvwxyz);

        //}

        //[TestMethod()]
        //public void StringAsFullDecimal__Assert_Expecteds()    {
        //   /* groundwork */
        //   string arg = "tuv";   // raw decimal of this, using my tenning scheme, is 12888 
        //   int targetLength = 8;

        //   decimal expected = 12888000m;
        //   decimal actual;


        //   /* exercising the code */
        //   actual = SortingAlgorithms_<string>.StringAsFullDecimal(arg, targetLength);


        //   /* testing */
        //   Assert.AreEqual(expected, actual);
        //}

        //[TestMethod()]
        //public void DecimalDigitsLength__Assert_Expecteds()    {

        //   /* 5 tests, each for a different-length decimal */

        //   /* groundwork */
        //   int actual;


        //   /* exercising the code and testing in alternation */
        //   actual = SortingAlgorithms_<int>.DecimalDigitsLength(5m);
        //   Assert.AreEqual(1, actual);

        //   actual = SortingAlgorithms_<int>.DecimalDigitsLength(15m);
        //   Assert.AreEqual(2, actual);

        //   actual = SortingAlgorithms_<int>.DecimalDigitsLength(105m);
        //   Assert.AreEqual(3, actual);

        //   actual = SortingAlgorithms_<int>.DecimalDigitsLength(10005m);
        //   Assert.AreEqual(5, actual);

        //   actual = SortingAlgorithms_<int>.DecimalDigitsLength(10000005m);
        //   Assert.AreEqual(8, actual);
        //}

        #endregion Deprecated : Generalized RadixSort

    }
}
