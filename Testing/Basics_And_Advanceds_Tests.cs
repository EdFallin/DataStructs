﻿using System;
using System.Collections;

using DataStructs;
using Advanceds;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Testing
{
    [TestClass]
    public class Basics_And_Advanceds_Tests
    {
        #region Resized()

        [TestMethod()]
        public void Resized_Test_Shortening_Array_Assert_Expected()   // working  
        {
            // getting the selected count of elements from an array,
            // starting at zero, without using Linq or equivalents

            // GROUNDWORK  
            char[] subject = { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };

            // testables
            char[] expected = { 'a', 'b', 'c', 'd' };
            char[] actual;


            // EXERCISING THE CODE  
            actual = Arrays.Resized(subject, 4);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Resized_Test_Lengthening_Assert_Expected()   // working  
        {
            // testing with ints because int 0 is equatable with other 0s, but char \0 isn't equatable with other \0s

            // GROUNDWORK  
            int[] subject = { 1, 2, 3, 4, 5 };

            int[] expected = { 1, 2, 3, 4, 5, 0, 0, 0 };
            int[] actual;


            // EXERCISING THE CODE  
            actual = Arrays.Resized(subject, 8);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion Resized()


        #region CombineArrays()

        [TestMethod()]
        public void CombineArrays_Test_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            int[] first = { 3, 2, 5 };
            int[] second = { 4, 7, 6 };
            int[] third = { 9, 8 };

            int[] expected = { 3, 2, 5, 4, 7, 6, 9, 8 };
            int[] actual;


            // EXERCISING THE CODE  
            actual = Arrays.CombineArrays(first, second, third);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombineArrays_Test_Some_Empty_Arrays_Assert_Expected()   // working  
        {
            // GROUNDWORK  
            int[] emptyOne = { };
            int[] first = { 3, 2, 5 };
            int[] second = { 4, 7, 6 };
            int[] emptyTwo = { };
            int[] third = { 9, 8 };
            int[] emptyThree = { };
            int[] emptyFour = { };


            int[] expected = { 3, 2, 5, 4, 7, 6, 9, 8 };
            int[] actual;


            // EXERCISING THE CODE  
            actual = Arrays.CombineArrays(emptyOne, first, second, emptyTwo, third, emptyThree, emptyFour);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion CombineArrays()


        #region CombineBitArrays()

        [TestMethod()]
        public void CombineBitArrays_Test_Assert_Expected()   // working  
        {
            // CollectionAssert.AreEqual() can be used with BitArray objects just as with regular array objects... !

            // GROUNDWORK  
            BitArray first = new BitArray(new bool[] { true, false, true, false });
            BitArray second = new BitArray(new bool[] { true, false });

            BitArray expected = new BitArray(new bool[] { true, false, true, false, true, false });
            BitArray actual;


            // EXERCISING THE CODE  
            actual = Arrays.CombineBitArrays(first, second);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CombineBitArrays_Test_Some_Empty_Arrays_Assert_Expected()   // working  
        {
            // CollectionAssert.AreEqual() can be used with BitArray objects just as with regular array objects... !

            // GROUNDWORK  
            BitArray emptyOne = new BitArray(0);
            BitArray first = new BitArray(new bool[] { true, false, true, false });
            BitArray emptyTwo = new BitArray(0);
            BitArray second = new BitArray(new bool[] { true, false });
            BitArray emptyThree = new BitArray(0);

            BitArray expected = new BitArray(new bool[] { true, false, true, false, true, false });
            BitArray actual;


            // EXERCISING THE CODE  
            actual = Arrays.CombineBitArrays(emptyOne, first, emptyTwo, second, emptyThree);


            // TESTING  
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion CombineBitArrays()


        #region RandomZeroUpPermutation()

        [TestMethod()]
        public void RandomZeroUpPermutation_Test_Assert_Expected()   /* working */ {
            /* groundwork */
            int arg = 75;   // 75 numbers, starting at 0 

            int expectedCount = 75;
            int expectedMinValue = 0;
            int expectedMaxValue = 74;
            bool expectedValuesAreRepeated = false;

            int[] actual;

            int actualMinValue = int.MaxValue;
            int actualMaxValue = int.MinValue;
            bool actualValuesAreRepeated = false;


            /* exercising the code */
            actual = Permutations.RandomZeroUpPermutation(arg);

            /* getting metadata */
            for (int i = 0; i < actual.Length; i++) {
                if (actual[i] < actualMinValue)
                    actualMinValue = actual[i];
            }

            for (int i = 0; i < actual.Length; i++) {
                if (actual[i] > actualMaxValue)
                    actualMaxValue = actual[i];
            }

            // an upward loop with a downward loop inside it; an Θ(n^2) operation if no repeated values 
            for (int up = 1; up < actual.Length; up++) {
                for (int down = up - 1; down > 0; down--) {
                    if (actual[up] == actual[down]) {
                        actualValuesAreRepeated = true;
                        break;
                    }
                }
            }


            /* testing */
            Assert.AreEqual(expectedCount, actual.Length);
            Assert.AreEqual(expectedValuesAreRepeated, actualValuesAreRepeated);
            Assert.AreEqual(expectedMinValue, actualMinValue);
            Assert.AreEqual(expectedMaxValue, actualMaxValue);
        }

        #endregion RandomZeroUpPermutation()


        #region NextPowerOfTwo()

        [TestMethod()]
        public void NextPowerOfTwo_Test_Assert_Expected_Values()  /* working */  {
            //**  groundwork  **//

            /* numbers whose power of two I can easily determine, and one that is already a power of two */
            int firstFirst = 10;
            int first = 234;
            int second = 499;
            int third = 758;

            int already = 128;

            int firstExpected = 16;
            int firstActual;

            int secondExpected = 256;
            int secondActual;

            int thirdExpected = 512;
            int thirdActual;

            int fourthExpected = 1024;
            int fourthActual;

            int alreadyExpected = 128;
            int alreadyActual;


            //**  exercising the code  **//
            firstActual = Basics.NextPowerOfTwo(firstFirst);
            secondActual = Basics.NextPowerOfTwo(first);
            thirdActual = Basics.NextPowerOfTwo(second);
            fourthActual = Basics.NextPowerOfTwo(third);

            alreadyActual = Basics.NextPowerOfTwo(already);


            //**  testing  **//
            Assert.AreEqual(firstExpected, firstActual);
            Assert.AreEqual(secondExpected, secondActual);
            Assert.AreEqual(thirdExpected, thirdActual);
            Assert.AreEqual(fourthExpected, fourthActual);

            Assert.AreEqual(alreadyExpected, alreadyActual);
        }

        #endregion NextPowerOfTwo()


        #region AreOrdered()

        [TestMethod]
        public void AreOrdered_Test_Ints_Assert_True_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            int earlier = 5;
            int later = 10;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = Basics.AreOrdered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered_Test_Ints_Assert_True_Given_Equal()  /* working */  {
            //**  groundwork  **//
            int earlier = 100;
            int later = 100;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = Basics.AreOrdered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered_Test_Ints_Assert_False_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            int earlier = 100;
            int later = 50;

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = Basics.AreOrdered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered_Test_Strings_Assert_True_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            string earlier = "abc";
            string later = "pqr";

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = Basics.AreOrdered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreOrdered_Test_Strings_Assert_False_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            string earlier = "pqr";
            string later = "abc";

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = Basics.AreOrdered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AreOrdered()


        #region AreDisordered()

        [TestMethod]
        public void AreDisordered_Test_Ints_Assert_False_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            int earlier = 5;
            int later = 10;

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = Basics.AreDisordered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreDisordered_Test_Ints_Assert_False_Given_Equal()  /* working */  {
            //**  groundwork  **//
            int earlier = 100;
            int later = 100;

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = Basics.AreDisordered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreDisordered_Test_Ints_Assert_True_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            int earlier = 100;
            int later = 50;

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = Basics.AreDisordered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreDisordered_Test_Strings_Assert_False_Given_Ordered()  /* working */  {
            //**  groundwork  **//
            string earlier = "abc";
            string later = "pqr";

            bool expected = false;
            bool actual = true;


            //**  exercising the code  **//
            actual = Basics.AreDisordered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AreDisordered_Test_Strings_Assert_True_Given_Disordered()  /* working */  {
            //**  groundwork  **//
            string earlier = "pqr";
            string later = "abc";

            bool expected = true;
            bool actual = false;


            //**  exercising the code  **//
            actual = Basics.AreDisordered(earlier, later);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AreDisordered()


        #region GenericToBinary.ToBinary()

        /* --> exception when arg is null */
        /* --> exception when arg is not a primitive */

        [TestMethod]
        public void ToBinary_Test__Int32s__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  exercising the code  **//
            byte[] binary = GenericToBinary.ToBinary(15);


            //**  testing  **//
            int actual = BitConverter.ToInt32(binary, 0);
            Assert.AreEqual(15, actual);
        }

        [TestMethod]
        public void ToBinary_Test__CharArray__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  groundwork  **//
            char[] topic = ("a short string").ToCharArray();   // parentheses now optional (!) 
            char[] expected = (char[])topic.Clone();   // shallow copy is new values copy for primitives; *** returns `object`, so must be cast *** 


            //**  exercising the code  **//
            byte[] binary = GenericToBinary.ToBinary(topic);


            //**  testing  **//
            char[] actual = UnwindBytes(binary);
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Int32ToByteArray_Test__Known_Arg__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  groundwork  **//
            int topic = 27;


            //**  exercising the code  **//
            byte[] binary = GenericToBinary.Int32ToByteArray(topic);


            //**  testing  **//
            int actual = BitConverter.ToInt32(binary, 0);
            Assert.AreEqual(27, actual);
        }

        [TestMethod]
        public void CharArrayToByteArray_Test__Known_Arg__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  groundwork  **//
            char[] topic = { 't', 'o', 'p', 'i', 'c' };
            char[] expected = (char[])topic.Clone();   // shallow copy, but that is fine with a value type 


            //**  exercising the code  **//
            byte[] binary = GenericToBinary.CharArrayToByteArray(topic);


            //**  testing  **//
            char[] actual = UnwindBytes(binary);

            CollectionAssert.AreEqual(expected, actual);
        }

        #region Local Friendlies

        /* --> maybe add to my generic converter as RewindBytes() or similar */

        public char[] UnwindBytes(byte[] binary)  /* verified */  {
            char[] output = new char[binary.Length / 2];
            //char[] output = new char[binary.Length];

            for (int i = 0, offset = 0; i < binary.Length; i += 2) {
                byte[] bytes = { binary[i], binary[i + 1] };
                char passer = BitConverter.ToChar(bytes, 0);

                //binary.CopyTo(output, 0);

                output[offset] = passer;
                offset++;
            }

            return output;
        }

        #endregion Local Friendlies

        #endregion GenericToBinary.ToBinary()


        #region GenericToBinary<T>.ToBinary()

        [TestMethod()]
        public void ToBinary_Test__Known_Arg__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  groundwork  **//
            GenericToBinary<Int32> target = new GenericToBinary<int>();
            int topic = 107;


            //**  exercising the code  **//
            byte[] binary = target.ToBinary(topic);


            //**  testing  **//
            int actual = BitConverter.ToInt32(binary, 0);
            Assert.AreEqual(107, actual);
        }

        [TestMethod()]
        public void ToBinary_Test__Known_Complex_Arg__Assert_Expected_Round_Trip_Results()  /* working */  {
            //**  groundwork  **//
            GenericToBinary<char[]> target = new GenericToBinary<char[]>();
            char[] topic = ("a topic").ToCharArray();
            char[] expected = (char[])topic.Clone();   /* .Clone() returns `object`, so must be cast to correct type */


            //**  exercising the code  **//
            byte[] binary = target.ToBinary(topic);


            //**  testing  **//
            char[] actual = UnwindBytes(binary);
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion GenericToBinary<T>.ToBinary()


        #region GenericToBinary.SizeOfT()

        [TestMethod()]
        public void SizeOfT_Test__Known_Sizes__Assert_Expected_Sizes()  /* working */  {
            //**  groundwork  **//
            Int32 anInt32 = 16;
            Int64 anInt64 = 2700;

            //**  testing  **//
            Assert.AreEqual(4, GenericToBinary.SizeOfT(anInt32));
            Assert.AreEqual(8, GenericToBinary.SizeOfT(anInt64));
        }

        #endregion GenericToBinary.SizeOfT()

    }
}
