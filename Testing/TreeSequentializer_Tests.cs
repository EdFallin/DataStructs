﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class TreeSequentializer_Tests
    {
        /* --> let's get these tests and units working, or else consider trashing this sequentializing stuff completely - - though preferably not */

        #region Overview

        /* test general tree                    
                                                
            a							        
            |                                   
            -----------------                   
            b				c			        
            |               |                   
            -------------   ---------           
            d	e	f	g	h	i	j	        
            |       |           |   |           
            |       |           |   -----       
            k		n			t	u	v       
            |       |                           
            -----   ---------                   
            l	m	o	p	q			        
                            |                   
                            -----               
                            r	s		        
                                                
 */

        /* preorder traversal of test general tree:  a, b, d, k, l, m, e, f, n, o, p, q, r, s, g, c, h, i, t, j, u, v */

        /* postorder traversal of test tree:  l, m, k, d, e, o, p, r, s, q, n, f, g, b, h, t, i, u, v, j, c, a */



        /* test binary tree                                                 
                                                                            
                               a-0                                          
                               |                                            
                       ------------------                                   
                       b-1              c-5                                 
                       |                |                                   
                   ---------       ---------                                
                   d-2     e-4     f-6     g-10                             
                   |               |                                        
               -----           --------                                     
               h-3             i-7     j-8                                  
                                       |                                    
                                       -----                                
                                           k-9                              
                                                                            
            */

        /* preorder traversal of test binary tree: a, b, d, h, e, c, f, i, j, k, g */

        /* inorder traversal of test binary tree: h, d, b, e, a, i, f, j, k, c, g */

        #endregion Overview


        #region Friendlies

        public GeneralTree_<char> TestGeneralTree()   // verified  
        {
            GeneralTree_Node_<char> node;

            #region Diagram of Test Tree

            /* test tree                        
                                                
                a							    
                |                               
                -----------------               
                b				c			    
                |               |               
                -------------   ---------       
                d	e	f	g	h	i	j	    
                |       |           |   |       
                |       |           |   -----   
                k		n			t	u	v   
                |       |                       
                -----   ---------               
                l	m	o	p	q			    
                                |               
                                -----           
                                r	s		    
                                                
            */

            #endregion Diagram of Test Tree

            GeneralTree_<char> tree = new GeneralTree_<char>('a');
            node = tree.Root;

            tree.AddChild(node, 'b');
            tree.AddChild(node, 'c');

            node = node.FirstChild;   // 'b' 
            tree.AddChild(node, 'd');
            tree.AddChild(node, 'e');
            tree.AddChild(node, 'f');
            tree.AddChild(node, 'g');

            node = tree.Root;
            node = node.FirstChild.RightSibling;   // 'c' 
            tree.AddChild(node, 'h');
            tree.AddChild(node, 'i');
            tree.AddChild(node, 'j');

            node = tree.Root;
            node = node.FirstChild.FirstChild;   // 'd' 
            tree.AddChild(node, 'k');
            node = node.FirstChild;   // 'k' 
            tree.AddChild(node, 'l');
            tree.AddChild(node, 'm');

            node = tree.Root;
            node = node.FirstChild.FirstChild.RightSibling.RightSibling;    // 'f' 
            tree.AddChild(node, 'n');
            node = node.FirstChild;   // 'n' 
            tree.AddChild(node, 'o');
            tree.AddChild(node, 'p');
            tree.AddChild(node, 'q');

            node = node.FirstChild.RightSibling.RightSibling;   // 'q' 
            tree.AddChild(node, 'r');
            tree.AddChild(node, 's');

            node = tree.Root;
            node = node.FirstChild.RightSibling.FirstChild.RightSibling;   // 'i' 
            tree.AddChild(node, 't');

            node = node.RightSibling;   // 'j' 
            tree.AddChild(node, 'u');
            tree.AddChild(node, 'v');

            return tree;
        }

        public BinaryTree_PointerNodes_<char, int> TestBinaryTree()   // passed  
        {
            PointerNode_<char, int> node;

            #region Diagram of Test Tree

            /* test tree                                                    
                                                                            
                               a-0                                          
                               |                                            
                       ------------------                                   
                       b-1              c-5                                 
                       |                |                                   
                   ---------       ---------                                
                   d-2     e-4     f-6     g-10                             
                   |               |                                        
               -----           --------                                     
               h-3             i-7     j-8                                  
                                       |                                    
                                       -----                                
                                           k-9                              
                                                                            
            */

            #endregion Diagram of Test Tree

            // tree, root, level 0 
            BinaryTree_PointerNodes_<char, int> tree = new BinaryTree_PointerNodes_<char, int>('a', 0);
            node = tree.Root;

            // level 1 
            node.LeftChild = new PointerNode_<char, int>('b', 1);
            node.RightChild = new PointerNode_<char, int>('c', 5);

            // left subtree, level 2 to bottom 
            node = node.LeftChild;   // at 'b' 
            node.LeftChild = new PointerNode_<char, int>('d', 2);
            node.RightChild = new PointerNode_<char, int>('e', 4);
            node = node.LeftChild;   // at 'd' 
            node.LeftChild = new PointerNode_<char, int>('h', 3);

            // right subtree, level 2 to bottom 
            node = tree.Root;
            node = node.RightChild;   // at 'c' 
            node.LeftChild = new PointerNode_<char, int>('f', 6);
            node.RightChild = new PointerNode_<char, int>('g', 10);
            node = node.LeftChild;   // at 'f' 
            node.LeftChild = new PointerNode_<char, int>('i', 7);
            node.RightChild = new PointerNode_<char, int>('j', 8);
            node = node.RightChild;   // at 'j' 
            node.RightChild = new PointerNode_<char, int>('k', 9);

            return tree;
        }

        // for traversing binary trees 
        public object GatherKeys(object topic_object, PointerNode_<char, int> node)   // ok; validated elsewhere  
        {
            string topic = topic_object as string;

            // first invocation only 
            if (topic == null)
                topic = string.Empty;

            topic += Convert.ToString(node.Key);

            return topic;
        }

        #endregion Friendlies


        #region Friendlies Testing

        [TestMethod()]
        public void TestBinaryTree_Test_()   // verified  
        {
            // GROUNDWORK  

            string expectedPre = "abdhecfijkg";
            string actualPre;

            string expectedIno = "hdbeaifjkcg";
            string actualIno;

            // EXERCISING THE CODE  
            BinaryTree_PointerNodes_<char, int> target = this.TestBinaryTree();


            // TESTING  
            actualPre = (string)target.TraversePreorder(this.GatherKeys);
            Assert.AreEqual(expectedPre, actualPre);

            actualIno = (string)target.TraverseInorder(this.GatherKeys);
            Assert.AreEqual(expectedIno, actualIno);
        }

        #endregion Friendlies Testing


        [TestMethod()]
        public void Poly_Test_Sequentializing_And_Desequentializing_A_General_Tree_Assert_Input_Equals_Output()  /* working */  {
            ///* groundwork */
            //GeneralTree_<char> target = this.TestGeneralTree();
            //Queue_<char> topic;
            //GeneralTree_<char> product;

            //int nodesInTestGeneralTree = 22;

            //char endOfDownward = '\t';
            //char endOfRightward = '\r';

            //char[] expected = target.TraversePreOrder(nodesInTestGeneralTree);
            //char[] actual;



            ///* exercising the code */
            //topic = TreeSequentializer_.SequentializeGeneralTree<char>(target, endOfDownward, endOfRightward);
            
            //{
            //   StringBuilder s = new StringBuilder();

            //   for (int ix = 0; ix < topic.Length; ix++) {
            //       char atIx = topic.Dequeue();
            //       s.Append(atIx);
            //   }

            //   string t = s.Replace("\t", "[t]").Replace("\r", "[r]").ToString();
            //   System.Windows.Forms.MessageBox.Show(t);
            //}

            ////product = TreeSequentializer_.DesequentializeToGeneralTree<char>(topic, endOfSubtree);

            ////actual = product.TraversePreOrder(nodesInTestGeneralTree);


            /////* testing */
            ////CollectionAssert.AreEqual(expected, actual);
        }


        // use `public struct Duple_<K, T> to store binary trees, with `K key` and T element`

        [TestMethod()]
        public void SequentializeBinaryTree_Test_()
        {
            //// GROUNDWORK  
            //Object target = new Object();

            //Output expected = Expected;
            //Output actual;


            //// EXERCISING THE CODE  
            //actual = target.SequentializeBinaryTree();


            //// TESTING  
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void DesequentializeToBinaryTree_Test_()
        {
            //// GROUNDWORK  
            //Object target = new Object();

            //Output expected = Expected;
            //Output actual;


            //// EXERCISING THE CODE  
            //actual = target.DesequentializeToBinaryTree();


            //// TESTING  
            //Assert.AreEqual(expected, actual);
        }

    }
}
