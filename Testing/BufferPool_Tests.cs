﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DataStructs;

namespace Testing
{
    [TestClass]
    public class BufferPool_Tests
    {
        #region Fields

        int _clusterSize = 16;
        string _results = "fail";
        byte[] _data = null;
        bool _wroteData = false;

        #endregion Fields


        #region Friendlies

        public void AssignReaderAndWriter()  /* ok */  {
            BufferPool_.DataReader = this.ReadData;
            BufferPool_.DataWriter = this.WriteData;
        }

        public byte[] ReadData(int clusterIdentity)  /* passed */  {
            byte[] data = new byte[_clusterSize];
            data.Initialize();   // all 0s 

            /* init data by filling with the cluster-identity arg repeated as a byte */
            for (int i = 0; i < _clusterSize; i++) {
                data[i] = (byte)clusterIdentity;
            }

            /* record actual invocation of this method */
            this._results = String.Format("Friendly ReadData() called with number {0} and cluster size {1}.", clusterIdentity, _clusterSize);

            return data;
        }

        public void WriteData(int diskBlock, byte[] data)  /* ok */  {
            this._data = data;
            this._wroteData = true;
        }

        public void CleanseResults()  /* ok */  {
            this._results = "fail";
        }

        public void CleanseData()  /* ok */  {
            this._data = null;
        }

        public void ClearWroteData()  /* ok */  {
            this._wroteData = false;
        }

        #endregion Friendlies


        #region Friendlies Testing

        [TestMethod()]
        public void ReadData_Test_Assert_Expected_Values()  /* working */  {
            //**  groundwork  **//
            int identifier = 5;
            // default length is 16 

            byte[] expected = { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
            byte[] actual;


            //**  exercising the code  **//
            actual = this.ReadData(identifier);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ReadData_Test_Assert_Expected_Results_String()  /* working */  {
            //**  groundwork  **//
            int identifier = 5;
            this._clusterSize = 8;

            string expected = "Friendly ReadData() called with number 5 and cluster size 8.";
            string actual;


            //**  exercising the code  **//
            this.ReadData(identifier);   // returned value not used 
            actual = this._results;

            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Friendlies Testing


        [TestMethod()]
        public void BufferIndexByDiskBlock_Test__Assert_Expected_Index()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            int expected = 4;
            int actual;


            //**  exercising the code  **//
            // the buffer for disk-block "7" is at index 4 in the linked list 
            actual = (int)p.Invoke("BufferIndexByDiskBlock", 7);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveBuffer_Test_3_Asserts__Assert_Expected_Buffer_Different_Indexed_Buffer_And_List_Length()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            Buffer_ output;

            int expected = 7;   // disk block for buffer at index 4 
            int actual;


            //**  exercising the code  **//
            // buffers are addressed by their list indices in this method 
            output = (Buffer_)p.Invoke("RemoveBuffer", 4);


            //**  testing  **//
            // returned / removed buffer 
            actual = output.DiskBlock;
            Assert.AreEqual(expected, actual);

            // buffer at same index ( different from original ( 7 ) if algo is working )
            buffers.PositionAtIndex(4);
            Buffer_ atIndex4 = buffers.CurrentValue;
            Assert.AreNotEqual(7, atIndex4.DiskBlock);

            // length of buffers list ( 7 ( 1 less than before ) if algo is working )
            Assert.AreEqual(7, buffers.Length);
        }

        [TestMethod()]
        public void OldestDumpableBuffer_Test__Assert_Last_Buffer_Index()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            // clearing all retains ( done for consistency; all are init to 0 ) 
            {
                buffers.PositionAtStart();

                for ( /* no init ops */ ; buffers.CurrentPosition < buffers.Length; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 0);
                }
            }

            int expected = 7;   // index of last 
            int actual;


            //**  exercising the code  **//
            actual = (int)p.Invoke("OldestDumpableBuffer");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void OldestDumpableBuffer_Test__Assert_Middle_Buffer_Index()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            // highest-indexed buffer with 0 retains is oldest-dumpable; first 6 (through index 5) have no retains, so index 5 is returned 
            {
                buffers.PositionAtStart();

                // these already have 0 retains; done for clarity 
                for ( /* no init ops */ ; buffers.CurrentPosition < 6; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 0);
                }

                // no reset of position ( .PositionAtStart() ), so continues with remaining buffers (at indices 6 and 7) 
                for ( /* no init ops */ ; buffers.CurrentPosition < buffers.Length; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 1);
                }
            }

            int expected = 5;   // index of oldest (highest-indexed) buffer that has 0 retains 
            int actual;


            //**  exercising the code  **//
            actual = (int)p.Invoke("OldestDumpableBuffer");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InvalidateBuffer_Test__Assert_Expected_Buffer_Flag_Set()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            bool expected = true;   // index of last 
            bool actual;


            //**  exercising the code  **//
            buffers.PositionAtIndex(3);
            p.Invoke("InvalidateBuffer", buffers.CurrentValue);   // arg must be a Buffer_ object 


            //**  testing  **//
            actual = buffers.CurrentValue.Invalidated;   // position in `buffers hasn't changed 
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertBufferAtFront_Test_2_Asserts__Assert_Expected_Buffers_At_Index_Zero()  /* working */  {
            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  
            buffers.Append(new Buffer_(6));   // 5  
            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  


            Buffer_ expected = new Buffer_(100);
            Buffer_ actual;


            //**  exercising the code and testing  **//
            // 1st move-to-front 
            p.Invoke("InsertBufferAtFront", expected);   // arg must be a Buffer_ object 

            buffers.PositionAtStart();
            actual = buffers.CurrentValue;
            Assert.AreEqual(expected, actual);   // object-to-object checks for identicality; should be identical 

            // 2nd move-to-front 
            expected = new Buffer_(200);
            p.Invoke("InsertBufferAtFront", expected);

            buffers.PositionAtStart();
            actual = buffers.CurrentValue;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ReplacementBuffer_Test__Old_Buffer_Must_Be_Written_Out__5_Asserts__Each_As_Listed_In_Comments()  /* working */  {
            /* in this test, the old buffer *should* be written out */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            BufferPool_.DataReader = this.ReadData;
            BufferPool_.DataWriter = this.WriteData;

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  

            Buffer_ subject = new Buffer_(6);
            subject.Data = new byte[] { 100, 99, 98, 97, 96, 95, 94, 93 };
            subject.Altered = true;   // should be written back 
            buffers.Append(subject);          // 5  

            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            // highest-indexed buffer with 0 retains is oldest-dumpable; first 6 (through index 5) have no retains, so index 5 is returned 
            {
                buffers.PositionAtStart();

                // these already have 0 retains; done for clarity 
                for ( /* no init ops */ ; buffers.CurrentPosition < 6; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 0);
                }

                // no reset of position ( .PositionAtStart() ), so continues with remaining buffers (at indices 6 and 7) 
                for ( /* no init ops */ ; buffers.CurrentPosition < buffers.Length; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 1);
                }
            }

            this.CleanseData();

            int expectedNewDiskBlock = 20;
            byte[] expectedNewData = this.ReadData(20);
            byte[] expectedOldData = new byte[] { 100, 99, 98, 97, 96, 95, 94, 93 };   // same as buffer contents above 

            Buffer_ actual;


            //**  exercising the code  **//
            actual = (Buffer_)p.Invoke("ReplacementBuffer", 20);


            //**  testing  **//
            // new buffer 
            Assert.AreEqual(expectedNewDiskBlock, actual.DiskBlock);
            CollectionAssert.AreEqual(expectedNewData, actual.Data);

            // invalidation of old buffer 
            Assert.AreEqual(true, subject.Invalidated);

            // writing out of old buffer 
            {
                Assert.IsTrue(this._data != null);

                if (this._data != null) {
                    CollectionAssert.AreEqual(expectedOldData, this._data);
                }
            }
        }

        [TestMethod()]
        public void ReplacementBuffer_Test__Old_Buffer_Must_Not_Be_Written_Out__4_Asserts__Each_As_Listed_In_Comments()  /* working */  {
            /* in this test, the old buffer should *not* be written out */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            PrivateObject p = new PrivateObject(target);

            BufferPool_.DataReader = this.ReadData;
            BufferPool_.DataWriter = this.WriteData;

            List_DoublyLinked_<Buffer_> buffers = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            buffers.Append(new Buffer_(8));   // 0  
            buffers.Append(new Buffer_(4));   // 1  
            buffers.Append(new Buffer_(2));   // 2  
            buffers.Append(new Buffer_(3));   // 3  
            buffers.Append(new Buffer_(7));   // 4  

            Buffer_ subject = new Buffer_(6);
            subject.Data = new byte[] { 100, 99, 98, 97, 96, 95, 94, 93 };
            subject.Altered = false;   // does not need writing back 
            buffers.Append(subject);          // 5  

            buffers.Append(new Buffer_(5));   // 6  
            buffers.Append(new Buffer_(9));   // 7  

            // highest-indexed buffer with 0 retains is oldest-dumpable; first 6 (through index 5) have no retains, so index 5 is returned 
            {
                buffers.PositionAtStart();

                // these already have 0 retains; done for clarity 
                for ( /* no init ops */ ; buffers.CurrentPosition < 6; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 0);
                }

                // no reset of position ( .PositionAtStart() ), so continues with remaining buffers (at indices 6 and 7) 
                for ( /* no init ops */ ; buffers.CurrentPosition < buffers.Length; buffers.Next()) {
                    PrivateObject b = new PrivateObject(buffers.CurrentValue);
                    b.SetField("_retains", 1);
                }
            }

            this.CleanseData();

            int expectedNewDiskBlock = 20;
            byte[] expectedNewData = this.ReadData(20);
            byte[] expectedOldData = new byte[] { 100, 99, 98, 97, 96, 95, 94, 93 };   // same as buffer contents above 
            Buffer_ actual;


            //**  exercising the code  **//
            actual = (Buffer_)p.Invoke("ReplacementBuffer", 20);


            //**  testing  **//
            // new buffer 
            Assert.AreEqual(expectedNewDiskBlock, actual.DiskBlock);
            CollectionAssert.AreEqual(expectedNewData, actual.Data);

            // invalidation of old buffer 
            Assert.AreEqual(true, subject.Invalidated);

            // no writing out of old buffer 
            Assert.AreEqual(null, this._data);
        }

        [TestMethod()]
        public void AcquireBuffer_Test__New_Buffer_With_Unfull_Pool__4_Asserts__As_Remarked()  /* working */  {
            /* asserts :  proxy returned has right block; writer not invoked; pool is 1 more than prior length; latest buffer at front */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);

            BufferProxy_ proxy = null;
            PrivateObject p;
            Buffer_ buffer;

            int expectedDiskBlock;
            int actualDiskBlock;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            int expectedPoolListLength;
            int actualPoolListLength;

            // prior buffer acquires; 5 in all, top index is 4, found at position 0 in pool 
            for (int ix = 0; ix < 5; ix++) {
                proxy = target.AcquireBuffer(ix);

                // setting altered flag; retains count is already 0 
                p = new PrivateObject(proxy);
                buffer = (Buffer_)p.GetField("_buffer");
                buffer.Altered = true;
            }

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // all-new data block, but pool isn't full, so reduced ops & data changes 
            proxy = target.AcquireBuffer(5);   // 6th buffer 
            p = new PrivateObject(proxy);
            buffer = (Buffer_)p.GetField("_buffer");


            //**  testing  **//
            expectedDiskBlock = 5;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);

            expectedWriterInvoked = false;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            p = new PrivateObject(target);
            List_DoublyLinked_<Buffer_> pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            expectedPoolListLength = 6;
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            pool.PositionAtStart();
            buffer = pool.CurrentValue;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);
        }

        [TestMethod()]
        public void AcquireBuffer_Test__New_Buffer_With_Empty_Pool__4_Asserts__As_Remarked()  /* working */  {
            /* asserts :  proxy returned has right block; writer not invoked; pool is 1 long; latest buffer at front */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);

            BufferProxy_ proxy = null;
            PrivateObject p;
            Buffer_ buffer;

            int expectedDiskBlock;
            int actualDiskBlock;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            int expectedPoolListLength;
            int actualPoolListLength;

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // all-new data block, but pool is empty, so reduced ops & data changes; 
            // *no* prior buffer acquires 
            proxy = target.AcquireBuffer(700000);
            p = new PrivateObject(proxy);
            buffer = (Buffer_)p.GetField("_buffer");


            //**  testing  **//
            expectedDiskBlock = 700000;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);

            expectedWriterInvoked = false;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            p = new PrivateObject(target);
            List_DoublyLinked_<Buffer_> pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            expectedPoolListLength = 1;
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            pool.PositionAtStart();
            buffer = pool.CurrentValue;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);
        }

        [TestMethod()]
        public void AcquireBuffer_Test__Existing_Buffer_With_Unfull_Pool__4_Asserts__As_Remarked()  /* working */  {
            /* asserts :  proxy returned has right block; writer not invoked; pool is same length as before; latest buffer at front */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);

            BufferProxy_ proxy = null;
            PrivateObject p;
            Buffer_ buffer;

            int expectedDiskBlock;
            int actualDiskBlock;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            int expectedPoolListLength;
            int actualPoolListLength;

            // prior buffer acquires; 5 total, top index is 4, found at position 0 in pool 
            for (int ix = 0; ix < 5; ix++) {
                proxy = target.AcquireBuffer(ix);

                // setting altered flag; retains count is already 0 
                p = new PrivateObject(proxy);
                buffer = (Buffer_)p.GetField("_buffer");
                buffer.Altered = true;
            }

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // existing data block; pool isn't full, so reduced ops & data changes 
            proxy = target.AcquireBuffer(3);
            p = new PrivateObject(proxy);
            buffer = (Buffer_)p.GetField("_buffer");


            //**  testing  **//
            expectedDiskBlock = 3;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);

            expectedWriterInvoked = false;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            p = new PrivateObject(target);
            List_DoublyLinked_<Buffer_> pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            expectedPoolListLength = 5;   // top index is 4, so 5 buffers 
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            pool.PositionAtStart();
            buffer = pool.CurrentValue;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);
        }

        [TestMethod()]
        public void AcquireBuffer_Test__New_Buffer_With_Full_Pool__5_Asserts__As_Remarked()  /* working */  {
            /* asserts :  proxy returned has right block; writer *is* invoked; correct data written; pool is same length, full, as before; latest buffer at front */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);

            BufferProxy_ proxy = null;
            PrivateObject p;
            Buffer_ buffer;
            List_DoublyLinked_<Buffer_> pool;

            int expectedDiskBlock;
            int actualDiskBlock;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            int expectedPoolListLength;
            int actualPoolListLength;

            // third-to-last buffer should be tossed and written; because buffers are in effect 
            // in reverse order, this buffer should have mock data-block 2 
            byte[] expectedWrittenData;
            byte[] actualWrittenData;

            // prior buffer acquires; 8 in all, top index is 7; pool is full 
            for (int ix = 0; ix < 8; ix++) {
                proxy = target.AcquireBuffer(ix);

                // setting altered flag; retains count is defaulted to 0 
                p = new PrivateObject(proxy);
                buffer = (Buffer_)p.GetField("_buffer");

                buffer.Altered = true;
            }


            // releasing all but the last 2 buffers (blocks 0 and 1), because all buffers are retained when acquired 
            {
                p = new PrivateObject(target);
                pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");

                for (int ix = 0; ix < (pool.Length - 2); ix++) {
                    pool.PositionAtIndex(ix);
                    buffer = pool.CurrentValue;
                    buffer.Release();
                }
            }

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // all-new data block against a full pool, so all ops & data changes 
            proxy = target.AcquireBuffer(100);
            p = new PrivateObject(proxy);
            buffer = (Buffer_)p.GetField("_buffer");


            //**  testing  **//
            expectedDiskBlock = 100;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);

            expectedWriterInvoked = true;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            expectedWrittenData = this.ReadData(2);
            actualWrittenData = this._data;
            CollectionAssert.AreEqual(expectedWrittenData, actualWrittenData);

            p = new PrivateObject(target);
            pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");   // actually same as before; retained here for readability 
            expectedPoolListLength = 8;
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            pool.PositionAtStart();
            buffer = pool.CurrentValue;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);
        }

        [TestMethod()]
        public void AcquireBuffer_Test__Existing_Buffer_With_Full_Pool__5_Asserts__As_Remarked()  /* working */  {
            /* asserts :  proxy returned has right block; writer not invoked; no data written; pool is same length, full, as before; latest buffer at front */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);

            BufferProxy_ proxy = null;
            PrivateObject p;
            Buffer_ buffer;

            int expectedDiskBlock;
            int actualDiskBlock;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            int expectedPoolListLength;
            int actualPoolListLength;

            // last buffer in pool should be tossed and written; it should have mock data-block 7 
            byte[] expectedWrittenData;
            byte[] actualWrittenData;

            // prior buffer acquires; 8 in all, top index is 7; pool is full 
            for (int ix = 0; ix < 8; ix++) {
                proxy = target.AcquireBuffer(ix);

                // setting altered flag; retains count is already 0 
                p = new PrivateObject(proxy);
                buffer = (Buffer_)p.GetField("_buffer");
                buffer.Altered = true;
            }

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // existing data block against a full pool; existing, so reduced ops & data changes 
            proxy = target.AcquireBuffer(2);
            p = new PrivateObject(proxy);
            buffer = (Buffer_)p.GetField("_buffer");


            //**  testing  **//
            expectedDiskBlock = 2;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);

            expectedWriterInvoked = false;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            expectedWrittenData = null;
            actualWrittenData = this._data;
            CollectionAssert.AreEqual(expectedWrittenData, actualWrittenData);

            p = new PrivateObject(target);
            List_DoublyLinked_<Buffer_> pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            expectedPoolListLength = 8;
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            pool.PositionAtStart();
            buffer = pool.CurrentValue;
            actualDiskBlock = buffer.DiskBlock;
            Assert.AreEqual(expectedDiskBlock, actualDiskBlock);
        }


        /* Buffer_ methods */

        [TestMethod()]
        public void ReturnData_Test__No_Start_Data__2_Asserts__Assert_Expected_Returned_Data_And_Stored_Data()  /* working */  {
            //**  groundwork  **//
            Buffer_ target = new Buffer_(10);
            PrivateObject p = new PrivateObject(target);

            this.CleanseResults();
            BufferPool_.DataReader = this.ReadData;   // referenced internally by Buffer_ objects 

            byte[] expected = this.ReadData(10);
            byte[] actual;


            //**  exercising the code  **//
            actual = (byte[])p.Invoke("ReturnData");


            //**  testing  **//
            // returned data 
            CollectionAssert.AreEqual(expected, actual);

            // stored data 
            actual = (byte[])p.GetField("_data");
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ReturnData_Test__No_Start_Data__3_Asserts__Assert_No_Second_Call_Of_Reader_And_Expected_Returned_Data_And_Stored_Data()  /* working */  {
            //**  groundwork  **//
            Buffer_ target = new Buffer_(10);
            PrivateObject p = new PrivateObject(target);

            this.CleanseResults();
            BufferPool_.DataReader = this.ReadData;   // referenced internally by Buffer_ objects 

            string expectedText = "No second call.";
            byte[] expected = this.ReadData(10);

            string actualText = "fail";
            byte[] actual;


            //**  exercising the code  **//
            p.Invoke("ReturnData");         // return value tossed 
            this._results = "No second call.";   // _results should retain this value after second .ReturnData() call 
            actual = (byte[])p.Invoke("ReturnData");


            //**  testing  **//
            // reader called or not 
            actualText = this._results;
            Assert.AreEqual(expectedText, actualText);

            // returned data 
            CollectionAssert.AreEqual(expected, actual);

            // stored data 
            actual = (byte[])p.GetField("_data");
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void IsRetained_Test__Valid_Input__Assert_()  /* working */  {
            //**  groundwork  **//
            Buffer_ target = new Buffer_(100);

            bool expected;
            bool actual;


            //**  exercising the code and testing  **//
            // not retained at init 
            actual = target.IsRetained;
            expected = false;
            Assert.AreEqual(expected, actual);

            // now should be retained 
            target.Retain();
            expected = true;
            actual = target.IsRetained;
            Assert.AreEqual(expected, actual);

            // and now not retained 
            target.Release();
            expected = false;
            actual = target.IsRetained;
            Assert.AreEqual(expected, actual);

            // multiple retains; retained 
            target.Retain();
            target.Retain();
            target.Retain();
            expected = true;
            actual = target.IsRetained;
            Assert.AreEqual(expected, actual);

            // multiple releases, but fewer than retains; retained 
            target.Release();
            target.Release();
            expected = true;
            actual = target.IsRetained;
            Assert.AreEqual(expected, actual);

            // enough releases to cancel all retains; not retained 
            target.Release();
            expected = false;
            actual = target.IsRetained;
            Assert.AreEqual(expected, actual);
        }


        /* BufferProxy_ methods */

        [TestMethod()]
        public void Invalidated_Test__Buffer_Invalidated__Assert_Property_Returns_True()  /* working */  {
            //**  groundwork  **//
            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            subject.Invalidated = true;
            actual = target.Invalidated;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Invalidated_Test__Buffer_Not_Invalidated__Assert_Property_Returns_False()  /* working */  {
            //**  groundwork  **//
            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            bool expected = false;
            bool actual;


            //**  exercising the code  **//
            actual = target.Invalidated;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Invalidated_Test__Buffer_Invalidated_Then_Revalidated__Assert_Property_Returns_True()  /* working */  {
            /* my design is that once a buffer is invalidated, its proxy remains invalidated */

            //**  groundwork  **//
            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            subject.Invalidated = true;
            subject.Invalidated = false;
            actual = target.Invalidated;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Invalidated_Test__Proxy_Invalidated__Assert_Property_Returns_True()  /* working */  {
            //**  groundwork  **//
            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            target.Invalidated = true;
            actual = target.Invalidated;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Invalidated_Test__Proxy_Invalidated_Then_Revalidated__Assert_Property_Returns_True()  /* working */  {
            /* my design is that once a buffer or its proxy is invalidated, its proxy remains invalidated */

            //**  groundwork  **//
            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            bool expected = true;
            bool actual;


            //**  exercising the code  **//
            target.Invalidated = true;
            target.Invalidated = false;
            actual = target.Invalidated;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Data_Test__Plain_Init__Assert_Expected_Data()  /* working */  {
            //**  groundwork  **//
            BufferPool_.DataReader = this.ReadData;

            BufferProxy_ target = new BufferProxy_(new Buffer_(1000));

            byte[] expected = this.ReadData(1000);
            byte[] actual;


            //**  exercising the code  **//
            actual = target.Data;


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Data_Test__Invalidated_Buffer__Assert_Expected_Exception()  /* working */  {
            //**  groundwork  **//
            BufferPool_.DataReader = this.ReadData;

            Buffer_ subject = new Buffer_(9000);
            BufferProxy_ target = new BufferProxy_(subject);

            string expectedExceptionText = "The buffer you are addressing has been invalidated.";
            string actualExceptionText = null;

            byte[] expectedData = null;
            byte[] actualData = null;


            //**  exercising the code  **//
            subject.Invalidated = true;

            try {
                actualData = target.Data;
            }
            catch (Exception x) {
                actualExceptionText = x.Message;
            }


            //**  testing  **//
            Assert.AreEqual(expectedExceptionText, actualExceptionText);

            CollectionAssert.AreEqual(expectedData, actualData);
        }

        [TestMethod()]
        public void Data_Test__Invalidated_Proxy__Assert_Expected_Exception()  /* working */  {
            //**  groundwork  **//
            BufferPool_.DataReader = this.ReadData;

            Buffer_ subject = new Buffer_(9000);
            BufferProxy_ target = new BufferProxy_(subject);

            string expectedExceptionText = "The buffer you are addressing has been invalidated.";
            string actualExceptionText = null;

            byte[] expectedData = null;
            byte[] actualData = null;


            //**  exercising the code  **//
            subject.Invalidated = true;

            try {
                actualData = target.Data;
            }
            catch (Exception x) {
                actualExceptionText = x.Message;
            }


            //**  testing  **//
            Assert.AreEqual(expectedExceptionText, actualExceptionText);

            CollectionAssert.AreEqual(expectedData, actualData);
        }

        [TestMethod()]
        public void Data_Test__Setter_Called__2_Asserts__Assert_Expected_Data_In_Buffer_And_Altered_Flag_In_Buffer_Set()  /* working */  {
            //**  groundwork  **//
            BufferPool_.DataReader = this.ReadData;

            Buffer_ subject = new Buffer_(100);
            BufferProxy_ target = new BufferProxy_(subject);

            byte[] expectedData = { 8, 16, 32, 64 };
            byte[] actualData;

            bool expectedFlag = true;
            bool actualFlag;


            //**  exercising the code  **//
            target.Data = expectedData;


            //**  testing  **//
            actualData = subject.Data;
            CollectionAssert.AreEqual(expectedData, actualData);

            actualFlag = subject.Altered;
            Assert.AreEqual(expectedFlag, actualFlag);
        }

        [TestMethod()]
        public void Release_Test__Valid_Call__2_Asserts__Assert_Expected_Retains_On_Buffer_And_Invalidated_Flag_Set()  /* working */  {
            //**  groundwork  **//
            Buffer_ subject = new Buffer_(9);
            BufferProxy_ target = new BufferProxy_(subject);

            PrivateObject p = new PrivateObject(subject);

            int expected = 3;
            int actual;


            //**  exercising the code  **//
            // a total of 4 retains ( since proxy not obtained through pool's acquire method ) 
            subject.Retain();
            subject.Retain();
            subject.Retain();
            subject.Retain();

            // dropping one retain and invalidating; then just invalidating, without effect 
            target.Release();
            target.Release();

            actual = (int)p.GetField("_retains");


            //**  testing  **//
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(true, target.Invalidated);
        }

        [TestMethod()]
        public void FlushPool_Test__Valid_Call__5_Asserts__As_Remarked()  /* working */  {
            /* asserts :  writer is invoked; expected written data; pool length is zero; a proxy is invalidated; getting data from proxy throws exception */

            //**  groundwork  **//
            BufferPool_ target = new BufferPool_(8, 8, this.ReadData, this.WriteData);
            List_DoublyLinked_<Buffer_> pool;
            PrivateObject p = null;

            BufferProxy_ proxy = null;

            bool expectedWriterInvoked;
            bool actualWriterInvoked;

            byte[] expectedWrittenData;
            byte[] actualWrittenData;

            int expectedPoolListLength;
            int actualPoolListLength;

            bool expectedProxyInvalidated = true;
            bool actualProxyInvalidated;

            byte[] proxyData = null;
            string expectedDataExceptionText = "The buffer you are addressing has been invalidated.";
            string actualDataExceptionText = null;


            // buffers end up in reverse order; 3rd from end (5th buffer, at index 4) to be written out 
            for (int ix = 0; ix < 8; ix++) {
                proxy = target.AcquireBuffer(ix);
            }

            // reading an existing buffer; this moves it to the front 
            proxy = target.AcquireBuffer(3);

            // writing to the buffer so that a flush will write it out 
            proxy.Data = new byte[] { 2, 4, 6, 8, 10, 12 };

            this.CleanseData();
            this.CleanseResults();
            this.ClearWroteData();


            //**  exercising the code  **//
            // all buffers are invalidated, even if currently in use; altered ones written to disk 
            target.FlushPool();

            // getting data from an invalidated buffer should fail 
            try {
                proxyData = proxy.Data;
            }
            catch (Exception x) {
                actualDataExceptionText = x.Message;
            }

            //**  testing  **//
            expectedWriterInvoked = true;
            actualWriterInvoked = this._wroteData;
            Assert.AreEqual(expectedWriterInvoked, actualWriterInvoked);

            expectedWrittenData = new byte[] { 2, 4, 6, 8, 10, 12 };   // changed data added before flush call 
            actualWrittenData = this._data;
            CollectionAssert.AreEqual(expectedWrittenData, actualWrittenData);

            p = new PrivateObject(target);
            pool = (List_DoublyLinked_<Buffer_>)p.GetField("_pool");
            expectedPoolListLength = 0;
            actualPoolListLength = pool.Length;
            Assert.AreEqual(expectedPoolListLength, actualPoolListLength);

            actualProxyInvalidated = proxy.Invalidated;
            Assert.AreEqual(expectedProxyInvalidated, actualProxyInvalidated);

            Assert.AreEqual(expectedDataExceptionText, actualDataExceptionText);
        }

    }
}
