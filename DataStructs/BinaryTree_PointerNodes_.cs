﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_PointerNodes_<K, T>
    {
        #region Definitions

        public delegate object Traverser(object topic, PointerNode_<K, T> node);

        #endregion Definitions


        #region Fields

        PointerNode_<K, T> _root;

        #endregion Fields


        #region Properties

        // a Parent property / method is not supported for pointer-backed trees, 
        //     on the assumption that they aren't used like graphs; 
        // no L-child or R-child properties; those are provided by the nodes themselves 

        public PointerNode_<K, T> Root {
            get { return _root; }
            set { _root = value; }
        }

        #endregion Properties


        #region Constructors

        public BinaryTree_PointerNodes_(K key, T element)   // passed  
        {
            _root = new PointerNode_<K, T>(key, element);
        }

        #endregion Constructors


        #region Traversals

        public object TraversePreorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePreorder(t, results, this._root);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified   
        {
            // first the current node
            topic = t(topic, node);

            // then its L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePreorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePreorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraverseInorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecurseInorder(t, results, this._root);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified  
        {
            // first the L child / subtree
            if (node.LeftChild != null)
                topic = RecurseInorder(t, topic, node.LeftChild);

            // then the current node
            topic = t(topic, node);

            // then the R child / subtree
            if (node.RightChild != null)
                topic = RecurseInorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraversePostorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePostorder(t, results, this._root);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified  
        {
            // first the L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePostorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePostorder(t, topic, node.RightChild);

            // then the current node
            topic = t(topic, node);

            return topic;
        }

        #endregion Traversals


        #region Breadth-First Traversals

        /* so, it turns out that the way to do a breadth-first preorder traversal (at least, to do so easily) is with a queue */
        /* I call it "queuecursion" or "qcursion"; so, I'm trying it out here now */
        /* as written, sloppy because takes two possible approaches, but for edu purposes, that's good enough */

        /* for breadth-first postorder traversal, a stack is good choice, but you must first build it as you would 
         * for a preorder traversal, actually using a queue, then reversing the elements at each new level as you copy to a stack */
        /* "stackcursion" is my term for using a stack instead of recursion, but since queueing is also involved here, this is really "qstackcursion" */

        public object TraverseBreadthFirstPreorder(Traverser t)  /* passed */  {
            object results = null;

            /* this is a queue of pointer nodes because the tree is made up of pointer nodes */
            Queue_<PointerNode_<K, T>> queue = Queue_<PointerNode_<K, T>>.SupplyQueue_T(QueueType.Array);


            /* the queueing and ops */
            queue.Enqueue(this._root);

            while (queue.Length > 0) {
                // get / dequeue current node, found at current start of queue 
                PointerNode_<K, T> current = queue.Dequeue();

                // handle this node 
                results = t(results, current);

                /* add next nodes as encountered; their child nodes will be added to the queue directly after they are dequeued */
                if (current.LeftChild != null)
                    queue.Enqueue(current.LeftChild);

                if (current.RightChild != null)
                    queue.Enqueue(current.RightChild);

                /*  because this is a binary tree, I can traverse simply by directly retrieving L and R children, but I have to skip missing children */
                /*  with a general tree, I would instead traverse children using a loop */
            }


            /* convey outward */
            return results;
        }

        /* no recursor needed, because qcursion */

        public object TraverseBreadthFirstPostorder(Traverser t)  /* passed */  {
            object results = null;


            /* these are a queue and stack of pointer nodes because the tree is made up of pointer nodes */
            Queue_<PointerNode_<K, T>> queue = Queue_<PointerNode_<K, T>>.SupplyQueue_T(QueueType.Array);
            Stack_<PointerNode_<K, T>> stack = Stack_<PointerNode_<K, T>>.SupplyStack_T(StackType.LinkedListBased);


            /* the queueing and stacking, but not the ops */
            queue.Enqueue(this._root);

            while (queue.Length > 0) {
                // dequeue this node and add it to the stack 
                PointerNode_<K, T> current = queue.Dequeue();
                stack.Push(current);

                /* add next nodes in reverse order; their child nodes will be added to the queue directly after they are dequeued */
                if (current.RightChild != null)
                    queue.Enqueue(current.RightChild);

                if (current.LeftChild != null)
                    queue.Enqueue(current.LeftChild);

                /*  because this is a binary tree, I can traverse simply by directly retrieving R and L children, but I have to skip missing children */
                /*  with a general tree, better to loop, adding children to a local stack, then pop them to the queue in reverse order, for later dequeueing to main stack */
            }


            /* operations on the stacked nodes */
            while (stack.Length > 0)
                results = t(results, stack.Pop());


            /* convey outward */
            return results;
        }

        /* no recursor, because stackcursion / qstackcursion */

        #endregion Breadth-First Traversals

    }

    public class PointerNode_<K, T>
    {
        #region Fields

        K _key;
        T _element;
        PointerNode_<K, T> _leftChild;
        PointerNode_<K, T> _rightChild;

        #endregion Fields


        #region Properties

        public K Key {
            get { return _key; }
            set { _key = value; }
        }

        public T Element {
            get { return _element; }
            set { _element = value; }
        }

        public PointerNode_<K, T> LeftChild {
            get { return _leftChild; }
            set { _leftChild = value; }
        }

        public PointerNode_<K, T> RightChild {
            get { return _rightChild; }
            set { _rightChild = value; }
        }

        public bool IsLeaf    // passed  
        {
            get { return ((this._leftChild == null) && (this._rightChild == null)); }
        }

        #endregion Properties


        #region Constructors

        public PointerNode_(K key, T element) {
            _key = key;
            _element = element;
            _leftChild = null;
            _rightChild = null;
        }

        public PointerNode_(PointerNode_<K, T> leftChild, PointerNode_<K, T> rightChild) {
            _leftChild = leftChild;
            _rightChild = rightChild;
        }

        #endregion Constructors


        // no methods
    }

}