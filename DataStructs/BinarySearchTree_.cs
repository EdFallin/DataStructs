﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinarySearchTree_<K, T> : Dictionary_<K, T> where K : IComparable<K>
    {
        #region Definitions

        public delegate object Traverser(object topic, PointerNode_<K, T> node);

        // used only with iterative handling, of dropping an entry at least
        private enum Side
        {
            None,
            Left,
            Right,
        }

        #endregion Definitions


        #region Fields

        PointerNode_<K, T> _root = null;
        int _count = 0;

        PointerNode_<K, T> _sought = null;
        PointerNode_<K, T> _swap = null;

        #endregion Fields


        #region Properties

        // may be useful primarily for testing
        public PointerNode_<K, T> Root
        {
            get { return _root; }
        }

        public int Count
        {
            get { return _count; }
        }

        #endregion Properties


        #region Constructors

        public BinarySearchTree_(K key, T element)
        {
            _root = new PointerNode_<K, T>(key, element);
            _count = 1;
        }

        #endregion Constructors


        #region Methods

        public override void AddEntry(K key, T element)   // passed  
        {
            // all crux operations are performed via the recursor
            this.AddRecursor(_root, key, element);

            // metadata
            _count++;
        }

        public override void Clear()   // ok  
        {
            // GC will collect all nodes
            _root = null;

            // metadata
            _count = 0;
        }

        public override T DropAnEntry()   // passed  
        {
            // just drop the current root and do the usual repointing and/or swapping

            K key = _root.Key;
            T results = DropEntry(key);

            return results;
        }

        public override T DropEntry(K key)   // passed  
        {
            // all 3 algos here tested and passed tests;
            // currently using standard algo

            #region Standard Algo, As Found In Reftext

            // finds node first and retains value of sought node,
            // then handles deleting using exfactored ops

            T results = default(T);

            _sought = FindRecursor(_root, key);
            results = _sought.Element;

            // crux ops are almost all in this recursor; in some cases it invokes another 2 recursors
            _root = DropRecursor(_root, key);

            // metadata
            _count--;

            // clearing metadata
            _sought = null;
            _swap = null;

            return results;

            #endregion Standard Algo, As Found In Reftext


            #region My Algo, Reducing Recursive Traversals

            //T results = default(T);

            //// crux ops are all in this recursor except retrieving results and handling metadata
            //_root = FindSwapDropRecursor(_root, key);

            //results = _sought.Element;

            //// metadata
            //_count--;

            //// clearing metadata
            //_sought = null;
            //_swap = null;

            //return results;

            #endregion My Algo, Reducing Recursive Traversals


            #region My Non-Recursive Algo

            //T results = DropIteratively(key);

            //_count--;
            //_sought = null;
            //_swap = null;

            //return results;

            #endregion My Non-Recursive Algo

        }

        public override T GetEntry(K key)   // passed  
        {
            // if the key isn't found, default(T) is returned

            // all crux operations are performed via the recursor
            PointerNode_<K, T> sought = FindRecursor(_root, key);
            T entry = sought.Element;

            return entry;
        }

        public override void SetEntry(K key, T element)   // passed  
        {
            PointerNode_<K, T> sought = FindRecursor(_root, key);
            sought.Element = element;
        }

        #endregion Methods


        #region Internals

        private PointerNode_<K, T> AddRecursor(PointerNode_<K, T> node, K key, T element)   // verified  
        {
            // if the node we've branched to is null, return a new node and exit;
            // if we haven't yet reached a null node, recurse to either L or R child and point to the output;
            // cumulatively, this sets all pointers in a chain from added node to starting node when recursion stack collapses

            if (node == null)
                return new PointerNode_<K, T>(key, element);

            // branch to L child at this level if `key is < node's key, otherwise R child (including when keys are equal);
            // recursion sets up a chain of pointer assignments that is performed in reverse order when final node is init'ed
            if (node.Key.CompareTo(key) > 0)
                node.LeftChild = AddRecursor(node.LeftChild, key, element);
            else
                node.RightChild = AddRecursor(node.RightChild, key, element);

            // return the current node, so it will be pointed to in one of the branches above when the recursion stack collapses;
            // this is where all passing-back occurs except at the end when a null is found and the new node is added
            return node;
        }

        private PointerNode_<K, T> FindRecursor(PointerNode_<K, T> node, K key)   // verified  
        {
            // if keys are equal, return `node.Element;
            // otherwise branch to L or R child and invoke recursor;
            // value eventually is passed back as recursion stack collapses;
            // 
            // if no value matches, returns a default; I might have a deliberate fail-value in working code

            if (node == null)
                return null;

            if (node.Key.Equals(key))
                return node;

            // these can return a null node
            if (node.Key.CompareTo(key) > 0)
            {
                return FindRecursor(node.LeftChild, key);
            }
            else
            {
                return FindRecursor(node.RightChild, key);
            }
        }

        private PointerNode_<K, T> DropRecursor(PointerNode_<K, T> node, K key)   // verified  
        {
            // reftext also checks if `node is null; this recursor always starts at _root,
            // so that's only needed if consumer is not supersafe


            // first finds sought, then returns a node that's either sought, with new values, or replaces sought;
            // node is entrained in repointing at rcollapse

            // find along, branching L or R, but not if keys are equal
            if (node.Key.CompareTo(key) > 0)
                node.LeftChild = DropRecursor(node.LeftChild, key);
            else if (node.Key.CompareTo(key) < 0)
                node.RightChild = DropRecursor(node.RightChild, key);

            // when keys are equal, either return a new node instead of the current one,
            // or find node with values to swap, swap them in, return same node, and drop swap-node
            if (node.Key.Equals(key))
            {
                // dropping this node during repointing at rcollapse; it was already found outside, so it isn't needed
                if (node.IsLeaf)
                    return null;

                // when not a leaf, if one is empty, the other is present;
                // this node is replaced by its only child during repointing at rcollapse
                if (node.LeftChild == null)
                    return node.RightChild;

                if (node.RightChild == null)
                    return node.LeftChild;

                // if both children are present, least descendant in R subtree is found and its values are swapped in;
                // no repointing, instead returning same node, since this node's position isn't actually changing;
                // least R-side descendant is dropped using exfactored ops and exmethod also repoints in its own rcollapse

                _swap = FindLeastRecursor(node.RightChild);

                node.Key = _swap.Key;
                node.Element = _swap.Element;

                // if node's R child is a leaf, this repoints it to null;
                // otherwise no change at this level, but at end of subtree, least node is dropped
                node.RightChild = RemoveLeastRecursor(node.RightChild);
            }

            // the now-current node is returned if we haven't already exited; it most likely has new contents
            return node;
        }

        private PointerNode_<K, T> RemoveLeastRecursor(PointerNode_<K, T> node)   // passed
        {
            // when recursion stack collapses, sets second-to-last L child to the R child of the now-removed last L child;
            // sets all L children back to starting node during the collapse; retains the node removed for use elsewhere

            if (node.LeftChild != null)
            {
                // recursion
                node.LeftChild = RemoveLeastRecursor(node.LeftChild);

                // this node is passed upward, so that except for the last / least node,
                // each node's L child is reassigned to its existing L child
                return node;
            }
            else
            {
                // when recursion stack collapses and this is returned, the effect is to assign
                // the parent node's L child to the R child of what was the parent's L child (which may be empty)
                return node.RightChild;
            }
        }

        private PointerNode_<K, T> FindLeastRecursor(PointerNode_<K, T> node)   // passed  
        {
            // similar idea as RemoveLeastRecursor(), but without the repointing or removing;
            // move to last L child in `node's subtree and return it

            if (node.LeftChild != null)
                node = FindLeastRecursor(node.LeftChild);

            // at collapse of rstack, node is passed progressively to original call
            return node;
        }

        #endregion Internals


        #region Alternate Internals

        #region Simplified Recursion

        private PointerNode_<K, T> FindSwapDropRecursor(PointerNode_<K, T> node, K key)   // verified  
        {
            // this system is what I originally intended instead of reftext's form;
            // my version here tunes DropEntry(), reducing ϴ from 3(log(n)) to plain log(n);


            // finds sought, retains it for outside, then takes repointing steps; or else calls exmethod that retains _swap
            // but drops it from tree, then repoints all at rcollapse

            // find along, branching L or R, but not if keys are equal
            if (node.Key.CompareTo(key) > 0)
                node.LeftChild = FindSwapDropRecursor(node.LeftChild, key);
            else if (node.Key.CompareTo(key) < 0)
                node.RightChild = FindSwapDropRecursor(node.RightChild, key);

            // just like other version generally, when keys equal, return a new node instead of
            // current, or swap values with another one that's dropped
            if (node.Key.Equals(key))
            {
                // retain node as separate metadata temporarily, so it isn't lost if `node's values are reassigned
                _sought = new PointerNode_<K, T>(node.Key, node.Element);

                // dropping this node during repointing at rcollapse; it was already found outside, so it isn't needed
                if (node.IsLeaf)
                    return null;

                // when not a leaf, if one is empty, the other is present;
                // this node is replaced by its only child during repointing at rcollapse
                if (node.LeftChild == null)
                    return node.RightChild;

                if (node.RightChild == null)
                    return node.LeftChild;

                // if both children are present, least R-subtree node is retained as _swap, then dropped with repointing at rcollapse
                node.RightChild = SwapRemoveLeastRecursor(node.RightChild);

                // values are swapped without altering `node's pointers; _swap should never be null, basically
                node.Key = _swap.Key;
                node.Element = _swap.Element;
            }

            // the now-current node is returned if we haven't already exited; it most likely has new contents
            return node;
        }

        private PointerNode_<K, T> SwapRemoveLeastRecursor(PointerNode_<K, T> node)   // verified  
        {
            // when recursion stack collapses, sets second-to-last L child to the R child of the now-removed last L child;
            // sets all L children back to starting node during the collapse; retains the node removed for use elsewhere

            if (node.LeftChild != null)
            {
                // recursion
                node.LeftChild = SwapRemoveLeastRecursor(node.LeftChild);

                // this node is passed upward, so that except for the last / least node,
                // each node's L child is reassigned to its existing L child
                return node;
            }
            else
            {
                // retaining the contents of this node, since it's the one with contents to be swapped
                _swap = node;

                // when recursion stack collapses and this is returned, the effect is to assign
                // the parent node's L child to the R child of what was the parent's L child (which may be empty)
                return node.RightChild;
            }
        }

        #endregion Simplified Recursion


        #region Non-Recursive Algorithms

        // these loop algorithms can be used instead of recursive methods to do all crux operations
        // in AddEntry() ( using AddIteratively() ), in GetEntry() ( using FindIteratively() ),
        // and in DropEntry() and DropAnEntry() ( using DropIteratively() and its AssignToChosenSide() )

        private void AddIteratively(K key, T element)   // verified  
        {
            // loop, branching L and R, checking at each child till null,
            // then adding new where that empty child is

            PointerNode_<K, T> current = _root;
            PointerNode_<K, T> prior = null;
            PointerNode_<K, T> next = new PointerNode_<K, T>(key, element);

            while (current != null)
            {
                prior = current;

                if (current.Key.CompareTo(key) > 0)
                {
                    current = current.LeftChild;

                    if (current == null)
                        prior.LeftChild = next;
                }
                else
                {
                    current = current.RightChild;

                    if (current == null)
                        prior.RightChild = next;
                }
            }
        }

        private T FindIteratively(K key)   // verified  
        {
            T results = default(T);

            PointerNode_<K, T> current = _root;

            // loops once past the sought key, if it is found
            while (current != null)
            {
                if (current.Key.CompareTo(key) > 0)
                    current = current.LeftChild;
                else
                    current = current.RightChild;

                if (current != null)
                {
                    if (current.Key.Equals(key))
                        results = current.Element;
                }
            }

            return results;
        }

        private T DropIteratively(K key)   // verified  
        {
            T results = default(T);


            // just in case
            _sought = null;
            _swap = null;

            // used for repointing
            PointerNode_<K, T> current = _root;
            PointerNode_<K, T> previous = current;
            Side side = Side.None;


            // starting at root, loop through children until sought, then branch to each case,
            // and if two children, loop down L children of R subtree until done
            while (_sought == null)
            {
                // retaining parent for repointing
                previous = current;

                // no `== null` testing because I'm assuming the consumer is supersafe

                // branch L or R until key matches, then restart loop
                if (current.Key.CompareTo(key) > 0)
                {
                    current = current.LeftChild;
                    side = Side.Left;
                    continue;
                }

                if (current.Key.CompareTo(key) < 0)
                {
                    current = current.RightChild;
                    side = Side.Right;
                    continue;
                }

                // retain a matched node and exit loop
                if (current.Key.Equals(key))
                    _sought = current;
            }


            // retain value for output
            results = _sought.Element;


            // if sought is a leaf, set the pointer to it to null, dropping it, then exit
            if (_sought.IsLeaf)
                AssignToChosenSide(side, previous, null);

            // if sought isn't a leaf but only has one child, set the pointer to its sole child, then exit
            else if (_sought.LeftChild == null)
                AssignToChosenSide(side, previous, current.RightChild);
            
            else if (_sought.RightChild == null)
                AssignToChosenSide(side, previous, current.RightChild);

            // if sought has two children, find least node in its R subtree, swap values in, and drop least node, then exit
            else
            {
                // rearranging to start at R child of sought with parent node available
                previous = current;
                current = current.RightChild;

                // in case R subtree consists only of subroot, allows proper repointing
                side = Side.Right;

                // moving downward through R subtree, looking for least, and assigning its R child if any, otherwise null,
                // to R child (of sought) if R subroot is least, otherwise to L child of parent of least
                while (_swap == null)
                {
                    // moving along L children; after we've done this once, we know we'll be repointing from
                    // R child of least to L child of least's parent, so `side is switched
                    if (current.LeftChild != null)
                    {
                        previous = current;
                        current = current.LeftChild;
                        side = Side.Left;
                    }
                    else
                    {
                        // retain least node, then drop it by setting the correct pointer at its parent to its R child (which may be null)
                        _swap = current;
                        AssignToChosenSide(side, previous, current.RightChild);
                    }
                }

                // swap in values from least node of R subtree, preserving BST Property
                _sought.Key = _swap.Key;
                _sought.Element = _swap.Element;
            }

            // convey output
            return results;
        }

        private void AssignToChosenSide(Side side, PointerNode_<K, T> previous, PointerNode_<K, T> current)   // verified  
        {
            switch (side)
            {
                case Side.Left:
                    previous.LeftChild = current;
                    break;
                case Side.Right:
                    previous.RightChild = current;
                    break;
                case Side.None:
                // fall-through
                default:
                    throw new Exception("Side.None encountered, indicating that an algo has not worked correctly");
            }
        }

        #endregion Non-Recursive Algorithms

        #endregion Alternate Internals


        #region Traversals

        public object TraversePreorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePreorder(t, results, this._root);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified  
        {
            // first the current node
            topic = t(topic, node);

            // then its L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePreorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePreorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraverseInorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecurseInorder(t, results, this._root);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified  
        {
            // first the L child / subtree
            if (node.LeftChild != null)
                topic = RecurseInorder(t, topic, node.LeftChild);

            // then the current node
            topic = t(topic, node);

            // then the R child / subtree
            if (node.RightChild != null)
                topic = RecurseInorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraversePostorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePostorder(t, results, this._root);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, PointerNode_<K, T> node)   // verified  
        {
            // first the L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePostorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePostorder(t, topic, node.RightChild);

            // then the current node
            topic = t(topic, node);

            return topic;
        }

        #endregion Traversals

    }
}
