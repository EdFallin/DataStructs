﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructs
{
    public static class WiseBits
    {
        public static byte ToByte(this string binary)   // passed  
        {
            // final arg indicates base-2 (binary)
            byte result = Convert.ToByte(binary, 2);
            return result;
        }

        public static byte AsByte(string binary)   // passed  
        {
            // final arg indicates base-2 (binary)
            byte result = Convert.ToByte(binary, 2);
            return result;
        }

        // can't override .ToString() without parameters
        public static string ToBinaryString(this byte binary)   // passed  
        {
            return WiseBits.AsString(binary);
        }

        public static string AsString(byte binary)   // working  
        {
            // the number is converted to a possibly-short string of 1s and 0s because the final arg specifies base-2;
            // the number is padded with anywhere from 0 to 7 0s to make a full octet

            string passer = Convert.ToString(binary, 2);
            string result = passer.PadLeft(8, '0');

            return result;
        }
    }
}
