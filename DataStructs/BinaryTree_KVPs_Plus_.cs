﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_KVPs_Plus_<K, T>
    {
        #region Definitions

        public delegate object Traverser(object topic, CompositePointerNode_<K, T> node);

        #endregion Definitions


        #region Fields

        CompositePointerNode_<K, T> _root;

        #endregion Fields


        #region Properties

        // a Parent property / method is not supported for pointer-backed trees, 
        //     on the assumption that they aren't used like graphs; 
        // no L-child or R-child properties; those are provided by the nodes themselves 

        public CompositePointerNode_<K, T> Root  /* ok */  {
            get { return _root; }
            set { _root = value; }
        }

        #endregion Properties


        #region Constructors

        public BinaryTree_KVPs_Plus_(K key, T element)  /* passed */  {
            // 4-arg form inits an inode; assigned to metadata right away 
            _root = CompositePointerNode_<K, T>.InitCompositePointerNode_(key, element, null, null);
        }

        public BinaryTree_KVPs_Plus_()  /* ok */  {
            // this constructor exists only to allow use within another structure, like a Huffman Coding Tree 

            /* no operations */
        }

        #endregion Constructors


        #region Traversals

        public object TraversePreorder(Traverser t)  /* passed */  {
            object results = null;

            results = RecursePreorder(t, results, this._root);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, CompositePointerNode_<K, T> node)  /* verified */  {
            // first the current node 
            topic = t(topic, node);

            // then its L and R children / subtrees 
            if (node.LeftChild != null)
                topic = RecursePreorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePreorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraverseInorder(Traverser t)  /* passed */  {
            object results = null;

            results = RecurseInorder(t, results, this._root);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, CompositePointerNode_<K, T> node)  /* verified */  {
            // first the L child / subtree 
            if (node.LeftChild != null)
                topic = RecurseInorder(t, topic, node.LeftChild);

            // then the current node 
            topic = t(topic, node);

            // then the R child / subtree 
            if (node.RightChild != null)
                topic = RecurseInorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraversePostorder(Traverser t)  /* passed */  {
            object results = null;

            results = RecursePostorder(t, results, this._root);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, CompositePointerNode_<K, T> node)  /* verified */  {
            // first the L and R children / subtrees 
            if (node.LeftChild != null)
                topic = RecursePostorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePostorder(t, topic, node.RightChild);

            // then the current node 
            topic = t(topic, node);

            return topic;
        }

        #endregion Traversals


        #region Indexing

        /*  --> what I need to do here is figure out the indexing and incrementing; I need index to increase at each node visited, 
         *  but not to cause problems because I init it back one (at -1) or because I mess it up in other ways;  I think I should 
         *  draw up a diagram and look it over before coding any more  */

        public CompositePointerNode_<K, T> this[int inOrderIndex] {
            get {
                int currentIndex = 0;
                CompositePointerNode_<K, T> result = this.RecurseInorderToIndex(inOrderIndex, ref currentIndex, this._root);
                return result;
            }
        }

        public CompositePointerNode_<K, T> this[K inOrderKey] {
            get { return null; }
        }


        private CompositePointerNode_<K, T> RecurseInorderToIndex(int targetIndex, ref int currentIndex, CompositePointerNode_<K, T> node) {
            CompositePointerNode_<K, T> target = null;
            // first the L child / subtree; must always be recursed 
            if (node.LeftChild != null) {
                target = RecurseInorderToIndex(targetIndex, ref currentIndex, node.LeftChild);
            }

            // then the current node; index is always incrd here, and only here, for +1 per in-order node 
            if (currentIndex++ == targetIndex) {
                target = node;
                return target;
            }

            // then the R child / subtree; can be skipped once targeted is found 
            if (node.RightChild != null) {
                target = RecurseInorderToIndex(targetIndex, ref currentIndex, node.RightChild);
            }

            /* a fail condition */
            return null;
        }



        #endregion Indexing


    }
}
