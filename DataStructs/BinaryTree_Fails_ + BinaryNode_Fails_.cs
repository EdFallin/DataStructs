﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_Fails_<K, T> : BinaryTree_<K, T>
    {
        BinaryNode_Fails_<K, T> tree = null;

        public BinaryTree_Fails_()
        {
            tree = new BinaryNode_Fails_<K, T>();
        }


        #region Dictionary_ Implementations

        public override K FailKey
        {
            get
            {
                return (this._failKey);
            }
            set
            {
                this._failKey = value;
            }
        }

        public override T FailValue
        {
            get
            {
                return (this._failValue);
            }
            set
            {
                this._failValue = value;
            }
        }

        public override void AddEntry(K key, T element)
        {
            //  no operations
        }

        public override void Clear()
        {
            //  no operations
        }

        public override T DropAnEntry()
        {
            return (this._failValue);
        }

        public override T DropEntry(K key)
        {
            return (this._failValue);
        }

        public override T GetEntry(K key)
        {
            return (this._failValue);
        }

        public override void SetEntry(K key, T element)
        {
            //  no operations
        }

        #endregion Dictionary_ Implementations


        #region Current Node

        public override K NodeKey
        {
            get
            {
                return (this._failKey);
            }
            set
            {
                //  no operations
            }
        }

        public override T NodeElement
        {
            get
            {
                return (this._failValue);
            }
            set
            {
                //  no operations
            }
        }

        public override bool NodeIsLeaf
        {
            get
            {
                return (false);
                return (true);
            }
        }

        #endregion Current Node


        #region Changing Tree Contents

        public override void AddLeftChild(K key, T element)
        {
            //  no operations
        }

        public override void AddRightChild(K key, T element)
        {
            //  no operations
        }

        public override void DropLeftChild()
        {
            //  no operations
        }

        public override void DropRightChild()
        {
            //  no operations
        }

        #endregion Changing Tree Contents


        #region Stepwise Traversal

        public override void MoveToRoot()
        {
            //  no operations
        }

        public override void MoveToLeftChild()
        {
            //  no operations
        }

        public override void MoveToRightChild()
        {
            //  no operations
        }

        #endregion Stepwise Traversal


        #region Left Child Node

        public override K LeftChildNodeKey
        {
            get
            {
                return (this._failKey);
            }
            set
            {
                //  no operations
            }
        }

        public override T LeftChildNodeElement
        {
            get
            {
                return (this._failValue);
            }
            set
            {
                //  no operations
            }
        }

        #endregion Left Child Node


        #region Right Child Node

        public override K RightChildNodeKey
        {
            get
            {
                return (this._failKey);
            }
            set
            {
                //  no operations
            }
        }

        public override T RightChildNodeElement
        {
            get
            {
                return (this._failValue);
            }
            set
            {
                //  no operations
            }
        }

        #endregion Right Child Node


        #region Traversals

        public override object[] TraversePreOrder(TraversalOperation t)
        {
            return (new object[] { null });
        }

        public override object[] TraverseInOrder(TraversalOperation t)
        {
            return (new object[] { null });
        }

        public override object[] TraversePostOrder(TraversalOperation t)
        {
            return (new object[] { null });
        }

        #endregion Traversals

    }

    public class BinaryNode_Fails_<K, T> : BinaryNode_<K, T>   //  OK  
    {
        //  these can be set as needed to ensure a test fails
        #region Fail Fields

        public bool isLeaf;
        public K key = default(K);
        public T element = default(T);

        public BinaryNode_Fails_<K, T> nonChild = null;

        #endregion Fail Fields


        #region Properties (Including Child Nodes)

        override public K Key
        {
            get { return (key); }
            set { }
        }

        override public T Element
        {
            get { return (element); }
            set { }
        }

        override public BinaryNode_<K, T> LeftChild
        {
            get { return (nonChild); }
            set { }
        }

        override public BinaryNode_<K, T> RightChild
        {
            get { return (nonChild); }
            set { }
        }

        override public bool IsLeaf
        {
            get { return (isLeaf); }
        }

        #endregion Properties (Including Child Nodes)


        public BinaryNode_Fails_()
        {
            //  no operations; fail values should be set using public fail fields
        }

    }
}