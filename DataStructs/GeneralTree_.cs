﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    /*  this is the gamma-tree design; a capital gamma ( Γ ) looks like the way each node's two pointers are directed, 
     *  and the resulting structure if not abstracted to a normal tree representation */

    public class GeneralTree_Node_<T>   // ok; essentially all trivial; only .IsALeaf tested  
    {
        #region Fields

        T _content;

        GeneralTree_Node_<T> _firstChild;
        GeneralTree_Node_<T> _rightSibling;

        #endregion Fields


        #region Properties

        public T Content {
            get { return _content; }
            set { _content = value; }
        }

        public GeneralTree_Node_<T> FirstChild {
            get { return _firstChild; }
            set { _firstChild = value; }
        }

        public GeneralTree_Node_<T> RightSibling {
            get { return _rightSibling; }
            set { _rightSibling = value; }
        }

        public bool IsALeaf {
            get { return (this._firstChild == null); }
        }

        #endregion Properties


        #region Constructors

        public GeneralTree_Node_(T content) {
            _content = content;
            _firstChild = null;
            _rightSibling = null;
        }

        #endregion Constructors
    }


    public class GeneralTree_<T>
    {
        #region Fields

        GeneralTree_Node_<T> _root = null;

        #endregion Fields


        #region Properties

        public GeneralTree_Node_<T> Root    // ok  
        {
            get { return _root; }
        }

        #endregion Properties


        #region Constructors

        public GeneralTree_(T rootContent)   // ok  
        {
            _root = new GeneralTree_Node_<T>(rootContent);
        }

        #endregion Constructors


        #region Basic Tree Methods

        public void AddChild(GeneralTree_Node_<T> parent, T content)   // passed  
        {
            // add a first child to `parent if no children;
            // otherwise, add new child at the end

            if (parent.FirstChild == null) {
                parent.FirstChild = new GeneralTree_Node_<T>(content);
            }
            else {
                // step along children to last, add sibling
                GeneralTree_Node_<T> current = parent.FirstChild;

                while (current.RightSibling != null)
                    current = current.RightSibling;

                current.RightSibling = new GeneralTree_Node_<T>(content);
            }
        }

        public void RemoveFirstChild(GeneralTree_Node_<T> parent)   // passed  
        {
            // remove first child, dropping its subtree, and make
            // its any right sibling the new first child

            if (parent.FirstChild != null) {
                // if `parent only has one child, this has the same effect as directly setting it to null
                GeneralTree_Node_<T> next = parent.FirstChild.RightSibling;
                parent.FirstChild = next;
            }
        }

        public void RemoveNextChild(GeneralTree_Node_<T> leftSibling)   // passed  
        {
            // remove the right sibling after the arg node
            // and point to any right sibling beyond it

            // this works properly with first child and/or when further sibling is null
            if (leftSibling.RightSibling != null)
                leftSibling.RightSibling = leftSibling.RightSibling.RightSibling;
        }

        public void RemoveLastChild(GeneralTree_Node_<T> parent)   // passed  
        {
            // remove the last child added and still present, dropping its subtree;
            // if the last present is the first child, drop it

            GeneralTree_Node_<T> next = parent.FirstChild;

            // no ops needed
            if (next == null)
                return;

            if (next.RightSibling == null) {
                // drop the first child if it is the only child, then exit
                parent.FirstChild = null;
                return;
            }

            // starting at first child, stepping to the second-to-last child;
            // this is an O(n) operation for n children
            while (next.RightSibling.RightSibling != null)
                next = next.RightSibling;

            // deleting the last from the second-to-last
            next.RightSibling = null;
        }

        #endregion Basic Tree Methods


        /* the traversals here don't support the visitor design pattern; in theory that wouldn't be too hard to implement */

        #region Preorder Traversal

        public T[] TraversePreOrder(int expectedCountOfNodes)   // passed  
        {
            T[] results = new T[expectedCountOfNodes];

            // `next is the index for the next value to add to results;
            // it must be passed by reference from the beginning
            int next = 0;

            // recursor has no return value; elements are added to `results
            PreOrderRecursor(this._root, results, ref next);

            return results;
        }

        public void PreOrderRecursor(GeneralTree_Node_<T> node, T[] results, ref int next)   // verified  
        {
            // `next is passed by reference so it is always current

            // this level
            results[next] = node.Content;

            // next level
            {
                node = node.FirstChild;

                while (node != null) {
                    next++;
                    PreOrderRecursor(node, results, ref next);
                    node = node.RightSibling;
                }
            }
        }

        #endregion Preorder Traversal


        #region Postorder Traversal

        public T[] TraversePostOrder(int expectedCountOfNodes)   // passed  
        {
            // same approach and same remarks as in TraversePreOrder(); `ref` for `next is key

            T[] results = new T[expectedCountOfNodes];
            int next = 0;

            PostOrderRecursor(this._root, results, ref next);

            return results;
        }

        private void PostOrderRecursor(GeneralTree_Node_<T> node, T[] results, ref int next)   // verified  
        {
            // retaining current node for later this-level ops
            GeneralTree_Node_<T> current = node;

            // next level
            {
                node = node.FirstChild;

                while (node != null) {
                    PostOrderRecursor(node, results, ref next);
                    node = node.RightSibling;

                    // incrementing must be done *after* the rcall to avoid over-advancing this index value
                    next++;
                }
            }

            // this level; at this point, `next may be far past its inbound arg value
            results[next] = current.Content;
        }

        #endregion Postorder Traversal

    }
}
