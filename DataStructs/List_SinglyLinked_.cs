﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // as in the reftext, this class keeps track of the position *before* that
    // reported to consuming code as current, so forward linking is simple and O(1)

    public class List_SinglyLinked_<T> : List_<T>
    {
        #region Fields

        // in the reftext, these are `head, `tail, and `current respectively 
        ForwardNode<T> _start;
        ForwardNode<T> _end;
        ForwardNode<T> _pointed;

        // _position is my idea here, as a way to eliminate some O(n) operations 
        int _length;
        int _position;

        #endregion Fields


        #region Constructors

        public List_SinglyLinked_(int length)
            : this() {
            // no operations; `length is ignored
        }

        public List_SinglyLinked_() {
            // _start is a node that always exists but is never used for data;
            // its existence evidently makes coding easier
            _start = new ForwardNode<T>(null);
            _pointed = _start;
            _end = _pointed;
            _position = 0;
            _length = 0;
        }

        #endregion Constructors


        #region Properties, Including Currents And Metadata

        public override int CurrentPosition  /* passed */ {
            // my implementation differs from the reftext here, which iterates from
            // `head to the current element, an O(n) operation;
            // by tracking position with _position ( _location's index + 1 ), I skip that;
            // 
            // however, I have to add this special case to return -1 when the list is empty
            // as my array-based class does
            get {
                if (_length == 0)
                    return -1;

                return _position;
            }
        }

        public override int Top   /* ok */ {
            // `Top is the highest 0-based index used in the list
            get { return (_length - 1); }
        }

        public override int Length   /* ok */  {
            get { return _length; }
        }

        public override T CurrentValue   /* passed */ {
            // _pointed always points at the node *before* what consumers see as current,
            // so this uses .Next of _pointed and then that's .Content;
            // same as reftext's implementation
            get {
                // if at the last value; `null isn't usable here, but otherwise same as reftext
                if (_pointed.Next == null)
                    return default(T);

                return _pointed.Next.Content;
            }
            set {
                // no setter in reftext;
                // 
                // this is a test for _end; I could add a new element here, but that duplicates .Append(),
                // so instead this does nothing if at the last value
                if (_pointed.Next == null)
                    return;

                _pointed.Next.Content = value;
            }
        }

        #endregion Properties, Including Currents And Metadata


        #region Consecutive Positioning

        public override void Next()  /* passed */  {
            // this moves to the next node, unlike the property of ForwardNode

            // unlike the reftext, I throw an error if at the end
            if (_pointed == _end)
                throw new Exception("_pointed == _end");

            // moving forward simply; after this move, new .Next points to
            // the following node
            _pointed = _pointed.Next;

            // this is my innovation to avoid O(n) elsewhere
            _position++;
        }

        public override void Previous()  /* passed */  {
            // similar to reftext, but instead of directly comparing contents of
            // each node, I just move forward using _position-1 as the target;
            // still an O(n) operation, but tuned, meaning faster than the other

            if (_pointed == _start)
                throw new Exception("_pointed == _start");

            ForwardNode<T> result = _start;

            for (int i = 0; i < _position - 1; i++)
                result = result.Next;

            // changing the internal location
            _pointed = result;

            // continuing my innovation
            _position--;
        }

        #endregion Consecutive Positioning


        #region Nonconsecutive Positioning

        public override void PositionAtEnd()  /* passed */  {
            // different from reftext, which seems to work incorrectly;
            // using an existing method to accomplish this trivially
            PositionAtIndex(this.Top);
        }

        public override void PositionAtIndex(int index)  /* passed */  {
            // not the same as reftext, but based on same idea;
            // here, I use my indexing to eliminate comparing Ts;
            // also a moderate reduction sometimes when target index is later in the list than the current index;
            // drag, the reftext mentions that as an alternative implementation

            if (index < 0 || index > this.Top)
                throw new Exception("@PositionAtIndex: index < 0 || index > this.Top");

            ForwardNode<T> result = null;
            int start = -1;

            // tuning: starts at place where fewest calls to .Next() are needed;
            // however, still an O(n) operation
            if (index < _position) {
                result = _start;
                start = 0;
            }
            else {
                result = _pointed;
                start = _position;
            }

            for (int i = start; i < index; i++)
                result = result.Next;

            // changing the internal location
            _pointed = result;

            // my innovation
            _position = index;
        }

        public override void PositionAtStart()  /* passed */  {
            // _start is never directly used, but is addressed here
            // because to consumers, outcomes will be like
            // pointing at _start.Next
            _pointed = _start;

            // my innovation
            _position = 0;
        }

        #endregion Nonconsecutive Positioning


        #region Changing Contents

        public override void Append(T element)  /* passed */  {
            // this is all essentially the same as the reftext
            // ( "appending" is adding at the end of the list )

            // a new element is added, with a null next because it will be at the end,
            // with nothing to point to
            ForwardNode<T> subject = new ForwardNode<T>(element, null);

            // the element is linked into the list, and then the list's _end definition is changed;
            // property usage in C# differs from `setNext in reftext, which returns the next
            _end.Next = subject;
            _end = subject;

            // list metadata
            _length++;
        }

        public override void InsertAtCurrent(T element)  /* passed */  {
            // basically the same as the reftext, with different code organization, 
            // except my use of indices, which is tuning 
            // 
            // inserting at the current index doesn't change the _position 

            // first, create new node with the current node's next as its next, 
            // after which, the node is "half" in the list 
            ForwardNode<T> next = _pointed.Next;
            ForwardNode<T> subject = new ForwardNode<T>(element, next);

            // second, point the current node at the new one 
            _pointed.Next = subject;

            // third, handle condition of inserting right before last node; 
            // reftext handles this by comparing `current and `tail ( my _pointed and _end ) 
            if (_position == this.Top)
                _end = subject;

            // fourth, handle list metadata 
            _length++;
        }

        public override T RemoveAtCurrent()  /* passed */  {
            // basically the same as the reftext 

            // if at the last value; `null isn't usable here, but otherwise same as reftext 
            if (_pointed.Next == null)
                return default(T);

            // save the current node ( the .Next of _pointed ) to return it at end 
            T result = _pointed.Next.Content;

            // shortening the list if we're at the end, and adjusting metadata ( my innovation ); 
            // otherwise, knocking the .Next out of the list by redirecting _pointed 
            // to link to the node after it 
            // 
            // this is different from the reftext, where `else` clause is not conditional, 
            // but which should work the same as my `_pointed.Next = null` and with a condition, 
            // my `.Next = .Next.Next`, because in the reftext's handling, `.Next.Next` should be `null; 
            // 
            // arguably, mine is less concise but more understandable 
            // 
            if (_position == this.Top)  // this means _pointed is at _end -1, and _pointed.Next is _end 
            {
                _pointed.Next = null;   // pointing to nothing since we're at the end now
                _end = _pointed;
                _position--;
            }
            else {
                // point the .Next of _pointed to the .Next of the one being removed;
                // this knocks the node out of the list, but it is only deleted by the GC
                _pointed.Next = _pointed.Next.Next;
            }

            // list metadata
            _length--;

            return result;
        }

        public override void Clear()  /* passed */  {
            // essentially the same as the reftext;
            // however, I don't create a new _start,
            // which seems unnecessary
            _start.Next = null;
            _pointed = _start;
            _end = _pointed;

            _position = 0;
            _length = 0;
        }

        #endregion Changing Contents
    }
}
