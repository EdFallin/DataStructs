﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // unlike in the reftext, this class keeps track of the same position 
    // reported to consuming code as current

    public class List_DoublyLinked_<T> : List_<T>
    {
        #region Fields

        // internal-only nodes
        TwoWayNode<T> _start;
        TwoWayNode<T> _end;

        TwoWayNode<T> _pointed;

        // _position is a way to eliminate some O(n) operations
        int _length;
        int _position;

        #endregion Fields


        #region Constructors

        public List_DoublyLinked_(int length)   // ok  
            : this() {
            // no operations; `length is ignored
        }

        public List_DoublyLinked_()   // passed  
        {
            _start = new TwoWayNode<T>(null, null);
            _end = new TwoWayNode<T>(null, _start);
            _start.Next = _end;

            // _start is never accessible by consumers, but is the logical place
            // to initialize these usually-usable elements
            _pointed = _start;
            _length = 0;
            _position = -1;
        }

        #endregion Constructors


        #region Properties - List Metadata

        public override int Length    // passed  
        {
            get { return _length; }
        }

        public override int CurrentPosition    // passed  
        {
            get { return _position; }
        }

        public override int Top {
            // `Top is the highest 0-based index used in the list
            get { return (_length - 1); }
        }

        #endregion Properties - List Metadata


        #region Properties - Current Element Value

        public override T CurrentValue    // passed  
        {
            get { return _pointed.Content; }
            set { _pointed.Content = value; }
        }

        #endregion Properties - Current Element Value


        #region Changing List Contents

        public override void Append(T element)   // passed  
        {
            // adding new node right before end
            TwoWayNode<T> subject = new TwoWayNode<T>(element, _end, _end.Previous);

            // linking previously before-end node to new node
            _end.Previous.Next = subject;

            // linking end to new node
            _end.Previous = subject;

            // list metadata
            _length++;
        }

        public override void InsertAtCurrent(T element)  /* passed */  {
            // create a new node that links to previous and current 
            TwoWayNode<T> node = new TwoWayNode<T>(element, _pointed, _pointed.Previous);

            // skipping cases where a .Previous or a .Next are null (which are at start or end of list), these ops: 
            //     link previous of current to new node (as .Next of previous); 
            //     link current to new node (as .Previous of current); 
            //     replace current as metadata ( _pointed ) with new node 
            if (_pointed != null) {
                if (_pointed.Previous != null) {
                    _pointed.Previous.Next = node;
                    _pointed.Previous = node;
                }
            }

            _pointed = node;


            // list metadata, on main code path 
            _length++;
        }

        public override T RemoveAtCurrent()  /* passed */  {
            // retaining value
            T content = _pointed.Content;

            // stitching together the nodes that are adjacent to _pointed, 
            // effectively dropping it from the list 
            _pointed.Previous.Next = _pointed.Next;
            _pointed.Next.Previous = _pointed.Previous;

            // changing _pointed to originally-next element; released link object can now be garbage-collected 
            _pointed = _pointed.Next;

            // list metadata 
            _length--;

            return content;
        }

        public override void Clear() {
            // tests:
            //     x _length is 0;
            //     x no element can be returned

            _start = new TwoWayNode<T>(null, null);
            _end = new TwoWayNode<T>(null, _start);
            _start.Next = _end;

            // _start is never accessible by consumers, but is the logical place
            // to initialize these usually-usable elements
            _pointed = _start;
            _length = 0;
            _position = -1;
        }

        #endregion Changing List Contents


        #region Consecutive Positioning

        public override void Next()   // passed  
        {
            _pointed = _pointed.Next;
            _position++;
        }

        public override void Previous()   // passed  
        {
            _pointed = _pointed.Previous;
            _position--;
        }

        #endregion Consecutive Positioning


        #region Nonconsecutive Positioning

        public override void PositionAtIndex(int index)   // passed  
        {
            // tuned using double-linking: moves forward or backward depending on current index
            if (index > _position) {
                while (_position < index) {
                    _pointed = _pointed.Next;
                    _position++;
                }
            }
            else {
                while (_position > index) {
                    _pointed = _pointed.Previous;
                    _position--;
                }
            }
        }

        public override void PositionAtStart()  /* passed */  {
            _pointed = _start.Next;
            _position = 0;
        }

        public override void PositionAtEnd()  /* passed */  {
            _pointed = _end.Previous;
            _position = this.Top;
        }

        #endregion Nonconsecutive Positioning

    }
}
