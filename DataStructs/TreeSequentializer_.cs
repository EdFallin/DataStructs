﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    /* replacement for the Tuple<> constructs that .NET offers, which are immutables */
    public struct Duple_<K, T>
    {
        public K key;
        public T element;
    }


    public class TreeSequentializer_
    {
        // used for initial calls to recursors for easiest internal coding
        const int SEQUENCE_INIT = -1;

        private enum Direction
        {
            Depth,
            Width,
        }


        #region Sequentializing General Trees

        public static Queue_<T> SequentializeGeneralTree<T>(GeneralTree_<T> tree, T endOfDownward, T endOfRightward) {

            /* using my linked-list Queue_<T> because it extends as needed and works with queueing / dequeueing */
            Queue_<T> sequence = Queue_<T>.SupplyQueue_T(QueueType.LinkedList);

            /* recursor adds to queue internally */
            GeneralTreeSequentializationRecursor(tree.Root, endOfDownward, endOfRightward, sequence);

            return sequence;
        }

        private static void GeneralTreeSequentializationRecursor<T>(GeneralTree_Node_<T> node, T endOfDownward, T endOfRightward, Queue_<T> sequence) {
            /* rcalls downward if there's a child; if not a child, moves rightward until first sibling with a child, then rcalls downward */

            while (node != null) {
                // this level 
                sequence.Enqueue(node.Content);

                if (node.FirstChild != null) {
                    GeneralTreeSequentializationRecursor<T>(node.FirstChild, endOfDownward, endOfRightward, sequence);
                }

                node = node.RightSibling;
            }

            // node == null here, so either the end of a subtree or a set of siblings (which is also a subtree), so...
            sequence.Enqueue(endOfDownward);
        }


        #endregion Sequentializing General Trees


        #region Desequentializing General Trees

        public static GeneralTree_<T> DesequentializeToGeneralTree<T>(Queue_<T> sequence, T endOfDownward, T endofRightward) {
            // declared / passed by ref for reading `sequence
            int index = 1;

            GeneralTree_<T> tree = new GeneralTree_<T>(default(T));    // root
            //GeneralTreeSequentializationRecursor(sequence, flagValue, ref index, tree.Root, Direction.Depth);

            return tree;
        }

        private static void GeneralTreeSequentializationRecursor<T>(T[] sequence, T flagValue, ref int index, GeneralTree_Node_<T> current, Direction direction) {

            #region Test Tree

            /* test tree                        
                                                
            a							        
            |                                   
            -----------------                   
            b				c			        
            |               |                   
            -------------   ---------           
            d	e	f	g	h	i	j	        
            |       |           |   |           
            |       |           |   -----       
            k		n			t	u	v       
            |       |                           
            -----   ---------                   
            l	m	o	p	q			        
                            |                   
                            -----               
                            r	s		        
                                                
 */

            #endregion Test Tree

            // read dataword;
            T element = sequence[index];

            // if !node, exit with ++index;   // move back / up toward root
            if (element.Equals(flagValue)) {
                index++;
                return;
            }

            // if node, set `next to a new node with sequence[index] value
            GeneralTree_Node_<T> next = new GeneralTree_Node_<T>(sequence[index]);

            // depending on `direction, assign `next to the correct relative of `current
            if (direction == Direction.Depth)
                current.FirstChild = next;
            else
                current.RightSibling = next;

            // index++;
            index++;

            // peek at sequence[index];    // ( the new `index value );
            // if peeked == flagValue, direction for rcall = Width, else = Depth
            if (sequence[index].Equals(flagValue)) { direction = Direction.Width; }
            else { direction = Direction.Depth; }

            // rcall()
            GeneralTreeSequentializationRecursor<T>(sequence, flagValue, ref index, next, direction);
        }

        #endregion Desequentializing General Trees


        #region Sequentializing Binary Trees

        public static Duple_<K, T>[] SequentializeBinaryTree<K, T>(BinaryTree_PointerNodes_<K, T> tree, K flagValue) {
            /* the T value for each flag dataword is default(T); value is tossed */

            return null;
        }

        #endregion Sequentializing Binary Trees


        public static BinaryTree_PointerNodes_<K, T> DesequentializeToBinaryTree<K, T>(Duple_<K, T> sequence, K flagValue) {
            return null;
        }
    }
}
