﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_Array_<K, T>
    {
        #region Definitions

        const int DEFAULT_ARRAY_SIZE = 63;  // 1 less than you'd think, because root has no sibling
        const int NO_NODE = -1;

        public delegate object Traverser(object topic, K key, T element);

        #endregion Definitions


        #region Fields

        Tuple<K, T>[] _array = null;
        int _arrayTop = -1;     // top index possible

        int _treeTop = -1;      // top index used
        int _current = -1;

        #endregion Fields


        #region Properties

        public K NodeKey    // ok  
        {
            get
            {
                return _array[_current].Item1;
            }
            set
            {
                // tuples are read-only, so the existing one is completely replaced
                Tuple<K, T> current = _array[_current];
                _array[_current] = new Tuple<K, T>(value, current.Item2);
            }
        }

        public T NodeElement    // ok  
        {
            get
            {
                return _array[_current].Item2;
            }
            set
            {
                // see comments at NodeKey -set()
                Tuple<K, T> current = _array[_current];
                _array[_current] = new Tuple<K, T>(current.Item1, value);
            }
        }

        public bool NodeIsLeaf    // passed  
        {
            // in a complete tree, any node whose L child would have an index higher than the top index is itself a leaf;
            // only L child needs to be looked at, as a node with a L child is always an inode

            get
            {
                // returns NO_NODE when calculated index is greater than last used array element
                int anyLeftChild = CalculateAnyLeftChild(_current);

                // node *is* a leaf when any-L-child returns NO_NODE
                return (anyLeftChild == NO_NODE);
            }
        }

        #endregion Properties


        #region Constructors

        // let's go with a fixed-size array for simplicity at experimenting
        public BinaryTree_Array_(K key, T element)   // passed  
        {
            _array = new Tuple<K, T>[DEFAULT_ARRAY_SIZE];
            _arrayTop = _array.Length - 1;

            // args are assigned to root
            _array[0] = new Tuple<K, T>(key, element);

            // init pointing to root
            _current = 0;
            _treeTop = 0;
        }

        #endregion Constructors


        #region Methods

        public void AddNode(K key, T element)   // passed  
        {
            Tuple<K, T> topic = new Tuple<K, T>(key, element);

            // only add a node if we won't exceed our array
            if (_treeTop < _arrayTop)
                _array[++_treeTop] = topic;
        }

        public void RemoveTopNode()   // passed  
        {
            // changing the metadata alone would effectively remove the top node,
            // but I'm nulling it also so the GC will clean it up
            _array[_treeTop] = null;

            // metadata
            _treeTop--;

            // if current node was last node, move it to new last node
            if (_current == (_treeTop + 1))
                _current = _treeTop;
        }

        public void MoveToRoot()   // passed  
        {
            // root is always at index 0
            _current = 0;
        }

        public void MoveToLeftChild()   // passed  
        {
            _current = CalculateLeftChild(_current);
        }

        public void MoveToRightChild()   // passed  
        {
            _current = CalculateRightChild(_current);
        }

        public void MoveToParent()   // passed  
        {
            _current = CalculateParent(_current);
        }

        public void MoveToLeftSibling()   // passed  
        {
            _current = CalculateLeftSibling(_current);
        }

        public void MoveToRightSibling()   // passed  
        {
            _current = CalculateRightSibling(_current);
        }

        #endregion Methods


        #region Traversals

        public object TraversePreorder(Traverser t)   // passed  
        {
            // parent, then L child, then R child

            object results = null;

            this.MoveToRoot();

            results = RecursePreorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // parent, then L child, then R child

            // this level ( parent / this node )
            topic = t(topic, key, element);

            // if the current node is a leaf or is missing a child, one or both children will
            // throw errors; those are expected errors, so catch and I ignore them

            try
            {
                // L child
                this.MoveToLeftChild();
                topic = RecursePreorder(t, topic, this.NodeKey, this.NodeElement);

                // returning to this node, since class retains node index as metadata
                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            try
            {
                // R child
                this.MoveToRightChild();
                topic = RecursePreorder(t, topic, this.NodeKey, this.NodeElement);

                // returning to this node, since class retains node index as metadata
                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            return topic;
        }

        public object TraverseInorder(Traverser t)   // passed  
        {
            // L child, then parent, then R child

            object results = null;

            this.MoveToRoot();

            results = RecurseInorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // L child, then parent, then R child

            // L and R children are in try-catch blocks because they may not exist,
            // but recursion should continue (or unwind), so exceptions are ignored

            try
            {
                this.MoveToLeftChild();
                topic = RecurseInorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            // this level
            topic = t(topic, key, element);

            try
            {
                this.MoveToRightChild();
                topic = RecurseInorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            return topic;
        }

        public object TraversePostorder(Traverser t)   // passed  
        {
            // L child, then R child, then parent

            object results = null;

            this.MoveToRoot();

            results = this.RecursePostorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // L child, then R child, then parent

            // using try-catch blocks to ignore expected .Move_ fails

            try
            {
                this.MoveToLeftChild();
                topic = this.RecursePostorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            try
            {
                this.MoveToRightChild();
                topic = this.RecursePostorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            // this level
            topic = t(topic, key, element);

            return topic;
        }

        #endregion Traversals


        #region Internals

        // exfactored index calculators ensure consistent behavior of methods

        private int CalculateParent(int current)   // verified  
        {
            if (current == 0)
                throw new Exception("Node at index " + current.ToString() + " is the root, so it does not have a parent");

            // int division here will return correct parent index for both L and R children
            int results = (current - 1) / 2;

            return results;
        }

        private int CalculateLeftChild(int current)   // verified  
        {
            int results = (current * 2) + 1;

            // because the tree is complete, this calculation is true for all children
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a left child");

            return results;
        }

        private int CalculateAnyLeftChild(int current)   // verified  
        {
            int results = (current * 2) + 1;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateRightChild(int current)   // verified  
        {
            int results = (current * 2) + 2;

            // see comments at CalculateLeftChild()
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right child");

            return results;
        }

        private int CalculateAnyRightChild(int current)   // verified /- in cousin code  
        {
            int results = (current * 2) + 2;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateLeftSibling(int current)   // verified  
        {
            if ((current % 2) != 0)
                throw new Exception("Node at index " + current.ToString() + " is not a right child, so it does not have a left sibling");

            return (current - 1);
        }

        private int CalculateRightSibling(int current)   // verified  
        {
            if ((current % 2) == 0)
                throw new Exception("Node at index " + current.ToString() + " is not a left child, so it does not have a right sibling");

            if ((current + 1) > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right sibling");

            return (current + 1);
        }

        #endregion Internals

    }
}
