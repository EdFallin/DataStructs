﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    /*  «abstract» can be a working class, you just can't init it directly; 
     *  if you write a ctor, it's called by concretes' ctors  */
    public abstract class NodeBpt<K> : IKeyedObject<K> where K : IEquatable<K>, IComparable<K>
    {
        #region Definitions

        const int MAX_NODES = 10;
        const int NONE = -1;

        #endregion Definitions


        #region Properties

        // should be set to the same number as the related B+tree //
        public static int MaxNodes { get; set; }

        public static int Top {
            get {
                return (MaxNodes - 1);
            }
        }

        public static int None { get; } = NONE;


        // same as Key of first item in Contents //
        public K Key { get; set; }

        // keys are exposed publicly for all nodes, and offsets are used with child nodes / elements  //
        public K[] Keys { get; set; } = new K[BPlusTree<K>.MaxNodes];

        // this is _either_ the .Children or the .Elements //
        public abstract IKeyedObject<K>[] Contents { get; }

        // TopKey == the offset of the last filled el of Keys; ops on Keys must maintain this value  //
        public int TopKey { get { return (Count - 1); } set { Count = value + 1; } }

        // Count == the number of filled Keys; kept in sync with TopKey by making this a dependency of that //
        public int Count { get; set; }

        #endregion Properties


        #region Constructors

        public NodeBpt() {
            /* no operations */
        }

        public NodeBpt(K key) {
            Key = key;
        }

        #endregion Constructors


        #region Methods

        public abstract NodeBpt<K> Insert(int location, IKeyedObject<K> insertable);

        public abstract NodeBpt<K> Split();

        public abstract void AddToContents(int location, IKeyedObject<K> insertable);

        public abstract bool Remove(int location, IKeyedObject<K> removable);

        #endregion Methods

    }
}