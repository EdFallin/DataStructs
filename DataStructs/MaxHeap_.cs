﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class MaxHeap_<T> where T : IComparable<T>
    {
        #region Definitions

        const int DEFAULT_ARRAY_SIZE = 63;  // 1 less than you'd think, because root has no sibling 
        const int NO_NODE = -1;

        public delegate object Traverser(object topic, T content);

        #endregion Definitions


        #region Fields

        T[] _array = null;
        int _arrayTop = -1;     // top index possible 

        int _treeTop = -1;      // top index used 
        int _current = -1;

        #endregion Fields


        #region Properties

        public int Count    // ok  
        {
            get { return (_treeTop + 1); }
        }

        public T NodeContent    // ok  
        {
            get {
                return _array[_current];
            }
            set {
                T current = _array[_current];
                _array[_current] = value;
            }
        }

        public bool NodeIsLeaf {
            // in a complete tree, any node whose L child would have an index higher than the top index is itself a leaf; 
            // only L child needs to be looked at, as a node with a L child is always an inode 

            get {
                // returns NO_NODE when calculated index is greater than last used array element 
                int anyLeftChild = CalculateAnyLeftChild(_current);

                // node *is* a leaf when any-L-child returns NO_NODE 
                return (anyLeftChild == NO_NODE);
            }
        }

        #endregion Properties


        #region Constructors

        // constructor like the one in reftext, for fastest ordering 
        public MaxHeap_(T[] contents, int maxHeapSize)  /* ok */  {
            // set up internal array; can be larger than arg array; 
            // skipping size checks here because assuming consumer is supersafe 
            _array = new T[maxHeapSize];

            // filling array 
            for (int i = 0; i < contents.Length; i++)
                _array[i] = contents[i];

            // metadata 
            _arrayTop = _array.Length - 1;
            _treeTop = contents.Length - 1;

            // start with pointing to root 
            _current = 0;

            // partially ordering the nodes to convert to heap 
            this.OrderHeap();
        }

        // alternative constructor from array-backed tree 
        public MaxHeap_(T content)  /* ok */  {
            _array = new T[DEFAULT_ARRAY_SIZE];
            _arrayTop = _array.Length - 1;

            // args are assigned to root 
            _array[0] = content;

            // init pointing to root 
            _current = 0;
            _treeTop = 0;
        }

        #endregion Constructors


        #region Tree Methods

        public void AddNode(T content)  /* ok */  {
            // only add a node if we won't exceed our array 
            if (_treeTop < _arrayTop)
                _array[++_treeTop] = content;
        }

        public void RemoveTopNode()  /* passed */  {
            // changing the metadata alone would effectively remove the top node, 
            // but I'm assigning default(T), usually null, so that the GC can clean it up 
            _array[_treeTop] = default(T);

            // metadata 
            _treeTop--;

            // if current node was last node, move it to new last node 
            if (_current == (_treeTop + 1))
                _current = _treeTop;
        }

        public void MoveToRoot()  /* ok */  {
            // root is always at index 0 
            _current = 0;
        }

        public void MoveToLeftChild()  /* ok */  {
            _current = CalculateLeftChild(_current);
        }

        public void MoveToRightChild()  /* ok */  {
            _current = CalculateRightChild(_current);
        }

        public void MoveToParent()  /* ok */  {
            _current = CalculateParent(_current);
        }

        public void MoveToLeftSibling()  /* ok */  {
            _current = CalculateLeftSibling(_current);
        }

        public void MoveToRightSibling()  /* ok */  {
            _current = CalculateRightSibling(_current);
        }

        #endregion Tree Methods


        #region Heap Methods

        public void OrderHeap()  /* ok */  {
            // starting at first non-leaf, calling SwapDown() to build subheaps, progressively building bigger subheaps until full heap 

            // here I use the reftext's way of determining what is not a leaf:  [[[number of elements] ÷2] -1] 
            int start = ((_treeTop + 1) / 2) - 1;

            for (int i = start; i >= 0; i--)
                this.SwapDown(i);
        }

        public void AddHeapNode(T content)  /* passed */  {
            // node is added to end, then is swapped up to good level only 
            this.AddNode(content);

            this.SwapUp(_treeTop);
        }

        public T RemoveHeapNode(int subject)  /* passed */  {
            // node is removed, end node is moved to its position, then moved node is either swapped up or down, whichever is needed 

            // sometimes addressing array directly 

            T results = _array[subject];
            T newSubject = _array[_treeTop];

            // effectively, a swap of the two nodes 
            this.RemoveTopNode();
            _array[subject] = newSubject;

            // moved node is either swapped up or down, depending on how it may be disordered 
            {
                bool doSwapUp = false;

                if (subject > 0) {
                    int parent = this.CalculateParent(subject);

                    if (_array[subject].CompareTo(_array[parent]) > 0)
                        doSwapUp = true;
                }

                if (doSwapUp)
                    SwapUp(subject);
                else
                    SwapDown(subject);
            }

            // convey output 
            return results;
        }

        public T PopHeapRoot()  /* passed */  {
            // exfactored to RemoveHeapNode() ; root is at index 0 
            T results = RemoveHeapNode(0);
            return results;
        }

        #endregion Heap Methods


        #region Traversals

        public object TraversePreorder(Traverser t)  /* passed */  {
            object results = null;

            this.MoveToRoot();
            results = RecursePreorder(t, results, this.NodeContent);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, T content)  /* verified */  {
            // parent, then L child, then R child 

            // this level ( parent / this node ) 
            topic = t(topic, content);

            // if the current node is a leaf or is missing a child, one or both children will 
            // throw errors; those are expected errors, so catch and I ignore them 

            try {
                // L child
                this.MoveToLeftChild();
                topic = RecursePreorder(t, topic, this.NodeContent);

                // returning to this node, since class retains node index as metadata 
                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            try {
                // R child 
                this.MoveToRightChild();
                topic = RecursePreorder(t, topic, this.NodeContent);

                // returning to this node, since class retains node index as metadata 
                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            return topic;
        }

        public object TraverseInorder(Traverser t)  /* passed */  {
            object results = null;

            this.MoveToRoot();
            results = RecurseInorder(t, results, this.NodeContent);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, T content)  /* verified */  {
            // L child, then parent, then R child 

            // L and R children are in try-catch blocks because they may not exist, 
            // but recursion should continue (or unwind), so exceptions are ignored 

            try {
                this.MoveToLeftChild();
                topic = RecurseInorder(t, topic, this.NodeContent);

                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            // this level 
            topic = t(topic, content);

            try {
                this.MoveToRightChild();
                topic = RecurseInorder(t, topic, this.NodeContent);

                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            return topic;
        }

        public object TraversePostorder(Traverser t)  /* passed */  {
            object results = null;

            this.MoveToRoot();
            results = this.RecursePostorder(t, results, this.NodeContent);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, T content)  /* verified */  {
            // L child, then R child, then parent 

            // using try-catch blocks to ignore expected .Move_ fails 

            try {
                this.MoveToLeftChild();
                topic = this.RecursePostorder(t, topic, this.NodeContent);

                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            try {
                this.MoveToRightChild();
                topic = this.RecursePostorder(t, topic, this.NodeContent);

                this.MoveToParent();
            }
            catch {
                /* no operations */
            }

            // this level
            topic = t(topic, content);

            return topic;
        }

        #endregion Traversals


        #region Tree Internals

        // exfactored index calculators ensure consistent behavior of methods 

        private int CalculateParent(int current)  /* ok */  {
            if (current == 0)
                throw new Exception("Node at index " + current.ToString() + " is the root, so it does not have a parent");

            // int division here will return correct parent index for both L and R children 
            int results = (current - 1) / 2;

            return results;
        }

        private int CalculateLeftChild(int current)  /* ok */  {
            int results = (current * 2) + 1;

            // because the tree is complete, this calculation is true for all children 
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a left child");

            return results;
        }

        private int CalculateAnyLeftChild(int current)  /* ok */  {
            int results = (current * 2) + 1;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateRightChild(int current)  /* ok */  {
            int results = (current * 2) + 2;

            // see comments at CalculateLeftChild()
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right child");

            return results;
        }

        private int CalculateAnyRightChild(int current)  /* ok */  {
            int results = (current * 2) + 2;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateLeftSibling(int current)  /* ok */  {
            if ((current % 2) != 0)
                throw new Exception("Node at index " + current.ToString() + " is not a right child, so it does not have a left sibling");

            return (current - 1);
        }

        private int CalculateRightSibling(int current)  /* ok */  {
            if ((current % 2) == 0)
                throw new Exception("Node at index " + current.ToString() + " is not a left child, so it does not have a right sibling");

            if ((current + 1) > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right sibling");

            return (current + 1);
        }

        #endregion Tree Internals


        #region Heap Internals

        private void SwapDown(int startNodeIndex)  /* passed */  {
            // starting at arg node, swapping disordered parent with max child and moving to was-max-child, until no swap or at a leaf 

            int current = startNodeIndex;

            // conventional `while loop to bypass ops as soon as first leaf at least 
            while (!this.IsLeaf(current)) {
                // when using data, addressing _array directly throughout;  is the same as a node's key 

                int left = this.CalculateAnyLeftChild(current);
                int right = this.CalculateAnyRightChild(current);

                // only R child can be missing if not a leaf 
                int maxChild = left;

                T leftContent = _array[left];

                // determining the greater child if both exist; if keys equal, R child treated as greater (?) 
                if (right != NO_NODE) {
                    T rightContent = _array[right];
                    maxChild = (leftContent.CompareTo(rightContent) > 0 ? left : right);
                }

                // if current and maxChild are disordered, swap and move to child; otherwise exit loop and method 
                if (_array[current].CompareTo(_array[maxChild]) < 0) {
                    T newCurrent = _array[maxChild];
                    T newMaxChild = _array[current];

                    // CRUX 
                    {
                        // the actual swapping 
                        _array[current] = newCurrent;
                        _array[maxChild] = newMaxChild;

                        // moving to max child
                        current = maxChild;
                    }
                    // END CRUX 
                }
                else {
                    // if no swap, exit now 
                    break;
                }
            }
        }

        private void SwapUp(int startNodeIndex)  /* passed */  {
            // starting at arg node, swapping with disordered parent and moving to was-parent, until no swap or at root 

            int current = startNodeIndex;

            // do-while loop for simplest syntax 
            do {
                // when using data, addressing _array directly throughout;  is the same as a node's key 
                int parent = CalculateParent(current);

                // if current and parent are disordered, swap and move to parent; otherwise exit loop and method 
                if (_array[current].CompareTo(_array[parent]) > 0) {
                    // tuples are immutable, so I have to create new ones here for swap
                    T newParent = _array[current];
                    T newCurrent = _array[parent];

                    // CRUX 
                    {
                        // the actual swapping 
                        _array[current] = newCurrent;
                        _array[parent] = newParent;

                        // moving to parent 
                        current = parent;
                    }
                    // END CRUX 
                }
                else {
                    // if no swap, exit now 
                    break;
                }

            } while (current > 0);

        }

        private bool IsLeaf(int subject)  /* ok */  {
            // returns NO_NODE when calculated index is greater than last used array element 
            int anyLeftChild = CalculateAnyLeftChild(subject);

            // node *is* a leaf when any-L-child returns NO_NODE 
            return (anyLeftChild == NO_NODE);
        }

        #endregion Heap Internals
    }

    public class MaxHeap_<K, T> where K : IComparable<K>
    {
        #region Definitions

        const int DEFAULT_ARRAY_SIZE = 63;  // 1 less than you'd think, because root has no sibling
        const int NO_NODE = -1;

        public delegate object Traverser(object topic, K key, T element);

        #endregion Definitions


        #region Fields

        Tuple<K, T>[] _array = null;
        int _arrayTop = -1;     // top index possible

        int _treeTop = -1;      // top index used
        int _current = -1;

        #endregion Fields


        #region Properties

        public int Count    // ok  
        {
            get { return _treeTop + 1; }
        }
        
        public K NodeKey    // ok  
        {
            get
            {
                return _array[_current].Item1;
            }
            set
            {
                // tuples are read-only, so the existing one is completely replaced
                Tuple<K, T> current = _array[_current];
                _array[_current] = new Tuple<K, T>(value, current.Item2);
            }
        }

        public T NodeElement    // ok  
        {
            get
            {
                return _array[_current].Item2;
            }
            set
            {
                // see comments at NodeKey -set()
                Tuple<K, T> current = _array[_current];
                _array[_current] = new Tuple<K, T>(current.Item1, value);
            }
        }

        public bool NodeIsLeaf    // passed  
        {
            // in a complete tree, any node whose L child would have an index higher than the top index is itself a leaf;
            // only L child needs to be looked at, as a node with a L child is always an inode

            get
            {
                // returns NO_NODE when calculated index is greater than last used array element
                int anyLeftChild = CalculateAnyLeftChild(_current);

                // node *is* a leaf when any-L-child returns NO_NODE
                return (anyLeftChild == NO_NODE);
            }
        }

        #endregion Properties


        #region Constructors

        // constructor like the one in reftext, for fastest ordering
        public MaxHeap_(Tuple<K, T>[] contents, int maxHeapSize)   // verified  
        {
            // set up internal array; can be larger than arg array;
            // skipping size checks here because assuming consumer is supersafe
            _array = new Tuple<K, T>[maxHeapSize];

            // filling array
            for (int i = 0; i < contents.Length; i++)
                _array[i] = contents[i];

            // metadata
            _arrayTop = _array.Length - 1;
            _treeTop = contents.Length - 1;

            // start with pointing to root
            _current = 0;

            // partially ordering the nodes to convert to heap
            this.OrderHeap();
        }

        // alternative constructor from array-backed tree
        public MaxHeap_(K key, T element)   // passed  
        {
            _array = new Tuple<K, T>[DEFAULT_ARRAY_SIZE];
            _arrayTop = _array.Length - 1;

            // args are assigned to root
            _array[0] = new Tuple<K, T>(key, element);

            // init pointing to root
            _current = 0;
            _treeTop = 0;
        }

        #endregion Constructors


        #region Tree Methods

        public void AddNode(K key, T element)   // passed  
        {
            Tuple<K, T> topic = new Tuple<K, T>(key, element);

            // only add a node if we won't exceed our array
            if (_treeTop < _arrayTop)
                _array[++_treeTop] = topic;
        }

        public void RemoveTopNode()   // passed  
        {
            // changing the metadata alone would effectively remove the top node,
            // but I'm nulling it also so the GC will clean it up
            _array[_treeTop] = null;

            // metadata
            _treeTop--;

            // if current node was last node, move it to new last node
            if (_current == (_treeTop + 1))
                _current = _treeTop;
        }

        public void MoveToRoot()   // passed  
        {
            // root is always at index 0
            _current = 0;
        }

        public void MoveToLeftChild()   // passed  
        {
            _current = CalculateLeftChild(_current);
        }

        public void MoveToRightChild()   // passed  
        {
            _current = CalculateRightChild(_current);
        }

        public void MoveToParent()   // passed  
        {
            _current = CalculateParent(_current);
        }

        public void MoveToLeftSibling()   // passed  
        {
            _current = CalculateLeftSibling(_current);
        }

        public void MoveToRightSibling()   // passed  
        {
            _current = CalculateRightSibling(_current);
        }

        #endregion Tree Methods


        #region Heap Methods

        public void OrderHeap()   // passed  
        {
            // starting at first non-leaf, calling SwapDown() to build subheaps, progressively building bigger subheaps until full heap

            // here I use the reftext's way of determining what is not a leaf:  [[[number of elements] ÷2] -1]
            int start = ((_treeTop + 1) / 2) - 1;

            for (int i = start; i >= 0; i--)
                this.SwapDown(i);
        }

        public void AddHeapNode(K key, T element)   // passed  
        {
            // node is added to end, then is swapped up to good level only

            this.AddNode(key, element);

            this.SwapUp(_treeTop);
        }

        public T RemoveHeapNode(int subject)   // passed  
        {
            // node is removed, end node is moved to its position, then moved node is either swapped up or down, whichever is needed

            // sometimes addressing array directly

            T results = _array[subject].Item2;

            // tuples are immutable, so a new one is needed to replace subject
            Tuple<K, T> newSubject = new Tuple<K, T>(_array[_treeTop].Item1, _array[_treeTop].Item2);

            // effectively, a swap of the two nodes
            this.RemoveTopNode();
            _array[subject] = newSubject;

            // moved node is either swapped up or down, depending on how it may be disordered
            {
                bool doSwapUp = false;

                if (subject > 0)
                {
                    int parent = this.CalculateParent(subject);

                    if (_array[subject].Item1.CompareTo(_array[parent].Item1) > 0)
                        doSwapUp = true;
                }

                if (doSwapUp)
                    SwapUp(subject);
                else
                    SwapDown(subject);
            }

            // convey output
            return results;
        }

        public T PopHeapRoot()   // passed  
        {
            // same implementation as RemoveHeapNode(), so exfactored to that; root is at index 0

            T results = RemoveHeapNode(0);

            return results;
        }

        #endregion Heap Methods


        #region Traversals

        public object TraversePreorder(Traverser t)   // passed  
        {
            // parent, then L child, then R child

            object results = null;

            this.MoveToRoot();

            results = RecursePreorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // parent, then L child, then R child

            // this level ( parent / this node )
            topic = t(topic, key, element);

            // if the current node is a leaf or is missing a child, one or both children will
            // throw errors; those are expected errors, so catch and I ignore them

            try
            {
                // L child
                this.MoveToLeftChild();
                topic = RecursePreorder(t, topic, this.NodeKey, this.NodeElement);

                // returning to this node, since class retains node index as metadata
                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            try
            {
                // R child
                this.MoveToRightChild();
                topic = RecursePreorder(t, topic, this.NodeKey, this.NodeElement);

                // returning to this node, since class retains node index as metadata
                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            return topic;
        }

        public object TraverseInorder(Traverser t)   // passed  
        {
            // L child, then parent, then R child

            object results = null;

            this.MoveToRoot();

            results = RecurseInorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // L child, then parent, then R child

            // L and R children are in try-catch blocks because they may not exist,
            // but recursion should continue (or unwind), so exceptions are ignored

            try
            {
                this.MoveToLeftChild();
                topic = RecurseInorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            // this level
            topic = t(topic, key, element);

            try
            {
                this.MoveToRightChild();
                topic = RecurseInorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            return topic;
        }

        public object TraversePostorder(Traverser t)   // passed  
        {
            // L child, then R child, then parent

            object results = null;

            this.MoveToRoot();

            results = this.RecursePostorder(t, results, this.NodeKey, this.NodeElement);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, K key, T element)   // verified  
        {
            // L child, then R child, then parent

            // using try-catch blocks to ignore expected .Move_ fails

            try
            {
                this.MoveToLeftChild();
                topic = this.RecursePostorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            try
            {
                this.MoveToRightChild();
                topic = this.RecursePostorder(t, topic, this.NodeKey, this.NodeElement);

                this.MoveToParent();
            }
            catch
            {
                // no operations
            }

            // this level
            topic = t(topic, key, element);

            return topic;
        }

        #endregion Traversals


        #region Tree Internals

        // exfactored index calculators ensure consistent behavior of methods

        private int CalculateParent(int current)   // verified  
        {
            if (current == 0)
                throw new Exception("Node at index " + current.ToString() + " is the root, so it does not have a parent");

            // int division here will return correct parent index for both L and R children
            int results = (current - 1) / 2;

            return results;
        }

        private int CalculateLeftChild(int current)   // verified  
        {
            int results = (current * 2) + 1;

            // because the tree is complete, this calculation is true for all children
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a left child");

            return results;
        }

        private int CalculateAnyLeftChild(int current)   // verified  
        {
            int results = (current * 2) + 1;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateRightChild(int current)   // verified  
        {
            int results = (current * 2) + 2;

            // see comments at CalculateLeftChild()
            if (results > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right child");

            return results;
        }

        private int CalculateAnyRightChild(int current)   // verified  
        {
            int results = (current * 2) + 2;

            if (results > _treeTop)
                results = NO_NODE;

            return results;
        }

        private int CalculateLeftSibling(int current)   // verified  
        {
            if ((current % 2) != 0)
                throw new Exception("Node at index " + current.ToString() + " is not a right child, so it does not have a left sibling");

            return (current - 1);
        }

        private int CalculateRightSibling(int current)   // verified  
        {
            if ((current % 2) == 0)
                throw new Exception("Node at index " + current.ToString() + " is not a left child, so it does not have a right sibling");

            if ((current + 1) > _treeTop)
                throw new Exception("Node at index " + current.ToString() + " does not have a right sibling");

            return (current + 1);
        }

        #endregion Tree Internals


        #region Heap Internals

        private void SwapDown(int startNodeIndex)   // passed  
        {
            // starting at arg node, swapping disordered parent with max child and moving to was-max-child, until no swap or at a leaf

            int current = startNodeIndex;

            // conventional `while loop to bypass ops as soon as first leaf at least
            while (!this.IsLeaf(current))
            {
                // when using data, addressing _array directly throughout; .Item1 is the same as a node's key

                int left = this.CalculateAnyLeftChild(current);
                int right = this.CalculateAnyRightChild(current);

                // only R child can be missing if not a leaf
                int maxChild = left;

                K leftKey = _array[left].Item1;

                // determining the greater child if both exist; if keys equal, R child treated as greater (?)
                if (right != NO_NODE)
                {
                    K rightKey = _array[right].Item1;
                    maxChild = (leftKey.CompareTo(rightKey) > 0 ? left : right);
                }

                // if current and maxChild are disordered, swap and move to child; otherwise exit loop and method
                if (_array[current].Item1.CompareTo(_array[maxChild].Item1) < 0)
                {
                    // tuples are immutable, so I have to create new ones here for swap
                    Tuple<K, T> newCurrent = Tuple.Create(_array[maxChild].Item1, _array[maxChild].Item2);
                    Tuple<K, T> newMaxChild = Tuple.Create(_array[current].Item1, _array[current].Item2);

                    // CRUX
                    {
                        // the actual swapping
                        _array[current] = newCurrent;
                        _array[maxChild] = newMaxChild;

                        // moving to max child
                        current = maxChild;
                    }
                    // END CRUX
                }
                else
                {
                    // if no swap, exit now
                    break;
                }
            }
        }

        private void SwapUp(int startNodeIndex)   // passed  
        {
            // starting at arg node, swapping with disordered parent and moving to was-parent, until no swap or at root

            int current = startNodeIndex;

            // do-while loop for simplest syntax
            do
            {
                // when using data, addressing _array directly throughout; .Item1 is the same as a node's key
                int parent = CalculateParent(current);

                // if current and parent are disordered, swap and move to parent; otherwise exit loop and method
                if (_array[current].Item1.CompareTo(_array[parent].Item1) > 0)
                {
                    // tuples are immutable, so I have to create new ones here for swap
                    Tuple<K, T> newParent = Tuple.Create(_array[current].Item1, _array[current].Item2);
                    Tuple<K, T> newCurrent = Tuple.Create(_array[parent].Item1, _array[parent].Item2);

                    // CRUX
                    {
                        // the actual swapping
                        _array[current] = newCurrent;
                        _array[parent] = newParent;

                        // moving to parent
                        current = parent;
                    }
                    // END CRUX
                }
                else
                {
                    // if no swap, exit now
                    break;
                }

            } while (current > 0);

        }

        private bool IsLeaf(int subject)   // verified  
        {
            // returns NO_NODE when calculated index is greater than last used array element
            int anyLeftChild = CalculateAnyLeftChild(subject);

            // node *is* a leaf when any-L-child returns NO_NODE
            return (anyLeftChild == NO_NODE);
        }

        #endregion Heap Internals
    }
}
