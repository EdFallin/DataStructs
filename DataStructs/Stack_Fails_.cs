﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // this class exists only to verify unit tests, by always returning bad values

    public class Stack_Fails_<T> : Stack_<T>
    {
        public override void Clear()
        {
            // no operations
        }

        public override int Length
        {
            get
            {
                return int.MinValue;
            }
        }

        public override T Peek
        {
            get
            {
                return default(T);
            }
        }

        public override T Pop()
        {
            return default(T);
        }

        public override void Push(T element)
        {
            // no operations
        }
    }
}
