﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public enum QueueType
    {
        None,
        Fail,
        Array,
        LinkedList,
    }

    // ADT plus factory method 

    public class Queue_<T>
    {
        #region Fields

        // these fields are primarily for testing and may not be valid for all concrete classes
        protected T _peek = default(T);
        protected T _dequeue = default(T);
        protected int _length = 0;

        #endregion Fields


        #region Properties, Including Peek

        public virtual T Peek
        {
            get { return default(T); }
        }

        public virtual int Length
        {
            get { return -1; }
        }

        #endregion Properties, Including Peek


        #region Changing List Contents

        public virtual void Clear() { }

        public virtual void Enqueue(T element) { }

        public virtual T Dequeue() { return default(T); }

        #endregion Changing List Contents


        #region Static Factory Method For Concrete Queues

        public static Queue_<T> SupplyQueue_T(QueueType type)
        {
            Queue_<T> queue = null;

            switch (type)
            {
                case QueueType.Array:
                    queue = new Queue_Array_<T>();
                    break;
                case QueueType.LinkedList:
                    queue = new Queue_LinkedList_<T>();
                    break;
                default:
                    queue = new Queue_Fails_<T>();
                    break;
            }

            return queue;
        }

        #endregion Static Factory Method For Concrete Queues

    }
}
