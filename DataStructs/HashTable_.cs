﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataStructs
{
    /* forward declaration of my namespace (!) for hash-table -related enums */
    using HashState_;


    namespace HashState_
    {
        /* moved out of the <K, T> class for simpler consuming syntax, but in this new namespace so they're not at DataStructs root level */

        public enum SearchResult
        {
            None,   // anti-default, fail condition 
            Failed,
            Continue,
            Succeeded,
            Impossible,
            Repeated,
        }

        public enum SlotState
        {
            None,   // fail condition 
            Full,
            Tombstone,
        }
    }


    public class HashTable_<K, T> where K : IComparable<K>, IEquatable<K>
    {
        #region Definitions, Including Quadruple_ And Duple_

        private const int None = -1;

        #region Private Classes Quadruple_ And Duple_

        /* hash table elements are always Quadruples; not declared with <K, T> as those are already defined in the enclosing scope */
        private class Quadruple_ : IComparable<Quadruple_>, IEquatable<Quadruple_>
        {
            #region Public Fields

            public K Key;
            public T Value;
            public SlotState State = SlotState.None;   // anti-default 
            public int Count = 0;   // for rehashing by frequency of access 

            #endregion Public Fields


            #region Constructors

            public Quadruple_(K key, T value) {
                /* Count is always 0 at init */
                this.Key = key;
                this.Value = value;
                this.State = SlotState.Full;   // since we've inited this with values, any slot this is in is full 
            }

            #endregion Constructors


            #region Interface Implementations

            /* needed for the two implemented interfaces, which are needed for my quick-sort implemn */
            /* illogical-seeming ops here are what is needed for rehashing; those objects are never compared otherwise */

            public int CompareTo(Quadruple_ other)  /* ok */  {
                if (other == null) {
                    throw new ArgumentException("Argument cannot be null.", "other");
                }

                return this.Count.CompareTo(other.Count);
            }

            public bool Equals(Quadruple_ other)  /* ok */  {
                if (other == null) {
                    return false;
                }

                return this.Count.Equals(other.Count);
            }

            #endregion Interface Implementations


        }


        /* return values paired to avoid overloading slot values with negative numbers for enumerations, a hack */
        private class Duple_
        {
            #region Public Fields

            public SearchResult Result = SearchResult.None;
            public int Slot = HashTable_<K, T>.None;

            #endregion Public Fields


            #region Constructors

            /* two opposite outcomes; a both-parameters constructor seems unnecessary */

            public Duple_(SearchResult result) {
                this.Result = result;
            }

            public Duple_(int slot) {
                this.Result = SearchResult.Succeeded;
                this.Slot = slot;
            }

            #endregion Constructors
        }

        #endregion Private Classes Quadruple_ And Duple_

        #endregion Definitions, Including Quadruple_ And Duple_


        #region Fields

        Quadruple_[] _table;
        int _size = 0;
        int _keys = 0;
        int _tombstones = 0;
        T _failValue;   // returned to consumer when sought isn’t found 

        //**  *** following is for testing .AnyRehashNow() only; retained for any retesting ***  **//
        //bool _rehashBegun = false;
        //**  *** preceding is for testing .AnyRehashNow() only; retained for any retesting ***  **//

        #endregion Fields


        #region Disabled Private Properties

        /* these are retained for any retesting */

        /* for testing only, ∴ private */
        //private string QuadrupleFullName {
        //   get {
        //       Type t = typeof(Quadruple_);
        //       return t.FullName;
        //   }
        //}

        /* for testing only, ∴ private */
        //private string DupleFullName {
        //   get {
        //       Type type = typeof(Duple_);
        //       return type.FullName;
        //   }
        //}

        /* for testing only, ∴ private */
        //private bool RehashBegun {
        //   get { return _rehashBegun; }
        //   set { _rehashBegun = value; }
        //}

        #endregion Disabled Private Properties


        #region Properties

        /* no public properties for metadata, except size; this class is opaque */

        public int Size {
            get {
                return this._size;
            }
        }

        #endregion Properties


        #region Constructors

        public HashTable_(int size, T failValue) {
            size = Basics.NextPowerOfTwo(size);   // 2^x to allow easy probe rehashing 
            _table = new Quadruple_[size];
            _size = size;
            _failValue = failValue;
        }

        #endregion Constructors


        #region Find() And Its Dependencies

        public T Find(K key)  /* passed */  {
            T result;

            /* the actual search; exfactored to finding index of a match, or a fail flag */
            Duple_ searchResult = SearchForKey(key);


            /* fail code path */
            if (searchResult.Result != SearchResult.Succeeded) {
                return this._failValue;
            }


            /* succeed code path */
            int slot = searchResult.Slot;

            result = _table[slot].Value;
            _table[slot].Count++;   // metadata for rehashing 

            return result;
        }

        private Duple_ SearchForKey(K key)  /* passed */  {
            /* returning HashResult and index for use internally and as bulk of the external Find() */

            /* *crux* - - the actual hashing in the hash table */
            int slot = Math.Abs(key.GetHashCode()) % this._size;   // surprisingly, some hash values (at least for strings) are negative, so I have to abs those 
            /* *end crux* */


            /* second hashing, done now to eliminate checks in following loop */
            int offset = Math.Abs(key.GetProbeHash()) % this._size;   // *my extension method* 
            offset = this.MakeHashOddNumber(offset);


            /*  home position and following probe sequence using rehashed value to skip by added same lengths until match, empty, or all visited; 
             *  each new calculated slot has to be %ed to keep slots within size of backing array; 
             *  empty is a failed search because probe sequences never have gaps; tombstones and mismatches skipped, as value can be beyond them  */
            for (int ix = 0; ix < _size; ix++, slot = (slot + offset) % this._size) {
                SearchResult slotHashResult = this.SlotResult(slot, key);

                if (slotHashResult == SearchResult.Succeeded) {
                    return (new Duple_(slot));
                }

                if (slotHashResult == SearchResult.Failed) {
                    return (new Duple_(slotHashResult));
                }
            }

            // if we’ve reached this point, the search has failed because all slots have been tried 
            return (new Duple_(SearchResult.Failed));
        }

        private int MakeHashOddNumber(int subject)  /* passed */  {
            /* algo: if ^subject is even, incr it by 1 to make it odd */

            /*  testing against table size not needed, as table is always internally resized to a power of 2, always even; 
             *  ∴ a % always produces a number < size, and max possible output is odd, never needing incrementing  */

            int output = subject % this._size;

            if ((output % 2) == 0) {
                output++;
            }

            return output;
        }

        private SearchResult SlotResult(int slot, K key)  /* passed */  {
            /*  returns flag value used in search for existing key: 
             *      either a fail when null (because no nulls in a valid probe sequence), 
             *      or a succeed when key is found, or a continue at a tombstone or different key  */
            if (_table[slot] == null) {
                return SearchResult.Failed;
            }

            if (_table[slot].State == SlotState.Full) {
                if (_table[slot].Key.Equals(key)) {
                    return SearchResult.Succeeded;
                }
            }

            /* full slots with a different key, or those with a tombstone, should just be skipped in consuming code */
            return SearchResult.Continue;
        }

        #endregion Find() And Its Dependencies


        #region Indexer (No New Dependencies)

        /*  implementing the same kind of functionality found in Dictionary<>, but finding / getting only, as setting 
         *  would probably mean working together finding, inserting, and changing, more than I want to do right now  */

        public T this[K key]   /* passed */  {
            get {
                return this.Find(key);
            }
            /* no set accessor */
        }

        #endregion Indexer (No New Dependencies)


        #region Insert() And Its New Dependencies

        public SearchResult Insert(K key, T value)  /* passed */  {
            /* the actual search; exfactored */
            Duple_ searchResult = SearchForAvailableSlot(key);


            /* fail code path; passing along enumerated fail flag */
            if (searchResult.Result != SearchResult.Succeeded) {
                return searchResult.Result;
            }


            /*  succeed code path: 
             *  since now, no repeat, but an open slot or tombstone, actually inserting a new element; 
             *  if a tombstone is present in a non-null slot, it is overwritten and GC will collect it later */
            Quadruple_ subject = new Quadruple_(key, value);
            _table[searchResult.Slot] = subject;

            // maintaining metadata to determine any rehashing 
            _keys++;


            // maintaining table efficiency; exfactored; ideally would occur as tail call after return 
            this.AnyRehashNow();

            // conveying 
            return SearchResult.Succeeded;
        }

        private Duple_ SearchForAvailableSlot(K key)  /* passed */  {
            /*  this method finds slots for inserts, and therefore looks for a first non-full slot; 
             *  but if ^key is matched, a fail enum is returned, as keys must be unique; 
             *  if no empty slots, another fail enum is returned; that is unlikely with rehashing  */


            /* *crux* - - the actual hashing in the hash table */
            int slot = Math.Abs(key.GetHashCode()) % this._size;
            /* *end crux* */


            /* second hashing, done now to eliminate checks in following loop */
            int offset = Math.Abs(key.GetProbeHash()) % this._size;
            offset = this.MakeHashOddNumber(offset);


            /*  trying at home position and down probe sequence, skipping same lengths by addition, 
             *  until an empty or tombstone for succeed, or until all slots visited or repeated key for fail; 
             *  each new calculated slot has to be %ed to keep slots within size of backing array  */
            for (int ix = 0; ix < _size; ix++, slot = (slot + offset) % this._size) {

                /* empty or tombstone state is a successful search for available slot; exfactored */
                if (this.SlotIsAvailable(slot)) {
                    return (new Duple_(slot));
                }

                /* a found key means a repeated key, which is a fail condition here; a hack? */
                if (this.SlotResult(slot, key) == SearchResult.Succeeded) {
                    return (new Duple_(SearchResult.Repeated));
                }

                /* the implicit, remaining option is a full slot with a different key; searching can continue */
            }


            // if we’ve reached this point, no free slots in table; inserting is impossible  
            return (new Duple_(SearchResult.Impossible));
        }

        private bool SlotIsAvailable(int slot)  /* passed */  {
            /*  returns flag value used in search for a first available slot: 
             *      true if an empty or tombstone is found, 
             *      false in any other case, to continue search  */

            if (_table[slot] == null) {
                return true;
            }
            else if (_table[slot].State == SlotState.Tombstone) {
                return true;
            }

            return false;
        }

        private void AnyRehashNow()  /* passed */  {
            /*  calls .Rehash() when current array too full or too many are tombstones (regardless of total elements); 
             *  int math here reverses proportions in "load factor" for calcs  */

            /*  this method assumes that the backing array should always be doubled in size; 
             *  in fact, it makes sense with many deletes (tombstones) for the array to be halved in size; 
             *  this is all I want to try to implement at present  */

            //**  *** following is for testing only; retained for any retesting ***  **//
            //_rehashBegun = false;
            //**  *** preceding is for testing only; retained for any retesting ***  **//


            if (_keys > 0) /* no point in rehashing, even based on tombstones, when there are no keys */ {
                if ((_size / _keys) < 2) {
                    this.Rehash(_size * 2);
                }
                else if ((_tombstones > 0) && ((_keys / _tombstones) < 2)) /* a reasonable guess at too-many tombstones  */ {
                    this.Rehash(_size);
                }
            }
        }

        private void Rehash(int size)  /* passed */  {
            /*  this method assumes that the backing array is not zero length, and that there are keys to rehash; 
             *  since this method is called only when the backing array is more than half full (including tombstones), 
             *  these should be safe assumptions  */


            //**  *** following is for testing .AnyRehashNow() only; retained for any retesting ***  **//
            //_rehashBegun = true;
            ///*  ops can't continue during testing, as mismatched metadata counts and actual data counts 
            // *  will cause fails during sort (which doesn't support nulls)  */
            //return;
            //**  *** preceding is for testing .AnyRehashNow() only; retained for any retesting ***  **//


            /* all-new hash object, whose hash table will be used so that existing insert and probe methods can be used */
            HashTable_<K, T> rehashed = new HashTable_<K, T>(size, this._failValue);  // minimal constructor OK here 


            /*  creating a sortable array of only the meaningful elements, then quick-sorting it by frequency of access, most-frequent first; 
             *  keys count excludes tombstones; this metadata is kept coordinated during inserts and deleted  */
            Quadruple_[] sortArray = new Quadruple_[this._keys];

            for (int ix = 0, offset = 0; ix < this._size; ix++) {
                if (_table[ix] != null) {
                    if (_table[ix].State != SlotState.Tombstone) {
                        sortArray[offset] = _table[ix];
                        offset++;
                    }
                }
            }

            sortArray = SortingAlgorithms_<Quadruple_>.Sort_Quick_Insertion_(sortArray);


            /* the actual rehashing, of only the meaningful elements, from their sorted thru array; using .Insert() ensures all are slotted correctly */
            for (int ix = 0; ix < this._keys; ix++) {
                rehashed.Insert(sortArray[ix].Key, sortArray[ix].Value);
            }


            /* swapping the backing arrays, changing metadata; ^rehashed should be collected by GC after this, since no longer referenced */
            this._table = rehashed._table;
            this._size = rehashed._size;
        }

        #endregion Insert() And Its New Dependencies


        #region Delete() (No New Dependencies)

        public SearchResult Delete(K key)  /* passed */  {
            /* algo:  find at index, then "delete" from that index by flagging it as a tombstone */

            /* the search for the existing key, exfactored, returns flag or slot */
            Duple_ searchResult = SearchForKey(key);


            /* fail path */
            if (searchResult.Result != SearchResult.Succeeded) {
                return searchResult.Result;
            }


            /* succeed path */
            int slot = searchResult.Slot;

            _table[slot].State = SlotState.Tombstone;

            /* values in "deleted" element are not actually deleted, but can't be found using Find() */

            // metadata 
            _keys--;
            _tombstones++;

            // maintaining table efficiency; exfactored; ideally would occur as tail call after return 
            this.AnyRehashNow();

            /* conveying results */
            return SearchResult.Succeeded;
        }

        #endregion Delete() (No New Dependencies)

    }


    /* extension to System.Object so all objects can return a second hash whenever I need them to */
    public static class HashExtensions_
    {
        public static int GetProbeHash(this object subject)  /* passed */  {
            /* the second hash, for any probe sequence, must be completely independent of the first hash result; */
            /* my jury-rigged hash is to convert any non-string into a string, and to reverse strings, then hash the string; */
            /* since .GetHashCode() can return negative numbers, I'm not Math.Abs()-ing them here for consistent handling in consuming code */

            string topic = string.Empty;

            if (subject.GetType() == typeof(string)) {
                char[] asChars = (subject as string)
                    .Reverse()
                    .ToArray();

                topic = new string(asChars);
            }
            else {
                topic = subject.ToString();
            }

            return topic.GetHashCode();
        }
    }
}
