﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class Stack_Array_<T> : Stack_<T>
    {
        #region Definitions

        const int defaultSize = 100;

        #endregion Definitions


        #region Fields

        T[] _stack;
        int _length = 0;

        #endregion Fields


        #region Properties, including .Peek

        public override int Length { get { return _length; } }     /* passed */

        public override T Peek { get { return _stack[_length - 1]; } }    /* passed */

        #endregion Properties, including .Peek


        #region Constructors

        public Stack_Array_()  /* ok */  {
            _stack = new T[defaultSize];
        }

        public Stack_Array_(int capacity)  /* verified */  {
            _stack = new T[capacity];
        }

        #endregion Constructors


        #region Changing list contents

        public override void Push(T element)  /* passed */  {
            // put new element at first empty index (same as _length),
            // then increment length to be one more than last used index

            _stack[_length] = element;
            _length++;
        }

        public override T Pop()  /* passed */  {
            // retain element at last used index,
            // then decrement index, in effect throwing out former last value

            T result = _stack[_length - 1];
            _length--;

            return result;
        }

        public override void Clear()  /* passed */  {
            // simply setting metadata to the empty-default
            // so that stacking proceeds from the beginning again,
            // in effect throwing out all former values

            _length = 0;
        }

        #endregion Changing list contents
    }
}
