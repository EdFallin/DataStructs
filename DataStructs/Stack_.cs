﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public enum StackType
    {
        None,
        Fail,
        ArrayBased,
        LinkedListBased,
    }


    // ADT, virtual except for the factory method

    public class Stack_<T>
    {
        #region Properties, Including Peek

        public virtual int Length { get { return -1; } }

        public virtual T Peek { get { return default(T); } }

        #endregion Properties, Including Peek


        #region Changing List Contents

        public virtual void Push(T element)
        {
            // no operations 
        }

        public virtual T Pop() { return default(T); }

        public virtual void Clear() { }

        #endregion Changing List Contents


        #region Static Factory Method For Concrete Stacks

        public static Stack_<T> SupplyStack_T(StackType stackType)
        {
            Stack_<T> stack = null;

            switch (stackType)
            {
                case StackType.ArrayBased:
                    stack = new Stack_Array_<T>();
                    break;
                case StackType.LinkedListBased:
                    stack = new Stack_LinkedList_<T>();
                    break;
                default:
                    stack = new Stack_Fails_<T>();
                    break;
            }

            return stack;
        }

        #endregion Static Factory Method For Concrete Stacks
    }
}
