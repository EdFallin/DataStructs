﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    public class ForkBpt<K> : NodeBpt<K>, IKeyedObject<K> where K : IEquatable<K>, IComparable<K>
    {
        #region Properties

        /* at a fork, contents are child nodes */
        public override IKeyedObject<K>[] Contents { get; } = new NodeBpt<K>[MaxNodes];

        #endregion Properties


        #region Constructors

        public ForkBpt() {
            /* no operations */
        }

        public ForkBpt(K key) : base(key) {
            /* no operations */
        }

        #endregion Constructors


        #region Methods

        /* inserting restructures at _current_ node; removing restructures / sloshes at node _below_ */

        #region Insert() and dependencies

        public override NodeBpt<K> Insert(int location, IKeyedObject<K> insertable)  /* verified */  {
            /* first see if Split() must be invoked; if so, invoke it; 
             * then invoke AddToContents() on the correct node */

            /* do-split path */
            if (this.TopKey == Top) {
                /* move half of this node to a new sibling */
                NodeBpt<K> sibling = this.Split();

                /* simple insert with shift; changed TopKey */
                if (location < this.TopKey) {
                    this.AddToContents(++location, insertable);
                }
                else {
                    location -= this.Count;  // if split, has shifted Lward 
                    sibling.AddToContents(++location, insertable);
                }

                return sibling;
            }

            /* no-split path */
            this.AddToContents(++location, insertable);
            return this;
        }

        public override NodeBpt<K> Split()  /* verified */  {
            /* initing a new sibling and shifting Rmost ½ or [½ -1] contents to it */

            ForkBpt<K> sibling = new ForkBpt<K>();

            int max = MaxNodes;  // ~this is known to be full 
            int midpoint = (max / 2) + 1;  // make L have more items after split 

            /* copy R half to sibling; not shifting, so incr loop */
            for (int from = midpoint, to = 0; from < max; from++, to++) {
                sibling.Contents[to] = this.Contents[from];
                sibling.Keys[to] = this.Contents[from].Key;
            }

            /* metadata of both nodes; complementary counts / tops */
            sibling.Count = max - midpoint;
            this.Count = midpoint;

            sibling.Key = sibling.Keys[0];

            /* conveying */
            return sibling;
        }

        public override void AddToContents(int location, IKeyedObject<K> insertable)  /* verified */  {
            /* ~insertable should be the correct node, and ~location correctly adjusted; 
             * called on whichever node ~insertable belongs on, so ~this used throughout */

            /* shift upward 1, from R end Lward to preserve contents */
            for (int from = this.TopKey, to = this.Count; from >= location; from--, to--) {
                this.Contents[to] = this.Contents[from];
                this.Keys[to] = this.Contents[from].Key;
            }


            /* the actual insertion, 1 Lward of prior ~location child */
            this.Keys[location] = insertable.Key;
            this.Contents[location] = insertable;

            /* metadata */
            this.Key = this.Keys[0];  // may have changed 
            this.TopKey++;
        }

        #endregion Insert() and dependencies


        #region Remove() and dependencies

        /* in the reftext, this is called 'underflowing' instead, which emphasizes that it's only _possibly_ removing */
        public override bool Remove(int location, IKeyedObject<K> removable)  /* verified */  {
            /* handling _leafward contents_ of this node, not node itself; 
             * slosh and maybe merge, returning ~true if merged, for rootward reacting */

            /* if _also_ underflowing here, try sloshing; if none, perform merging */
            /* ~didSlosh == true if sloshing possible / enough */
            bool didSlosh = this.Slosh(location);

            /* if sloshing performed, no rootward underflows, so ~false */
            if (didSlosh) {
                return false;
            }

            /* merge two child nodes */
            this.Merge(location, this.Contents[location] as NodeBpt<K>);

            /* return to rcaller with flag ~true if this node is now underflowing */
            if (this.Count < (MaxNodes / 2)) {
                return true;
            }

            /* return to rcaller with flag ~false if not underflowing; no rootward changes needed */
            return false;
        }


        #region Sloshing

        public bool Slosh(int location)  /* verified */  {
            /* if underflowing here (determined at caller), try redistributing children */

            /* if a sibling can slosh, slosh to first such so each has ½ of _sum_ of both; 
             * if sloshing performed, return ~true so no downstream merge */

            NodeBpt<K> underflowing = this.Contents[location] as NodeBpt<K>;

            bool didSlosh = false;

            /* try each possible sibling in turn, L then R */

            if (location > 0) {
                didSlosh = this.SloshFromLeftIf(location, underflowing);
            }

            /* if sloshing performed from L, can exit with set flag */
            if (didSlosh) {
                return didSlosh;
            }

            if (location < this.TopKey) {
                didSlosh = this.SloshFromRightIf(location, underflowing);
            }

            /* return with flag from possible sloshing from R */
            return didSlosh;
        }

        protected bool SloshFromLeftIf(int location, NodeBpt<K> underflowing)  /* verified */  {
            int half = MaxNodes / 2;

            NodeBpt<K> leftSibling = this.Contents[location - 1] as NodeBpt<K>;

            if (leftSibling.Count > half) {
                int sloshable = (leftSibling.Count - half) / 2;  // split about evenly across nodes 
                sloshable = sloshable > 0 ? sloshable : 1;  // always at least 1 to move 

                /* shift items in ~underflowing upward, since slosheds at L end; from R end Lward to preserve */
                for (int from = underflowing.TopKey, to = underflowing.TopKey + sloshable; from >= 0; from--, to--) {
                    underflowing.Contents[to] = underflowing.Contents[from];
                    underflowing.Keys[to] = underflowing.Contents[from].Key;
                }

                /* slosh across the nodes, from L end of sloshables Rward; traversal direction doesn't matter */
                for (int from = leftSibling.Count - sloshable, to = 0; from < leftSibling.Count; from++, to++) {
                    underflowing.Contents[to] = leftSibling.Contents[from];
                    underflowing.Keys[to] = leftSibling.Contents[from].Key;
                }

                /* horizontal metadata, reflecting count of sloshed */
                underflowing.TopKey += sloshable;
                leftSibling.TopKey -= sloshable;

                /* vertical metadata, reflecting changed contents */
                underflowing.Key = underflowing.Keys[0];
                this.Keys[location] = underflowing.Key;

                /* sloshing succeeded; caller --> ~false to recursor */
                return true;
            }

            /* sloshing failed; next, other-side slosh tried, ± caller --> ~true to recursor  */
            return false;
        }

        protected bool SloshFromRightIf(int location, NodeBpt<K> underflowing)  /* verified */  {
            int half = MaxNodes / 2;

            NodeBpt<K> rightSibling = this.Contents[location + 1] as NodeBpt<K>;

            if (rightSibling.Count > half) {
                int sloshable = (rightSibling.Count - half) / 2;  // split about evenly across nodes 
                sloshable = sloshable > 0 ? sloshable : 1;  // always at least 1 to move 

                /* slosh across nodes, from ~rightSibling's L end to ~underflowing's R end; traversal direction doesn't matter */
                for (int from = 0, to = underflowing.Count; from < sloshable; from++, to++) {
                    underflowing.Contents[to] = rightSibling.Contents[from];
                    underflowing.Keys[to] = rightSibling.Contents[from].Key;
                }

                /* shift remaining contents of ~rightSibling Lward; from L end Rward to preserve */
                for (int from = sloshable, to = 0; from < rightSibling.Count; from++, to++) {
                    rightSibling.Contents[to] = rightSibling.Contents[from];
                    rightSibling.Keys[to] = rightSibling.Contents[from].Key;
                }

                /* horizontal metadata, reflecting count of sloshed */
                underflowing.TopKey += sloshable;
                rightSibling.TopKey -= sloshable;

                /* vertical metadata, reflecting changed contents */
                rightSibling.Key = rightSibling.Keys[0];
                this.Keys[location + 1] = rightSibling.Key;

                /* sloshing succeeded; caller --> ~false to recursor */
                return true;
            }

            /* sloshing failed; other-side slosh already failed; caller --> ~true to recursor  */
            return false;
        }

        #endregion Sloshing


        #region Merging

        public bool Merge(int location, NodeBpt<K> underflowing)  /* verified */  {
            /* if this point reached, no sloshing possible, so restructuring here */

            /* get top key indices of any siblings, max defaults --> skip ops if no sibling */
            int leftTopKey = int.MaxValue;
            int rightTopKey = int.MaxValue;

            if (location > 0) {
                leftTopKey = (Contents[location - 1] as NodeBpt<K>).TopKey;
            }

            if (location < this.TopKey) {
                rightTopKey = (Contents[location + 1] as NodeBpt<K>).TopKey;
            }

            /* merge with the non-null sibling with the _fewest_ children */
            if (leftTopKey < rightTopKey) {
                this.MergeWithLeft(location, underflowing);
            }
            else {  /* both can't be null, so if this path, R is present */
                this.MergeWithRight(location, underflowing);
            }

            /* rpop: return ~true so rootward rcaller knows it may need to restructure */
            return true;
        }

        protected void MergeWithLeft(int location, NodeBpt<K> underflowing)  /* verified */  {
            /* guaranteed present at this point */
            NodeBpt<K> leftSibling = this.Contents[location - 1] as NodeBpt<K>;

            /* merging: move _all_ of ~underflowing's Keys and Contents to R end of ~leftSibling */
            for (int from = 0, to = leftSibling.Count; from < underflowing.Count; from++, to++) {
                leftSibling.Keys[to] = underflowing.Keys[from];
                leftSibling.Contents[to] = underflowing.Contents[from];
            }

            /* vital horizontal metadata; no metadata needed for ~underflowing */
            leftSibling.TopKey += underflowing.Count;

            /* remove ~underflowing at ~location from this level's children & keys */
            for (int from = location + 1, to = location; from < this.Count; from++, to++) {
                this.Contents[to] = this.Contents[from];
                this.Keys[to] = this.Contents[from].Key;
            }

            /* vital vertical metadata */
            this.TopKey--;
        }

        protected void MergeWithRight(int location, NodeBpt<K> underflowing)  /* verified */  {
            /* guaranteed present at this point */
            NodeBpt<K> rightSibling = this.Contents[location + 1] as NodeBpt<K>;

            /* jump-shift ~rightSibling's Keys and Contents Rward so room for those from ~underflowing */
            int jump = rightSibling.TopKey + underflowing.Count;

            for (int from = rightSibling.TopKey, to = jump; from >= 0; from--, to--) {
                rightSibling.Keys[to] = rightSibling.Keys[from];
                rightSibling.Contents[to] = rightSibling.Contents[from];
            }

            /* merging: move _all_ of ~underflowing's Keys and Contents to L end of ~rightSibling */
            for (int from = 0, to = 0; from < underflowing.Count; from++, to++) {
                rightSibling.Keys[to] = underflowing.Keys[from];
                rightSibling.Contents[to] = underflowing.Contents[from];
            }

            /* vital horizontal metadata; none needed for ~underflowing */
            rightSibling.TopKey += underflowing.Count;
            rightSibling.Key = rightSibling.Keys[0];

            /* replace ~underflowing at ~location in this level's children & keys with its R sibling */
            for (int from = location + 1, to = location; from < this.Count; from++, to++) {
                this.Contents[to] = this.Contents[from];
                this.Keys[to] = this.Contents[from].Key;
            }

            /* vertical metadata */
            this.Key = this.Keys[0];  // may have changed 
            this.TopKey--;
        }

        #endregion Merging

        #endregion Remove() and dependencies

        #endregion Methods

    }
}