﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advanceds;


namespace DataStructs
{
    public enum Usage
    {
        Debug,
        Release,
    }

    /* I am working this out a second time (I think) using .NET BitArrays, in my Bitqueue class */

    public class HuffmanCodingTree_<T>     // although HCTs are usually used with chars, the T elements can be of varying types; keys are always ints 
    {
        #region Fields

        /* the HCT itself, a binary tree, and the minheap used to construct it */
        BinaryTree_CompositePointerNodes_<int, T> _huffmanTree = new BinaryTree_CompositePointerNodes_<int, T>();
        MinHeap_<int, CompositePointerNode_<int, T>> _sorter = null;

        /* the HCT converted to bit vectors */
        List<Tuple<T, BitQueue>> _lookups = new List<Tuple<T, BitQueue>>();
        bool[] _path = new bool[0];

        #endregion Fields


        #region Properties

        public BinaryTree_CompositePointerNodes_<int, T> HuffmanTree {
            get { return _huffmanTree; }
            set { _huffmanTree = value; }
        }

        internal MinHeap_<int, CompositePointerNode_<int, T>> Sorter {
            get { return _sorter; }
            set { _sorter = value; }
        }

        public Tuple<T, BitQueue>[] Lookups {
            get { return _lookups.ToArray(); }
        }

        #endregion Properties


        #region Constructors

        public HuffmanCodingTree_(Tuple<int, T>[] values)
            : this(values, Usage.Release)   // ok  
        {
            /* no additional operations */
        }

        public HuffmanCodingTree_(Tuple<int, T>[] values, Usage usage)   // passed  
        {
            Tuple<int, CompositePointerNode_<int, T>>[] contents = new Tuple<int, CompositePointerNode_<int, T>>[values.Length];

            // converting arg tuples to new tuples of keys and composite nodes (all of them leaves here) 
            for (int i = 0; i < values.Length; i++) {
                Tuple<int, T> local = values[i];
                contents[i] = new Tuple<int, CompositePointerNode_<int, T>>(
                    local.Item1, CompositePointerNode_<int, T>.InitCompositePointerNode_(local.Item1, local.Item2)
                    );
            }

            // new tuples moved to the minheap field and sorted 
            _sorter = new MinHeap_<int, CompositePointerNode_<int, T>>(contents, contents.Length);


            // organizing heap contents into the Huffman Coding Tree if useful for current op 
            if (usage == Usage.Release)
                this.ConstructHuffmanFromHeap();
        }

        #endregion Constructors


        #region Methods

        public BitQueue Encode(string target)  /* passed */  {
            BitQueue results = new BitQueue();   // empty; with this implemn, encoding unfortunately means repeatedly expanding this array 

            // RAII 
            if (this._lookups.Count == 0)
                this.FillLookups();

            // convert string char-by-char to a series of string encodings using lookups, 
            // and merge those into a ready-for-conversion string 
            for (int i = 0; i < target.Length; i++) {
                BitQueue encoding = this._lookups.Find(e => (e.Item1.Equals(target[i]))).Item2;
                results.EnqueueBits(encoding.BitsBooleanArray);
            }

            return results;
        }

        public string Decode(BitQueue encodedText)  /* passed */  {
            // iterative design with a loop that branches L or R in HCT based on an encoding char 
            //     and emits output when at a leaf, restarting at root with first char of next encoding; 
            // the emitting and restarting must come after branching, or codes are off by 1 char, 
            //     and encoding produces wrong results 

            StringBuilder results = new StringBuilder();


            // initializing node used in loop to needed starting value (root of HCT) 
            CompositePointerNode_<int, T> current = this._huffmanTree.Root;


            // looking at each bit in the encoding and branching accordingly 
            for (int i = 0; i < encodedText.Length; i++) {
                bool branching = encodedText.DequeueBit();   // Queue design and dequeueing allow us to always be at the right bit 

                // branching L or R through tree based on char to head toward encoded leaf 
                if (branching == false)
                    current = current.LeftChild;

                if (branching == true)
                    current = current.RightChild;

                // if we have now reached a leaf, its T is added to output, and we return to tree's root to traverse a new code 
                if (current.IsLeaf) {
                    results.Append(current.Element);
                    current = this._huffmanTree.Root;
                }
            }

            return results.ToString();
        }

        #endregion Methods


        #region Huffman Internals

        private void ConstructHuffmanFromHeap()   // passed  
        {
            // iteratively, the two lowest elements from the heap (which may be leaves or subtrees) are popped 
            // and made the two children of a new root, whose key is the sum of their keys; 
            // that subtree is placed back onto the heap; its root usually becomes an inode later, its key summed rootward; 
            // when only one item is left in the heap, that's the Huffman coding tree 

            while (this._sorter.Count > 1) {
                CompositePointerNode_<int, T> first = this._sorter.PopHeapRoot();
                CompositePointerNode_<int, T> second = this._sorter.PopHeapRoot();

                int subtreeKey = first.Key + second.Key;

                CompositePointerNode_<int, T> subtree =
                    CompositePointerNode_<int, T>.InitCompositePointerNode_(subtreeKey, default(T), first, second);

                // I didn't implement an if-not-0 check in my SwapUp() method in MinHeap_, so I'm checking 
                //     for an empty array here; however, ultimately this test best belongs in MinHeap_; 
                // as written, the latest subtree is returned when count of 0 instead of coming from the heap 
                if (this._sorter.Count > 0)
                    this._sorter.AddHeapNode(subtreeKey, subtree);
                else
                    // assigning the constructed tree to the field 
                    this._huffmanTree.Root = subtree;
            }
        }

        private void FillLookups()  /* passed */  {
            // externally persistent for straight use with recursor 
            BitQueue passer = new BitQueue();

            // recursion that stores each path in the collective field; no return value 
            this.RecursePreorderForPath(this._huffmanTree.Root, passer);
        }

        private void RecursePreorderForPath(CompositePointerNode_<int, T> subject, BitQueue passer)  /* verified */  {
            /* if we've reached a leaf, the binary path is already correct, so we can just add it and bail out */
            if (subject.IsLeaf) {
                this._lookups.Add(new Tuple<T, BitQueue>(subject.Element, passer.Clone()));   // .Clone() so lookups don't all share same (and useless) instance of `passer
                return;
            }

            int lengthSoFar = passer.Length;

            // current level: branch L or R and add matching 0 or 1 (`false or `true) to the Bitqueue 
            if (subject.LeftChild != null) {
                passer.EnqueueBit(false);
                this.RecursePreorderForPath(subject.LeftChild, passer);
            }

            // drop bits just added past this node, so further branchings do not use unrelated bits 
            passer.TruncateTo(lengthSoFar);

            if (subject.RightChild != null) {
                passer.EnqueueBit(true);
                this.RecursePreorderForPath(subject.RightChild, passer);
            }

            // drop bits just added past this node, so further branchings do not use unrelated bits 
            passer.TruncateTo(lengthSoFar);
        }

        #endregion Huffman Internals


    }
}
