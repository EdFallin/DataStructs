﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BufferPool_
    {
        #region Definitions

        public delegate byte[] ReadData(int diskBlock);
        public delegate void WriteData(int diskBlock, byte[] data);
        public const int NONE = -1;

        #endregion Definitions


        #region Fields

        List_DoublyLinked_<Buffer_> _pool = new List_DoublyLinked_<Buffer_>();   // my class 
        int _maxBuffers = 0;
        int _bufferSize = 0;

        // delegates 
        static ReadData _readData = null;
        static WriteData _writeData = null;

        #endregion Fields


        #region Properties

        public static ReadData DataReader  /* verified */  {
            get { return BufferPool_._readData; }
            set { BufferPool_._readData = value; }
        }

        public static WriteData DataWriter  /* verified */  {
            get { return BufferPool_._writeData; }
            set { BufferPool_._writeData = value; }
        }

        #endregion Properties


        #region Constructors

        public BufferPool_(int maxBuffers, int bufferSize, ReadData reader, WriteData writer)  /* verified */  {
            _maxBuffers = maxBuffers;
            _bufferSize = bufferSize;
            _readData = reader;
            _writeData = writer;
        }

        #endregion Constructors


        #region Public Methods

        public BufferProxy_ AcquireBuffer(int diskBlock)  /* passed */  {
            /* algo : find and return a buffer, or add one, or replace one; returned one to front */
            Buffer_ output = null;
            int existingBufferIndex = this.BufferIndexByDiskBlock(diskBlock);

            if (existingBufferIndex != NONE) {
                output = this.RemoveBuffer(existingBufferIndex);
            }
            else if (_pool.Length < _maxBuffers) {
                output = new Buffer_(diskBlock);
            }
            else {   // LRU 
                output = this.ReplacementBuffer(diskBlock);
            }

            this.InsertBufferAtFront(output);   // LRU 
            output.Retain();   // a buffer can’t be dumped as long as it is retained once or more 

            return (new BufferProxy_(output));   // buffer is wrapped in proxy for consumer 
        }

        public void FlushPool()  /* passed */  {
            /* working backward from end to avoid any possible invalid indices as items are dumped */
            
            _pool.PositionAtEnd();

            while (_pool.CurrentPosition >= 0) {
                Buffer_ current = _pool.CurrentValue;
                if (current.Altered) {
                    BufferPool_._writeData(current.DiskBlock, current.Data);   // static 
                }

                current.Invalidated = true;
                _pool.RemoveAtCurrent();   // after this, these will be collected by the GC once client code stops referencing them 

                _pool.Previous();
            }
        }

        #endregion Public Methods


        #region Private Methods

        private int BufferIndexByDiskBlock(int diskBlock)  /* passed */  {
            /* a linear search of buffers, Θ(n) ; pool expected to be small */

            _pool.PositionAtStart();

            for (   /* no init ops */; _pool.CurrentPosition < _pool.Length; _pool.Next()) {
                if (_pool.CurrentValue.DiskBlock == diskBlock) {
                    return _pool.CurrentPosition;
                }
            }

            // code path if arg block is not in buffer pool 
            return NONE;
        }

        private Buffer_ RemoveBuffer(int bufferIndex)  /* passed */  {
            /* used to move a buffer for both reinserting at front and dropping */
            Buffer_ output = null;
            _pool.PositionAtIndex(bufferIndex);
            output = _pool.RemoveAtCurrent();

            return output;
        }

        private Buffer_ ReplacementBuffer(int diskBlock)  /* passed */  {
            /* implements the LRU algorithm */
            int target = this.OldestDumpableBuffer();
            Buffer_ buffer = this.RemoveBuffer(target);

            // changed existing data written to its disk block 
            if (buffer.Altered) {
                BufferPool_.DataWriter(buffer.DiskBlock, buffer.Data);
            }   // a static 

            this.InvalidateBuffer(buffer);   // indirectly flagging buffer IPIs for consumer 

            buffer = new Buffer_(diskBlock);   // new buffer for new data; not yet in list 
            return buffer;

            /// *** following is for drafting only ***  
            //return null;
            /// *** preceding is for drafting only ***  
        }

        private int OldestDumpableBuffer()  /* passed */  {
            /* highest-indexed buffer with 0 retains is oldest-dumpable */
            int output = NONE;

            /* a reverse traversal, so I use doubly-linked list for field; on average is in Θ(n) */
            _pool.PositionAtEnd();

            while (_pool.CurrentPosition >= 0) {
                if (!(_pool.CurrentValue.IsRetained)) {
                    output = _pool.CurrentPosition;
                    break;
                }
                _pool.Previous();
            }

            return output;
        }

        private void InvalidateBuffer(Buffer_ buffer)  /* passed */  {
            /* the simple change here propagates as dependent objects are used */
            buffer.Invalidated = true;
        }

        private void InsertBufferAtFront(Buffer_ buffer)  /* passed */  {
            _pool.PositionAtStart();
            _pool.InsertAtCurrent(buffer);
        }

        #endregion Private Methods
    }


    public class Buffer_
    {
        #region Definitions

        public const int NONE = -1;

        #endregion Definitions


        #region Fields

        byte[] _data = null;
        int _diskBlock = NONE;

        bool _altered = false;

        bool _invalidated = false;

        int _retains = 0;

        #endregion Fields


        #region Properties

        public int DiskBlock  /* ok */  {
            get { return _diskBlock; }
            set { _diskBlock = value; }
        }

        public byte[] Data  /* ok */  {
            get { return this.ReturnData(); }
            set { _data = value; }
        }

        public bool IsRetained  /* passed */  {
            get { return (_retains > 0); }   // no set accessor 
        }

        public bool Invalidated  /* verified */  {
            get { return _invalidated; }
            // once a buffer has been invalidated, it stays invalidated 
            set { _invalidated = _invalidated || value; }
        }

        /* in normal use, this is set by the proxy when a write is performed; Buffer_ objects not directly exposed to client code */
        public bool Altered  /* verified */  {
            get { return _altered; }
            set { _altered = value; }
        }

        #endregion Properties


        #region Constructors

        public Buffer_(int diskBlock)  /* verified */  {
            _diskBlock = diskBlock;
        }

        #endregion Constructors


        #region Methods

        public void Retain()  /* verified */  {
            _retains++;
        }

        public void Release()  /* verified */  {
            _retains--;
        }

        private byte[] ReturnData()  /* passed */  {
            /*  return any existing data, assumed / guaranteed to be fresh */
            if (_data != null) {
                return _data;
            }
            else {
                _data = BufferPool_.DataReader(_diskBlock);
            }

            return _data;
        }

        #endregion Methods
    }


    public class BufferProxy_
    {
        #region Fields

        Buffer_ _buffer = null;
        bool _invalidated = false;

        #endregion Fields


        #region Properties

        public bool Invalidated  /* passed */  {
            get {
                // if actual buffer is invalid, so are all its proxies; this proxy can be individually invalidated 
                if (_invalidated || _buffer.Invalidated) {
                    _invalidated = true;
                }
                return _invalidated;
            }
            // can’t re-validate an invalidated proxy; uses || since flag is `true when invalidated 
            set {
                _invalidated = _invalidated || value;
            }
        }

        public byte[] Data  /* passed */  {
            get {
                if (this.Invalidated) {
                    this.ThrowInvalidatedException();
                    return null;   // should be unreachable code; apparently not, in properties only? 
                }
                else {
                    return _buffer.Data;
                }
            }
            set {
                if (this.Invalidated) {
                    this.ThrowInvalidatedException();
                }
                else {
                    _buffer.Data = value;
                    _buffer.Altered = true;
                }
            }
        }

        #endregion Properties


        #region Constructors

        public BufferProxy_(Buffer_ buffer)  /* verified */  {
            _buffer = buffer;
        }

        #endregion Constructors


        #region Methods

        /* no retain method, as this should not be needed by consumer code */

        // this method always sets _invalidated, but never re-releases, so retain counts stay correct 
        public void Release()  /* passed */  {
            if (!(this.Invalidated)) {
                _buffer.Release();
            }

            _invalidated = true;
        }

        private void ThrowInvalidatedException()  /* verified */  {
            throw new Exception("The buffer you are addressing has been invalidated.");
        }

        #endregion Methods
    }

}
