﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructs
{
    /* `vector` is a well-known term for a self-extending array */

    /* "extended" here means "plus indexing and extended with enumeration using in-class method", as those are defined in C# syntax */

    public class List_Vector_Extended_<T> : List_<T>, IEnumerable<T>
    {
        #region Definitions

        const int DEFAULT_SIZE = 16;

        #endregion Definitions


        #region Fields

        int _arraySize;  // of entire array 
        int _listSize;   // of array elements used by list 
        int _currentIndex;

        T[] _array;      // underlying data structure, consecutive memory blocks of the same size; 
        // in this enviro, each block holds a pointer to an object ( unless T is a primitive )

        #endregion Fields


        #region Constructors

        public List_Vector_Extended_()
            : this(DEFAULT_SIZE)  /* passed */  {
            // no operations 
        }

        public List_Vector_Extended_(int length)  /* passed */  {
            _array = new T[length];
            _arraySize = length;
        }

        #endregion Constructors


        #region Properties - List Metadata

        public override int Length {
            get { return _listSize; }
        }

        private int Max {
            get { return _arraySize; }
        }

        public override int CurrentPosition {
            get { return _currentIndex; }
        }

        public override int Top {
            get { return (_listSize - 1); }
        }

        #endregion Properties - List Metadata


        #region Property - Current Element Value

        public override T CurrentValue   /* passed */  {

            get  /* passed */  {
                if (_currentIndex > -1 && _currentIndex < this.Length)
                    return _array[_currentIndex];
                else
                    throw new Exception("@CurrentValue-get: _currentIndex < 0 || _currentIndex > this.Top");
            }

            set  /* passed */  {
                if (_currentIndex > -1 && _currentIndex < this.Length)
                    _array[_currentIndex] = value;
                else
                    throw new Exception("@CurrentValue-set: _currentIndex < 0 || _currentIndex > this.Top");
            }
        }

        #endregion Property - Current Element Value


        #region Changing List Contents

        // inserts rather than replacing 
        public override void InsertAtCurrent(T element)  /* passed */  {
            if (_listSize == _arraySize)
                this.ExtendArray();

            // first, existing elements have to be shifted forward in the array if possible; 
            //     using full _listSize because it will be filled after shifting 
            for (int i = _listSize; i > _currentIndex; i--)
                _array[i] = _array[i - 1];

            // second, the new element is inserted 
            _array[_currentIndex] = element;

            // third, metadata 
            _listSize++;
        }

        // returns removed element; 
        // if array has been extended, does not dump extra elements 
        public override T RemoveAtCurrent()  /* passed */  {
            // first, get value to return 
            T current = _array[_currentIndex];

            // second, shift all elements down by one; 
            //     that also throws away removed element; 
            //     leftover element is ignored, as it is left unindexed 
            for (int i = _currentIndex; i < this.Top; i++) {
                _array[i] = _array[i + 1];
            }

            // third, metadata 
            _listSize--;

            // fourth, output 
            return current;
        }

        public override void Append(T element)  /* passed */  {
            if (_listSize == _arraySize)
                this.ExtendArray();

            // first, appending at 1 past last index ( .Top ) 
            _array[_listSize] = element;

            // second, metadata 
            _listSize++;
        }

        public override void Clear()  /* verified */  {
            // no deallocation; left to the GC 
            _listSize = 0;
            _currentIndex = -1;
        }

        #endregion Changing List Contents


        #region Consecutive Positioning

        public override void Next()  /* passed */  {
            if (_currentIndex == this.Top)
                throw new Exception("@Next: _currentIndex == this.Top");

            _currentIndex++;
        }

        public override void Previous()  /* passed */  {
            if (_currentIndex == 0)
                throw new Exception("@PositionAtStart: _currentIndex == 0");

            _currentIndex--;
        }

        #endregion Consecutive Positioning


        #region Nonconsecutive Positioning

        public override void PositionAtStart()  /* passed */  {
            _currentIndex = 0;
        }

        public override void PositionAtEnd()  /* passed */  {
            _currentIndex = this.Top;
        }

        public override void PositionAtIndex(int index)  /* passed */  {
            if (index < 0 || index > this.Top)
                throw new Exception("@PositionAtIndex: index < 0 || index > this.Top");

            _currentIndex = index;
        }

        #endregion Nonconsecutive Positioning


        #region Indexing
        /* indexing is an O(1) operation, because the underlying structure is an array */

        public T this[int index]  /* passed */  {
            get  /* passed */  { return this._array[index]; }
            set   /* passed */ { this._array[index] = value; }
        }

        #endregion Indexing


        #region Enumerating

        /*  enumerating without a separate enumerator class using two .GetEnumerator interface implementations; 
         *  ***both*** must be coded for a class to implement IEnumerable<T> this way, and the <T> one must be `public`; 
         *  the <T> one is the one actually called, at least normally  */

        /* this is the one that actually gets invoked, at least normally, with generics */
        public System.Collections.Generic.IEnumerator<T> GetEnumerator() {
            for (int i = 0; i < this.Length; i++) {
                yield return this[i];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            for (int i = 0; i < this.Length; i++) {
                yield return this[i];
            }
        }


        #endregion Enumerating


        #region Internals

        private void ExtendArray()  /* passed */  {
            /* alias existing array, make new of double the size, copy over, repoint to new */

            T[] swap = _array;

            /* new size */
            _arraySize = _arraySize * 2;
            _arraySize = _arraySize > 0 ? _arraySize : 1;  // if needed, coerce to minimum 

            /* new array */
            _array = new T[_arraySize];

            /* copying */
            for (int i = 0; i < _listSize; i++) {
                _array[i] = swap[i];
            }
        }

        #endregion Internals

    }
}
