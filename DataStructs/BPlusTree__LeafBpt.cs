﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    public class LeafBpt<K> : NodeBpt<K>, IKeyedObject<K> where K : IEquatable<K>, IComparable<K>
    {
        #region Properties

        /* at leaves, contents are key-pointer indexer elements */
        public override IKeyedObject<K>[] Contents { get; } = new ElementBpt<K>[MaxNodes];

        #endregion Properties


        #region Constructors

        public LeafBpt() {
            /* no operations */
        }

        public LeafBpt(K key) : base(key) {
            /* no operations */
        }

        #endregion Constructors


        #region Methods

        #region Insert() and dependencies

        public override NodeBpt<K> Insert(int location, IKeyedObject<K> insertable)  /* verified */  {
            /* first see if Split() must be invoked; if so, invoke it; 
             * then invoke AddToContents() on the correct node, 
             *     incrementing ~location so correct for insert */

            /* do-split path */
            if (this.TopKey == Top) {
                /* move half of this node to a new sibling */
                NodeBpt<K> sibling = this.Split();

                /* simple insert with shift; changed TopKey */
                if (location < this.TopKey) {
                    this.AddToContents(++location, insertable);
                }
                else {
                    location -= this.Count;  // if split, has shifted Lward 
                    sibling.AddToContents(++location, insertable);
                }

                return sibling;
            }

            /* no-split path */
            this.AddToContents(++location, insertable);
            return this;
        }

        public override NodeBpt<K> Split()  /* verified */  {
            /* initing a new sibling and shifting Rmost ½ or [½ -1] contents to it */

            LeafBpt<K> sibling = new LeafBpt<K>();

            int max = MaxNodes;  // readability 
            int midpoint = (max / 2) + 1;  // make L have more items after split 

            /* first copy R half to sibling, then emplace ~insertable, with shifting */
            /* copy R half to sibling; not shifting, so incr loop */
            for (int from = midpoint, to = 0; from < max; from++, to++) {
                sibling.Contents[to] = this.Contents[from];
                sibling.Keys[to] = this.Contents[from].Key;
            }

            /* metadata of both nodes; complementary counts / tops */
            sibling.Count = max - midpoint;
            this.Count = midpoint;

            sibling.Key = sibling.Keys[0];

            /* conveying */
            return sibling;
        }

        public override void AddToContents(int location, IKeyedObject<K> insertable)  /* verified */  {
            /* ~insertable should be the correct node, and ~location correctly adjusted; 
             * called on whichever node ~insertable belongs on, so ~this used throughout */

            /* adds ~insertable to Contents and linked list, its key to Keys; adjusts metadata */

            #region Linked list

            /* repointing the element into the linked list; done first for reliable ~location; 
             * Elvising and null-guarding as real link items are not guaranteed */

            ElementBpt<K> inserting = insertable as ElementBpt<K>;

            ElementBpt<K> current = null;
            ElementBpt<K> leftLink = null;
            ElementBpt<K> rightLink = null;

            /* inserting L of ~current now? */
            if (location < this.Count) {
                current = this.Contents[location] as ElementBpt<K>;
                leftLink = current?.Previous;  // ends up 1 L of ~inserting 
                rightLink = current;
            }
            else {
                leftLink = this.Contents[location - 1] as ElementBpt<K>;
                rightLink = leftLink?.Next;
            }

            /* new element */
            inserting.Previous = leftLink;
            inserting.Next = rightLink;

            /* either side */
            if (leftLink != null) {
                leftLink.Next = inserting;
            }

            if (rightLink != null) {
                rightLink.Previous = inserting;
            }

            #endregion Linked list

            #region Keys and Contents

            /* shift upward 1, from R end Lward to preserve contents */
            for (int from = this.TopKey, to = this.Count; from >= location; from--, to--) {
                this.Contents[to] = this.Contents[from];
                this.Keys[to] = this.Contents[from].Key;
            }

            /* the actual insertion, 1 Lward of prior ~location element */
            this.Keys[location] = insertable.Key;
            this.Contents[location] = insertable;


            /* metadata */
            this.Key = this.Keys[0];  // may have changed 
            this.TopKey++;

            #endregion Keys and Contents
        }

        #endregion Insert() and dependencies


        #region Remove (no dependencies)

        public override bool Remove(int location, IKeyedObject<K> removable)  /* verified */  {
            /* remove ~removable by shifting ± changing metadata + taking out of linked list; 
             * look for underflow, if so, return ~true, otherwise ~false */

            /* redefining ~location; _exact_ in leaf; either matches or doesn't */
            location = Searchers_.FindBinary(this.Keys, removable.Key, 0, this.TopKey);

            /* not-found path; returns ~false, same as a found-without-underflow */
            if (location == None) {
                return false;
            }

            #region Linked list

            /* repointing the element out of the linked list; done first for reliable ~location */
            ElementBpt<K> removed = this.Contents[location] as ElementBpt<K>;
            ElementBpt<K> leftLink = removed.Previous;
            ElementBpt<K> rightLink = removed.Next;

            /* removed element; GC will collect */
            removed.Previous = null;
            removed.Next = null;

            /* either side; null-guarded for extreme ends */
            if (leftLink != null) {
                leftLink.Next = rightLink;
            }

            if (rightLink != null) {
                rightLink.Previous = leftLink;
            }

            #endregion Linked list

            #region Keys and Contents

            /* removing just by shift, usually; Rward from removal site to preserve items */
            for (int from = location + 1, to = location; from < this.Count; from++, to++) {
                this.Keys[to] = this.Keys[from];
                this.Contents[to] = this.Contents[from];
            }

            /* when ~removable at TopKey, this code path necessary; metadata later */
            if (location == this.TopKey) {
                this.Contents[location] = null;
            }

            /* vital metadata; if ~removable at TopKey, in effect removes its .Key */
            this.TopKey--;

            /* no sloshing or merging here, as that is best done by parent node */

            #endregion Keys and Contents

            /* if underflow, _parent_ node will slosh or else merge as needed */
            if (this.Count < (MaxNodes / 2)) {
                return true;
            }

            /* ~removable found and removed, without underflow */
            return false;
        }

        #endregion Remove (no dependencies)

        #endregion Methods

    }
}