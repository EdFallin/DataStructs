﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class Queue_Fails_<T> : Queue_<T>
    {
        // when using this class, the _ fields must be set to fail values of type T, and of int for _length


        #region Properties, Including Peek

        public override T Peek
        {
            get { return this._peek; }
        }

        public override int Length
        {
            get { return this._length; }
        }

        #endregion Properties, Including Peek




        #region Constructors

        public Queue_Fails_()
        {
            // no operations
        }

        public Queue_Fails_(int length)
        {
            // no operations
        }

        #endregion Constructors


        #region Changing List Contents

        public override void Clear()
        {
            // no operations
        }

        public override void Enqueue(T element)
        {
            // no operations
        }

        public override T Dequeue() { return this._dequeue; }

        #endregion Changing List Contents
    }
}
