﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class Queue_Array_<T> : Queue_<T>
    {
        #region Fields

        T[] _array = null;
        int _size;      // array size, regardless of length of queue
        int _first;     // first one queued, and the one that is dequeued next
        int _last;      // last one queued, and the one that is dequeued last

        #endregion Fields


        #region Properties, Including Peek

        public override T Peek    // passed  
        {
            get
            {
                // oldest is returned
                if (_length == 0)
                    return default(T);

                return _array[_first];
            }
        }

        public override int Length    // passed  
        {
            get { return this._length; }
        }

        #endregion Properties, Including Peek


        #region Constructors

        public Queue_Array_()   // passed  
            : this(8)
        {
            // no operations
        }

        public Queue_Array_(int length)   // verified  
        {
            this.Init(length);
        }

        // allows constructor and clearer to use the same code
        private void Init(int length)   // verified  
        {
            _array = new T[length];
            _size = length;
            _first = 0;
            _last = -1;
            _length = 0;
        }

        #endregion Constructors


        #region Changing List Contents

        public override void Clear()   // passed  
        {
            this.Init(_size);
        }

        public override void Enqueue(T element)   // passed  
        {
            if (_length == _size)
                throw new Exception("_length == _size");

            // incrementing; then modulo (remainder) converts
            // all numbers to those within array size
            _last++;
            _last %= _size;

            // assigning newest value
            _array[_last] = element;

            // queue metadata
            _length++;
        }

        public override T Dequeue()   // passed  
        {
            if (_length == 0)
                throw new Exception("_length == 0");

            // retain current first / oldest
            T results = _array[_first];

            // advance start of queue; modulo converts to within array size
            _first++;
            _first %= _size;

            // queue metadata
            _length--;

            return results;
        }

        #endregion Changing List Contents
    }
}
