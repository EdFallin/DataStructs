﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    /* >>> when adjacency-list graph things are done, maybe add an ADT interface from its surface 
     *     and implement that also in an adjacency-matrix form, basically using the same tests */
    public class Graph_
    {
        #region Definitions, including all of Edge class

        private const int UNWEIGHTED = int.MaxValue;
        private const int MAX_WEIGHT = int.MaxValue;

        private const int NONE = -1;

        private const bool UNREACHED = false;
        private const bool REACHED = true;

        /* public to allow easy testing */
        public class Edge
        {
            public int FarEnd { get; set; }
            public int Weight { get; set; }

            public Edge Next { get; set; }

            public Edge(int farEnd) : this(farEnd, UNWEIGHTED)  /* passed */  {
                /* no additional operations */
            }

            public Edge(int farEnd, int weight)  /* passed */  {
                FarEnd = farEnd;
                Weight = weight;
            }
        }

        /* public to allow easy testing */
        public enum GraphColor
        {
            None = 0,  // default value 
            Black,
            White,
        }

        #endregion Definitions, including all of Edge class


        #region Static properties

        public static int Unweighted { get => UNWEIGHTED; }
        public static int None { get => NONE; }

        public static bool Unreached { get => UNREACHED; }
        public static bool Reached { get => REACHED; }

        #endregion Static properties


        #region Fields

        // the actual graph: vertices are defined by their indices here, 
        // and edges by their presence in the linked list at a vertex 
        Edge[] _edges;

        // metadata used in searches (traversals) and dependent algorithms 
        bool[] _marks;

        bool _isDirected;

        #endregion Fields


        #region Properties

        public int VertexCount { get; protected set; }
        public int EdgeCount { get; protected set; }

        #endregion Properties


        #region Algorithm delegate properties

        public Action<int> AtArriveNewVertex { get; set; }
        public Action<int, int> AtAnyEdge { get; set; }  /* this is unlike reftext; always invoked when traversing edges */
        public Action<int, int> AtNewEdge { get; set; }
        public Action<int> AtDepartNewVertex { get; set; }

        #endregion Algorithm delegate properties


        #region Constructors and dependencies

        public Graph_(int vertexCount, bool isDirected)  /* verified */  {
            _edges = new Edge[vertexCount];
            _marks = new bool[vertexCount];

            VertexCount = vertexCount;

            _isDirected = isDirected;

            // to avoid checking for null, or using Elvis operators 
            InitNullObjectDelegates();
        }

        private void InitNullObjectDelegates()  /* verified */  {
            AtArriveNewVertex = (int unused) => { };
            AtAnyEdge = (int unusedNear, int unusedFar) => { };
            AtNewEdge = (int unusedNear, int unusedFar) => { };
            AtDepartNewVertex = (int unused) => { };
        }

        #endregion Constructors and dependencies


        #region Getting values of vertices

        /* this is only implemented as results for the whole graph, since it's only needed in a few cases */

        public (int[] outDegrees, int[] inDegrees) VertexDirectedDegrees()  /* passed */  {
            /* in-degrees and out-degrees can be found in an O(n + m) traversal like this */

            int[] inDegrees = new int[VertexCount];
            int[] outDegrees = new int[VertexCount];

            // all vertices are traversed 
            for (int vertex = 0; vertex < VertexCount; vertex++) {
                // all edges of each vertex are traversed 
                Edge edge = _edges[vertex];

                while (edge != null) {
                    // these follow from the definitions of in- and out-degrees 
                    outDegrees[vertex]++;
                    inDegrees[edge.FarEnd]++;

                    edge = edge.Next;
                }
            }

            return (outDegrees, inDegrees);
        }

        #endregion Getting values of vertices


        #region Getting values of edges

        public bool HasEdge(int nearEnd, int farEnd)  /* passed */  {
            // get first edge, if any 
            Edge edge = _edges[nearEnd];

            // traverse all edges, returning if any matches 
            while (edge != null) {
                if (edge.FarEnd == farEnd) {
                    return true;
                }

                edge = edge.Next;
            }

            // returning if none matched 
            return false;
        }

        public int WeightOfEdge(int nearEnd, int farEnd)  /* passed */  {
            Edge edge = _edges[nearEnd];

            // O(n) traversal of edges from this vertex 
            while (edge != null) {
                if (edge.FarEnd == farEnd) {
                    return edge.Weight;
                }

                edge = edge.Next;
            }

            return NONE;
        }

        #endregion Getting values of edges


        #region Adding, changing, and removing edges

        #region Remarks about vertex degrees

        /* I'm not using built-in degrees, but they can be added and defined like this if needed: 
               two arrays of ints are defined, each the same size as _edges, and all 0s; 
               accessors are created, not properties, so that values can't be set directly; 
               on directed graphs, when a new edge is set, out-degree is incremented 
                   at the near end, and in-degree is incremented at the far end; 
               on undirected graphs, in the conditional for setting the reverse edge, 
                   out-degree is incremented at the _far_ end, and in-degree at the _near_ end; 
               in degree and out degree for a vertex are always the same on an undirected graph 
                   this way, and a degree can be defined as either one of them; 
               on directed graphs, degree can instead be calculated as [[out-degree] +[in-degree]]; 
               if degrees are used, they must be decremented correctly when cutting edges, 
                   and they must not be changed when changing the weight of an existing edge  */

        #endregion Remarks about vertex degrees

        public void SetEdge(int nearEnd, int farEnd, int weight)  /* passed */  {
            /* sets one directed Edge if this is a directed graph; 
             * sets pair of directed Edges if an undirected graph */

            /* edges are inserted in order, making each edge added an O(n) operation; 
             * temporary sentinel at the head of the linked list (factored) simplifies operations; 
             * since the sentinel's .Next is set to the list's head, sentinel is never addressed 
             * by other graph ops, and should be garbage-collected after this method exits */

            // getting edge to insert _after_, and setting a sentinel for easier work with linked list 
            (Edge sentinel, Edge at) = GetPrecedingDirectedEdge(nearEnd, farEnd);

            // @at is the element in the linked list _before_ the targeted one 
            if ((at.Next != null) && (at.Next.FarEnd == farEnd)) {
                // changing the weight of existing edge (after @at) to @weight 
                SetExistingDirectedEdge(at, weight);
            }
            else {
                // adding a new edge after @at, and using @sentinel to set start of linked list 
                SetNewDirectedEdge(sentinel, at, nearEnd, farEnd, weight);

                // graph metadata; only once, even if paired edges for undirected graphs 
                EdgeCount++;
            }

            // if a directed graph, no reverse edge 
            if (_isDirected) {
                return;
            }

            // if an undirected graph, the same ops, reusing instances / symbols, 
            // but now with vertices swapped, and without graph metadata; 
            // all ops repeated because @nearEnd and @farEnd edges are different 
            (sentinel, at) = GetPrecedingDirectedEdge(farEnd, nearEnd);

            if ((at.Next != null) && (at.Next.FarEnd == nearEnd)) {
                // changing the weight of existing reverse edge @at to @weight 
                SetExistingDirectedEdge(at, weight);
            }
            else {
                // adding a new edge after @at, and using @sentinel to set start of linked list 
                SetNewDirectedEdge(sentinel, at, farEnd, nearEnd, weight);
            }
        }


        #region Dependencies of SetEdge(int, int, int)

        private void SetExistingDirectedEdge(Edge at, int weight)  /* verified */  {
            Edge edge = at.Next;
            edge.Weight = weight;
        }

        private void SetNewDirectedEdge(Edge sentinel, Edge at, int nearEnd, int farEnd, int weight)  /* verified */  {
            // new edge to insert 
            Edge edge = new Edge(farEnd, weight);

            // linking the new edge into the list 
            edge.Next = at.Next;
            at.Next = edge;

            // setting the head of the list to the first real edge, which may be the one just inserted 
            _edges[nearEnd] = sentinel.Next;
        }

        #endregion Dependencies of SetEdge(int, int, int)


        public void SetEdge(int nearEnd, int farEnd)  /* passed */  {
            SetEdge(nearEnd, farEnd, UNWEIGHTED);
        }

        public void SetEdges(params (int nearEnd, int farEnd, int weight)[] edges)  /* passed */  {
            foreach ((int nearEnd, int farEnd, int weight) in edges) {
                SetEdge(nearEnd, farEnd, weight);
            }
        }

        public void SetEdges(params (int nearEnd, int farEnd)[] edges)  /* passed */  {
            foreach ((int nearEnd, int farEnd) in edges) {
                SetEdge(nearEnd, farEnd);
            }
        }

        public void CutEdge(int nearEnd, int farEnd)  /* passed */  {
            /* cuts one directed Edge if this is a directed graph; 
             * cuts pair of directed Edges if an undirected graph */

            /* as with setting edge, this uses a sentinel (factored) 
              * to simplify handling the start of the linked list */

            // getting edge to delete _after_, and setting a sentinel for easier work with linked list 
            (Edge sentinel, Edge at) = GetPrecedingDirectedEdge(nearEnd, farEnd);

            // @at is the element in the linked list _before_ the targeted one 
            if (at.Next.FarEnd == farEnd) {
                // splicing out the element after @at, and using @sentinel to set start of linked list 
                CutDirectedEdge(sentinel, at, nearEnd);

                // graph metadata; only once, even if paired edges for undirected graphs 
                EdgeCount--;
            }

            // if a directed graph, no reverse edge 
            if (_isDirected) {
                return;
            }

            // if an undirected graph, the same ops, reusing instances / symbols, 
            // but now with vertices swapped, and without graph metadata; 
            // all ops repeated because @nearEnd and @farEnd edges are different 
            (sentinel, at) = GetPrecedingDirectedEdge(farEnd, nearEnd);

            if (at.Next.FarEnd == nearEnd) {
                // splicing out the element after @at, and using @sentinel to set start of linked list 
                CutDirectedEdge(sentinel, at, farEnd);
            }
        }


        #region Dependencies of CutEdge()

        private void CutDirectedEdge(Edge sentinel, Edge at, int nearEnd)  /* verified */  {
            // cutting edge by splicing it out of list 
            at.Next = at.Next.Next;

            // setting first edge to head of list, which may be null; 
            // without this, nothing may be cut from vertex's edges 
            _edges[nearEnd] = sentinel.Next;
        }

        #endregion Dependencies of CutEdge()

        #endregion Adding, changing, and removing edges


        #region Dependencies of multiple methods

        private (Edge Sentinel, Edge At) GetPrecedingDirectedEdge(int nearEnd, int farEnd)  /* verified */  {
            // a sentinel is added first, pointing to head of list (or null), 
            // so that inserting operations can be less branched 
            Edge sentinel, at;
            at = sentinel = new Edge(NONE);  // weight not needed 
            at.Next = _edges[nearEnd];

            // traversing forward from the sentinel and across any real data; if none, @at remains @sentinel; 
            // at.Next's .FarEnd must be checked, not ×at's, to stop just _before_ where new edge belongs 
            while ((at.Next != null) && (at.Next.FarEnd < farEnd)) {
                at = at.Next;
            }

            // after the loop, either @at's .Next must be null, 
            // or .Next's .FarEnd must either be == or > @farEnd, 
            // which are all checked for in dependent method 

            return (sentinel, at);
        }

        #endregion Dependencies of multiple methods


        #region DepthFirstSearch() and dependencies

        public void DepthFirstSearch(int start)  /* passed */  {
            DepthFirstSearchRecursor(start);
        }

        private void DepthFirstSearchRecursor(int nearEnd)  /* verified */  {
            // search state 
            _marks[nearEnd] = REACHED;

            // delegated use-specific steps 
            AtArriveNewVertex(nearEnd);

            /* traversing edges and recursing */

            Edge edge = _edges[nearEnd];

            while (edge != null) {
                AtAnyEdge(nearEnd, edge.FarEnd);

                // vertices are only reached once from any root in depth-first search 
                if (_marks[edge.FarEnd] == UNREACHED) {
                    // delegated use-specific steps 
                    AtNewEdge(nearEnd, edge.FarEnd);

                    // recursion 
                    DepthFirstSearchRecursor(edge.FarEnd);
                }

                // continuing 
                edge = edge.Next;
            }

            // delegated use-specific steps 
            AtDepartNewVertex(nearEnd);
        }

        #endregion DepthFirstSearch() and dependencies


        #region BreadthFirstSearch()

        public void BreadthFirstSearch(int start)  /* passed */  {
            // initing queue with first vertex and flagging it visited 
            Queue_LinkedList_<int> queue = new Queue_LinkedList_<int>();
            queue.Enqueue(start);
            _marks[start] = REACHED;  // so that root isn't re-visited if directed-graph paths back to it 

            // using the queue, earliest-reached vertices are traversed from and discarded, 
            // with newly-reached vertices retained for later traversal in order, 
            // thereby reaching all connected vertices in breadth-first order 
            while (queue.Length > 0) {
                // getting the earliest-reached vertex still not traversed from 
                int nearEnd = queue.Dequeue();

                // delegated use-specific steps 
                AtArriveNewVertex(nearEnd);

                // traverse all the earliest vertex's edges, adding each unvisited far-end vertex to the queue 
                Edge edge = _edges[nearEnd];

                while (edge != null) {
                    // traversing down this edge to far-end vertex 
                    int farEnd = edge.FarEnd;

                    // delegated use-specific steps; unlike in the second reftex's version of this algo, 
                    // this call is not conditional, so delegate must evaluate any conditions internally 
                    AtAnyEdge(nearEnd, farEnd);

                    // when a vertex is first reached, enqueueing it and taking any use-specific steps 
                    if (_marks[farEnd] == UNREACHED) {
                        // continuing the queue 
                        queue.Enqueue(farEnd);

                        // delegated use-specific steps 
                        AtNewEdge(nearEnd, farEnd);

                        // second and later reachings are ignored 
                        _marks[farEnd] = REACHED;
                    }

                    // continuing traversal outward from this vertex 
                    edge = edge.Next;
                }

                // delegated use-specific steps 
                AtDepartNewVertex(nearEnd);
            }
        }

        #endregion BreadthFirstSearch()


        #region ResetSearch()

        public void ResetSearch()  /* passed */  {
            // wiping all marks by recreating the array, since default bool is false 
            _marks = new bool[_marks.Length];
        }

        #endregion ResetSearch()


        #region ShortestUnweightedPath()

        public int[] ShortestUnweightedPath(int start, int end)  /* passed */  {
            /* to get the shortest path, you perform a BFS from @start, 
             * retaining parent at each adjacency using AtNewEdge(), 
             * then start at @end in parents array and work backwards */

            /* this can also be done without reversing by using a 
             * recursive BFS algo and adding a parent at rexit */

            // for reusable graphs 
            ResetSearch();

            int[] parents = new int[_edges.Length];

            // sentinel for unparented node/s in search 
            for (int at = 0; at < parents.Length; at++) {
                parents[at] = NONE;
            }

            // delegate that retains parentings 
            AtNewEdge = (int near, int far) => {
                parents[far] = near;
            };

            // gathering the parentings 
            BreadthFirstSearch(start);

            // converting @parents to output by moving backwards from @end, then reversing 
            int[] output = PathFromReversedParentPointings(parents, end);
            return output;
        }

        private int[] PathFromReversedParentPointings(int[] parents, int end) {
            /* path of indices in @parents is followed backwards from @end 
               and stored, then reversed to produce a path from @start */

            // vector because length not yet known 
            List_Vector_<int> path = new List_Vector_<int>();
            path.PositionAtStart();  // just in case 

            // assembling parenting path from @end backwards, in effect 
            path.Append(end);
            int parent = parents[end];

            while (parent != NONE) {
                path.Append(parent);
                parent = parents[parent];
            }

            // reversing to fixed-length output type and returning 
            int[] output = new int[path.Length];

            int top = output.Length - 1;

            for (int from = 0, to = top; from < path.Length; from++, to--) {
                path.PositionAtIndex(from);
                output[to] = path.CurrentValue;
            }

            _i--;
            return output;
        }

        int _i = 0;

        #endregion ShortestUnweightedPath()


        #region ConnectedComponents() and dependencies

        /* the algorithm here is only well-defined on undirected graphs, but it also 
         * works on directed ones if components are completely unconnected; 
         * if they are weakly connected or strongly connected, its results 
         * are unreliable, and depend on where traversal starts */

        public (int Component, int[] Vertices)[] ConnectedComponents()  /* passed */  {
            /* to get connected components, you run BFS from the first vertex, 
             * retaining each _unreached_ vertex in tree as it is first _dequeued_ 
             * (after checking state at new edge), then repeat from each unreached vertex 
             * (which can't be in a found component) until all are traversed */

            // done only once, before process, so reached state persists between BFS calls 
            ResetSearch();

            // throughput 
            List_Vector_<List_Vector_<int>> asVectorOfVectors = new List_Vector_<List_Vector_<int>>();

            // repeated-BFS algorithm 
            for (int root = 0; root < _edges.Length; root++) {
                // skipping vertices already in a component 
                if (_marks[root] == REACHED) {
                    continue;
                }

                // accumulator of new component's vertices 
                List_Vector_<int> vertices = new List_Vector_<int>();

                // accumulation delegate for BFS, run when a vertex is reached only the very first time 
                AtNewEdge = (int near, int far) => {
                    // adding root only if an unreached edge is found during BFS 
                    if (vertices.Length == 0) {
                        vertices.Append(near);
                    }

                    vertices.Append(far);
                };

                // finding vertices in new component 
                BreadthFirstSearch(root);

                // retaining for output if BFS found a traversal tree from @root 
                if (vertices.Length > 0) {
                    asVectorOfVectors.Append(vertices);
                }
            }

            // converting to and returning output 
            (int Component, int[] Vertices)[] output = ComponentTuplesFromVectors(asVectorOfVectors);
            return output;
        }

        private (int Component, int[] Vertices)[] ComponentTuplesFromVectors(
            List_Vector_<List_Vector_<int>> asVectorOfVectors)  /* verified */  {
            /* unrolling vectors and rolling their contents into arrays bundled with indices into tuples */

            // sizing output 
            (int Component, int[] Vertices)[] output = new (int, int[])[asVectorOfVectors.Length];

            // traversing vertex vectors and converting each one to a tuple of identifier and vertices 
            for (int component = 0; component < asVectorOfVectors.Length; component++) {
                // whole component's identifier, just a sequential number 
                output[component].Component = component;

                // vertices, first localizing throughput and sizing output 
                asVectorOfVectors.PositionAtIndex(component);
                List_Vector_<int> vertices = asVectorOfVectors.CurrentValue;

                output[component].Vertices = new int[vertices.Length];

                // traversing all vertices in component in the order found, and converting to output 
                for (int vertex = 0; vertex < vertices.Length; vertex++) {
                    vertices.PositionAtIndex(vertex);
                    output[component].Vertices[vertex] = vertices.CurrentValue;
                }
            }

            return output;
        }

        #endregion ConnectedComponents() and dependencies


        #region TwoColored() and dependencies

        /* the algorithm here is correct only on undirected graphs, because in a directed graph, it's possible to start 
         * BFS at a midpoint first in the middle of a chain of edges, then elsewhere in the middle or at an end, 
         * and produce a different partial coloring each time, leading to inconsistent / unreliable results; 
         *                                                                      
         * to two-color a directed graph, it may be easiest to rewrite it as an undirected graph, 
         * and then run this algorithm / method on that graph, without alterations here */

        public (bool IsTwoColored, GraphColor[] Coloring) TwoColored()  /* passed */  {
            /* looking for two-coloring means doing BFSs from each unreached vertex, 
             * coloring alternately, and exiting with false if any far vertex has 
             * the same color as the near vertex it is now being reached from */

            // throughput and output 
            GraphColor[] colors = new GraphColor[_edges.Length];  // all inited to 0 / default 
            bool isTwoColored = true;

            // delegate that does coloring and looks for coloring-fail condition; 
            // this is called at every edge traversal (not ones to never-reached vertices), 
            // it allows looking at vertices from all their edges, to find adjacent same colors, 
            // while only setting the color of a far vertex the first time it is reached 
            AtAnyEdge = (int near, int far) => {
                // checking every time an edge reaches a vertex, so that 
                // not-two-color caused by back / cyclic edges is found 
                if (colors[far] == colors[near]) {
                    isTwoColored = false;
                }

                // only setting the first time an edge reaches 
                // a vertex, so that colors are set correctly 
                if (colors[far] == GraphColor.None) {
                    colors[far] = OppositeOf(colors[near]);
                }
            };


            // done only once, before process, so reached state persists between BFS calls 
            ResetSearch();

            // repeated-BFS algorithm 
            for (int root = 0; root < _edges.Length; root++) {
                if (_marks[root] == REACHED) {
                    continue;
                }

                // first color is set explicitly so .AtNewEdge delegate works correctly 
                colors[root] = GraphColor.Black;

                // exiting early if already known not to be two-colored 
                if (!isTwoColored) {
                    break;
                }

                // actually coloring and comparing, using .AtNewEdge defined earlier 
                BreadthFirstSearch(root);
            }

            return (isTwoColored, colors);
        }

        private GraphColor OppositeOf(GraphColor color)  /* verified */  {
            return (
                color == GraphColor.Black
                ? GraphColor.White
                : GraphColor.Black);
        }

        #endregion TwoColored() and dependencies


        #region ContainsCycle()

        /* the algorithm here is correct only on undirected graphs; 
         * however, the reftext didn't implement or discuss cycle-finding on directed graphs, 
         * so it's probably uncommon or too complicated to be worth doing normally */

        public bool ContainsCycle()  /* passed */  {
            /* trying DFS from all yet-unreached vertices, and at all edges rather than just new edges, 
             * seeing if parent of far end is not .None or the near vertex; 
             * if parent is a different vertex, then edge is a back edge and there is a cycle */

            ResetSearch();

            // output 
            bool doesContainCycle = false;

            // metadata collection, inited to sentinel values 
            int[] parents = new int[VertexCount];

            for (int at = 0; at < VertexCount; at++) {
                parents[at] = NONE;
            }

            // defining block that both sets parent, and checks @parents for cycles 
            AtAnyEdge = (near, far) => {
                /* // if parent of @far is .None, new edge, so set the parent and exit; 
                 * otherwise, see if @near is not parent of @far, or vice versa (for reverse edges); 
                 * if neither is parent of the other, this is a back edge, so set flag */

                if (parents[far] == NONE) {
                    parents[far] = near;
                    return;
                }

                if (parents[far] != near && parents[near] != far) {
                    doesContainCycle = true;
                }
            };


            // actually traversing all vertices 
            for (int root = 0; root < VertexCount; root++) {
                if (_marks[root] == REACHED) {
                    continue;
                }

                // actually searching for back edges from any vertex 
                DepthFirstSearch(root);
            }

            return doesContainCycle;
        }

        #endregion ContainsCycle()


        #region TopologicalSortMono() and dependencies

        /* topological sorting works only on directed and acyclic graphs (DAGs); 
         * this algorithm also works only when one component, as a simplification; 
         *                                                                  
         * instead, I could start a DFS at every vertex with an in-degree of 0, 
         * and return a collection of int arrays instead of just one array */

        public int[] TopologicalSortMono()  /* passed */  {
            // identifying vertex to start at (assumed to be one component) 
            int root = FindSingleComponentRoot();

            // used to store the sort, in reverse order; more than .VertexCount is never needed 
            Stack_Array_<int> stack = new Stack_Array_<int>(VertexCount);

            // delegate that builds topological sort in reverse order at rpops 
            AtDepartNewVertex = (vertex) => {
                stack.Push(vertex);
            };

            // actually traversing the graph and building stack through delegate 
            DepthFirstSearch(root);

            // popping from the stack to produce the forward-ordered output 
            int[] sorted = new int[stack.Length];

            // in this loop, can't compare against ×stack.Length, which is changes at each iteration 
            for (int of = 0; of < sorted.Length; of++) {
                sorted[of] = stack.Pop();
            }

            // output 
            return sorted;
        }

        private int FindSingleComponentRoot()  /* verified */  {
            /* if a directed graph has only one component, its root 
             * is the (sole) vertex with an in-degree of 0 */

            (int[] outDegrees, int[] inDegrees) = VertexDirectedDegrees();

            int root = NONE;

            for (int at = 0; at < VertexCount; at++) {
                if (inDegrees[at] == 0) {
                    root = at;
                    break;
                }
            }

            return root;
        }

        #endregion TopologicalSortMono() and dependencies


        #region TopologicalSortPoly()

        /* topological sorting works only on directed and acyclic graphs (DAGs); 
         * this algorithm works on any number of components, but interleaves resulting 
         * sorted values, as though all components may be addressed simultaneously */

        public int[] TopologicalSortPoly()  /* passed */  {
            // identifying vertices to start at (those which have no inbound edges) 
            (int[] outDegrees, int[] inDegrees) = VertexDirectedDegrees();

            Queue_Array_<int> vertices = new Queue_Array_<int>(VertexCount);

            for (int at = 0; at < VertexCount; at++) {
                if (inDegrees[at] == 0) {
                    vertices.Enqueue(at);
                }
            }

            // to retain results for output 
            int[] sorted = new int[VertexCount];
            int of = 0;

            // actually dequeueing those that already have no more preceding vertices 
            // (those with an in-degree of 0), decreasing the in-degrees of their neighbors, 
            // and finally enqueueing vertices that are then left with no more preceding 
            while (vertices.Length > 0) {
                // since a vertex is only enqueued when it has no preceding, 
                // it must be next in a sort order when it reaches front of queue 
                int vertex = vertices.Dequeue();
                sorted[of++] = vertex;

                // traversing edges to neighbors, decreasing their remaining preceding vertices, 
                // and enqueueing them if they are now next (since nothing remains preceding them) 
                Edge edge = _edges[vertex];

                while (edge != null) {
                    int farEnd = edge.FarEnd;
                    inDegrees[farEnd]--;

                    if (inDegrees[farEnd] == 0) {
                        vertices.Enqueue(farEnd);
                    }

                    edge = edge.Next;
                }
            }

            // output 
            return sorted;
        }

        #endregion TopologicalSortPoly()


        #region ShortestWeightedPath()

        public (int[] Path, int Length) ShortestWeightedPath(int start, int end) {
            ResetSearch();  // though not exactly a search, this algo uses search marks 

            // initing a collection of maxed distances and root parents 
            int[] distances = new int[VertexCount];
            int[] parents = new int[VertexCount];

            for (int of = 0; of < distances.Length; of++) {
                distances[of] = MAX_WEIGHT;  // sentinel value and comparison basis 
                parents[of] = NONE;            // no-parent (root) value 
            }

            // priming the algo so next [closest vertex] is adjacent to @start 
            distances[start] = 0;

            // iterating over the count of vertices to ensure completeness; counter is not used within loop 
            for (int counter = 0; counter < VertexCount; counter++) {
                // getting closest vertex to branch out from next 
                int closest = ClosestVertex();

                // retiring this vertex from later searching 
                _marks[closest] = true;

                int toClosest = distances[closest];
                Edge edge = _edges[closest];

                // recalculating all distances from @start to all vertices with an edge from @closest; 
                // in some terminologies at least, this is known as "relaxing" (!) 
                while (edge != null) {
                    int farEnd = edge.FarEnd;

                    // a marked vertex has been branched out from before, and so retired, 
                    // so here, only Edges of unmarked vertices are followed outward; 
                    // vertices here are _not_ marked as reached, so they may be @closest later 
                    if (_marks[farEnd] == UNREACHED) {
                        // values to compare are isolated locally 
                        int toFarEnd = distances[farEnd];
                        int alternative = toClosest + edge.Weight;

                        // if a path through @closest is shorter than the existing path, 
                        // the latter is replaced in @distances and @parents 
                        // with the former (the new path through @closest) 
                        if (toFarEnd > alternative) {
                            distances[farEnd] = alternative;
                            parents[farEnd] = closest;
                        }
                    }

                    // next branching out from @closest 
                    edge = edge.Next;
                }
            }

            // gathering and outputting targeted path and its distance 
            int[] path = PathFromReversedParentPointings(parents, end);
            int distance = distances[end];

            return (path, distance);
        }

        /* >>> not yet implemented */
        private int ClosestVertex() {
            return 0;
        }

        /*

        reftext: 
        static void Dijkstra(Graph G, int s, int[] D) {
            // Initialize 
            for (int i = 0; i < G.n; i++) {
                D[i] = Integer.MAX_VALUE;
            }

            D[s] = 0;

            // Process the vertices 
            for (int i = 0; i < G.n; i++) {
                // Find next-closest vertex 
                int v = minVertex(G, D);
                G.setMark(v, VISITED);

                // Unreachable 
                if (D[v] == Integer.MAX_VALUE) {
                    return; 
                }

                for (int w = G.first(v); w < G.n(); w = G.next(v, w)) {
                    if (D[w] > (D[v] + G.weight(v, w))) {
                        D[w] = D[v] + G.weight(v, w);
                    }
                }
            }
        }

        */

        /*

        rewritten: 
        public static void Dijkstra(int start, int[] distances) {
            // initializing all distances from @start to other vertices to ~infinity; 
            // these serve as the basis for comparing later, 
            //     and if not overwritten, they are a sort of sentinel value, 
            //     indicating a vertex that can't be reached from @start 
            for (int of = 0; of < this.VertexCount; of++) {
                distances[of] = int.MaxValue;
            }

            // distance from @start to itself is 0; 
            // setting this here, but not marking @start as visited yet, 
            //     means that the first time through the loop, 
            //     MinVertex() finds the next-lowest edge _from @start_, 
            //     and that makes all later calls to MinVertex() 
            //     branch ever outward from there 
            distances[start] = 0;  

            // traversing all vertices to calculate minimum distance at each one 
            for (int at = 0; at < this.VertexCount; at++) {
                // among all still-unreached vertices, finding the one 
                //     that has the shortest (least-weighted) edge 
                //     _and_ is a neighbor of an already-reached vertex; 
                // the way to find this vertex (whether with loops or a min-heap) is to find 
                //     any vertex that is not _marked_ as reached 
                //     _and_ has a path weight that is less than the sentinel, 
                //     which by definition means it has been found but not yet marked (see later) 
                int nearEnd = MinVertex(distances);

                // since this vertex is the basis for further operations, 
                //     it is now reached, so it is marked that way; 
                // the first time around, this sets @start to 0, and 
                //     the first "min vertex" is @start itself; 
                // later, when either flavor of MinVertex() looks at vertices, 
                //     the vertices adjacent to @start have path weights 
                //     but are _not_ marked "reached" in the nested loop later, 
                //     so MinVertex() only chooses from among them, 
                //     in effect considering only those vertices with edges extending from 
                //     the vertices directly reachable from @start; 
                // the same happens at each further iteration through this outer loop; 
                //     from these reachable vertices, 
                //     the one with the overall shortest 
                //     __path__ so far from @start is chosen; 
                // in second and later loop iterations, only this closest vertex 
                //     is marked here, so it won't be branched from again later 
                _marks[nearEnd] = REACHED;

                // if distance to the next-closest vertex is the sentinel value, 
                //     then since it's also the "shortest", neither it nor any others 
                //     can be reached, so exiting now is reasonable 
                if (distances[nearEnd] == int.MaxValue) {
                    return;
                }

                // the new, closest vertex C can be reached from 0 or 1 or more vertices already reached; 
                // for each edge leading from C to some far end F, 
                //     [[edge weight from C to F] +[weight of path to C]] L is compared with 
                //     [current path weight from start to F] E, 
                //     and if L is < than E, a shorter path to F exists through C than has been found before, 
                //     so the path weight stored for F is reduced to L; 
                // __none__ of the vertices whose path weights are set here are marked as reached, 
                //     which means they have path weights (distances) but are not "reached" 
                //     when looked at in the loop or min-heap code of MinVertex() 
                for (int farEnd = First(nearEnd); farEnd < this.VertexCount; farEnd = Next(nearEnd, farEnd)) {
                    if (distances[farEnd] > (distances[nearEnd] + Weight(nearEnd, farEnd))) {
                        distances[farEnd] = distances[nearEnd] + Weight(nearEnd, farEnd);
                    }
                }
            }
        }

        */

        /*
        considered: 
            an array is created to store distances from the start S, and elements are initialized to a sentinel value 
            that shows later on that they have not yet been found connected to S; 
                
            the distance at S ( D[S], from S to S ) is set to 0, which is true, but also "primes" the algorithm, 
                since comparisons used to find which vertices C are "connected" to S look at distances D[n]; 
            
            a loop is started to traverse all vertices, beginning as S: 
                
            the latest vertex, S the first time through, is now marked as "reached" (really, "finished"), 
                making it the first element of the vertices C already found to be 
                connected to S while extending outward from it; 
                    
            unreached vertices U are looked at to see if they have a D[_n_] that is < the sentinel value, 
                which means they have been found connected to S before, so they are in C and their D has been set, 
                but they have not been fully focused on and disposed of yet; 
            of the vertices looked at in U, the vertex that 
                _already_ has the least distance from S 
                is selected as the "min vertex" M; 
                    
            at the point in the loop where S was marked before, 
            M is marked as "reached", and operations will branch out from M along its edges 
                to find new paths or replacement shortest paths, 
                with this branching-from-closest being why this is a greedy algorithm; 
                    
                a nested loop is started for M's edges: 
                    
                for each edge from min-vertex M to a far end F that is unreached (:. in U), 
                    the distance D(M, F) along the edge is added to the total path distance to M, D[M], 
                    and compared with the existing path distance D[F] to vertex F; 
                    
                if the distance to F through M, [D(M, F) + D[M]] is < the existing D[F], 
                    then a new shortest path has been found through M, 
                    so the old D[F] is replaced with the new sum; 
                    
                    this comparison is always true when F has never been looked at yet, 
                        because D[F] in that case is always the max / sentinel value; 
                    this comparison may or may not be true if F has been looked at before: 
                        true if the shortest path so far now runs through M, otherwise false; 
                    
               !crucially, __none of the vertices looked at outwardly from M__ are marked "reached", 
                    perhaps conceptually because they haven't been the min vertex yet, 
                    but with the practical result that they have a D[_n_] to look at, 
                    but have not been disposed of, so the algo still looks at them 
                    when it is figuring out the next min vertex at the top of the outer loop 
           */

        /*
        including actual paths: 
            according to the third reftext, a parents array can be kept, 
                and whenever a new path is found, the parent (aka "predecessor") for the far-end vertex 
                is set to the near end of the edge at the min-vertex -finding point; 
            I guess that at the end, the shortest path from start to some end can be found by 
                reversing back along the set of predecessors, just like normal revering for parentings 
        */

        #endregion ShortestWeightedPath()

    }
}
