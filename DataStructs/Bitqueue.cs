﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    /* this class was all tested as part of the experimental project Bitsome */

    public class BitQueue : IEquatable<BitQueue>
    {
        /* in this class, I treat a BitArray as a sort of queue, but also provide for non-queue access; allows both building and traversing bitwise tries at the least */

        #region Definitions

        const int BITS_PER_BYTE = 8;

        #endregion Definitions


        #region Fields

        BitArray _subject = null;
        int _dequeueIndex = 0;

        #endregion Fields


        #region Properties

        public bool IsUsable  /* passed */  {
            get {
                /* only usable if all the conditions that mean a fail status are false */
                return (_subject != null && _subject.Length > 0 && _dequeueIndex >= 0 && _dequeueIndex < _subject.Length);
            }
        }

        public int Remaining  /* passed */  {
            get {
                if (this.IsUsable)
                    return (_subject.Length - _dequeueIndex);
                else
                    return 0;
            }
        }

        public int Length  /* verified */  {
            get { return this._subject.Length; }
        }

        public BitArray BitArray  /* ok */  {
            get { return this._subject; }
        }

        public bool[] BitsBooleanArray  /* passed */  {
            get {
                bool[] bits = new bool[this._subject.Length];
                this._subject.CopyTo(bits, 0);
                return bits;
            }
        }

        #endregion Properties


        #region Constructors

        public BitQueue()  /* verified */   {
            this._subject = new BitArray(0);
        }

        /* by making this `params`, I make it possible to pass individual booleans *or* an array of them */
        public BitQueue(params bool[] bits) {
            this._subject = BitQueue.EnqueueChunk(bits, this._subject);
        }

        #endregion Constructors


        #region Statics ( Enqueueing )

        public static BitArray EnqueueBit(bool bit, BitArray subject)  /* passed */  {
            /* a BitArray object has its length changed explicitly, and it must be changed when lengthening */

            /* if an array is already instantiated, lengthen it by one; any new bit is 0 (false) */
            if (subject == null)
                subject = new BitArray(1);
            else
                subject.Length++;   // sometimes an Θ(n) op, because bitarray is rewritten when it expands beyond an Int32 

            if (bit)
                subject[subject.Length - 1] = true;

            return subject;
        }

        public static BitArray EnqueueChunk(bool[] bits, BitArray subject)  /* passed */  {
            int initial = 0;

            /* get a ready `subject */
            if (subject == null) {
                subject = new BitArray(bits.Length);
            }
            else {
                initial = subject.Length;
                subject.Length += bits.Length;
            }

            /* this seems more efficient than making a separate BitArray and &ing them; probably only while length of `bits is not high */
            for (int i = 0; i < bits.Length; i++) {
                subject[initial + i] = bits[i];     // although these are binary bits, each is treated as a bool by the class 
            }

            return subject;
        }

        #endregion Statics ( Enqueueing )


        #region Enqueueing

        public void EnqueueBit(bool bit)  /* passed */   {
            this._subject = BitQueue.EnqueueBit(bit, this._subject);
        }

        /* variable-length parameters list; each arg a bit-bool */
        public void EnqueueBits(params bool[] bits)  /* passed */    {
            this._subject = BitQueue.EnqueueChunk(bits, this._subject);
        }

        #endregion Enqueueing


        #region Dequeueing

        public bool DequeueBit()  /* passed */  {
            return (bool)_subject[_dequeueIndex++];   // postfix incrementing; internal index is 0-limited 
        }

        public bool[] DequeueBits(int length)  /* passed */  {
            bool[] results = new bool[length];

            for (int i = 0; i < length; i++) {
                results[i] = _subject[_dequeueIndex++];   // postfix incrementing 
            }

            return results;
        }

        #endregion Dequeueing


        #region Resizing

        public void TruncateTo(int lengthToKeep)  /* passed */   {
            /* error condition; throwing exception instead of changing arg; arbitrary choice */
            if (lengthToKeep > this._subject.Length)
                throw new Exception("Cannot truncate a Bitqueue to a higher length.");

            /* built-in BitArray method */
            this._subject.Length = lengthToKeep;

            /* resetting metadata to a none-left value if no longer meaningful; a side effect */
            this._dequeueIndex = (this._dequeueIndex >= lengthToKeep ? lengthToKeep : _dequeueIndex);
        }

        #endregion Resizing


        #region Cloning

        /* make a different object that is exactly the same as this one, including metadata */
        public BitQueue Clone()  /* passed */  {
            BitQueue clone = new BitQueue();

            /* content */
            clone._subject = new BitArray(this._subject);

            /* metadata */
            clone._dequeueIndex = this._dequeueIndex;

            return clone;
        }

        #endregion Cloning


        #region Metadata

        public void ResetDequeueing()  /* ok */  {
            _dequeueIndex = 0;
        }

        #endregion Metadata


        #region Equality ( IEquatable )

        /* equality ignores metadata of anywhere dequeueing may have reached */

        public bool Equals(BitQueue other)  /* passed */  {
            /* these are by definition not equal */
            if (this._subject.Length != other._subject.Length)
                return false;

            /* use of |, &, or ^ do not eliminate need to check all bits */
            for (int i = 0; i < this._subject.Length; i++) {
                if (this._subject[i] != other._subject[i])
                    return false;
            }

            return true;
        }

        public override bool Equals(object other)  /* passed */  {
            /* if `other is null or is not a Bitqueue, it will be null after this op; either is always not equal */
            BitQueue otherBitqueue = other as BitQueue;

            if (otherBitqueue == null)
                return false;

            /* if a Bitqueue, check for same bit contents */
            return this.Equals(otherBitqueue);
        }

        public override int GetHashCode()  /* passed */  {
            /* algo: convert BitArray to numbers, sum their absolute values; down-convert and ensure it's positive (in case of wrapping) */

            long preHash = 0;

            int intsArrayLength;
            {
                /* min size for ints array is at least 1, and then [[all full ints] +[1, for any trailing BitArray bits]] */
                intsArrayLength = this._subject.Length / (sizeof(int) * BITS_PER_BYTE);

                intsArrayLength = Math.Max(1, intsArrayLength);

                if ((this._subject.Length % intsArrayLength) != 0)
                    intsArrayLength++;
            }

            /* use BitArray method to convert it to ints; each int is a chunk of 32 bits (including any trailing 0s at end) */
            int[] subjectAsInts;
            {
                subjectAsInts = new int[intsArrayLength];
                this._subject.CopyTo(subjectAsInts, 0);
            }

            /*  hash by adding absolute values of all ints from BitArray */
            {
                for (int i = 0; i < subjectAsInts.Length; i++) {
                    int next = Math.Abs(subjectAsInts[i]);
                    preHash += next;
                }
            }

            int hash = Math.Abs((int)preHash);   // conversion is lossy 
            return hash;
        }

        #endregion Equality ( IEquatable )
    }

}
