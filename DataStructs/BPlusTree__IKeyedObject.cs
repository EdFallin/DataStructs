﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    public interface IKeyedObject<K> where K : IEquatable<K>, IComparable<K>
    {
        K Key { get; set; }  // automatically ~public 
    }
}