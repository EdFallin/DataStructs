﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // this is my implementation of an OPPG ( only- parent-pointer general tree ),
    // as the reftext emphasizes its use for equivalence classes


    #region External Definitions

    // a node within a tree; instead of a tuple, which is immutable
    public struct EquivalenceNode_<T>
    {
        public T Content;
        public int Parent;
        public int NodeCount;
    }

    #endregion External Definitions


    public class EquivalenceClasses_GeneralTree_<T>
    {
        #region Definitions

        public const int NO_PARENT = -1;
        public const int NO_TREE = -1;
        public const int NO_COUNT = -1;

        #endregion Definitions


        #region Fields

        EquivalenceNode_<T>[] _array;
        int topNodeIndex = -1;

        #endregion Fields


        #region Constructors

        public EquivalenceClasses_GeneralTree_(int size)
        {
            _array = new EquivalenceNode_<T>[size];
        }

        public EquivalenceClasses_GeneralTree_()
            : this(64)
        {
            /* no operations */
        }

        #endregion Constructors


        #region Tree Methods

        public void AddRoot(T contents)   // passed  
        {
            // add at the first unused element
            _array[++topNodeIndex] = new EquivalenceNode_<T>();
            _array[topNodeIndex].Content = contents;
            _array[topNodeIndex].Parent = NO_PARENT;
            _array[topNodeIndex].NodeCount = 1;
        }

        public void AddChild(T contents, int parent)   // passed  
        {
            // adding at the first unused element, childing it to `parent
            _array[++topNodeIndex] = new EquivalenceNode_<T>();
            _array[topNodeIndex].Content = contents;
            _array[topNodeIndex].Parent = parent;

            // gathering metadata without compressing the path to this child, so that inserts are fast
            int root = FindRootWithoutCompression(topNodeIndex);

            // updating metadata
            _array[root].NodeCount++;
        }

        #endregion Tree Methods


        #region Traversals

        private List<T> Traverse(int root)   // verified  
        {
            // all work done in recursor; no real order is defined

            List<T> results = new List<T>();
            results = TraversalRecursor(root);

            return results;
        }

        private List<T> TraversalRecursor(int current)   // verified  
        {
            // performs these ops iteratively:
            //     adds current to the list;
            //     finds a node pointing to current, then rcalls on it;

            // may be an O(n^2) operation, since array is re-traversed at least partly for every node

            List<T> results = new List<T>() { _array[current].Content };

            for (int i = 0; i <= topNodeIndex; i++)
            {
                if (_array[i].Parent == current)
                {
                    // the rcall ( recursion )
                    results.AddRange(TraversalRecursor(i));
                }
            }

            return results;
        }

        #endregion Traversals


        #region Equivalence-Class Methods

        public int FindRoot(int index)   // passed  
        {
            // uses path compression by default

            return FindCompressPath(index);
        }

        private int FindCompressPath(int index)   // verified  
        {
            // if index.Parent is no-parent, return index;
            // otherwise, return results of iso-rcall

            // at root; start rcollapse
            if (_array[index].Parent == NO_PARENT)
                return index;

            // next level, linear / iso
            _array[index].Parent = FindCompressPath(_array[index].Parent);

            // convey to previous rframe
            return _array[index].Parent;
        }

        private int FindRootWithoutCompression(int index)   // passed ( as FindRoot(); return value only )
        {
            // alternative that does not use path compression

            while (_array[index].Parent != NO_PARENT)
                index = _array[index].Parent;

            return index;
        }

        public bool DoShareRoot(int first, int second)   // passed  
        {
            return (FindCompressPath(first) == FindCompressPath(second));
        }

        public void MergeTrees(int first, int second)   // passed  
        {
            // not exfactored so I can minimize repeating ops
            int firstRoot = FindCompressPath(first);
            int secondRoot = FindCompressPath(second);

            // if the two nodes are already in the same tree, the rest of the work is skipped

            if (firstRoot != secondRoot)
            {
                // exfactored; find the tree with the smaller number of nodes;
                // relatively expensive when there are a lot of trees
                int firstCount = _array[firstRoot].NodeCount;
                int secondCount = _array[secondRoot].NodeCount;

                //     exfactored: childing root of fewer-nodes tree to root of more-nodes tree,
                //         plus altering metadata to reflect merging
                if (firstCount > secondCount)
                    JoinTrees(secondRoot, firstRoot);
                else
                    JoinTrees(firstRoot, secondRoot);
            }
        }

        private void JoinTrees(int rootToMakeChild, int rootToMakeParent)   // verified  
        {
            // point root of first at root of second
            _array[rootToMakeChild].Parent = rootToMakeParent;

            // increase count of merged tree
            _array[rootToMakeParent].NodeCount += _array[rootToMakeChild].NodeCount;

            // drop former-tree count from ex-root node
            _array[rootToMakeChild].NodeCount = 0;
        }

        public void MakeEquivalent(int first, int second)   // passed  
        {
            // place in the same equivalence class if not already equivalent

            if (!DoShareRoot(first, second))
                MergeTrees(first, second);
        }

        public bool AreEquivalent(int first, int second)   // ok  
        {
            // trivial wrapper of passed unit

            return (DoShareRoot(first, second));
        }

        public List<List<T>> ListAllEquivalenceClasses()   // passed  
        {
            // for each root, do a preorder traversal to its own list;
            // gather lists, convey

            List<List<T>> results = new List<List<T>>();

            foreach (int root in TreeRoots())
            {
                if (root != NO_TREE)
                {
                    List<T> passer = Traverse(root);
                    results.Add(passer);
                }
            }

            return results;
        }

        private int[] TreeRoots()   // verified  
        {
            // basic root finding is a linear ( O(n) ) traversal of _array, but is only needed during traversals, and only once there

            int[] roots = new int[_array.Length];   // this is the most it could possibly be

            for (int i = 0; i < _array.Length; i++)
            {
                // array elements are not null by default, but this value is 0 by default
                if (_array[i].NodeCount > 0)
                    roots[i] = i;
                else
                    roots[i] = NO_TREE;
            }

            // the roots array could be collapsed, but at the cost of at least 2 more O(n) traversals, making it pointless to do so
            return roots;
        }

        #endregion Equivalence-Class Methods

    }
}
