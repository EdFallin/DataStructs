﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructs
{
    // all classes and interfaces in DataStructs end in _ 
    // to avoid possibly interfering with similar framework elements

    // abstract data type for a list, to be implemented as array and linked list
    public abstract class List_<T>
    {
        // this array primarily works using stepwise iteration, as in a `for loop

        #region Properties - List Metadata

        public abstract int Length { get; }

        public abstract int CurrentPosition { get; }

        // size minus 1, used for clearer code
        public abstract int Top { get; }

        #endregion Properties - List Metadata



        #region Property - Current Element Value

        public abstract T CurrentValue { get; set; }

        #endregion Property - Current Element Value



        #region Constructors *Not Abstract*

        public List_() { }

        public List_(int length) { }

        #endregion Constructors *Not Abstract*



        #region Changing List Contents

        public abstract void InsertAtCurrent(T element);

        public abstract T RemoveAtCurrent();

        public abstract void Append(T element);

        public abstract void Clear();

        #endregion Changing List Contents



        #region Consecutive Positioning

        public abstract void Next();

        public abstract void Previous();

        #endregion Consecutive Positioning



        #region Nonconsecutive Positioning

        public abstract void PositionAtStart();

        public abstract void PositionAtEnd();

        public abstract void PositionAtIndex(int index);

        #endregion Nonconsecutive Positioning
    }
}
