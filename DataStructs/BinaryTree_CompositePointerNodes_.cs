﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_CompositePointerNodes_<K, T>
    {
        #region Definitions

        public delegate object Traverser(object topic, CompositePointerNode_<K, T> node);

        #endregion Definitions


        #region Fields

        CompositePointerNode_<K, T> _root;

        #endregion Fields


        #region Properties

        // a Parent property / method is not supported for pointer-backed trees,
        //     on the assumption that they aren't used like graphs;
        // no L-child or R-child properties; those are provided by the nodes themselves

        public CompositePointerNode_<K, T> Root    // passed  
        {
            get { return _root; }
            set { _root = value; }
        }

        #endregion Properties


        #region Constructors

        public BinaryTree_CompositePointerNodes_(K key, T element)   // passed  
        {
            // 4-arg form inits an inode; assigned to metadata right away
            _root = CompositePointerNode_<K,T>.InitCompositePointerNode_(key, element, null, null);
        }

        public BinaryTree_CompositePointerNodes_()   // ok  
        {
            // this constructor exists only to allow use within another structure, like a Huffman Coding Tree

            /* no operations */
        }

        #endregion Constructors


        #region Traversals

        public object TraversePreorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePreorder(t, results, this._root);

            return results;
        }

        private object RecursePreorder(Traverser t, object topic, CompositePointerNode_<K, T> node)   // verified  
        {
            // first the current node
            topic = t(topic, node);

            // then its L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePreorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePreorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraverseInorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecurseInorder(t, results, this._root);

            return results;
        }

        private object RecurseInorder(Traverser t, object topic, CompositePointerNode_<K, T> node)   // verified  
        {
            // first the L child / subtree
            if (node.LeftChild != null)
                topic = RecurseInorder(t, topic, node.LeftChild);

            // then the current node
            topic = t(topic, node);

            // then the R child / subtree
            if (node.RightChild != null)
                topic = RecurseInorder(t, topic, node.RightChild);

            return topic;
        }

        public object TraversePostorder(Traverser t)   // passed  
        {
            object results = null;

            results = RecursePostorder(t, results, this._root);

            return results;
        }

        private object RecursePostorder(Traverser t, object topic, CompositePointerNode_<K, T> node)   // verified  
        {
            // first the L and R children / subtrees
            if (node.LeftChild != null)
                topic = RecursePostorder(t, topic, node.LeftChild);

            if (node.RightChild != null)
                topic = RecursePostorder(t, topic, node.RightChild);

            // then the current node
            topic = t(topic, node);

            return topic;
        }

        #endregion Traversals

    }


    // CompositePointerNode_ is a mostly virtual superclass, with factory methods for subclasses

    public class CompositePointerNode_<K, T>
    {
        #region Fields

        protected K _key;
        protected T _element;

        #endregion Fields


        #region Properties

        // some of these properties are set up as though a leaf, for simplest fail-state results

        virtual public K Key { get { return default(K); } set { } }

        virtual public T Element { get { return default(T); } set { } }

        virtual public CompositePointerNode_<K, T> LeftChild { get { return null; } set { } }

        virtual public CompositePointerNode_<K, T> RightChild { get { return null; } set { } }

        virtual public bool IsLeaf { get { return true; } }

        #endregion Properties


        #region Init Methods

        // exists to allow simplest custom constructors in child classes
        public CompositePointerNode_()
        {
            // no operations
        }

        // inits an internal node
        public static CompositePointerNode_<K, T> InitCompositePointerNode_(K key, T element,
            CompositePointerNode_<K, T> leftChild, CompositePointerNode_<K, T> rightChild)   // passed  
        {
            CompositePointerNode_<K, T> node = new CompositePointerNode_Internal_<K, T>(key, element, leftChild, rightChild);
            return node;
        }

        // inits a leaf node
        public static CompositePointerNode_<K, T> InitCompositePointerNode_(K key, T element)   // passed  
        {
            CompositePointerNode_<K, T> node = new CompositePointerNode_Leaf_<K, T>(key, element);
            return node;
        }

        #endregion Init Methods

    }


    public class CompositePointerNode_Internal_<K, T> : CompositePointerNode_<K, T>
    {
        #region Fields

        CompositePointerNode_<K, T> _leftChild;
        CompositePointerNode_<K, T> _rightChild;

        #endregion Fields


        #region Properties

        public override K Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public override T Element
        {
            get { return _element; }
            set { _element = value; }
        }

        public override CompositePointerNode_<K, T> LeftChild    // passed  
        {
            get { return _leftChild; }
            set { _leftChild = value; }
        }

        public override CompositePointerNode_<K, T> RightChild    // passed  
        {
            get { return _rightChild; }
            set { _rightChild = value; }
        }

        public override bool IsLeaf
        {
            // by definition, never a leaf

            get { return false; }
        }

        #endregion Properties


        #region Constructors

        public CompositePointerNode_Internal_(K key, T element, CompositePointerNode_<K, T> leftChild, CompositePointerNode_<K, T> rightChild)   // passed  
        {
            _key = key;
            _element = element;
            _leftChild = leftChild;
            _rightChild = rightChild;
        }

        #endregion Constructors


        // no methods
    }


    public class CompositePointerNode_Leaf_<K, T> : CompositePointerNode_<K, T>
    {
        #region Properties

        public override K Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public override T Element
        {
            get { return _element; }
            set { _element = value; }
        }

        public override bool IsLeaf    // passed  
        {
            // naturally, a leaf is always a leaf

            get { return true; }
        }

        #endregion Properties


        #region Constructors

        public CompositePointerNode_Leaf_(K key, T element)   // passed  
        {
            // no child nodes or pointers for them

            _key = key;
            _element = element;
        }

        #endregion Constructors


        // no methods
    }
}
