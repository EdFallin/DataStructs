﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    //  I'm implementing a binary-tree container class, inheriting from and extending abstract Dictionary_,
    //  to allow consistent handling of pointer-backed and array-backed trees
    //  (even though the former don't need a container class);

    //  the basic classes follow the reftext's convention of having a dictionary-like key as well as an actual element value

    public enum BinaryTreeType
    {
        Pointers,
        Array,
        Fails,
        ArrayFails,
    }

    //  this class serves as an abstract entry point for different implementations
    public abstract class BinaryTree_<K, T> : Dictionary_<K, T>
    {
        public delegate object TraversalOperation(K key, T element);

        protected K _failKey;
        protected T _failValue;

        //  no constructor; defined only in concrete subclasses

        abstract public K NodeKey { get; set; }
        abstract public T NodeElement { get; set; }
        abstract public bool NodeIsLeaf { get; }

        abstract public void AddLeftChild(K key, T element);
        abstract public void AddRightChild(K key, T element);

        abstract public void DropLeftChild();
        abstract public void DropRightChild();

        abstract public void MoveToRoot();

        abstract public void MoveToLeftChild();
        abstract public void MoveToRightChild();

        abstract public K LeftChildNodeKey { get; set; }
        abstract public T LeftChildNodeElement { get; set; }

        abstract public K RightChildNodeKey { get; set; }
        abstract public T RightChildNodeElement { get; set; }

        abstract public object[] TraverseInOrder(TraversalOperation t);
        abstract public object[] TraversePostOrder(TraversalOperation t);
        abstract public object[] TraversePreOrder(TraversalOperation t);
    }

    public class BinaryNode_<K, T>   //  OK  
    {
        #region Properties (Including Child Nodes)

        virtual public K Key
        {
            get { return default(K); }
            set { }
        }

        virtual public T Element
        {
            get { return default(T); }
            set { }
        }

        virtual public BinaryNode_<K, T> LeftChild
        {
            get { return null; }
            set { }
        }

        virtual public BinaryNode_<K, T> RightChild
        {
            get { return null; }
            set { }
        }

        virtual public bool IsLeaf
        {
            get { return (false); }
        }

        #endregion Properties (Including Child Nodes)


        #region Static Factory Method For Concrete Binary Trees

        public static BinaryNode_<K, T> SupplyBinaryNode_K_T(BinaryTreeType type)
        {
            BinaryNode_<K, T> rootNode = null;

            switch (type)
            {
                case BinaryTreeType.Pointers:
                    rootNode = new BinaryNode_Pointers_<K, T>();
                    break;
                //case BinaryTreeType.Array:
                //    throw new NotImplementedException("No array-implemented binary tree exists yet");
                //break;
                case BinaryTreeType.Fails:
                //  fall-through
                default:
                    rootNode = new BinaryNode_Fails_<K, T>();
                    break;
            }

            return (rootNode);
        }

        #endregion Static Factory Method For Concrete Binary Trees
    }

    public static class TreeNursery_<K, T>
    {
        //  this is a factory class for all binary tree types

        public static BinaryTree_<K, T> GrowTree(BinaryTreeType type)   //  PASSED  
        {
            BinaryTree_<K, T> tree = null;

            switch (type)
            {
                case BinaryTreeType.Pointers:
                    tree = new BinaryTree_Pointers_<K, T>();
                    break;
                case BinaryTreeType.Array:
                    tree = new BinaryTree_Array_<K, T>();
                    break;
                case BinaryTreeType.Fails:
                    tree = new BinaryTree_Fails_<K, T>();
                    break;
                case BinaryTreeType.ArrayFails:
                    tree = new BinaryTree_Array_Fails_<K, T>();
                    break;
                default:
                    throw new Exception("switch(type) equals default");
            }

            return (tree);
        }
    }

}
