﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class Queue_LinkedList_<T> : Queue_<T>
    {
        #region Fields

        ForwardNode<T> _beforeFirst;
        ForwardNode<T> _last;

        #endregion Fields


        #region Properties, Including Peek

        public override T Peek  /* passed */  {
            // if any are present, oldest is returned 
            get {
                if (this._length == 0) {
                    return default(T);
                }

                return _beforeFirst.Next.Content;
            }
        }

        public override int Length  /* passed */  {
            get { return this._length; }
        }

        #endregion Properties, Including Peek


        #region Constructors

        public Queue_LinkedList_()  /* verified */  {
            this.Init();
        }

        public Queue_LinkedList_(int length)
            : this()  /* ok */  {
            // no operations; length is ignored 
        }

        private void Init()  /* verified */  {
            _beforeFirst = new ForwardNode<T>(null);
            _last = _beforeFirst;
            this._length = 0;
        }

        #endregion Constructors


        #region Changing List Contents

        public override void Clear()  /* passed */  {
            this.Init();
        }

        public override void Enqueue(T content)  /* passed */  {
            // add newest element to end of queue 
            ForwardNode<T> next = new ForwardNode<T>(content, null);
            _last.Next = next;
            _last = next;

            // queue metadata 
            this._length++;
        }

        public override T Dequeue()  /* passed */  {
            // retain value of oldest T queued 
            T content = _beforeFirst.Next.Content;

            // knock oldest element out of queue 
            _beforeFirst.Next = _beforeFirst.Next.Next;

            // re-associate queue start and end if emptying queue 
            if (_beforeFirst.Next == null) {
                _last = _beforeFirst;
            }

            // queue metadata 
            _length--;

            return content;
        }

        #endregion Changing List Contents
    }
}
