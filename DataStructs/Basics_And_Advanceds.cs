﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanceds
{
    public class Arrays
    {
        public static T[] Resized<T>(T[] subject, int length)  /* passed */  {
            // initializes with the default(T), so if `subject is shorter than `length, still useful 
            T[] results = new T[length];

            // only iterate through `subject next as far as appropriate 
            int max = (length > subject.Length ? subject.Length : length);

            // transfer elements 
            for (int i = 0; i < max; i++)
                results[i] = subject[i];

            return results;
        }

        public static T[] CombineArrays<T>(params T[][] subjects)  /* passed */  {
            // the T[][] syntax is technically for a jagged array, 
            // but that seems to work / be the same as an array of arrays 
            // of the underlying type T 

            T[] results;

            // calculate length for joined `results 
            {
                int totalLength = 0;

                foreach (T[] subject in subjects) {
                    totalLength += subject.Length;
                }

                results = new T[totalLength];
            }


            // accumulate all elements from all arrays; this can be done more 
            // succinctly with .CopyTo() of ^subject to ^results, with an offset; 
            // however, both are O(n) operations, so it's hardly worth it 
            {
                int totalIndex = 0;

                foreach (T[] subject in subjects) {
                    for (int i = 0; i < subject.Length; i++)
                        results[totalIndex++] = subject[i];
                }
            }

            return results;
        }

        public static BitArray CombineBitArrays(params BitArray[] subjects)  /* passed */  {
            // the T[][] syntax is technically for a jagged array, 
            // but that seems to work / be the same as an array of arrays 
            // of the underlying type T 

            BitArray results;

            // calculate length for joined `results 
            {
                int totalLength = 0;

                foreach (BitArray subject in subjects)
                    totalLength += subject.Length;

                // init with all 0's
                results = new BitArray(totalLength, false);
            }


            // accumulate all elements from all arrays; an O(n) operation across all bits; 
            // handling this in chunks of 32- or 64-bits would be better, but isn't worth it here 
            {
                int totalIndex = 0;

                foreach (BitArray subject in subjects) {
                    for (int i = 0; i < subject.Length; i++)
                        results[totalIndex++] = subject[i];
                }
            }

            return results;
        }
    }

    public class Permutations
    {
        static int _seed = 0;

        public static int[] RandomZeroUpPermutation(int length)  /* passed */ {
            int[] results = new int[length];

            Random random = new Random(_seed++);

            for (int up = 0; up < length; up++) {

                int number = random.Next(0, length);

                bool isARepeatedValue = false;

                for (int down = up - 1; down >= 0; down--) {
                    if (number == results[down])
                        isARepeatedValue = true;
                }

                // new numbers are added to the results, but when a number isn't new, 
                // the counter is decremented so eventually the current element is filled 
                if (!isARepeatedValue)
                    results[up] = number;
                else
                    up--;
            }

            return results;
        }
    }

    /* non-static so more than one can easily coexist in consuming code; dependent on static ^GenericToBinary class */
    public class GenericToBinary<T>
    {
        #region Fields

        /* this is a (static) converting method, wrapping one from BitConverter, in the static GenericToBinary class */
        private GenericToBinary.ToByteArray _converter;

        #endregion Fields


        #region Constructors

        public GenericToBinary() {
            /* delegate all work to method in non-generic GenericToBinary class; method determined using metadata also stored there */
            for (int i = 0; i < GenericToBinary.TypeCodes.Length; i++) {
                if (Type.GetTypeCode(typeof(T)) == GenericToBinary.TypeCodes[i]) {
                    _converter = GenericToBinary.Converters[i];
                    break;
                }
            }
        }

        #endregion Constructors


        #region Crux Public Method

        public byte[] ToBinary(object topic)  /* passed */  {
            /* conversion delegated to method in non-generic GenericToBinary class */
            return _converter(topic);
        }

        #endregion Crux Public Method
    }

    /*  various conversions to and from binary ( byte[] ) don't have all the functionality I need, and workarounds mostly fail; 
     *  this class supports easy conversion to byte[] of all primitives, plus char[]; this non-generic form determines conversion at invoke-time  */
    public static class GenericToBinary
    {
        #region Definitions

        public delegate byte[] ToByteArray(object topic);

        #endregion Definitions


        #region Public Fields

        public static TypeCode[] TypeCodes = { 
                                            TypeCode.Int32, TypeCode.Int16, TypeCode.Int64, TypeCode.Single, 
                                            TypeCode.UInt16, TypeCode.UInt32, TypeCode.UInt64, 
                                            TypeCode.Boolean, TypeCode.Char, TypeCode.Double, Type.GetTypeCode(typeof(char[])), };

        public static ToByteArray[] Converters = {
                                               Int32ToByteArray, Int16ToByteArray, Int64ToByteArray, SingleToByteArray,
                                               UInt16ToByteArray, UInt32ToByteArray, UInt64ToByteArray,
                                               BooleanToByteArray, CharToByteArray, DoubleToByteArray, CharArrayToByteArray, };

        #endregion Public Fields


        #region Crux Public Methods

        public static byte[] ToBinary(object topic)  /* passed */  {
            TypeCode code = Type.GetTypeCode(topic.GetType());

            for (int i = 0; i < TypeCodes.Length; i++) {
                if (code == TypeCodes[i]) {
                    ToByteArray converter = Converters[i];
                    byte[] output = converter(topic);
                    return output;
                }
            }

            return null;
        }

        #endregion Crux Public Methods


        #region Methods Used As ToByteArray Delegates

        public static byte[] Int32ToByteArray(object topic)  /* passed */  {
            return BitConverter.GetBytes((Int32)topic);
        }

        public static byte[] Int16ToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Int16)topic);
        }

        public static byte[] Int64ToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Int64)topic);
        }

        public static byte[] UInt16ToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((UInt16)topic);
        }

        public static byte[] UInt32ToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((UInt32)topic);
        }

        public static byte[] UInt64ToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((UInt64)topic);
        }

        /// <summary>
        /// `Single` and `float` are the same primitive 
        /// </summary>
        public static byte[] SingleToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Single)topic);
        }

        public static byte[] DoubleToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Double)topic);
        }

        public static byte[] CharToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Char)topic);
        }

        public static byte[] BooleanToByteArray(object topic)  /* ok */  {
            return BitConverter.GetBytes((Boolean)topic);
        }

        public static byte[] CharArrayToByteArray(object topic)  /* passed */  {
            char[] subject = (char[])topic;
            byte[] output = new byte[subject.Length * 2];

            /* convert each char in array to its own byte[] and copy that to output array using calculated offsets */
            for (int i = 0; i < subject.Length; i++) {
                byte[] passer = BitConverter.GetBytes(subject[i]);
                output[i * 2] = passer[0];
                output[(i * 2) + 1] = passer[1];
            }

            return output;
        }

        #endregion Methods Used As ToByteArray Delegates


        #region Other Public Methods

        public static int SizeOfT(object topic)  /* passed */  {
            return System.Runtime.InteropServices.Marshal.SizeOf(topic);
        }

        #endregion Other Public Methods

    }
}

namespace DataStructs
{
    /* extended math function(s), easier comparing, anything else like that */
    public class Basics
    {
        public static int NextPowerOfTwo(int number)  /* passed */  {
            /* principle: next power of two equals [2 raised to [next whole number beyond log-two of input]] */

            double raiseBy = 0;

            double logBaseTwo = Math.Log(number, 2);
            raiseBy = Math.Ceiling(logBaseTwo);

            return (int)Math.Pow(2, raiseBy);
        }

        public static bool AreOrdered<T>(T earlier, T later) where T : IComparable<T>  /* passed */  {
            /* "ordered" here means "in ascending order" */
            int results = earlier.CompareTo(later);

            if (results > 0)
                return false;

            return true;
        }

        public static bool AreDisordered<T>(T earlier, T later) where T : IComparable<T>  /* passed */  {
            /* "disordered" here means "not in ascending order"; */
            /* I could also have implemented this as !(.AreOrdered()), but that bothered me */

            int results = earlier.CompareTo(later);

            /*  results > 0 ( always 1 ) means earlier is > later, so disordered; 
             *  equal is considered ordered, so all other results are ordered  */
            if (results > 0) {
                return true;
            }

            return false;
        }

    }

    public static class Extensions
    {
        public static bool IsLessThan<T>(this T subject, T topic) where T : IComparable<T>
            => Basics.AreDisordered(subject, topic);
    }

}
