﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DataStructs
{
    public class SortingExternal_ReplacementSelection_<T> where T : struct, IComparable<T>, IEquatable<T>
    {
        #region Definitions, Including Internal Classes

        //private int MAX_RUNS = 8;

        public class RunBlock
        {
            #region Fields

            byte[] _sharedBuffer;   /* a reference to the core as a whole */

            T[] _sharedTBuffer;
            private int _startOffset;

            /* these are for rebuilding as buffers and offsets */
            private int _bytesInType = 0;
            private int _size = 0;
            private int _top = 0;

            /* for easier handling by consumers, now and with buffers-offsets */
            private int _index = 0;

            #endregion Fields


            /* passed */
            #region Properties

            #region Data

            public T Datum  /* ok */  {
                get { return ElementByOffset(_index); }
            }

            #endregion Data


            #region Metadata

            public int Size  /* passed */  {
                get { return _size; }
            }

            public int Top {
                get { return (Size - 1); }
            }

            /* because RunBlocks are used with .Advance(), they become exhausted after that is called after their last data (at _top / Top ) */
            public bool Exhausted  /* passed */  {
                get { return (_index > Top); }
            }

            #endregion Metadata

            #endregion Properties


            /* passed */
            #region Constructors

            public RunBlock(T[] sharedBuffer, int startOffset, int numberOfTs)  /* verified */  {
                _sharedTBuffer = sharedBuffer;
                _startOffset = startOffset;


                /* surface and internal metadata; all calculated using args directly to avoid confusion */
                int maximumSize = sharedBuffer.Length - startOffset;

                _size = (maximumSize < numberOfTs
                    ? maximumSize
                    : numberOfTs);
            }

            #endregion Constructors


            /* passed */
            #region Methods

            public void Advance()  /* passed */  {
                _index++;
            }

            private T ElementByOffset(int offset)  /* passed */  {
                int totalOffset = _startOffset + offset;

                return _sharedTBuffer[totalOffset];

                //**  *** new above, old below *** *********************************************************************************  **//

                T element;

                /* first get bytes from buffer, using correct byte offsets and sizes */
                int typeBytesOffset = offset * _bytesInType;
                int localOffset = _startOffset + typeBytesOffset;

                byte[] asBytes = new byte[_bytesInType];

                for (int i = 0; i < _bytesInType; i++) {
                    asBytes[i] = _sharedBuffer[localOffset + i];
                }


                /* next get bits from bytes; direct translation, in same order */
                BitArray asBits = new BitArray(asBytes);


                /* copy bits first to a 1-element output array, needed for .CopyTo(), before final output */
                T[] elements = new T[1];
                asBits.CopyTo(elements, 0);

                element = elements[0];
                return element;
            }

            #endregion Methods


            /* passed */
            #region Indexers

            public T this[int index]  /* passed */  {
                get { return ElementByOffset(index); }
            }

            #endregion Indexers

        }

        /* used in merging method */
        public class DatumBlockIndexDuple
        {
            public T Datum;
            public int RunBlockIndex;

            public DatumBlockIndexDuple(T datum, int runBlockIndex) {
                this.Datum = datum;
                this.RunBlockIndex = runBlockIndex;
            }
        }

        public class RunFile
        {
            #region Fields

            private string _fileName;

            #endregion Fields


            #region Properties

            public string FileName {
                get { return _fileName; }
                set { _fileName = value; }
            }

            #endregion Properties


            #region Constructors

            public RunFile(string fileName) {
                _fileName = fileName;
            }

            #endregion Constructors
        }

        #endregion Definitions, Including Internal Classes


        #region Fields

        string _subjectFile = null;
        string _subjectPath = null;

        T[] _core = null;
        MinHeapSharedArray_<T> _heap = null;

        #endregion Fields


        #region Constructors

        /* this exists only for compatibility with tests that don't have a file name included */
        public SortingExternal_ReplacementSelection_() {
            _subjectPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public SortingExternal_ReplacementSelection_(string subjectPathAndFile) {
            _subjectFile = Path.GetFileName(subjectPathAndFile);
            _subjectPath = Path.GetDirectoryName(subjectPathAndFile);
        }

        #endregion Constructors



        #region SelectReplace()

        public T SelectReplace(T next)  /* passed */  {
            T current = _heap.NodeContent;

            /* if the incoming value is greater than the root of the current heap, it belongs in this heap, otherwise in the free core */
            if (Basics.AreDisordered(next, current)) {
                _heap.NodeContent = next;
                _heap.SwapDown(0);   // first index / offset in heap 
            }
            else {
                /* already-retained value is popped, heap shrinks and reorders, and ^next is placed in core right after its end */
                _heap.PopHeapRoot();
                int firstFree = _heap.Count;
                _core[firstFree] = next;
            }

            return current;
        }

        #endregion SelectReplace()



        /* passed */
        #region MergeRuns() And Dependencies

        private void MergeRuns(RunBlock[] runBlocks, Queue_Array_<T[]> merged, int outputBlockLength)  /* passed */  {
            int totalOutputCount = SumOfSizesOfRunBlocks(runBlocks);
            int outputCount = 0;

            while (outputCount < totalOutputCount) {
                T[] output = new T[outputBlockLength];

                /*  looping across all output slots, each to be filled with the least of the inputs currently available; 
                 *  passer for least ( ^leastDuple ) is re-nulled every time so inner loop's self-init works properly;
                 *  this could be rewritten as an all-local-values loop, but this should compile about the same and is probably clearer  */
                for (int nextOut = 0; nextOut < output.Length; nextOut++) {
                    DatumBlockIndexDuple leastDuple = null;

                    for (int blockIndex = 0; blockIndex < runBlocks.Length; blockIndex++) {
                        /* any exhausted blocks are just skipped, whether at either end or in middle */
                        if (runBlocks[blockIndex].Exhausted) {
                            continue;
                        }

                        if (leastDuple == null) {
                            leastDuple = new DatumBlockIndexDuple(runBlocks[blockIndex].Datum, blockIndex);
                        }
                        else {
                            leastDuple = LeastDatumDuple(leastDuple, new DatumBlockIndexDuple(runBlocks[blockIndex].Datum, blockIndex));
                        }
                    }

                    /* conveying least found across the set just looked at, and moving to the next datum where the least was found */
                    output[nextOut] = leastDuple.Datum;
                    runBlocks[leastDuple.RunBlockIndex].Advance();
                }

                merged.Enqueue(output);
                outputCount += output.Length;
            }
        }

        /* --> somehow I need to find out why this can't be found in tests when changed to `private`, then fix that */
        public int SumOfSizesOfRunBlocks(RunBlock[] runBlocks)  /* passed */  {
            int sum = 0;

            foreach (RunBlock runBlock in runBlocks) {
                sum += runBlock.Size;
            }

            return sum;
        }

        /*  *** this method is the ***crux of the crux*** in the merging cycle; this key dependency 
         *  of .MergeRuns() is what actually chooses next ordered values within that method ***  */
        private DatumBlockIndexDuple LeastDatumDuple(DatumBlockIndexDuple first, DatumBlockIndexDuple second)  /* passed */  {
            if (Basics.AreOrdered(first.Datum, second.Datum)) {
                return first;
            }

            return second;
        }

        #endregion MergeRuns() And Dependencies


        /* passed */
        #region MergingIsDone()

        public bool MergingIsDone(Queue_Array_<RunFile> nexts)  /* passed */  {
            return (nexts.Length == 1);
        }

        #endregion MergingIsDone()


        /* passed */
        #region WriteRunToDisk()

        /// <summary>
        /// Writes a run to disk synchronously.
        /// If written asynchronously, correct results cannot be guaranteed.
        /// To achieve concurrency, instead run this in its own thread.
        /// </summary>
        public RunFile WriteRunToDisk(Queue_Array_<T[]> queue, int totalBlocks)  /* passed */  {
            const int NO_OFFSET_IN_BUFFER = 0;

            Advanceds.GenericToBinary<T> converter = new Advanceds.GenericToBinary<T>();

            string fileName = Path.GetRandomFileName();
            fileName = Path.Combine(_subjectPath, fileName);

            using (FileStream writer = new FileStream(fileName, FileMode.CreateNew)) {
                int blocksWritten = 0;

                /* outer loop traverses over queue's blocks; inner loop traverses over individual Ts in block */
                while (blocksWritten < totalBlocks) {
                    T[] array = queue.Dequeue();

                    for (int i = 0; i < array.Length; i++) {
                        byte[] passer = converter.ToBinary(array[i]);
                        writer.Write(passer, NO_OFFSET_IN_BUFFER, passer.Length);
                    }

                    blocksWritten++;
                }

                writer.Close();
            }

            return new RunFile(fileName);
        }

        #endregion WriteRunToDisk()


        /* passed */
        #region CoreToSharedMinHeap()

        private void CoreToSharedMinHeap()  /* passed */  {
            /* point to core data as a min-heap and (partially) order the data  */
            _heap = new MinHeapSharedArray_<T>(_core);
        }

        #endregion CoreToSharedMinHeap()

    }
}
