﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    //  «K» is for «key»  //

    public class BPlusTree<K> where K : IEquatable<K>, IComparable<K>
    {
        //  a node as root, field; search, add, remove; recursors for those  //

        #region Constants

        const int MAX_NODES = 10;
        const int NONE = -1;

        #endregion Constants


        #region Fields

        NodeBpt<K> _root = new LeafBpt<K>();

        #endregion Fields


        #region Properties

        //  constant / static properties  //
        public static int MaxNodes => MAX_NODES;
        public static int None => NONE;

        //  variable properties  //
        public NodeBpt<K> Root => _root;

        #endregion Properties


        #region Public methods

        public Element<K> Search(K sought) => null;

        #region Private dependencies of Search()
        //  recursor  //

        private Element<K> SearchRecursor(K sought, NodeBpt<K> node) {
            /*  a preorder linear 'zigzag' isorecursion  */

            /*  algorithm: 
             *      search inexact / closest in ~node's children; 
             *      if fork, rcall; 
             *      if leaf and _exact_ match, return element; 
             *      if leaf without match, return null (fail value)  */

            int closest = Searchers_.FindBinaryClosestBelow<K>(node.Keys, sought);

            if (!node.IsALeaf) {
                ForkBpt<K> fork = node as ForkBpt<K>;
                NodeBpt<K> rSite = fork.Children[closest];

                Element<K> returned = SearchRecursor(sought, rSite);
                return (returned);
            }
            else {
                if (node.Keys[closest].Equals(sought)) {
                    LeafBpt<K> leaf = node as LeafBpt<K>;
                    return (leaf.Elements[closest]);
                }
                else {
                    return (null);
                }
            }
        }

        #endregion Private dependencies of Search()


        public void Insert(Element<K> insertable) {
        }

        #region Private dependencies of Insert()
        //  recursor, plus node-splitting code  //

        private object InsertRecursor(Element<K> insertable) {
            return (null);
        }

        #endregion Private dependencies of Insert()


        public void Remove(K removable) {
            //  removes any whole KeyPointerBpt_<K> with key of ~removable  //
        }

        #region Private dependencies of Remove()
        //  recursor, plus node-combining code and any of its dependencies, like sloshing  //

        private object RemoveRecursor(K removable) {
            return (null);
        }

        #endregion Private dependencies of Remove()


        #endregion Public methods

    }



    /*  «abstract» can be working class, you just can't init directly; if a ctor, it's called by concretes' ctors  */
    public abstract class NodeBpt<K> where K : IEquatable<K>, IComparable<K>
    {
        //  contents only, not operations  //

        public K Key { get; set; }

        public abstract bool IsALeaf { get; }

        //  keys are exposed publicly for all nodes, and offsets are used with child nodes / elements  //
        public K[] Keys { get; set; }

    }


    public class ForkBpt<K> : NodeBpt<K> where K : IEquatable<K>, IComparable<K>
    {
        public override bool IsALeaf => false;

        //  child nodes are matched with keys by offset  //
        public NodeBpt<K>[] Children { get; set; } = new NodeBpt<K>[BPlusTree<K>.MaxNodes];
    }


    public class LeafBpt<K> : NodeBpt<K> where K : IEquatable<K>, IComparable<K>
    {
        public override bool IsALeaf => true;

        //  elements pointing to subject's content are matched with keys by offset  //
        public Element<K>[] Elements { get; set; } = new Element<K>[BPlusTree<K>.MaxNodes];
    }


    public class Element<K> where K : IEquatable<K>, IComparable<K>
    {
        //  the elements of the tree, made up of keys and pointers to content in the subject  //

        #region Properties

        public K Key { get; set; }
        public int Pointer { get; set; }   // pointer to record on disk as offset in file 

        //  !!: doubly-linked list; linked-list code must be added in  //
        public Element<K> Next { get; set; }

        public Element<K> Previous { get; set; }

        #endregion Properties


        #region Constructors

        public Element(K key) : this(key, BPlusTree<K>.None) {
            /*  no operations  */
        }

        public Element(K key, int pointer) {
            Key = key;
            Pointer = pointer;
        }

        #endregion Constructors


        //  any methods are linked-list ops; can copy / retype from there / TwoWayNode, or from Retainer here  //
    }

    public class Retainer
    {
        /*  this class just retains good implementations from my previous pass through B+trees; 
         *  these don't compile now, and will have to be changed  */

        ///*  removes at end, returning removed  */
        //public IKeyedObject<K> Drop()  /* passed */  {
        //    IKeyedObject<K> passer = _contents[Top];

        //    /*  emptying pointer at top el, to avoid confusion  */
        //    _contents[Top] = null;

        //    Top--; Count--;
        //    return (passer);
        //}

        //public void Insert(IKeyedObject<K> item, int index)  /* passed */  {
        //    /*  add Count-vs-Size -based exceptions  */
        //    ShiftOneRight(index);
        //    _contents[index] = item;

        //    Top++;
        //}

        //public IKeyedObject<K> Remove(int index)  /* passed */  {
        //    IKeyedObject<K> passer = _contents[index];
        //    ShiftOneLeft(index);

        //    /*  emptying pointer at top el, to avoid confusion  */
        //    _contents[Top] = null;

        //    Top--;

        //    return (passer);
        //}

        //public void Clear()  /* passed */  {
        //    /*  a hard clear, not just changing metadata  */

        //    for (int i = 0; i < _contents.Length; i++) {
        //        _contents[i] = null;
        //    }

        //    Count = 0;
        //    Top = -1;
        //}


        ///*  shifting only needed by 1 at a time; ^leftmost is insert / remove point, but [ +1] within methods  */

        ///*  shifting R when inserting  */
        //private void ShiftOneRight(int leftmost)  /* verified */  {
        //    leftmost++;   // local scope only 

        //    /*  shifting R, step Lward so you don't overwrite values as you go, 
        //     *  from Count because it will be new top, 
        //     *  stopping loop at [[orig ^Lmost] +1] because [ -1] in body  */
        //    for (int i = Count; i >= leftmost; i--) {
        //        _contents[i] = _contents[i - 1];
        //    }

        //    Count++;
        //}

        ///*  shifting left when removing  */
        //private void ShiftOneLeft(int leftmost)  /* verified */  {
        //    leftmost++;   // local scope only 

        //    /*  shifting L, step Rward so you don't overwrite as you go; 
        //     *  from [[orig ^Lmost] +1] because [ -1] in body, 
        //     *  to [Count -1] because [at Count] is beyond valid region  */
        //    for (int i = leftmost; i < Count; i++) {
        //        _contents[i - 1] = _contents[i];
        //    }

        //    Count--;
        //}

        //#endregion Changing List Contents
    }
}
