﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // for use in singly-linked lists, at least

    // coding is so trivial that I don't think unit testing is needed at this point

    public class ForwardNode<T>
    {
        #region Fields

        T _content;
        ForwardNode<T> _next;   // a pointer to another ForwardNode

        #endregion Fields


        #region Properties

        // these properties match the reftext's equivalent get-type and set-named methods
        public T Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public ForwardNode<T> Next
        {
            get { return _next; }
            set { _next = value; }
        }

        #endregion Properties


        #region Constructors

        // two constructors:
        // first, with 2 args, is to build a complete node simultaneously placed in a linked list;
        // second, with 1 arg, is to build a node that is placed in a linked list but is not complete

        public ForwardNode(T content, ForwardNode<T> next)
        {
            _content = content;
            _next = next;
        }

        public ForwardNode(ForwardNode<T> next)
        {
            _next = next;
        }

        #endregion Constructors
    }
}
