﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class BinaryTree_Pointers_<K, T> : BinaryTree_<K, T>
    {
        #region Fields

        BinaryNode_<K, T> root = null;
        BinaryNode_<K, T> current = null;
        BinaryNode_<K, T> found = null;
        BinaryNode_<K, T> foundParent = null;

        #endregion Fields


        #region Constructors

        public BinaryTree_Pointers_()
        {
            //  if not given any values, the root node is instantiated with fail values
            root = new BinaryNode_Pointers_<K, T>(this._failKey, this._failValue);
            current = root;
        }

        public BinaryTree_Pointers_(K rootKey, T rootElement)
        {
            //  if given values, the root node is instantiated with them
            root = new BinaryNode_Pointers_<K, T>(rootKey, rootElement);
            current = root;
        }

        #endregion Constructors


        #region Dictionary_ Implementations

        public override K FailKey
        {
            get
            {
                return (this._failKey);
            }
            set
            {
                this._failKey = value;
            }
        }

        public override T FailValue
        {
            get
            {
                return (this._failValue);
            }
            set
            {
                this._failValue = value;
            }
        }

        public override void AddEntry(K key, T element)
        {
            //  either adding to root, or adding at first open child via recursion
            this.AddRecursively(key, element, root);
        }

        //  return value is whether the node was added, so recursion can stop immediately
        public bool AddRecursively(K key, T element, BinaryNode_<K, T> node)
        {
            //  preorder traversal, which should be fastest for a blind / random insert

            //  Act: this method looks at a parent and adds the child there,
            //  in case nulls are used for empties instead of fail-key nodes

            //  this level; a null or fail-key indicate an empty node
            if (node == null || node.Key.Equals(this._failKey))
            {
                node.Key = key;
                node.Element = element;
                return (true);
            }

            //  next level, left child
            bool added = false;
            added = AddRecursively(key, element, node.LeftChild);

            //  next level, right child
            if (!added)
                AddRecursively(key, element, node.RightChild);

            return (false);
        }

        public override void Clear()
        {
            //  elements are auto-reclaimed by the GC

            root = null;
            current = null;
        }

        public override T DropAnEntry()
        {
            //  drops an arbitrary entry;
            //  using preorder and deleting first found at greatest depth

            throw new NotImplementedException();
        }

        public bool FoundSomeParentEntryRecursively(BinaryNode_<K, T> node)
        {
            //  go down left side any children are leaves, and set result to this final parent;
            //  again, preorder seems reasonable

            if (node == null)
                return (false);

            if (!node.IsLeaf)
            {
                //  if both children are either null or a leaf, this is a parent we can delete from;
                //  otherwise, recurse forward / downward
                if (node.LeftChild == null || node.LeftChild.IsLeaf)
                {
                    if (node.RightChild == null || node.RightChild.IsLeaf)
                    { bool x = false; }
                }
            }

            return (false);
        }

        public override T DropEntry(K key)
        {
            T droppedValue = this._failValue;

            if (FoundParentRecursively(key, this.root))
            {
                BinaryNode_<K, T> target = null;

                //  we know for sure that one of the children is the targeted node;
                //  drop by nulling pointer to it; no dealloc of node until after element retained
                if (foundParent.LeftChild.Key.Equals(key))
                {
                    target = found.LeftChild;
                    target.LeftChild = null;
                }
                else
                {
                    target = found.RightChild;
                    target.RightChild = null;
                }

                //  nodes used calculate their own leaf-ness on the fly;
                //  no flag-assigning needed

                //  retain for return
                droppedValue = target.Element;
            }

            return (droppedValue);
        }

        public bool FoundParentRecursively(K key, BinaryNode_<K, T> node)
        {
            //  used for deleting; if the parent is found, deleting its child is easy;
            //  again, preorder seems reasonable

            //  this level, first returning if we've reached an empty;
            //  if not empty, only if it's an inode (a leaf's children will both be nulls);
            //  no `elses needed; returns at first match
            if (node == null)
                return (false);

            if (!node.IsLeaf)
            {
                if (node.LeftChild != null)
                {
                    if (node.LeftChild.Key.Equals(key))
                    {
                        this.foundParent = node;
                        return (true);
                    }
                }

                if (node.RightChild != null)
                {
                    if (node.RightChild.Key.Equals(key))
                    {
                        this.foundParent = node;
                        return (true);
                    }
                }
            }

            //  next level, left child
            bool found = FoundParentRecursively(key, node.LeftChild);

            //  next level, right child
            if (!found)
                found = FoundParentRecursively(key, node.RightChild);

            return (found);
        }

        public override T GetEntry(K key)
        {
            if (FoundRecursively(key, this.root))
                return (this.found.Element);
            else
                return (this._failValue);
        }

        public override void SetEntry(K key, T element)
        {
            if (FoundRecursively(key, this.root))
                this.found.Element = element;
        }

        public bool FoundRecursively(K key, BinaryNode_<K, T> node)
        {
            //  again, preorder seems reasonable

            //  this level; first returning if we've reached an empty,
            //  otherwise matching by key
            if (node == null)
                return (false);

            if (node.Key.Equals(key))
            {
                this.found = node;
                return (true);
            }

            //  next level, left child
            bool found = false;
            found = this.FoundRecursively(key, node.LeftChild);

            //  next level, right child
            if (!found)
                found = this.FoundRecursively(key, node.RightChild);

            return (found);
        }

        #endregion Dictionary_ Implementations


        #region Current Node

        public override K NodeKey
        {
            get
            {
                return (current.Key);
            }
            set
            {
                current.Key = value;
            }
        }

        public override T NodeElement
        {
            get
            {
                return (current.Element);
            }
            set
            {
                current.Element = value;
            }
        }

        public override bool NodeIsLeaf
        {
            get { return (current.IsLeaf); }
        }

        #endregion Current Node


        #region Changing Tree Contents

        public override void AddLeftChild(K key, T element)
        {
            current.LeftChild = new BinaryNode_Pointers_<K, T>(key, element);
        }

        public override void AddRightChild(K key, T element)
        {
            current.RightChild = new BinaryNode_Pointers_<K, T>(key, element);
        }

        public override void DropLeftChild()
        {
            current.LeftChild = null;
        }

        public override void DropRightChild()
        {
            current.RightChild = null;
        }

        #endregion Changing Tree Contents


        #region Stepwise Traversal

        public override void MoveToRoot()
        {
            current = root;
        }

        public override void MoveToLeftChild()   //  PASSED  
        {
            current = current.LeftChild;
        }

        public override void MoveToRightChild()   //  PASSED  
        {
            current = current.RightChild;
        }

        #endregion Stepwise Traversal


        #region Left Child Node

        public override K LeftChildNodeKey
        {
            get
            {
                return (current.LeftChild.Key);
            }
            set
            {
                current.LeftChild.Key = value;
            }
        }

        public override T LeftChildNodeElement
        {
            get
            {
                return (current.LeftChild.Element);
            }
            set
            {
                current.LeftChild.Element = value;
            }
        }

        #endregion Left Child Node


        #region Right Child Node

        public override K RightChildNodeKey
        {
            get
            {
                return (current.RightChild.Key);
            }
            set
            {
                current.RightChild.Key = value;
            }
        }

        public override T RightChildNodeElement
        {
            get
            {
                return (current.RightChild.Element);
            }
            set
            {
                current.RightChild.Element = value;
            }
        }

        #endregion Right Child Node


        #region Traversals

        public override object[] TraversePreOrder(TraversalOperation t)
        {
            throw new NotImplementedException();
        }

        public override object[] TraverseInOrder(TraversalOperation t)
        {
            throw new NotImplementedException();
        }

        public override object[] TraversePostOrder(TraversalOperation t)
        {
            throw new NotImplementedException();
        }

        #endregion Traversals
    }



    #region Nodes Class

    public class BinaryNode_Pointers_<K, T> : BinaryNode_<K, T>   //  PASSED  
    {

        #region Fields

        private K _key;
        private T _element;

        //  pointers to the child nodes; no pointer is maintained to the parent node
        private BinaryNode_Pointers_<K, T> _leftChild;
        private BinaryNode_Pointers_<K, T> _rightChild;

        #endregion Fields


        #region Properties (Including Child Nodes)

        public override K Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public override T Element
        {
            get { return _element; }
            set { _element = value; }
        }

        public override BinaryNode_<K, T> LeftChild
        {
            get { return _leftChild; }
            set { _leftChild = (BinaryNode_Pointers_<K, T>)value; }
        }

        public override BinaryNode_<K, T> RightChild
        {
            get { return _rightChild; }
            set { _rightChild = (BinaryNode_Pointers_<K, T>)value; }
        }

        public override bool IsLeaf
        {
            get { return ((_leftChild == null) && (_rightChild == null)); }
        }

        #endregion Properties (Including Child Nodes)


        #region Constructors

        public BinaryNode_Pointers_()
        {
            //  with generics, `null is not guaranteed to be usable;
            //  but `null is the default for all non-primitives
            _key = default(K);
            _element = default(T);

            _leftChild = null;
            _rightChild = null;
        }

        public BinaryNode_Pointers_(K key, T element)
            : this()
        {
            _key = key;
            _element = element;
        }

        public BinaryNode_Pointers_(K key, T element, BinaryNode_Pointers_<K, T> leftChild, BinaryNode_Pointers_<K, T> rightChild)
            : this(key, element)
        {
            _leftChild = leftChild;
            _rightChild = rightChild;
        }

        #endregion Constructors
    }

    #endregion Nodes Class
}
