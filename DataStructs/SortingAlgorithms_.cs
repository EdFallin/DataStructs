﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class SortingAlgorithms_<T> where T : IComparable<T>, IEquatable<T>
    {
        #region Public Sort Methods, Any Private Methods For Them

        #region Exchange Sorts (Insertion Sort and Selection Sort)

        public static T[] Sort_Insertion_(T[] subject)  /* passed */  {
            /* insertion-sort algorithm: stretch forward, shuffle back only while disordered */

            // traversing the entire subject toward the top, to get each element to sort bottomward; 
            // with inner-loop's swapping, this builds a wholly-ordered stretch of sorted elements; 
            // this stretch may not be least-rooted until all of the subject has been traversed 
            for (int of = 1; of < subject.Length; of++) {
                // at each new topward element, traversing bottomward and swapping until ordered, then stopping; 
                // further bottomward traversing is unnecessary (and wasteful) because element can't be sorted further 
                for (int at = of; at >= 1; at--) {
                    int earlier = at - 1;
                    int later = at;

                    // exiting bottomward swapping when current element is no longer disordered; 
                    // this could also be placed in the condition for the inner loop 
                    if (AreOrdered(subject[earlier], subject[later])) {
                        break;
                    }

                    // actually sorting the latest two disordered adjacent elements 
                    SwapByIndex(subject, earlier, later);
                }
            }

            return subject;
        }

        public static T[] Sort_Selection_(T[] subject)  /* passed */ {
            // the algorithm, in worst Θ(n^2): 
            //     iterate from 0 toward high end, each time retaining the latest start index; 
            //     scan backward from end, at each element retaining either `latest or index of lowest; 
            //     when 0 is reached, swap element at `lowest with it (even if the same) 

            // from bottom up 
            for (int latest = 0, lowest = 0; latest < subject.Length; latest++, lowest = latest) {
                // from top down
                for (int backward = subject.Length - 1; backward >= latest; backward--) {
                    /* this is comparing against `lowest each time, which usually isn't `latest */
                    if (!AreOrdered(subject[lowest], subject[backward]))
                        lowest = backward;
                }

                SwapByIndex(subject, latest, lowest);
            }

            return subject;
        }

        #endregion Exchange Sorts (Insertion Sort and Selection Sort)


        #region Extended Exhange Sorts: Insertion Sort

        /* this sort exists for cases where some higher-end elements are nulls, 
         * defaults, or garbage values that should be ignored */

        public static T[] Sort_Insertion_Partial_(int rootedSize, T[] subject)  /* passed */  {
            /* insertion-sort algorithm: stretch forward, shuffle back only while disordered; 
             * here, only the elements from 0 through [@rootedSize -1] are sorted */

            // traversing the entire subject toward the top, to get each element to sort bottomward; 
            // with inner-loop's swapping, this builds a wholly-ordered stretch of sorted elements; 
            // this stretch may not be least-rooted until all of the subject has been traversed 
            for (int of = 1; of < rootedSize; of++) {
                // at each new topward element, traversing bottomward and swapping until ordered, then stopping; 
                // further bottomward traversing is unnecessary (and wasteful) because element can't be sorted further 
                for (int at = of; at >= 1; at--) {
                    int earlier = at - 1;
                    int later = at;

                    // exiting bottomward swapping when current element is no longer disordered; 
                    // this could also be placed in the condition for the inner loop 
                    if (AreOrdered(subject[earlier], subject[later])) {
                        break;
                    }

                    // actually sorting the latest two disordered adjacent elements 
                    SwapByIndex(subject, earlier, later);
                }
            }

            return subject;
        }

        #endregion Extended Exhange Sorts: Insertion Sort


        #region MergeSort Varieties

        public static T[] Sort_Merge_(T[] subject)  /* passed */ {
            // all ops are done with recursor, which splits progressively at rbuild, 
            // then merges with ordering at rcollapse 
            Recurse_Sort_Merge_(subject, new T[subject.Length], 0, subject.Length - 1);
            return subject;
        }

        private static void Recurse_Sort_Merge_(T[] subject, T[] swap, int thisHalfBottom, int thisHalfTop)  /* verified */ {
            // postorder pyramidal recursion: split `subject into sub-halves and rcall; 
            // at rcollapse, in post-rcall code merge each pair of sub-halves, selecting the lowest from each one 

            // exit condition; ends recursion at this branch 
            if (thisHalfBottom == thisHalfTop)
                return;

            // split; int-div equals floor; sub-halves' lengths may differ by 1 
            int lowerHalfTop = (thisHalfBottom + thisHalfTop) / 2;
            int upperHalfBottom = lowerHalfTop + 1;

            /* rcalls */
            Recurse_Sort_Merge_(subject, swap, thisHalfBottom, lowerHalfTop);
            Recurse_Sort_Merge_(subject, swap, upperHalfBottom, thisHalfTop);

            /* copying to `swap: a Θ(n) operation, yuck */
            /* my approach is different from the book's for indexing; I prefer 0-based for both */
            {
                // copy lower sub-half to swap
                for (int i = 0; i < (upperHalfBottom - thisHalfBottom); i++) {
                    swap[i + thisHalfBottom] = subject[i + thisHalfBottom];
                }

                // copy upper sub-half to swap, reversing order for straightest merging 
                for (int i = 0; i < (thisHalfTop - lowerHalfTop); i++) {
                    swap[upperHalfBottom + i] = subject[thisHalfTop - i];
                }
            }

            // merge the two sub-halves from the outer ends inward, traversing the matching segment of `swap 
            for (
                int swapTraverser = thisHalfBottom, nextLower = thisHalfBottom, nextUpper = thisHalfTop;
                swapTraverser <= thisHalfTop;
                swapTraverser++
                ) {
                // emplace elements in swap, lowest first, then incr/decr only the one that is emplaced 
                if (AreOrdered(swap[nextLower], swap[nextUpper]))
                    subject[swapTraverser] = swap[nextLower++];
                else
                    subject[swapTraverser] = swap[nextUpper--];
            }
        }

        public static T[] Sort_Merge_Insertion_(T[] subject)  /* passed */ {
            // all ops are done with recursor, which splits progressively at rbuild, 
            // then merges with ordering at rcollapse 
            Recurse_Sort_Merge_Insertion_(subject, new T[subject.Length], 0, subject.Length - 1);
            return subject;
        }

        private static void Recurse_Sort_Merge_Insertion_(T[] subject, T[] swap, int thisHalfBottom, int thisHalfTop)  /* verified */ {
            // postorder pyramidal recursion: split `subject into sub-halves and rcall; 
            // at rcollapse, merge each pair of sub-halves, selecting the lowest from each one 

            const int INSERTION_SORT_THRESHOLD = 9;  // ordinarily this would go somewhere else 

            /* skipping further recursion / merging when it no longer will be more efficient */
            if ((thisHalfTop - thisHalfBottom) <= INSERTION_SORT_THRESHOLD) {
                Sort_Insertion_Internal_(subject, thisHalfBottom, thisHalfTop);
                return;
            }

            // split; int-div equals floor; sub-halves' lengths may differ by 1 
            int lowerHalfTop = (thisHalfBottom + thisHalfTop) / 2;
            int upperHalfBottom = lowerHalfTop + 1;

            /* rcalls */
            Recurse_Sort_Merge_(subject, swap, thisHalfBottom, lowerHalfTop);
            Recurse_Sort_Merge_(subject, swap, upperHalfBottom, thisHalfTop);

            /* copying to `swap: a Θ(n) operation, yuck */
            /* my approach is different from the book's for indexing; I prefer 0-based for both */
            {
                // copy lower sub-half to swap 
                for (int i = 0; i < (upperHalfBottom - thisHalfBottom); i++) {
                    swap[i + thisHalfBottom] = subject[i + thisHalfBottom];
                }

                // copy upper sub-half to swap, reversing order for straightest merging 
                for (int i = 0; i < (thisHalfTop - lowerHalfTop); i++) {
                    swap[upperHalfBottom + i] = subject[thisHalfTop - i];
                }
            }

            // merge the two sub-halves from the outer ends inward, traversing the matching segment of `swap 
            for (
                int swapTraverser = thisHalfBottom, nextLower = thisHalfBottom, nextUpper = thisHalfTop;
                swapTraverser <= thisHalfTop;
                swapTraverser++
                ) {
                // emplace elements in swap, lowest first, and incr/decr only the one that is emplaced 
                if (AreOrdered(swap[nextLower], swap[nextUpper]))
                    subject[swapTraverser] = swap[nextLower++];
                else
                    subject[swapTraverser] = swap[nextUpper--];
            }
        }

        #endregion MergeSort Varieties


        #region QuickSort Varieties

        public static T[] Sort_Quick_(T[] subject)  /* passed */ {
            // algo: 
            //     start recursion; recursor chooses pivot, moves pivot aside, pivots items into partitions, 
            //     moves pivot to point between partitions, then rcalls on each partition 

            // recursor; `subject is passed by reference, so no output needed 
            Recurse_Sort_Quick_(subject, 0, subject.Length - 1);
            return subject;
        }

        private static void Recurse_Sort_Quick_(T[] subject, int bottom, int top)  /* verified */ {
            #region Algorithm

            // algo: 
            //     recursively do the following: 
            //         choose a pivot element; 
            //         swap that value to the high end of the current subject to get it out of the way; 
            //         put everything below that element's value on one side of current subject, everything else on the other,
            //             by moving inward from the ends and swapping disordered pairs, thus avoiding metadata calculations at least; 
            //         swap the pivot to the bottom of the high end, where it is equal or less than everything in high end; 
            //         rcall against each partition 

            #endregion Algorithm

            /* the find-pivot algo used next is the simplest one with a reliable success rate; others are possible and can be better */

            // pivot ops: find, retain value for partitioning, move pivot aside till later 
            int pivotIndex = (bottom + top) / 2;    // int division, ~floor 
            T pivotValue = subject[pivotIndex];

            SwapByIndex(subject, pivotIndex, top);

            // metadata for converging loops
            int upward = bottom - 1;
            int downward = top;
            int splitPoint;

            // swapping values to partitions using converging indexers, always comparing for lower < *pivot* and upper > *pivot* 
            while (upward < downward) {
                // converging loops, traversing only; prefix unary operators are needed to continue traversing after ordered / equal elements; 
                // `downward starts at 1 less than `top, where by now the pivot is set aside; 
                // tests of counters against bounds of sub-subject are needed to prevent over-partitioning and exceeding array's limits 
                while (upward < downward && AreOrdered(subject[++upward], pivotValue)) ;
                while (downward > upward && AreOrdered(pivotValue, subject[--downward])) ;

                // swap two elements now without more conditions, as the loops have converged 
                // to a disordered pair, except at very last iteration 
                SwapByIndex(subject, upward, downward);
            }

            // undoing bad last swap; required to prevent repeated swaps of same elements, in and out of order; 
            // preferred to having earlier SwapByIndex() conditional at every swap 
            SwapByIndex(subject, upward, downward);

            // metadata for rcalls; element at this index after next op (the pivot) will never be part of later partitions 
            splitPoint = downward;

            // pivot op: swap pivot element to point where greater-thans begin 
            SwapByIndex(subject, splitPoint, top);

            // rcalls; element at splitPoint not moved; it is already in its final position 
            if ((splitPoint - bottom) > 1)
                Recurse_Sort_Quick_(subject, bottom, splitPoint - 1);
            if ((top - splitPoint) > 1)
                Recurse_Sort_Quick_(subject, splitPoint + 1, top);
        }

        public static T[] Sort_Quick_Insertion_(T[] subject)  /* passed */ {
            // uses recursive quicksort algorithm until insertion sort is faster than more recursion, then switches to insertion sort; 
            // at that switch point, an element can be no more than the threshold's number out of place, making it less work-consuming than further recursion; 
            // unlike in merge-with-insertion sort, the insertion sort here is done all at once at the end 

            // recursor; `subject is passed by reference, so no output needed 
            Recurse_Sort_Quick_Insertion_(subject, 0, subject.Length - 1);

            // final insertion sort of entire subject; again, no output needed 
            Sort_Insertion_Internal_(subject, 0, subject.Length - 1);

            return subject;
        }

        private static void Recurse_Sort_Quick_Insertion_(T[] subject, int bottom, int top)  /* verified */ {

            const int INSERTION_SORT_THRESHOLD = 8;  // ordinarily this would go somewhere else; since `>` is used later, this equates to partitions of size 9 

            // pivot ops: find, retain value for partitioning, move pivot aside till later 
            int pivotIndex = (bottom + top) / 2;
            T pivotValue = subject[pivotIndex];

            SwapByIndex(subject, pivotIndex, top);

            // metadata for converging loops 
            int upward = bottom - 1;
            int downward = top;
            int splitPoint;

            // converging from ends, swapping values disordered versus *pivot* to produce partitions 
            while (upward < downward) {
                while (upward < downward && AreOrdered(subject[++upward], pivotValue)) ;
                while (downward > upward && AreOrdered(pivotValue, subject[--downward])) ;

                // swap converged-to disordered elements 
                SwapByIndex(subject, upward, downward);
            }

            // undoing bad last swap; required to prevent repeated swaps of same elements, in and out of order 
            SwapByIndex(subject, upward, downward);

            // metadata for rcalls 
            splitPoint = downward;

            // pivot op: swap pivot element to point where greater-thans begin 
            SwapByIndex(subject, splitPoint, top);

            // rcalls; element at splitPoint not moved; it is already in its final position 
            if ((splitPoint - bottom) > INSERTION_SORT_THRESHOLD)
                Recurse_Sort_Quick_(subject, bottom, splitPoint - 1);
            if ((top - splitPoint) > INSERTION_SORT_THRESHOLD)
                Recurse_Sort_Quick_(subject, splitPoint + 1, top);
        }

        #endregion QuickSort Varieties


        #region HeapSort

        public static T[] Sort_Heap_(T[] subject)  /* passed */ {
            // algo: make a maxheap from `subject and pop from it, placing values in output downward from top to 0 

            // copying input to an array suitable for my maxheap's key-value style; 
            // keys are used for sorting, but values are returned by pop method 
            Tuple<T, T>[] subjects = new Tuple<T, T>[subject.Length];

            for (int i = 0; i < subject.Length; i++)
                subjects[i] = new Tuple<T, T>(subject[i], subject[i]);

            // making the heap, which is ordered here and self-ordering later 
            MaxHeap_<T, T> heap = new MaxHeap_<T, T>(subjects, subject.Length);

            // popping all elements of heap and outputting from top to 0 of `results 
            T[] results = new T[subject.Length];

            for (int i = subject.Length - 1; i >= 0; i--)
                results[i] = heap.PopHeapRoot();

            // convey 
            return results;
        }

        #endregion HeapSort


        #region IndexSort

        public static int[] Sort_Index_(int[] subject)  /* passed */ {
            int[] passer = new int[subject.Length];

            // elements are copied to a passer so they can be placed back in the original 
            for (int i = 0; i < subject.Length; i++)
                passer[i] = subject[i];

            // the sort 
            for (int i = 0; i < passer.Length; i++)
                subject[passer[i]] = passer[i];

            return subject;   // now reordered 
        }

        #endregion IndexSort


        #region RadixSort

        public static int[] Sort_Radix_(int[] subject, int keyLength)   /* passed */ {
            // algo: 
            //     pass through integer keys as many times as they have digits, int-dividing them by 10 more each time to get next bottom digit; 
            //     use ~digit modulo 10~ to calculate bin locations in output as metadata; use same ~int-div and modulo~ to place them in bins, 
            //     incrementing locations from metadata, and in effect preserving the order from smaller-digit passes 

            /*  an alternative to all the math is to convert each number to a string of digit characters and 
             *  compare them right to left with left-side padding with "0" characters  */

            int[] passer = new int[subject.Length];  // the far end of a reordering step 
            int[] countsPerBin = new int[10];   // count of range for radix is the number of bins ( for digits, 0 to 9 ) 
            int[] binStartIndices = new int[countsPerBin.Length];

            /* for the first try, I'm implementing this with write-back to the original array, but I hope to abandon that */

            /*  for this first try, I'm also going to implement this on the assumption that the number of digits is the same for all; 
             *  later, I hope to abandon that for variable-length numbers, with virtual left-end padding with 0s for smaller numbers */

            // scan all atoms in each key in the subject 
            for (int scanner = 0, divisor = 1; scanner < keyLength; scanner++, divisor *= 10) {

                // re-init metadata arrays 
                for (int i = 0; i < countsPerBin.Length; i++) {
                    countsPerBin[i] = 0;
                    binStartIndices[i] = 0;
                }

                // at this atom, traverse all keys in the subject, modulus of atom is used as index of bin-counter to increment 
                for (int traverser = 0; traverser < subject.Length; traverser++) {
                    int binIndex = AtomModulo(subject[traverser], divisor);   // formula is [key ÷divisor] %10]
                    countsPerBin[binIndex]++;
                }

                // at this atom, convert counts to bin indices 
                for (int i = 0, accumulator = 0; i < binStartIndices.Length; i++) {
                    binStartIndices[i] = accumulator;
                    accumulator += countsPerBin[i];
                }

                // at this atom, copy elements to the correct bins without reordering them from past atoms' sub-orderings 
                for (int traverser = 0; traverser < subject.Length; traverser++) {
                    int binIndex = AtomModulo(subject[traverser], divisor);

                    int keyIndexWithinBin = binStartIndices[binIndex]++;

                    /* crux : putting the key into the correct spot in the correct bin */
                    passer[keyIndexWithinBin] = subject[traverser];
                    /* end crux */
                }

                // copy elements in their current arrangement back to the subject, ready for addressing the next atom 
                for (int i = 0; i < subject.Length; i++)
                    subject[i] = passer[i];

            }   // repeated until all atoms are exhausted 

            // convey
            return subject;
        }

        private static int AtomModulo(int subject, int divisor)   /* passed */ {
            // steps to look only at the current atom ( for an int, rightmost digit not yet scanned ) 
            int passer = subject / divisor;   // int-division throws out any digits smaller than the desired one 
            int result = passer % 10;   // modulo converts the rightmost digit to a number in the range of the radix
            return result;
        }

        #endregion RadixSort


        #region Deprecated : Generalized RadixSort

        #region Generalized Radix Sort

        private static bool _reversePaddingInRadixSort = false;

        public static bool ReversePaddingInRadixSort {
            get { return _reversePaddingInRadixSort; }
            set { _reversePaddingInRadixSort = value; }
        }

        public delegate decimal KeyToDecimal_Delegate_(T key);

        public delegate int KeyLength_Delegate_(T key);

        public static T[] Sort_Radix_Generalized_(
            T[] subject, KeyLength_Delegate_ measurer, KeyToDecimal_Delegate_ converter) {

            // convert all keys to decimals, cycling through each atom (probably a character in each one) 
            // and then doing a radix-sort on them; decimal chosen because it can contain the longest 
            // numbers, of 29 digits, as might be needed when converting a long string for instance 

            int maxKeyLength = 0;
            int[] keyLengths = new int[subject.Length];   // each length retained as metadata for fewer calcs in sort cycle 

            // original keys must be kept with their converted forms and their lengths, at both ends of sorting steps 
            Tuple<decimal, T, int>[] local = new Tuple<decimal, T, int>[subject.Length];
            Tuple<decimal, T, int>[] swapper = new Tuple<decimal, T, int>[subject.Length];

            decimal[] keysAsDecimals = new decimal[subject.Length];

            // a Θ(n) scan of each key to retain all key lengths and to get max length for cycling by atom 
            for (int i = 0; i < subject.Length; i++) {
                int thisKeyLength = measurer(subject[i]);
                keyLengths[i] = thisKeyLength;

                if (thisKeyLength > maxKeyLength)
                    maxKeyLength = thisKeyLength;
            }


            // converting keys to decimals using a delegate; though exfactored, usually this is probably an Θ(key-length) operation on each key, 
            // for a total of a slow Θ(n*(key-length)); this is the same as the main loop, though, in essence, so this roughly doubles overall time 
            for (int i = 0; i < subject.Length; i++)
                local[i] = Tuple.Create(converter(subject[i]), subject[i], keyLengths[i]);

            // metadata arrays, for digits found in an int (or whole-number decimal) 
            int[] binLengths = new int[10];
            int[] binStartIndices = new int[binLengths.Length];

            // the sort, scanning across atoms from least to greatest; atoms with no value for a key are considered 0s 
            for (int scanner = 0, divisor = 1; scanner < maxKeyLength; scanner++, divisor *= 10) {

                // re-initing my metadata arrays for this iteration 
                for (int i = 0; i < binLengths.Length; i++) {
                    binLengths[i] = 0;
                    binStartIndices[i] = 0;
                }

                // getting counts by bin for all keys, using int-div and modulo 
                for (int traverser = 0; traverser < subject.Length; traverser++) {
                    decimal thisKeyAsDecimal = local[traverser].Item1;

                    int binIdentityOfThisKey = 0;   // default bin for any digits without a value 

                    if (!_reversePaddingInRadixSort) {
                        if (scanner <= local[traverser].Item3)   // only calc a bin when we're within the digits of this key; .Item3 is this key's length 
                            binIdentityOfThisKey = (int)((thisKeyAsDecimal / divisor) % binLengths.Length);   // I calced this out here for practice with the algo 
                    }
                    else {
                        if (scanner < (maxKeyLength - local[traverser].Item3))
                            binIdentityOfThisKey = (int)((thisKeyAsDecimal / divisor) % binLengths.Length);
                    }

                    binLengths[binIdentityOfThisKey]++;
                }

                // converting counts to indices by accumulating counts so far, but only after past accumulation has been used 
                for (int i = 0, accumulator = 0; i < binLengths.Length; i++) {
                    binStartIndices[i] = accumulator;
                    accumulator += binLengths[i];
                }

                /* crux: all ops to convert atoms at this iteration to bins, not losing past ordering */
                for (int traverser = 0; traverser < subject.Length; traverser++) {
                    decimal thisKeyAsDecimal = local[traverser].Item1;

                    // as earlier, using a default bin, and calculating a real one only when we're within the digits of this key 
                    int binIdentityOfThisKey = 0;

                    if (!_reversePaddingInRadixSort) {
                        if (scanner <= local[traverser].Item3)
                            binIdentityOfThisKey = (int)((thisKeyAsDecimal / divisor) % binLengths.Length);   // practicing some more 
                    }
                    else {
                        if (scanner < (maxKeyLength - local[traverser].Item3))
                            binIdentityOfThisKey = (int)((thisKeyAsDecimal / divisor) % binLengths.Length);
                    }

                    // I get the index for this binIdentity, put the key's tuple there, then increment that index 
                    int thisKeyLocation = binStartIndices[binIdentityOfThisKey]++;

                    /* cruxmost */
                    swapper[thisKeyLocation] = local[traverser];
                    /* end cruxmost */
                }
                /* end crux */


                /* copy swapped items back to their local storer for the next sorting pass */
                for (int i = 0; i < local.Length; i++)
                    local[i] = swapper[i];

            }      /* repeated until all atoms of the longest key have been scanned */


            /* copy keys, now sorted, back to an array for output */
            T[] results = new T[subject.Length];

            for (int i = 0; i < subject.Length; i++)
                results[i] = local[i].Item2;   // .Item2 is the original key of type T 

            // convey 
            return results;
        }

        #endregion Generalized Radix Sort


        #region Delegates For Some Radixes

        #region For Int Radix

        /* a KeyLength_Delegate_ */
        public static int IntDigitsLength(int key)   /* passed */ {
            int length = 1;   // minimum length for a valid int key 

            // int.MaxValue (2147483647) has 10 digits; this is the most digits an int can have 
            for (int i = 0, divisor = 1; i < 10; i++, divisor *= 10) {
                length = i;

                // this int-div produces 0 if key < current 10-factor, so last-prior i is then the number of digits 
                if ((key / divisor) == 0)
                    break;
            }

            return length;
        }

        /* a KeyToDecimal_Delegate_ */
        public static decimal IntAsDecimal(int key)   /* ok */ {
            /* targetLength is ignored here, as left-end 0s handling in sort works instead */
            return (decimal)key;
        }

        #endregion For Int Radix


        #region For String Radix

        /* a KeyLength_Delegate_ */
        public static int StringDecimalDigitsLength(string key) {
            /* in the sorting code, the length of the decimal form of the string is needed, not that of the string itself */

            decimal asDecimal = StringAsDecimal(key);
            int length = DecimalDigitsLength(asDecimal);

            return length;
        }

        /* not a delegate; was a delegate during now-abandoned approach */
        public static decimal StringAsFullDecimal(string key, int targetLength)   /* passed */ {
            decimal result = 0;

            // get the string as its basis decimal, calculate how many digits it is, 
            // then multiply by the difference between that and the target length 
            decimal asDecimal = StringAsDecimal(key);

            int actualLength = DecimalDigitsLength(asDecimal);
            int factorial = targetLength - actualLength;

            result = asDecimal * (decimal)Math.Pow(10, factorial);
            return result;
        }

        /* a KeyToDecimal_Delegate_ */
        public static decimal StringAsDecimal(string key)   /* passed */ {
            decimal result = 0;

            /* tenning as a simple way to convert to a decimal: from right to left, each char's code is * by a 10-factor and added to the output */
            char[] chars = key.ToCharArray();

            for (int i = chars.Length - 1, multiplier = 1; i >= 0; i--, multiplier *= 10) {
                int charAsInt = (int)chars[i];
                result += (charAsInt * multiplier);
            }

            return result;
        }

        /* not a delegate; used in KeyLength_Delegate_ */
        public static int DecimalDigitsLength(decimal key)   /* passed */ {
            int length = 1;   // minimum length for a valid int key 

            // max decimal value has 29 digits; loop adjusted to work with decimal outputs and a < 1 test; 
            // min i must be 1 (so max i of 30) and min divisor must be 10 for calcs to work right 
            for (int i = 1, divisor = 10; i < 30; i++, divisor *= 10) {
                length = i;

                // this div is < 1 if key < current 10-factor, so last-prior i is then the number of digits 
                if ((key / divisor) < 1)
                    break;
            }

            return length;
        }

        #endregion For String Radix

        #endregion Delegates For Some Radixes

        #endregion Deprecated : Generalized RadixSort

        #endregion Public Sort Methods, Any Private Methods For Them


        #region Internal Static Utility Methods

        private static void Sort_Insertion_Internal_(T[] subject, int bottom, int top)  /* verified */  {
            /*  this variant is for use with merge-sort, since it switches to insertion sort addressing only parts
                of the whole subject; no return value, but `subject is available afterward in the calling scope */

            for (int farthest = bottom; farthest <= top; farthest++) {
                for (int backward = farthest; backward > bottom; backward--) {
                    if (!AreOrdered(subject[backward - 1], subject[backward]))
                        SwapByIndex(subject, backward - 1, backward);
                }
            }
        }

        private static bool AreOrdered(T earlier, T later)  /* passed */ {
            // "ordered" here means ascending order 

            int results = earlier.CompareTo(later);

            // results > 0 ( always 1 ) means earlier is > later, so disordered;
            // equal is considered ordered, so 
            // all other results are ordered 

            if (results > 0)
                return false;

            return true;
        }

        private static T[] SwapByIndex(T[] subject, int earlier, int later)  /* passed */ {
            // assumptions here are that `subject is valid, and the indices are within its range

            T passer = subject[later];

            subject[later] = subject[earlier];
            subject[earlier] = passer;

            return subject;
        }

        #endregion Internal Static Utility Methods
    }
}
