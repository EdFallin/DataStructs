﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public abstract class Dictionary_<K,T>
    {
        abstract public T GetEntry(K key);                  // in reftext, `find
        abstract public void SetEntry(K key, T element);    // not found in in reftext
        
        abstract public void AddEntry(K key, T element);
        abstract public T DropEntry(K key);                 // in reftext, `remove
        abstract public T DropAnEntry();                    // in reftext, `remove-any

        abstract public void Clear();                       // same in reftext

        // no constructor; abstract
    }
}
