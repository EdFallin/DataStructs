﻿using System;
using System.Collections;   // location of BitArray 
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs {
    public static class Searchers_ {
        #region Definitions

        public const int None = -1;

        #endregion Definitions


        #region Linear searching

        public static T FindLinearInTuplesArray<K, T>(K sought, Tuple<K, T>[] subject, T failValue)  /* passed */  {
            /* my syntax here assumes a key-value pair for clarity */
            for (int ix = 0; ix < subject.Length; ix++) {
                // .Item1 is effectively key, and .Item2 value 
                if (subject[ix].Item1.Equals(sought)) {
                    return subject[ix].Item2;
                }
            }

            // when sought not found, this is possibly better than throwing exception 
            return failValue;
        }

        public static T FindLinearInArray<T>(T sought, T[] subject, T failValue)  /* passed */  {
            /* my syntax here assumes a key-value pair for clarity */
            for (int ix = 0; ix < subject.Length; ix++) {
                // .Item1 is effectively key, and .Item2 value 
                if (subject[ix].Equals(sought)) {
                    return subject[ix];
                }
            }

            // when sought not found, this is possibly better than throwing exception 
            return failValue;
        }

        public static T FindLinearInTuplesEnumerable<K, T>(K sought, IEnumerable<Tuple<K, T>> subject, T failValue)  /* passed */  {
            /* here, the iterator pattern, with .NET's IEnumerator; if enumerator is defined, can always use `foreach` */

            foreach (Tuple<K, T> item in subject) {
                if (item.Item1.Equals(sought)) {
                    return item.Item2;
                }
            }

            return failValue;
        }

        public static T FindLinearInEnumerable<T>(T sought, IEnumerable<T> subject, T failValue)  /* passed */  {
            /* here, the iterator pattern, with .NET's IEnumerator; if enumerator is defined, can always use `foreach` */

            foreach (T item in subject) {
                if (item.Equals(sought)) {
                    return sought;
                }
            }

            return failValue;
        }

        #endregion Linear searching


        #region Binary searching

        #region FindBinary<K>(), over whole subject

        public static int FindBinary<K>(K[] subject, K sought)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* algorithm: 
             *     starting with 0 and top index, halve subject and look at midpoint element, 
             *     returning the index of that midpoint if the element matches sought; 
             *     otherwise exclude it and redefine subject to whichever half must contain sought if it is present; 
             *     if sought never found, exit the halving process when ends of range cross (bottom is higher than top), 
             *     because the top and bottom are the same when a single-item later half is addressed */

            int top = subject.Length - 1;
            int bottom = 0;

            // repeatedly halving until @midpoint matches @sought, or no more halving is possible; 
            // one end index is offset by 1 each time, so _after_ stretch is 1 long (@top == @bottom), 
            // the next midpoint offsetting means @top < @bottom, and loop exits 
            while (top >= bottom) {
                // @midpoint must be calculated this way to be correct 
                int midpoint = (top + bottom) / 2;
                K current = subject[midpoint];

                // @sought has been found, so the loop and method are exited; 
                // returning the index, for O(1) access to element by consumer 
                if (current.Equals(sought)) {
                    return midpoint;
                }

                // if @sought < latest middle value, look in the lower half of the current stretch, 
                // excluding the middle so that halving does not repeat some indices and miss others 
                if (sought.CompareTo(current) < 0) {
                    top = midpoint - 1;
                }
                // if @sought > latest middle value, look in the upper half, excluding the middle for the same reason 
                else {
                    bottom = midpoint + 1;
                }
            }

            // when @sought was not found 
            return Searchers_.None;
        }

        #endregion FindBinary<K>(), over whole subject


        #region FindBinaryRecursive<K>(), over whole subject

        public static int FindBinaryRecursive<K>(K[] subject, K sought)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            int result = FindBinaryRecursor(subject, sought, 0, subject.Length - 1);
            return result;
        }

        private static int FindBinaryRecursor<K>(K[] subject, K sought, int bottom, int top)  /* verified */  where K : IComparable<K>, IEquatable<K> {
            int result = Searchers_.None;

            //**  this level: exit condition or match  **//
            // an inverted restatement of the iterative continue condition 'while ((top - bottom) >= 0)'; 
            // in toss-the-midpoint algo, @top is equal to @bottom in the final-pass search cases; 
            // see further explanation of this exit condition in FindBinary<K>(K[], K) 
            if (top < bottom) {
                return Searchers_.None;
            }

            // calculating new midpoint over entire @subject 
            int mid = (top + bottom) / 2;

            // looking at newest middle item and returning if a match 
            K atMid = subject[mid];

            if (sought.Equals(atMid)) {
                return mid;
            }

            //**  recursion: branching to correct half, tossing the midpoint  **//
            // if no match at middle item, recursing over the half that might contain @sought, tossing the midpoint, 
            // which is needed for halving to be able to reach all indices, and since midpoint can't hold the answer 
            if (sought.CompareTo(atMid) < 0) {
                return FindBinaryRecursor(subject, sought, bottom, mid - 1);  // 'mid - 1' is tossing the midpoint, lower half 
            }
            else {
                return FindBinaryRecursor(subject, sought, mid + 1, top);  // 'mid + 1' is tossing the midpoint, upper half 
            }
        }

        #endregion FindBinaryRecursive<K>(), over whole subject


        #region FindBinary<K, T>(), over whole subject

        public static T FindBinary<K, T>(KeyValuePair<K, T>[] subject, K sought, T failValue)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            int top = subject.Length - 1;
            int bottom = 0;

            // repeatedly halving until @midpoint matches @sought, or no more halving is possible; 
            // one end index is offset by 1 each time, so _after_ stretch is 1 long (@top == @bottom), 
            // the next midpoint offsetting means @top < @bottom, and loop exits 
            while (top >= bottom) {
                // finding the midpoint and the key there, and returning 
                // the value there if the key matches @sought 
                int mid = (top + bottom) / 2;
                KeyValuePair<K, T> atMid = subject[mid];

                if (sought.Equals(atMid.Key)) {
                    return atMid.Value;
                }

                // if the key didn't match, rebounding to the 
                // correct half, minus the midpoint 
                if (sought.CompareTo(atMid.Key) < 0) {
                    top = mid - 1;
                }
                else {
                    bottom = mid + 1;
                }
            }

            // never found @sought, so returning an anti-default value 
            return failValue;
        }

        #endregion FindBinary<K, T>(), over whole subject


        #region FindBinary<K>(), over slice of subject

        public static int FindBinary<K>(K[] subject, K sought, int bottom, int top)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* algorithm: perform an ordinary toss-the-midpoint binary search, 
             * but starting with the provided stretch's bounding indices */

            // repeatedly halving until @midpoint matches @sought, or no more halving is possible; 
            // one end index is offset by 1 each time, so _after_ stretch is 1 long (@top == @bottom), 
            // the next midpoint offsetting means @top < @bottom, and loop exits 
            while (top >= bottom) {
                // latest midpoint, value, and return if a match 
                int mid = (top + bottom) / 2;
                K atMid = subject[mid];

                if (atMid.Equals(sought)) {
                    return mid;
                }

                // if latest doesn't match, rebounding to 
                // the correct half, minus the midpoint 
                if (sought.CompareTo(atMid) < 0) {
                    top = mid - 1;
                }
                else {
                    bottom = mid + 1;
                }
            }

            // returning 
            return Searchers_.None;
        }

        #endregion FindBinary<K>(), over slice of subject


        #region FindBinaryClosestBelow<K>() and FindBinaryClosestAbove<K>(), over whole subject

        public static int FindBinaryClosestBelow<K>(K[] subject, K sought)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* perform an ordinary binary search, but also keep track of the highest index searched that is below @sought; 
             * this highest-below is the middle used as bottom index when rebounding for a R-side search, 
             * since it must be the tightest bound, and when a L-side search, middle index is above @sought */

            int closest = Searchers_.None;  // -1; if @sought below all, none is closest below 

            int top = subject.Length - 1;  // otherwise, it's possible to address an index beyond @subject 
            int bottom = 0;

            // repeatedly halving until @midpoint matches @sought, or no more halving is possible; 
            // one end index is offset by 1 each time, so _after_ stretch is 1 long (@top == @bottom), 
            // the next midpoint offsetting means @top < @bottom, and loop exits 
            while (top >= bottom) {
                // finding any exact match at the current stretch's midpoint 
                int mid = (bottom + top) / 2;
                K atMid = subject[mid];

                if (sought.Equals(atMid)) {
                    return mid;
                }

                // rebounding if below, but not changing @closest, because this @mid is above, not below 
                if (sought.CompareTo(atMid) < 0) {
                    top = mid - 1;
                }
                // rebounding if above, and changing @closest since this is the closest-below reached so far; 
                // if @sought > all items, item at @top is eventually @mid and assigned here 
                else {
                    closest = mid;
                    bottom = mid + 1;
                }
            }

            return closest;
        }

        public static int FindBinaryClosestAbove<K>(K[] subject, K sought)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* perform an ordinary binary search, but also keep track of the lowest index searched that is above @sought; 
             * this lowest-above is the middle used as top index when rebounding for a L-side search, 
             * since it must be the tightest bound, and when a R-side search, middle index is below @sought */

            int bottom = 0;
            int top = subject.Length - 1;  // otherwise, it's possible to address an index beyond @subject 

            int closest = Searchers_.None;

            // repeatedly halving until @midpoint matches @sought, or no more halving is possible; 
            // one end index is offset by 1 each time, so _after_ stretch is 1 long (@top == @bottom), 
            // the next midpoint offsetting means @top < @bottom, and loop exits 
            while (top >= bottom) {
                // finding any exact match at the current stretch's midpoint 
                int mid = (top + bottom) / 2;
                K atMid = subject[mid];

                if (sought.Equals(atMid)) {
                    return mid;
                }

                // rebounding if below, and changing @closest, since this is the closest-above reached so far; 
                // if @sought < all items, item at @bottom is eventually @mid and assigned here 
                if (sought.CompareTo(atMid) < 0) {
                    closest = mid;
                    top = mid - 1;
                }
                // rebounding if above, but not changing @closest, since this @mid was below, not above 
                else {
                    bottom = mid + 1;
                }
            }

            return closest;
        }

        #endregion FindBinaryClosestBelow<K>() and FindBinaryClosestAbove<K>(), over whole subject


        #region FindBinaryClosestBelow<K>() and FindBinaryClosestAbove<K>(), over slice of subject

        public static int FindBinaryClosestBelow<K>(K[] subject, K sought, int bottom, int top)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* perform an ordinary binary search, but also keep track of the highest index searched that is below @sought; 
             * this highest-below is the middle used as bottom index when rebounding for a R-side search, 
             * since it must be the tightest bound, and when a L-side search, middle index is above @sought */

            int closest = Searchers_.None;  // -1; if @sought below all, none is closest below 

            /* this code is exactly the same as whole-subject closest-below, with explanation comments found there */
            while (top >= bottom) {
                int mid = (top + bottom) / 2;
                K atMid = subject[mid];

                if (sought.Equals(atMid)) {
                    return mid;
                }

                if (sought.CompareTo(atMid) < 0) {
                    top = mid - 1;
                }
                else {
                    closest = mid;
                    bottom = mid + 1;
                }
            }
            
            return closest;
        }

        public static int FindBinaryClosestAbove<K>(K[] subject, K sought, int bottom, int top)  /* passed */  where K : IComparable<K>, IEquatable<K> {
            /* perform an ordinary binary search, but also keep track of the lowest index searched that is above @sought; 
             * this lowest-above is the middle used as top index when rebounding for a L-side search, 
             * since it must be the tightest bound, and when a R-side search, middle index is below @sought */

            int closest = Searchers_.None;

            /* this code is exactly the same as whole-subject closest-above, with explanation comments found there */
            while (top >= bottom) {
                int mid = (top + bottom) / 2;
                K atMid = subject[mid];

                if (sought.Equals(atMid)) {
                    return mid;
                }

                if (sought.CompareTo(atMid) < 0) {
                    closest = mid;
                    top = mid - 1;
                }
                else {
                    bottom = mid + 1;
                }
            }

            return closest;
        }

        #endregion FindBinaryClosestBelow<K>() and FindBinaryClosestAbove<K>(), over slice of subject

        #endregion Binary searching


        #region Self-organizing list searching

        public static T FindSelfOrganize<K, T>(K sought, KeyValuePair<K, T>[] subject, T failValue)  /* passed */  {
            T output = failValue;
            int swapDistance = subject.Length / 10;   // int-div returns an int 

            /* linear search */
            for (int ix = 0; ix < subject.Length; ix++) {
                if (subject[ix].Key.Equals(sought)) {
                    output = subject[ix].Value;

                    // list self-ordering operations 
                    {
                        int target = (ix >= swapDistance ? ix - swapDistance : 0);   // staying within array 
                        KeyValuePair<K, T> passer = subject[target];
                        subject[target] = subject[ix];
                        subject[ix] = passer;
                    }

                    break;
                }
            }

            return output;
        }

        #endregion Self-organizing list searching


        #region Bit-array searching, and dependencies

        public static BitArray Digest<T>(T[] subject, T[] topics)  /* passed */  {
            /*  for each topics[] item in subject, sets an output bit in order listed in array */
            BitArray output = new BitArray(topics.Length);

            // a Θ(nm) operation; not great, but I’d use a delegate within loop in normal use
            for (int ix = 0; ix < topics.Length; ix++) {
                foreach (T element in subject) {
                    if (element.Equals(topics[ix])) {
                        output.Set(ix, true);
                        break;
                    }
                }
            }

            return output;
        }

        public static bool FindInDigest<T>(BitArray subject, T[] topics, T sought)  /* passed */  {
            /* in my implemn, `sought arg in native form; `topics index would be faster */
            bool output = false;
            int soughtIndex = -1;

            // since `sought is in native form, its bit-array index has to be found 
            for (int ix = 0; ix < topics.Length; ix++) {
                if (topics[ix].Equals(sought)) { soughtIndex = ix; }
            }

            if (soughtIndex == -1) {
                throw new Exception("Sought is not a topic.");
            }

            /* crux: the actual bit-vector operation */
            output = subject.Get(soughtIndex);

            return output;
        }

        public static bool TopicsAreInDigest<T>(BitArray subject, BitArray soughts)  /* passed */  {
            if (subject.Length != soughts.Length) {
                throw new Exception("Digests are not comparable.");
            }

            /* if `subject contains `soughts, Or ( `|` ) should return the original `subject` back */
            BitArray results = new BitArray(subject);   // making a copy of the input to avoid changing it during ops 
            results.Or(soughts);                        // perform | on all bits in the BitArrays 


            /* comparing as int[]s, time cost is 1/32 of bit-by-bit comparing; some conversion overhead */
            int lengthAsInts = (subject.Length / 8) + 1;   // calc enough 32-bit segments (ints) 

            int[] subjectAs32BitInts = new int[lengthAsInts];
            int[] resultsAs32BitInts = new int[lengthAsInts];

            subject.CopyTo(subjectAs32BitInts, 0);
            results.CopyTo(resultsAs32BitInts, 0);

            // 32 bits at a time; first different int means not all soughts in digest; trailing 0s if bitmaps not clean %32 
            for (int ix = 0; ix < lengthAsInts; ix++) {
                if (subjectAs32BitInts[ix] != resultsAs32BitInts[ix]) {
                    return false;
                }
            }

            /* code path when no bits were different */
            return true;
        }

        #endregion Bit-array searching, and dependencies

    }
}
