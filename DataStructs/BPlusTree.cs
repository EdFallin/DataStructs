﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    /* !!: after all work is done and passes tests, or maybe earlier, search for 
     *     learnings to put into docs, in addition to the code itself */

    public class BPlusTree<K> where K : IEquatable<K>, IComparable<K>
    {
        #region Definitions

        const int MAX_NODES = 10;
        const int NONE = -1;

        #endregion Definitions


        #region Fields

        NodeBpt<K> _root = new LeafBpt<K>(default(K));  // root's key is always ignored 

        #endregion Fields


        #region Properties

        // constant / static properties  */
        public static int MaxNodes { get; protected set; }

        public static int Top {
            get {
                return (MaxNodes - 1);
            }
        }

        public static int None => NONE;

        // variable properties  */
        public NodeBpt<K> Root {
            get {
                return _root;
            }
            set {
                _root = value;
            }
        }

        #endregion Properties


        #region Constructors

        public BPlusTree(int maxNodes = MAX_NODES) {
            MaxNodes = maxNodes;
            NodeBpt<K>.MaxNodes = MaxNodes;
        }

        #endregion Constructors


        #region Public methods

        #region Search() and recursor

        public ElementBpt<K> Search(K sought)  /* passed */  {
            ElementBpt<K> found = SearchRecursor(_root, sought);  // entirety is recursion 
            return found;
        }

        private ElementBpt<K> SearchRecursor(NodeBpt<K> node, K sought)  /* verified */  {
            /* rcall always to leaf; at leaf get value or fail, rcollapse; at forks, rcollapse */

            if (node is LeafBpt<K>) {
                /* site of any tree content */
                int exact = Searchers_.FindBinary(node.Keys, sought, 0, node.TopKey);

                /* rpop / rcollapse */
                if (exact != Searchers_.None) {
                    return node.Contents[exact] as ElementBpt<K>;
                }
                else {
                    return null;
                }
            }
            else {
                /* rsite and rcall */
                int closest = Searchers_
                    .FindBinaryClosestBelow(node.Keys, sought, 0, node.TopKey);
                ElementBpt<K> found =
                    SearchRecursor(node.Contents[closest] as NodeBpt<K>, sought);

                /* rpop */
                return found;
            }

        }

        #endregion Search() and recursor


        #region SearchUpward() and recursor

        public List_Vector_Extended_<ElementBpt<K>> SearchUpward(K lowest, K highest)  /* passed */  {
            ElementBpt<K> sought = SearchBottomRecursor(this.Root, lowest);

            List_Vector_Extended_<ElementBpt<K>> soughts
                = new List_Vector_Extended_<ElementBpt<K>>(MaxNodes);  // guess at min size 

            /* if ~lowest is beyond range, return empty vector */
            if (sought == null) {
                return soughts;
            }

            /* traverse els until ~highest, or end of els */
            while (sought.Key.CompareTo(highest) <= 0) {
                soughts.Append(sought);
                sought = sought.Next;

                /* end of els before ~highest */
                if (sought == null) {
                    break;
                }
            }

            return soughts;
        }

        private ElementBpt<K> SearchBottomRecursor(NodeBpt<K> node, K lowest)  /* verified */  {
            /* recursor for ranged search from lowest to highest */

            if (node is LeafBpt<K>) {
                /* site of exact indexer or closest that's in range (the first higher) */
                int closest = Searchers_.FindBinaryClosestAbove(node.Keys, lowest, 0, node.TopKey);

                /* rpop / rcollapse: fail path, then normal path */
                if (closest == None) {
                    return null;
                }

                return node.Contents[closest] as ElementBpt<K>;
            }
            else {
                /* rsite and rcall, closest-below since progressing leafward */
                int leafward = Searchers_.FindBinaryClosestBelow(node.Keys, lowest, 0, node.TopKey);
                NodeBpt<K> next = node.Contents[leafward] as NodeBpt<K>;
                ElementBpt<K> first = SearchBottomRecursor(next, lowest);

                /* rpop */
                return first;
            }
        }

        #endregion SearchUpward() and recursor


        #region SearchDownward() and recursor

        public List_Vector_Extended_<ElementBpt<K>> SearchDownward(K highest, K lowest)  /* passed */  {
            ElementBpt<K> sought = SearchTopRecursor(this.Root, highest);

            List_Vector_Extended_<ElementBpt<K>> soughts
                = new List_Vector_Extended_<ElementBpt<K>>(MaxNodes);  // guess at min size 

            /* if ~highest is beyond range, return empty vector */
            if (sought == null) {
                return soughts;
            }

            /* traverse els until ~lowest, or end of els */
            while (sought.Key.CompareTo(lowest) >= 0) {
                soughts.Append(sought);
                sought = sought.Previous;

                /* end of els before ~lowest */
                if (sought == null) {
                    break;
                }
            }

            return soughts;
        }

        private ElementBpt<K> SearchTopRecursor(NodeBpt<K> node, K highest)  /* verified */  {
            /* recursor for ranged search from highest to lowest */

            int closest = Searchers_.FindBinaryClosestBelow(node.Keys, highest, 0, node.TopKey);

            /* no-such-key path */
            if (closest == None) {
                return null;
            }

            if (node is LeafBpt<K>) {
                /* rpop / rcollapse */
                return node.Contents[closest] as ElementBpt<K>;
            }
            else {
                /* rsite and rcall */
                int leafward = Searchers_.FindBinaryClosestBelow(node.Keys, highest, 0, node.TopKey);
                NodeBpt<K> next = node.Contents[leafward] as NodeBpt<K>;
                ElementBpt<K> first = SearchTopRecursor(next, highest);

                /* rpop */
                return first;
            }
        }

        #endregion SearchDownward() and recursor


        #region Insert() and recursor

        public void Insert(ElementBpt<K> insertable)  /* passed */  {
            /* method core: recursion */
            NodeBpt<K> root = InsertRecursor(_root, insertable);

            /* if overflow at root, re-root tree with all-new root fork that always has 2 children */
            if (root != _root) {
                NodeBpt<K> newRoot = new ForkBpt<K>();

                /* using .Insert() requires -1 for Contents[0], since ++ internally; 
                 * the alternative is to set each of keys, contents, and top key */
                newRoot.Insert(-1, _root);
                newRoot.Insert(0, root);

                /* re-rooting tree */
                _root = newRoot;
            }
        }

        private NodeBpt<K> InsertRecursor(NodeBpt<K> node, ElementBpt<K> insertable)  /* verified */  {
            /* rcall always to leaf, insert there, getting same or new sibling, rcollapse returning that; 
             * at forks, check if new sibling, insert if so, rpop just-returned self or new sibling */

            /* determine rloc / insert-loc */
            int closest = Searchers_
                .FindBinaryClosestBelow(node.Keys, insertable.Key, 0, node.TopKey);

            #region At-leaf path, top of rstack

            /* if _current_ ~node is leaf */
            if (node is LeafBpt<K>) {
                /* insert, then rpop and rcollapse; may return new sibling of ~node */
                return node.Insert(closest, insertable);
            }

            #endregion At-leaf path, top of rstack

            #region At-fork path, with postorder ops

            /* rsite */
            NodeBpt<K> next = node.Contents[closest] as NodeBpt<K>;

            /* rcall */
            NodeBpt<K> back = InsertRecursor(next, insertable);

            /* postorder ops when overflow at rcalled; pointer comparison */
            if (back != next) {
                /* insert ± restructure at current node */
                NodeBpt<K> now = node.Insert(closest, back);

                /* rpop; ~now may be ~node or new sibling */
                return now;
            }

            /* rpop; when no overflow at rcalled */
            return node;

            #endregion At-fork path, with postorder ops
        }

        #endregion Insert() and recursor


        #region Remove() and recursor

        public void Remove(ElementBpt<K> removable)  /* passed */  {
            /* method core: recursion */
            bool didUnderflow = RemoveRecursor(_root, removable);

            /* if ~didUnderflow, merging ops already performed at this level in recursor; 
             * root can be < 1/2 full, but if is fork _and_ 1 child, replace it with child */

            if (didUnderflow) {
                if (_root is ForkBpt<K> && _root.Count == 1) {
                    _root = _root.Contents[0] as NodeBpt<K>;
                }
            }
        }

        private bool RemoveRecursor(NodeBpt<K> node, IKeyedObject<K> removable)  /* verified */  {
            /* rcall always to leaf, delete there, setting flag if underflow, rcollapse with flag; 
             * at forks, if flag leafward, delete-like slosh or merge, setting flag if underflow, rpop with flag */

            int closest = Searchers_
                .FindBinaryClosestBelow(node.Keys, removable.Key, 0, node.TopKey);

            #region At-leaf path, top of rstack

            if (node is LeafBpt<K>) {
                bool didUnderflow = node.Remove(closest, removable);

                /* rpop & rcollapse */
                return didUnderflow;
            }

            #endregion At-leaf path, top of rstack

            #region At-fork path, postorder ops

            /* rsite */
            NodeBpt<K> next = node.Contents[closest] as NodeBpt<K>;

            /* rcall */
            bool back = RemoveRecursor(next, removable);

            /* postorder ops if underflowed; remove here; may create new underflow */
            if (back) {
                bool now = node.Remove(closest, next);  // slosh or drop child node 

                /* rpop; now == children restructured, so _this_ node may now be underflowing */
                return now;
            }

            /* rpop when no underflow; no further restructuring rootward */
            return false;

            #endregion At-fork path, postorder ops
        }

        #endregion Remove() and recursor


        #region MassConstruct()

        public static BPlusTree<K> MassConstruct(ElementBpt<K>[] indexers, int maxNodes)  /* passed */  {
            /* order ~indexers, add to 3/4-full leaves, build levels 
             * of forks rootward until only 1 fork (made root) */

            indexers = SortingAlgorithms_<ElementBpt<K>>.Sort_Quick_(indexers);  // first, into order 

            BPlusTree<K> tree = new BPlusTree<K>(maxNodes);  // declared here to set .MaxNodes 
            int nodeCount = (maxNodes * 3) / 4;


            #region Construct leaves and linked list from indexers / els

            List_Vector_Extended_<NodeBpt<K>> leaves
                = new List_Vector_Extended_<NodeBpt<K>>(indexers.Length / nodeCount);  // exact size 

            LeafBpt<K> leaf = null;

            for (int i = 0, top = indexers.Length - 1; i < indexers.Length; i++) {
                int localized = i % nodeCount;

                /* add a new leaf when prior is full */
                if (localized == 0) {
                    leaf = new LeafBpt<K>();
                    leaf.Key = indexers[i].Key;
                    leaves.Append(leaf);
                }

                /* copy contents to leaf and produce metadata */
                leaf.Keys[localized] = indexers[i].Key;
                leaf.Contents[localized] = indexers[i];
                leaf.Count++;

                /* join el into linked list */
                if (i > 0) {
                    indexers[i].Previous = indexers[i - 1];
                }

                if (i < top) {
                    indexers[i].Next = indexers[i + 1];
                }
            }

            #endregion Construct leaves and linked list from indexers / els


            #region Construct levels of forks all the way to root

            /* forks, using nested loops; until only 1 leafward left, another level of forks to build */
            List_Vector_Extended_<NodeBpt<K>> leafward = leaves;

            List_Vector_Extended_<NodeBpt<K>> forks
                = new List_Vector_Extended_<NodeBpt<K>>(leafward.Length / nodeCount);  // first exact size 

            ForkBpt<K> fork = null;

            while (leafward.Length > 1) {
                /* gather leafward level into nodes (always forks) at this level */
                for (int i = 0; i < leafward.Length; i++) {
                    int localized = i % nodeCount;

                    if (localized == 0) {
                        fork = new ForkBpt<K>();
                        fork.Key = leafward[i].Key;
                        forks.Append(fork);
                    }

                    fork.Keys[localized] = leafward[i].Key;
                    fork.Contents[localized] = leafward[i];
                    fork.Count++;
                }

                /* after building level, re-aliasing as material for any next level to build on */
                leafward = forks;

                /* exit before starting new level when root's level reached */
                if (forks.Length == 1) {
                    break;
                }

                /* any next rootward level to build; can't reuse existing, would also clear ~leafward */
                forks = new List_Vector_Extended_<NodeBpt<K>>(leafward.Length / nodeCount);
            }

            #endregion Construct levels of forks all the way to root


            /* join tree object and its structure, convey */
            tree.Root = forks[0];  // always just 1 item in ~forks here 
            return tree;
        }

        #endregion MassConstruct()

        #endregion Public methods

    }

}

