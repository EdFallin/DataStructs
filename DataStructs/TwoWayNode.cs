﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    // for use in singly-linked lists, at least

    // coding is so trivial that I don't think unit testing is needed at this point

    public class TwoWayNode<T>
    {
        #region Fields

        T _content;
        TwoWayNode<T> _next;   // a pointer to another TwoWayNode
        TwoWayNode<T> _previous;

        #endregion Fields


        #region Properties

        // these properties match the reftext's equivalent get-type and set-named methods
        public T Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public TwoWayNode<T> Next
        {
            get { return _next; }
            set { _next = value; }
        }

        public TwoWayNode<T> Previous
        {
            get { return _previous; }
            set { _previous = value; }
        }

        #endregion Properties


        #region Constructors

        // two constructors:
        // first, with 2 args, is to build a complete node simultaneously placed in a linked list;
        // second, with 1 arg, is to build a node that is placed in a linked list but is not complete

        public TwoWayNode(T content, TwoWayNode<T> next, TwoWayNode<T> previous)
        {
            _content = content;
            _next = next;
            _previous = previous;
        }

        public TwoWayNode(TwoWayNode<T> next, TwoWayNode<T> previous)
        {
            _next = next;
            _previous = previous;
        }

        #endregion Constructors
    }
}
