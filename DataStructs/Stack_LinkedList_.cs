﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructs
{
    public class Stack_LinkedList_<T> : Stack_<T>
    {
        #region Fields

        ForwardNode<T> _top = null;
        int _length = 0;

        #endregion Fields


        #region Properties, Including Peek

        public override int Length { get { return _length; } }   // passed  

        public override T Peek { get { return _top.Content; } }   // passed  

        #endregion Properties, Including Peek


        #region Constructor

        public Stack_LinkedList_()   // ok  
        {
            // no operations; no header needed;
            // list is built in reverse, in essence
        }

        #endregion Constructor


        #region Changing List Contents

        public override void Push(T element)   // passed  
        {
            // simply add a node pointing to a "next" that is the previous node from an adding perspective;
            // in concept, the reverse direction from building a singly-linked list

            _top = new ForwardNode<T>(element, _top);
            _length++;
        }

        public override T Pop()   // passed  
        {

            // retain the content at the top, then set the top to be the next value "before" it,
            // which is the "next" since things are being constructed in reverse

            T content = _top.Content;
            _top = _top.Next;
            _length--;

            return content;
        }

        public override void Clear()   // passed  
        {
            _top = null;
            _length = 0;
        }

        #endregion Changing List Contents
    }
}
