﻿using DataStructs;
using System;
using System.Collections.Generic;

namespace DataStructs
{
    public class ElementBpt<K> : IKeyedObject<K>, IEquatable<ElementBpt<K>>, IComparable<ElementBpt<K>>
        where K : IEquatable<K>, IComparable<K>
    {
        // the indexing elements of the tree, made up of keys and pointers to content in the subject  //

        #region Definitions

        const int NONE = -1;

        #endregion Definitions


        #region Properties

        public static int None { get; } = NONE;

        public K Key { get; set; }
        public int Pointer { get; set; }   // pointer to record on disk as offset in file 

        public ElementBpt<K> Next { get; set; }

        public ElementBpt<K> Previous { get; set; }

        #endregion Properties


        #region Constructors

        public ElementBpt(K key) : this(key, None) {
            /* no operations */
        }

        public ElementBpt(K key, int pointer) {
            Key = key;
            Pointer = pointer;
        }

        #endregion Constructors


        #region IEquatable and equality

        public bool Equals(ElementBpt<K> other)  /* passed */  {
            if (other == null) {
                return false;
            }

            return (Key.Equals(other.Key) && Pointer.Equals(other.Pointer));
        }

        public override bool Equals(object obj) {
            ElementBpt<K> asElementBptK = obj as ElementBpt<K>;

            if (asElementBptK == null) {
                return false;
            }

            return Equals(asElementBptK);
        }

        public override int GetHashCode() {
            return (Key.GetHashCode() * Pointer.GetHashCode());
        }

        #endregion IEquatable and equality


        #region IComparable

        public int CompareTo(ElementBpt<K> other)  /* passed */  {
            /* comparability first by key, then by pointer */

            int ofKeys = this.Key.CompareTo(other.Key);

            if (ofKeys == 0) {
                return this.Pointer.CompareTo(other.Pointer);
            }

            return ofKeys;
        }

        #endregion IComparable
    }
}